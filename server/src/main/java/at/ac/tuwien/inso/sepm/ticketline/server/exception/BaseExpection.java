package at.ac.tuwien.inso.sepm.ticketline.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Set;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BaseExpection extends RuntimeException {
    private String bundleKey;
    private String details;

    public BaseExpection(String bundleKey) {
        this(bundleKey, null, null);
    }

    public BaseExpection(String bundleKey, String details) {
        this(bundleKey, details, null);
    }

    public BaseExpection(String bundleKey, Exception innerException) {
        this(bundleKey, null, innerException);
    }

    public BaseExpection(String bundleKey, String details, Exception innerException) {
        super(innerException);
        this.bundleKey = bundleKey;
        this.details = details;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setBundleKey(String bundleKey) {
        this.bundleKey = bundleKey;
    }

    public String getBundleKey() {
        return bundleKey;
    }
}
