package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.server.endpoint.base.EndpointBase;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.performances.PerformancesMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.service.HeaderTokenAuthenticationService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.PerformanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

@RestController
@RequestMapping(value = "/performances")
@Api(value = "performances")
public class PerformanceEndpoint extends EndpointBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(UsersEndpoint.class);

    private final PerformanceService performanceService;
    private final HeaderTokenAuthenticationService authenticationService;
    private final PerformancesMapper performancesMapper;

    @Autowired
    public PerformanceEndpoint(PerformanceService performanceService, HeaderTokenAuthenticationService authenticationService, PerformancesMapper performancesMapper) {
        this.performanceService = performanceService;
        this.authenticationService = authenticationService;
        this.performancesMapper = performancesMapper;
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Create a new event")
    public PerformanceDTO createPerformance(@RequestBody PerformanceDTO performanceDTO) {
        return performancesMapper.performanceToPerformanceDTO(
            performanceService.createPerformance(performancesMapper.performanceDTOToPerformance(performanceDTO))
        );
    }

    @RequestMapping(value = "/{performanceId}", method = RequestMethod.GET)
    @ApiOperation(value = "Gets a single performance")
    public PerformanceDTO findOne(@PathVariable Long performanceId) {
        return performancesMapper.performanceToPerformanceDTO(performanceService.findOne(performanceId));
    }

    @RequestMapping(value = "/byEvent/{eventId}", method = RequestMethod.GET)
    @ApiOperation(value = "Gets performances by event id")
    public List<PerformanceDTO> findByEvent(@PathVariable Long eventId) {
        return performancesMapper.performanceToPerformanceDTO(performanceService.findAllByEventId(eventId));
    }

    @RequestMapping(value = "/byHall/{hallId}", method = RequestMethod.GET)
    @ApiOperation(value = "Gets performances by event id")
    public List<PerformanceDTO> findByHall(@PathVariable Long hallId) {
        return performancesMapper.performanceToPerformanceDTO(performanceService.findAllByHallId(hallId));
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Get list of event")
    public List<PerformanceDTO> findAll() {
        return performancesMapper.performanceToPerformanceDTO(performanceService.findAll());
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @RequestMapping(value = "/seats/{performanceID}", method = RequestMethod.GET)
    @ApiOperation(value = "Get all seats for a performance + information about the state of the seat (free, etc.)")
    public List<SeatDTO> getSeats(@PathVariable Long performanceID) {
        return performanceService.getSeats(performanceID);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @RequestMapping(value = "/seats/bysector/{performanceId}", method = RequestMethod.GET)
    @ApiOperation(value = "Get all the sectors of a hall of a performance + number of free seats sorted by sector." +
        "Sectors are given as Strings, because Spring cannot handle enums as keys")
    public Map<String, Long> getSeatsBySector(@PathVariable Long performanceId) {
        SortedMap<String, Long> retSeatsBySector = new TreeMap<>();
        for(Map.Entry<Sector, Long> entry : performanceService.getSeatsBySector(performanceId).entrySet()){
            retSeatsBySector.put(entry.getKey().name(), entry.getValue());
        }
        return retSeatsBySector;
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Get all seats for a performance + information about the state of the seat (free, etc.)")
    public List<PerformanceDTO> search(@RequestBody PerformanceDTO performanceSearchDTO) {
        return performancesMapper.performanceToPerformanceDTO(performanceService.search(performanceSearchDTO));
    }

}
