package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.keys.SeatPK;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Seat {
    @EmbeddedId
    private SeatPK id = new SeatPK();

    @Column(insertable=false, updatable=false)
    @Min(0)
    private Long xCoord;

    @Column(insertable=false, updatable=false)
    @Min(0)
    private Long yCoord;

    @Column(nullable=false)
    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private Sector sector;

    @ManyToOne(optional = false, fetch=FetchType.EAGER)
    @JoinColumn(name="id_hall", insertable=false, updatable=false)
    private Hall hall;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="seat", cascade = CascadeType.ALL)
    private Set<SeatReservation> seatReservations = new HashSet<>();

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public SeatPK getId() {
        return id;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
        this.id.setId_hall(hall.getId());
    }

    public Long getxCoord() {
        return xCoord;
    }

    public void setxCoord(Long xCoord) {
        this.xCoord = xCoord;
        this.id.setxCoord(xCoord);
    }

    public Long getyCoord() {
        return yCoord;
    }

    public void setyCoord(Long yCoord) {
        this.yCoord = yCoord;
        this.id.setyCoord(yCoord);
    }

    public Set<SeatReservation> getSeatReservations() {
        return seatReservations;
    }

    public void setSeatReservations(Set<SeatReservation> seatReservations) {
        this.seatReservations = seatReservations;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Seat() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seat seat = (Seat) o;
        return Objects.equals(id, seat.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Seat{" +
            "id=" + id +
            ", sector=" + sector +
            '}';
    }

    public Seat(SeatPK id, @NotNull Sector sector, Hall hall, Set<SeatReservation> seatReservations) {
        this.id = id;
        this.sector = sector;
        this.hall = hall;
        this.seatReservations = seatReservations;
    }

    public static class Builder {
        private SeatPK id = new SeatPK();
        private @NotNull Sector sector;
        private Hall hall;
        private Set<SeatReservation> seatReservations;
        private @Min(0) Long xCoord;
        private @Min(0) Long yCoord;

        public Builder setXCoord(@Min(0) Long xCoord) {
            this.id.setxCoord(xCoord);
            this.xCoord = xCoord;
            return this;
        }

        public Builder setYCoord(@Min(0) Long yCoord) {
            this.id.setyCoord(yCoord);
            this.yCoord = yCoord;
            return this;
        }

        public Builder setSector(@NotNull Sector sector) {
            this.sector = sector;
            return this;
        }

        public Builder setHall(Hall hall) {
            this.hall = hall;
            this.id.setId_hall(hall.getId());
            return this;
        }

        public Builder setSeatReservations(Set<SeatReservation> seatReservations) {
            this.seatReservations = seatReservations;
            return this;
        }

        public Seat build() {
            return new Seat(id, sector, hall, seatReservations);
        }
    }
}
