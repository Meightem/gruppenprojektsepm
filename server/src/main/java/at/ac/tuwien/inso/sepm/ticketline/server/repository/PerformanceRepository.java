package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Performance;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.queryResults.SeatResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PerformanceRepository extends JpaRepository<Performance, Long> {
    /**
     *
     * @param id the id of the performance
     * @return  returns a list of all seats for a performance sorted by y-coordinate and x-coordinate
     *          + state information about a ticket that is given for the seat. If null, the seat is free.
     *          Cancelled Tickets are not returned, such that each seat of the hall of the performance is returned exactly once
     */
    @Query("select new at.ac.tuwien.inso.sepm.ticketline.server.repository.queryResults.SeatResult(s.xCoord, s.yCoord, s.sector, s.hall.id, sr.state) " +
        "from Seat s inner join Hall h on h.id = s.hall.id " +
        "inner join Performance p on (p.id = :id_performance and p.hall.id = h.id) " +
        "left outer join SeatReservation sr on (sr.performance.id = :id_performance and sr.seat.id = s.id and " +
        "sr.state <> at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState.CANCELLED) " +
        "order by s.yCoord, s.xCoord, s.sector")
    List<SeatResult> getSeats(@Param("id_performance") Long id);


    /**
     * Returns all the sectors of the hall of a performance and the free seats for that sector.
     * Note that, count(t.state) removes null values, but all null values are the seats that are free,
     * why we can use the value of count(s.sector)-count(t.state)
     * @param id the id of the performance
     * @return all sectors of a hall of a performance + free seats sorted by sector
     */
    @Query("select s.sector, count(s.sector) - count(sr.state)" +
        "from Seat s inner join Hall h on h.id = s.hall.id " +
        "inner join Performance p on (p.id = :id_performance and p.hall.id = h.id)" +
        "left outer join SeatReservation sr on (sr.performance.id = :id_performance and sr.seat.id = s.id and " +
        "sr.state <> at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState.CANCELLED) " +
        "group by s.sector " +
        "order by s.sector")
    List<Object[]> getSeatsBySectors(@Param("id_performance") Long id);
    @Query("SELECT p FROM Performance p WHERE p.event.id = :eventId")
    List<Performance> findAllByEventId(@Param("eventId") Long eventId);
}
