package at.ac.tuwien.inso.sepm.ticketline.server.repository.queryResults;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;

public class TopEvent {
    private Event event;
    private long count;

    public TopEvent(Event event, long count) {
        this.event = event;
        this.count = count;
    }

    public TopEvent() {
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
