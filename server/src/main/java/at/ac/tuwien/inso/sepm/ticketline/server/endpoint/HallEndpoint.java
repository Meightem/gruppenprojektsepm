package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.endpoint.base.EndpointBase;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.halls.HallsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.service.HallService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/halls")
@Api(value = "halls")
public class HallEndpoint extends EndpointBase {
    private final HallService hallService;
    private final HallsMapper hallsMapper;

    @Autowired
    public HallEndpoint(HallService hallService, HallsMapper hallsMapper) {
        this.hallService = hallService;
        this.hallsMapper = hallsMapper;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Get list of all halls")
    public List<HallDTO> findAll() {
        return hallsMapper.hallToHallDTO(hallService.findAll());
    }

    @RequestMapping(value = "/search/{text}",method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Get list of halls which match the search")
    public List<HallDTO> search(@PathVariable String text) {
        return hallsMapper.hallToHallDTO(hallService.search(text));
    }

}
