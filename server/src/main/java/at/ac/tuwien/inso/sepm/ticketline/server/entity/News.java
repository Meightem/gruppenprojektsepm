package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_news_id")
    @SequenceGenerator(name = "seq_news_id", sequenceName = "seq_news_id")
    private Long id;

    @Column(nullable = false, name = "published_at")
    @NotNull(message = "exception.news.persistence.date")
    private LocalDateTime publishedAt;

    @Column(nullable = false)
    @Size(min=1,max = 100,message = "exception.news.persistence.title;{min};{max}")
    @NotBlank(message = "exception.news.persistence.title.notblank")
    private String title;

    @Column(nullable = false, length = 10_000)
    @Size(min=1,max = 10_000,message = "exception.news.persistence.text;{min};{max}")
    @NotBlank(message = "exception.news.persistence.text.notblank")
    private String text;

    @Column(length = 16_000_000)
    @Size(max = 16_000_000,message = "exception.news.persistence.image;12MB")
    private String image;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<User> users;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(LocalDateTime publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "News{" +
            "id=" + id +
            ", publishedAt=" + publishedAt +
            ", title='" + title + '\'' +
            ", text='" + text + '\'' +
            ", image='" + image + '\'' +
            ", users=" + users +
            '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return Objects.equals(id, news.id) &&
            Objects.equals(publishedAt, news.publishedAt) &&
            Objects.equals(title, news.title) &&
            Objects.equals(text, news.text) &&
            Objects.equals(image, news.image) &&
            Objects.equals(users, news.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, publishedAt, title, text, image, users);
    }

    public static NewsBuilder builder() {
        return new NewsBuilder();
    }

    public static final class NewsBuilder {
        private Long id;
        private LocalDateTime publishedAt;
        private String title;
        private String text;
        private Set<User> users;
        private String image;

        private NewsBuilder() {
            users = new HashSet<>();
        }

        public NewsBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public NewsBuilder publishedAt(LocalDateTime publishedAt) {
            this.publishedAt = publishedAt;
            return this;
        }

        public NewsBuilder title(String title) {
            this.title = title;
            return this;
        }

        public NewsBuilder text(String text) {
            this.text = text;
            return this;
        }

        public NewsBuilder users(Set<User> users) {
            this.users = users;
            return this;
        }

        public NewsBuilder image(String image) {
            this.image = image;
            return this;
        }

        public News build() {
            News news = new News();
            news.setId(id);
            news.setPublishedAt(publishedAt);
            news.setTitle(title);
            news.setText(text);
            news.setUsers(users);
            news.setImage(image);
            return news;
        }
    }
}
