package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Ticket;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    /**
     * Find all tickets belonging to a customer.
     *
     * @param customer the customer owning the returned tickets.
     * @return List containing all tickets belonging to the given customer.
     */
    List<Ticket> findAllByCustomer(Customer customer);

    @Query("SELECT t from Ticket t JOIN Performance p ON (p.id = t.performance.id) " +
        "WHERE (t.customer.id = :customerID OR :ignoreCustomer = true) " +
        "AND (p.event.id = :eventID OR :ignoreEvent = true) " +
        "AND (t.state = :ticketState OR :ignoreState = true) ")
    List<Ticket> findByCustomerAndPerformance(@Param("customerID") Long customerId, @Param("eventID") Long eventId, @Param("ticketState") TicketState ticketState
     ,@Param("ignoreCustomer") boolean ignoreCustomer,@Param("ignoreEvent") boolean ignoreEvent, @Param("ignoreState") boolean ignoreState);
}
