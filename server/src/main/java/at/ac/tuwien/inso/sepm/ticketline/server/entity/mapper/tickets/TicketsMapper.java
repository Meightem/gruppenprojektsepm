package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.tickets;

import java.util.List;

import org.mapstruct.Mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Ticket;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface TicketsMapper {
    Ticket ticketDTOToTicket(TicketDTO ticketDTO);

    List<Ticket> ticketDTOToTicket(List<TicketDTO> ticketDTO);

    TicketDTO ticketToTicketDTO(Ticket ticket);

    List<TicketDTO> ticketToTicketDTO(List<Ticket> ticket);
}
