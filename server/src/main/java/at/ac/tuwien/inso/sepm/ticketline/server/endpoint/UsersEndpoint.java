package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import antlr.Token;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.endpoint.base.EndpointBase;
import at.ac.tuwien.inso.sepm.ticketline.server.security.AuthenticationConstants;
import at.ac.tuwien.inso.sepm.ticketline.server.service.HeaderTokenAuthenticationService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.UsersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping(value = "/users")
@Api(value = "users")
public class UsersEndpoint extends EndpointBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersEndpoint.class);

    @Autowired
    private final UsersService usersService;
    private final HeaderTokenAuthenticationService authenticationService;

    public UsersEndpoint(UsersService usersService, HeaderTokenAuthenticationService authenticationService) {
        this.usersService = usersService;
        this.authenticationService = authenticationService;
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Create a new user")
    public UserDTO createUser(@RequestBody UserDTO userDTO) {
        return usersService.createUser(userDTO);
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Get list of simple user entries")
    public List<UserDTO> findAll() {
        return usersService.getAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Update fields of user")
    public UserDTO updateUser(@RequestBody UserDTO userDTO, @PathVariable Long id, @ApiIgnore @RequestHeader(value = HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        LOGGER.debug("Update userDTO" + userDTO + " " + id);
        AuthenticationTokenInfo authenticationTokenInfo = authenticationService.authenticationTokenInfo(authorizationHeader.substring(AuthenticationConstants.TOKEN_PREFIX.length()).trim());
        userDTO.setId(id);
        return usersService.updateUser(userDTO, authenticationTokenInfo);
    }
}
