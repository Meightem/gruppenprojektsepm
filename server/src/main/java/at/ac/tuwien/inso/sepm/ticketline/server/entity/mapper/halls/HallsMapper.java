package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.halls;

import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HallsMapper {
    HallDTO hallToHallDTO(Hall hall);
    Hall HallDTOToHall(HallDTO hall);
    List<HallDTO> hallToHallDTO(List<Hall> hall);
    List<Hall> HallDTOToHall(List<HallDTO> hall);
}
