package at.ac.tuwien.inso.sepm.ticketline.server.entity.keys;

import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SeatPK implements Serializable {
    @Min(0)
    private Long xCoord;

    @Min(0)
    private Long yCoord;

    private Long id_hall;

    public SeatPK(Long xCoord, Long yCoord, Long id_hall) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.id_hall = id_hall;
    }

    public SeatPK() {
    }

    public Long getxCoord() {
        return xCoord;
    }

    public void setxCoord(Long xCoord) {
        this.xCoord = xCoord;
    }

    public Long getyCoord() {
        return yCoord;
    }

    public void setyCoord(Long yCoord) {
        this.yCoord = yCoord;
    }

    public Long getId_hall() {
        return id_hall;
    }

    public void setId_hall(Long id_hall) {
        this.id_hall = id_hall;
    }

    @Override
    public String toString() {
        return "SeatPK{" +
            "xCoord=" + xCoord +
            ", yCoord=" + yCoord +
            ", id_hall=" + id_hall +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SeatPK seatPK = (SeatPK) o;
        return Objects.equals(xCoord, seatPK.xCoord) &&
            Objects.equals(yCoord, seatPK.yCoord) &&
            Objects.equals(id_hall, seatPK.id_hall);
    }

    @Override
    public int hashCode() {

        return Objects.hash(xCoord, yCoord, id_hall);
    }
}
