package at.ac.tuwien.inso.sepm.ticketline.server.repository.dao;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class HallSearchDAO {
    private EntityManager entityManager;
    private IndexManager indexManager;

    public HallSearchDAO(EntityManager entityManager, IndexManager indexManager) {
        this.entityManager = entityManager;
        this.indexManager = indexManager;
    }

    @Transactional
    public List<Hall> searchHall(String text){

        indexManager.initSearchIndex();

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        Query query = fullTextEntityManager
            .getSearchFactory()
            .buildQueryBuilder()
            .forEntity(Hall.class)
            .overridesForField("street", "edgeNGram_query")
            .overridesForField("city", "edgeNGram_query")
            .overridesForField("name", "edgeNGram_query")
            .overridesForField("postcode", "edgeNGram_query")
            .overridesForField("country", "edgeNGram_query")
            .get()
            .simpleQueryString()
            .onFields("street","city","name","postcode","country")
            .withAndAsDefaultOperator()
            .matching(text)
            .createQuery();

        List<Hall> results = getJpaQuery(query).getResultList();

        return results;
    }

    private FullTextQuery getJpaQuery(org.apache.lucene.search.Query luceneQuery) {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
        return fullTextEntityManager.createFullTextQuery(luceneQuery, Hall.class);
    }

}
