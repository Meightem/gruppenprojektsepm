package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import at.ac.tuwien.inso.sepm.ticketline.server.repository.dao.BigDecimalNumericFieldBridge;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.dao.EventFieldBridge;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.dao.HallFieldBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.NumericField;

import javax.persistence.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Indexed
public class Performance{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_performance_id")
    @SequenceGenerator(name = "seq_performance_id", sequenceName = "seq_performance_id")
    private Long id;

    @Column(nullable=false)
    @NotNull(message="exception.performance.date.notnull")
    @Field
    private LocalDateTime date; // micro- and nanoseconds not supported in database

    @Column(precision=10, scale=2, nullable=false)
    @NotNull(message="exception.performance.price.notnull")
    @Field
    @NumericField
    @FieldBridge(impl = BigDecimalNumericFieldBridge.class)
    private BigDecimal price;

    @ManyToOne(optional = false, fetch=FetchType.EAGER)
    @JoinColumn(name="id_event")
    @NotNull(message="exception.performance.event.notnull")
    @Field
    @FieldBridge(impl = EventFieldBridge.class)
    private Event event;

    @ManyToOne(optional = false, fetch=FetchType.EAGER)
    @JoinColumn(name="id_hall")
    @NotNull(message="exception.performance.hall.notnull")
    @Field
    @FieldBridge(impl = HallFieldBridge.class)
    private Hall hall;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="performance", cascade = CascadeType.ALL)
    private Set<SeatReservation> seatReservations = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Set<SeatReservation> getSeatReservations() {
        return seatReservations;
    }

    public void setSeatReservations(Set<SeatReservation> seatReservations) {
        this.seatReservations = seatReservations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Performance that = (Performance) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(date, that.date) &&
            Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, date, price);
    }

    @Override
    public String toString() {
        return "Performance{" +
            "id=" + id +
            ", date=" + date +
            ", price=" + price +
            '}';
    }

    public Performance() {
    }

    public Performance(Long id, @NotNull LocalDateTime date, @NotNull BigDecimal price, @NotNull Event event, @NotNull Hall hall, Set<SeatReservation> seatReservations) {
        this.id = id;
        this.date = date;
        this.price = price;
        this.event = event;
        this.hall = hall;
        this.seatReservations = seatReservations;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private Long id;
        private @NotNull LocalDateTime date;
        private @NotNull BigDecimal price;
        private @NotNull Event event;
        private @NotNull Hall hall;
        private Set<SeatReservation> seatReservations;

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setDate(@NotNull LocalDateTime date) {
            this.date = date;
            return this;
        }

        public Builder setPrice(@NotNull BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder setEvent(@NotNull Event event) {
            this.event = event;
            return this;
        }

        public Builder setHall(@NotNull Hall hall) {
            this.hall = hall;
            return this;
        }

        public Builder setSeatReservations(Set<SeatReservation> seatReservations) {
            this.seatReservations = seatReservations;
            return this;
        }

        public Performance build() {
            return new Performance(id, date, price, event, hall, seatReservations);
        }
    }
}
