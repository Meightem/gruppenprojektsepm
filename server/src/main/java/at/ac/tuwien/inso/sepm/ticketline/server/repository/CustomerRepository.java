package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    /**
     * Find a single customer by its id
     * @param id
     * @return The customer if it was found
     */
    Optional<Customer> findOneById(Long id);

    /**
     * Find the anonymous user
     * @return the anonymous user if it was found
     */

    @Query("SELECT c FROM Customer c WHERE c.isAnonymousCustomer = true")
    Optional<Customer> findAnonymousCustomer();
}
