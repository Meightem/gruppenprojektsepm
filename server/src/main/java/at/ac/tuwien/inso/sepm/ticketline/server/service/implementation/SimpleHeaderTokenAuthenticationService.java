package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationToken;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import at.ac.tuwien.inso.sepm.ticketline.server.configuration.properties.AuthenticationConfigurationProperties;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ExtendedConstraintViolationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.UserRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.security.AuthenticationConstants;
import at.ac.tuwien.inso.sepm.ticketline.server.service.HeaderTokenAuthenticationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Validated
public class SimpleHeaderTokenAuthenticationService implements HeaderTokenAuthenticationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleHeaderTokenAuthenticationService.class);

    private final UserRepository userRepository;

    private final AuthenticationManager authenticationManager;
    private final ObjectMapper objectMapper;
    private final SecretKeySpec signingKey;
    private final SignatureAlgorithm signatureAlgorithm;
    private final Duration validityDuration;
    private final Duration overlapDuration;

    @Autowired
    public SimpleHeaderTokenAuthenticationService(
        UserRepository userRepository, @Lazy AuthenticationManager authenticationManager,
        AuthenticationConfigurationProperties authenticationConfigurationProperties,
        ObjectMapper objectMapper
    ) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.objectMapper = objectMapper;
        byte[] apiKeySecretBytes = Base64.getEncoder().encode(
            authenticationConfigurationProperties.getSecret().getBytes());
        signatureAlgorithm = authenticationConfigurationProperties.getSignatureAlgorithm();
        signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
        validityDuration = authenticationConfigurationProperties.getValidityDuration();
        overlapDuration = authenticationConfigurationProperties.getOverlapDuration();
    }

    @Override
    public AuthenticationToken authenticate(String username, CharSequence password) {
        if(username==null||username.isEmpty()||password==null||password.length()==0){
            throw new ExtendedConstraintViolationException("exception.authentication.constraint",null);
        }

        Authentication authentication = null;
        try{
            authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password));

            // reset failed attempts
            at.ac.tuwien.inso.sepm.ticketline.server.entity.User userPersistence = userRepository.findByUsername(authentication.getName());
            userPersistence.setFailedAttempts(0);
            userRepository.save(userPersistence);
        }
        catch(BadCredentialsException e){
            //invalid login, update failedAttempts
            LOGGER.info("User with username {} wants to login with wrong password", username);
            at.ac.tuwien.inso.sepm.ticketline.server.entity.User user = userRepository.findByUsername(username);
            boolean isUserCanBeLocked = userRepository.countByIsAdminAndIsLocked(true, false) > 1;
            if(user.getFailedAttempts() >= 4){
                if(user.getAdmin()==false || isUserCanBeLocked){
                    user.setFailedAttempts(0);
                    user.setLocked(true);
                    LOGGER.info("User with username {} got locked", username);
                }
            }
            else{
                user.setFailedAttempts(user.getFailedAttempts()+1);
            }
            userRepository.save(user);
            throw e;
        } catch (LockedException e) {
            // this user is locked
            LOGGER.info("Locked user with username {} wants to login", username);
            throw e;
        }


        Instant now = Instant.now();
        String authorities = "";
        try {
            authorities = objectMapper.writeValueAsString(authentication.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()));
        } catch (JsonProcessingException e) {
            LOGGER.error("Failed to wrap authorities", e);
        }
        String currentToken = Jwts.builder()
            .claim(AuthenticationConstants.JWT_CLAIM_PRINCIPAL_ID, null)
            .claim(AuthenticationConstants.JWT_CLAIM_PRINCIPAL, authentication.getName())
            .claim(AuthenticationConstants.JWT_CLAIM_AUTHORITY, authorities)
            .setIssuedAt(Date.from(now))
            .setNotBefore(Date.from(now))
            .setExpiration(Date.from(now.plus(validityDuration)))
            .signWith(signatureAlgorithm, signingKey)
            .compact();
        String futureToken = Jwts.builder()
            .claim(AuthenticationConstants.JWT_CLAIM_PRINCIPAL_ID, null)
            .claim(AuthenticationConstants.JWT_CLAIM_PRINCIPAL, authentication.getName())
            .claim(AuthenticationConstants.JWT_CLAIM_AUTHORITY, authorities)
            .setIssuedAt(Date.from(now))
            .setExpiration(Date.from(now
                .plus(validityDuration
                    .minus(overlapDuration)
                    .plus(validityDuration))))
            .setNotBefore(Date.from(now
                .plus(validityDuration
                    .minus(overlapDuration))))
            .signWith(signatureAlgorithm, signingKey)
            .compact();
        return AuthenticationToken.builder()
            .currentToken(currentToken)
            .futureToken(futureToken)
            .build();
    }

    @Override
    public AuthenticationTokenInfo authenticationTokenInfo(String headerToken) {
        final Claims claims = Jwts.parser()
            .setSigningKey(signingKey)
            .parseClaimsJws(headerToken)
            .getBody();
        List<String> roles = readJwtAuthorityClaims(claims);
        return AuthenticationTokenInfo.builder()
            .username((String) claims.get(AuthenticationConstants.JWT_CLAIM_PRINCIPAL))
            .roles(roles)
            .issuedAt(LocalDateTime.ofInstant(claims.getIssuedAt().toInstant(), ZoneId.systemDefault()))
            .notBefore(LocalDateTime.ofInstant(claims.getNotBefore().toInstant(), ZoneId.systemDefault()))
            .expireAt(LocalDateTime.ofInstant(claims.getExpiration().toInstant(), ZoneId.systemDefault()))
            .validityDuration(validityDuration)
            .overlapDuration(overlapDuration)
            .build();
    }

    @Override
    public AuthenticationToken renewAuthentication(String headerToken) {
        final Claims claims = Jwts.parser()
            .setSigningKey(signingKey)
            .parseClaimsJws(headerToken)
            .getBody();
        String futureToken = Jwts.builder()
            .claim(AuthenticationConstants.JWT_CLAIM_PRINCIPAL_ID, claims.get(AuthenticationConstants.JWT_CLAIM_PRINCIPAL_ID))
            .claim(AuthenticationConstants.JWT_CLAIM_PRINCIPAL, claims.get(AuthenticationConstants.JWT_CLAIM_PRINCIPAL))
            .claim(AuthenticationConstants.JWT_CLAIM_AUTHORITY, claims.get(AuthenticationConstants.JWT_CLAIM_AUTHORITY))
            .setIssuedAt(Date.from(Instant.now()))
            .setExpiration(Date.from(claims.getExpiration().toInstant()
                .plus(validityDuration
                    .minus(overlapDuration))))
            .setNotBefore(Date.from(claims.getExpiration().toInstant().minus(overlapDuration)))
            .signWith(signatureAlgorithm, signingKey)
            .compact();
        return AuthenticationToken.builder()
            .currentToken(headerToken)
            .futureToken(futureToken)
            .build();
    }

    @Override
    public User authenticate(String headerToken) {
        try {
            final Claims claims = Jwts.parser()
                .setSigningKey(signingKey)
                .parseClaimsJws(headerToken)
                .getBody();
            List<String> authoritiesWrapper = readJwtAuthorityClaims(claims);
            List<SimpleGrantedAuthority> authorities = authoritiesWrapper.stream()
                .map(roleName -> roleName.startsWith(AuthenticationConstants.ROLE_PREFIX) ?
                    roleName : (AuthenticationConstants.ROLE_PREFIX + roleName))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
            return new User(
                (String) claims.get(AuthenticationConstants.JWT_CLAIM_PRINCIPAL),
                headerToken,
                authorities);
        } catch (ExpiredJwtException e) {
            LOGGER.error("Error at authenticate: {}", e.getMessage());
            throw new CredentialsExpiredException(e.getMessage(), e);
        } catch (JwtException e) {
            LOGGER.error("Error at authenticate: {}", e.getMessage());
            throw new BadCredentialsException(e.getMessage(), e);
        }
    }

    private List<String> readJwtAuthorityClaims(Claims claims) {
        ArrayList<String> authoritiesWrapper = new ArrayList<>();
        try {
            authoritiesWrapper = objectMapper.readValue(claims.get(
                AuthenticationConstants.JWT_CLAIM_AUTHORITY, String.class),
                new TypeReference<List<String>>() {
                });
        } catch (IOException e) {
            LOGGER.error("Failed to unwrap roles", e);
        }
        return authoritiesWrapper;
    }
}