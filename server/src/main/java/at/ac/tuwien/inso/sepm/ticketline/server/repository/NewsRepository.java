package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {

    /**
     * Find a single news entry by id.
     *
     * @param id the is of the news entry
     * @return Optional containing the news entry
     */
    Optional<News> findOneById(Long id);

    /**
     * Find all news entries ordered by published at date (descending).
     *
     * @return ordered list of al news entries
     */
    List<News> findAllByOrderByPublishedAtDesc();

    /**
     * Gets all news not read by the given user.
     * @param userId the id of the user to look for
     * @return the news not read by the given user.
     */
    @Query("SELECT n FROM News n WHERE n.id NOT IN (SELECT n.id FROM n.users u WHERE u.id = :userId) ORDER BY n.publishedAt DESC")
    List<News> findUnreadNews(@Param(value = "userId") Long userId);
}