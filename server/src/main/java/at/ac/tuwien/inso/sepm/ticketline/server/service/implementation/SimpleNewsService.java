package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.news.NewsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BadRequestException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ExtendedConstraintViolationException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.NewsRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.NewsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Service
public class SimpleNewsService implements NewsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleNewsService.class);

    private final NewsRepository newsRepository;
    private final Validator validator;

    public SimpleNewsService(NewsRepository newsRepository, Validator validator) {
        this.newsRepository = newsRepository;
        this.validator = validator;
    }

    @Override
    public List<News> findAll() {
        return newsRepository.findAllByOrderByPublishedAtDesc();
    }

    @Override
    public News findOne(Long id) {
        return newsRepository.findOneById(id).orElseThrow(() -> new NotFoundException("exception.news.find.notfound"));
    }

    @Override
    public News publishNews(News news) {
        if(news.getId() != null && newsRepository.existsById(news.getId())){
            LOGGER.error("Error at publishNews: Bad Request - This news is already published.");
            throw new BadRequestException("exception.news.persistence.id");
        }

        news.setPublishedAt(LocalDateTime.now());

        Set<ConstraintViolation<News>> violations = validator.validate(news);
        if(!violations.isEmpty()) {
            LOGGER.error("Error at publishNews: Constraint Violation - News contains invalid values.");
            throw new ExtendedConstraintViolationException("exception.news.persistence.constraint",violations);
        }

        return newsRepository.save(news);
    }

    @Override
    public List<News> userUnreadNews(User user) {
        return newsRepository.findUnreadNews(user.getId());
    }

    @Override
	public News markNewsAsRead(News news, User user) {
        news.getUsers().add(user);
        return newsRepository.save(news);
    }
}
