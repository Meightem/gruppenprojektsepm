package at.ac.tuwien.inso.sepm.ticketline.server.exception;

import javax.validation.ConstraintViolation;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ExtendedConstraintViolationException extends BaseExpection {
    private final Set<String> violations;

    public ExtendedConstraintViolationException(Set<String> violationStrings, String bundleKey) {
        super(bundleKey);
        if (violationStrings == null) {
            this.violations = new HashSet<>();
        } else {
            this.violations = violationStrings;
        }
    }

    public ExtendedConstraintViolationException(String bundleKey, Set<? extends ConstraintViolation<?>> violations) {
        this(violationToStringSet(violations), bundleKey);
    }

    public static Set<String> violationToStringSet(Set<? extends ConstraintViolation<?>> violations) {
        if(violations==null)return null;
        return violations.stream()
            .map(ConstraintViolation::getMessage)
            .collect(Collectors.toSet());
    }

    public Set<String> getViolations() {
        return violations;
    }
}
