package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.events;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.TopEventDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.queryResults.TopEvent;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TopEventMapper {
    TopEventDTO TopEventToTopEventDTO(TopEvent topEvent);
    List<TopEventDTO> TopEventToTopEventDTO(List<TopEvent> topEvent);
    TopEvent TopEventDTOtoTopEvent(TopEventDTO topEventDTO);
    List<TopEvent> TopEventDTOtoTopEvent(List<TopEventDTO> topEventDTOS);

}
