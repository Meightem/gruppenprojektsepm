package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.HallRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.dao.HallSearchDAO;
import at.ac.tuwien.inso.sepm.ticketline.server.service.HallService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleHallService implements HallService {
    private final HallRepository hallRepository;
    private final HallSearchDAO hallSearchDAO;

    public SimpleHallService(HallRepository hallRepository, HallSearchDAO hallSearchDAO) {
        this.hallRepository = hallRepository;
        this.hallSearchDAO = hallSearchDAO;
    }

    @Override
    public List<Hall> findAll() {
        return hallRepository.findAll();
    }

    @Override
    public List<Hall> search(String text) {
        return hallSearchDAO.searchHall(text);
    }
}
