package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatState;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.keys.SeatPK;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.customers.CustomersMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.seats.SeatsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.tickets.TicketsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BadRequestException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ExtendedConstraintViolationException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.*;
import at.ac.tuwien.inso.sepm.ticketline.server.service.CustomersService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.PerformanceService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.TicketService;

import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import org.apache.tomcat.jni.Error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.constraints.NotNull;

@Service
public class SimpleTicketService implements TicketService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleTicketService.class);
    private final TicketRepository ticketRepository;
    private final CustomersService customersService;
	private final CustomersMapper customersMapper;
    private final PerformanceService performanceService;
    private final CustomerRepository customerRepository;
    private final PerformanceRepository performanceRepository;
    private final SeatRepository seatRepository;
    private final SeatReservationRepository seatReservationRepository;
    private final SeatsMapper seatsMapper;
    private final TicketsMapper ticketsMapper;

    @Autowired
    public SimpleTicketService(TicketRepository ticketRepository, CustomersService customersService, TicketsMapper ticketsMapper, CustomersMapper customersMapper, PerformanceService performanceService, CustomerRepository customerRepository, PerformanceRepository performanceRepository, SeatRepository seatRepository, SeatReservationRepository seatReservationRepository, SeatsMapper seatsMapper, TicketsMapper ticketsMapper1) {
        this.ticketRepository = ticketRepository;
        this.customersService = customersService;
        this.customersMapper = customersMapper;
        this.performanceService = performanceService;
        this.customerRepository = customerRepository;
        this.performanceRepository = performanceRepository;
        this.seatRepository = seatRepository;
        this.seatReservationRepository = seatReservationRepository;
        this.seatsMapper = seatsMapper;
        this.ticketsMapper = ticketsMapper1;
    }

    @Override
    public List<TicketDTO> findAll() {
        return ticketsMapper.ticketToTicketDTO(ticketRepository.findAll());
    }

    @Override
    @Transactional
    public List<Ticket> findAllByCustomer(Long customerID) {
        Customer customer = customersMapper.customerDTOToCustomer(customersService.get(customerID));
        return new ArrayList<Ticket> (customer.getTickets());
    }


    @Override
    public Ticket findOne(Long id) {
        return ticketRepository.findById(id).orElseThrow(() -> new NotFoundException("exception.ticket.find.notfound"));
    }

    @Transactional
    private Ticket buyReserved(Long id) {
        Ticket ticket = findOne(id);
        if (ticket.getState() != TicketState.RESERVED) {
            LOGGER.error("Error at buyReserved : Bad Request - Only reserved tickets can be bought.");
            throw new BadRequestException("exception.ticket.reserved.buy");
        }

        this.updateTicketState(id, TicketState.BOUGHT);
        return findOne(id);
    }

    @Override
    @Transactional
    public Ticket refundTicket(Long id) {
        Ticket ticket = findOne(id);
        if (ticket.getState() != TicketState.BOUGHT) {
            LOGGER.error("Error at refundTicket: Bad Request - Only bought tickets can be cancelled.");
            throw new BadRequestException("exception.ticket.bought.cancel");
        }
        this.updateTicketState(id, TicketState.CANCELLED);
        return findOne(id);
    }

    @Override
    public void delete(Long id) {
        ticketRepository.deleteById(id);
    }

    /**
     * validates if all seats can be bought or reserved for the given performance, validations include:
     * - seats must not be null or size zero
     * - the event type has to be EventType.SEATS
     * - performance has to be in the future
     * - no seatid has to be null
     * - every seat has to exist
     * - every seat has to be in the hall of the performance
     * - every seat has to be free for the performance
     *
     * @param performance the performance (note that id has to be valid)
     * @param seats       the seats
     */
    private void validatePurchaseOrReservationForSeats(Performance performance, List<SeatDTO> seats) {
        if (seats == null || seats.size() == 0) {
            LOGGER.error("Error at validatePurchaseOrReservationForSeats: Bad Request - At least 1 seat has to be bought or reserved.");
            throw new BadRequestException("excpetion.ticket.buyorreserve.seats.min");
        }
        if (performance.getEvent().getType() != EventType.SEATS) {
            LOGGER.error("Error at validatePurchaseOrReservationForSeats: Bad Request - Internal error. This method is only available for other eventtypes.");
            throw new BadRequestException("exception.ticket.buyorreserve.type");
        }
        if (performance.getDate().isBefore(LocalDateTime.now())) {
            LOGGER.error("Error at validatePurchaseOrReservationForSeats: Bad Request - This performance already took place.");
            throw new BadRequestException("exception.ticket.buyorreserve.date");
        }
        if (seats.stream().anyMatch(seatDTO -> {
            return seatDTO.getxCoord() == null || seatDTO.getyCoord() == null || seatDTO.getId_hall() == null;
        })) {
            LOGGER.error("Error at validatePurchaseOrReservationForSeats: Bad Request - Some sectors contain invalid counts.(negativ or not set)");
            throw new BadRequestException("exception.ticket.buyorreserve.seats.invalid");
        }
        if (seats.stream().anyMatch(seatDTO -> {
            return !seatDTO.getId_hall().equals(performance.getHall().getId());
        })) {
            LOGGER.error("Error at validatePurchaseOrReservationForSeats: Bad Request - Some seats don't belong to this performance.");
            throw new BadRequestException("exception.ticket.buyorreserve.seats.wronghall");
        }
        List<Seat> seatsFound = seatRepository.findAllById(seats.stream().map(seatDTO -> (new SeatPK(seatDTO.getxCoord()+1, seatDTO.getyCoord()+1, seatDTO.getId_hall()))).collect(Collectors.toList()));
        if (seatsFound.stream().anyMatch(seat -> seat == null)) {
            LOGGER.error("Error at validatePurchaseOrReservationForSeats: Not Found - Seat with specified id could not be found.");
            throw new NotFoundException("exception.hall.seat.notfound");
        }
        List<SeatDTO> seatsByFree = performanceService.getSeats(performance.getId());

        // Build map for fast check if every seat is free
        Map<SeatPK, Boolean> mapSeatsFree = new HashMap<>();
        for (SeatDTO seatDTO : seatsByFree) {
            if (seatDTO.getState() == SeatState.FREE) {
                mapSeatsFree.put(new SeatPK(seatDTO.getxCoord(), seatDTO.getyCoord(), seatDTO.getId_hall()), true);
            }
        }
        // Now check if every seat is in the map
        Set<String> violations = new HashSet<>();
        for (SeatDTO seatDTO : seats) {
            if (!mapSeatsFree.containsKey(new SeatPK(seatDTO.getxCoord(), seatDTO.getyCoord(), seatDTO.getId_hall()))) {
                violations.add(String.format("%s;%s;%s","exception.ticket.buyorreserve.seats.notfree",seatDTO.getxCoord(), seatDTO.getyCoord()));
            }
        }
        if(!violations.isEmpty()){
            LOGGER.error("Error at validatePurchaseOrReservationForSeats: Constraint Violation - Some seats contain invalid values.");
            throw new ExtendedConstraintViolationException(violations,"exception.ticket.buyorreserve.seats.invalid");
        }

        // Check if price is not over 400 Euros
        BigDecimal sumPrice = BigDecimal.ZERO;
        for(SeatDTO seatDTO : seats){
            BigDecimal seatPrice = seatDTO.getSector().getFactor().multiply(performance.getPrice()).round(new MathContext(2));
            sumPrice = sumPrice.add(seatPrice);
        }
        if(sumPrice.compareTo(new BigDecimal(400L)) > 0){
            LOGGER.error("Error at validatePurchaseOrReservationForSeats: Bad Request - Tickets cannot have a value of more than 400");
            throw new BadRequestException("exception.ticket.buyorrreserve.seats.toohigh");
        }
    }

    /**
     * creates SeatReservations for a given ticket (EventType.SEATS)
     *
     * @param ticket      the ticket (note that id has to be valid)
     * @param performance the performance (note that id has to be valid)
     * @param seats       all the seats to create seatreservations for (note that validatePurchaseOrReservationForSeats had to be called with all seats and the performance)
     */
    private void createSeatReservationsForTicketSeats(Ticket ticket, Performance performance, List<SeatDTO> seats) {
        List<SeatReservation> seatReservations = new ArrayList<>();
        for (SeatDTO seatDTO : seats) {
            seatReservations.add(SeatReservation.newBuilder().setPerformance(performance).setTicket(ticket).setSeat(seatsMapper.seatDTOToSeat(seatDTO)).build());
        }
        seatReservationRepository.saveAll(seatReservations);
    }

    @Override
    public Long buyTicketForSeats(boolean buy, Long customerId, Long performanceId, List<SeatDTO> seats) {
        if (!performanceRepository.existsById(performanceId)) {
            LOGGER.error("Error at buyTicketForSeats: Not Found  - Performance with specified id could not be found.");
            throw new NotFoundException("exception.performance.find.notfound");
        }
        if (!customerRepository.existsById(customerId)) {
            LOGGER.error("Error at buyTicketForSeats: Not Found  - Customer with specified id could not be found.");
            throw new NotFoundException("exception.customer.find.notfound");
        }
        Customer customer = customerRepository.getOne(customerId);
        Performance performance = performanceRepository.getOne(performanceId);

        validatePurchaseOrReservationForSeats(performance, seats);

        // now we know that everything is valid, we can create a ticket and all seatReservations
        Ticket ticket;
        if (buy) {
            ticket = Ticket.newBuilder().state(TicketState.BOUGHT).customer(customer).performance(performance).build();
        }
        else {
            ticket = Ticket.newBuilder().state(TicketState.RESERVED).customer(customer).performance(performance).build();
        }
        ticketRepository.save(ticket);

        createSeatReservationsForTicketSeats(ticket, performance, seats);

        return ticket.getId();
    }

    /**
     * validates if all number of seats for sector can be bought or reserved for the given performance, validations include:
     * - every demand has to be not null and there has to be a sum of demands > 0
     * - the event type has to be EventType.SECTORS
     * - performance has to be in the future
     * - every demand has to be positive
     * - all the sectors have to be valid and in the hall of the performance
     * - all demands have to be able to be satisfied
     *
     * @param performance    the performance (note that id has to be valid)
     * @param seatsBySectors the demand of number of seats by sector
     * @return the demands sorted by sector
     */
    private SortedMap<Sector, Long> validatePurchaseOrReservationForSectors(Performance performance, Map<String, Long> seatsBySectors) {
        if (seatsBySectors.entrySet().stream().anyMatch(seatsBySector -> {
            return seatsBySector.getValue() == null||seatsBySector.getValue()<0;
        })) {
            LOGGER.error("Error at validatePurchaseOrReservationForSectors: Bad Request - Some sectors contain invalid counts.(negativ or not set)");
            throw new BadRequestException("exception.ticket.buyorreserve.sector.invalid");
        }
        if (seatsBySectors.entrySet().stream().mapToLong(seatsBySector -> seatsBySector.getValue()).sum() <= 0) {
            LOGGER.error("Error at validatePurchaseOrReservationForSectors: Bad Request - At least 1 seat needs to be bought or reserved.");
            throw new BadRequestException("exception.ticket.buyorreserve.sector.min");
        }
        if (performance.getEvent().getType() != EventType.SECTORS) {
            LOGGER.error("Error at validatePurchaseOrReservationForSectors: Bad Request - Internal error. This method is only available for other eventtypes.");
            throw new BadRequestException("exception.ticket.buyorreserve.type");
        }
        if (performance.getDate().isBefore(LocalDateTime.now())) {
            LOGGER.error("Error at validatePurchaseOrReservationForSectors: Bad Request - This performance already took place.");
            throw new BadRequestException("exception.ticket.buyorreserve.date");
        }
        // we parse the strings to sectors
        SortedMap<Sector, Long> seatsBySectorsSorted = new TreeMap<>();
        SortedMap<Sector, Long> seatsBySectorsForPerformance = performanceService.getSeatsBySector(performance.getId());
        for (Map.Entry<String, Long> seatsBySector : seatsBySectors.entrySet()) {
            if (seatsBySector.getValue() < 0) {
                LOGGER.error("Error at validatePurchaseOrReservationForSectors: Bad Request - Some sectors contain invalid counts.(negativ or not set)");
                throw new BadRequestException("exception.ticket.buyorreserve.sector.invalid");
            }
            Sector sector;
            try {
                sector = Sector.valueOf(seatsBySector.getKey());
            } catch (Exception e) {
                LOGGER.error("Error at validatePurchaseOrReservationForSectors: Bad Request - Invalid sectorname found.");
                throw new BadRequestException("exception.ticket.buyorreserve.sector.sectorname");
            }
            if (!seatsBySectorsForPerformance.containsKey(sector)) {
                LOGGER.error("Error at validatePurchaseOrReservationForSectors: Bad Request - Some sectors don't exist at this performance");
                throw new BadRequestException("exception.ticket.buyorreserve.sector.performance");
            }
            seatsBySectorsSorted.put(sector, seatsBySector.getValue());
        }

        // now we check if every sector has enough places to satisfy our demands
        for (Map.Entry<Sector, Long> seatsBySector : seatsBySectorsSorted.entrySet()) {
            if (seatsBySector.getValue() > seatsBySectorsForPerformance.get(seatsBySector.getKey())) {
                // too high demand
                LOGGER.error("Error at validatePurchaseOrReservationForSectors: Bad Request - Some sectors don't have enough free places left.");
                throw new BadRequestException("exception.ticket.buyorreserve.sector.notfree");
            }
        }

        // check if price is too high
        BigDecimal sumPrice = BigDecimal.ZERO;
        for(Map.Entry<String, Long> seatsBySector : seatsBySectors.entrySet()){
            BigDecimal price = performance.getPrice().multiply(Sector.valueOf(seatsBySector.getKey()).getFactor()).round(new MathContext(2));
            price = price.multiply(new BigDecimal(seatsBySector.getValue()));
            sumPrice = sumPrice.add(price);
        }
        if(sumPrice.compareTo(new BigDecimal(400L)) > 0){
            LOGGER.error("Error at validatePurchaseOrReservationForSectors: Bad Request - Tickets cannot have a value of more than 400");
            throw new BadRequestException("exception.ticket.buyorrreserve.sectors.toohigh");
        }

        return seatsBySectorsSorted;
    }

    /**
     * creates SeatReservations for a given ticket (EventType.SECTORS)
     *
     * @param ticket         the ticket (note that id has to be valid)
     * @param performance    the performance (note that id has to be valid)
     * @param seatsBySectors all the demands of seats for sectors (note that validatePurchaseOrReservationForSectors has to be called before this function)
     */
    private void createSeatReservationsForTicketSectors(Ticket ticket, Performance performance, SortedMap<Sector, Long> seatsBySectors) {
        List<SeatDTO> seatsByFree = performanceService.getSeats(performance.getId());
        // we sort the seats by sectors to be able to create seatreservations faster
        seatsByFree.sort(new Comparator<SeatDTO>() {
            @Override
            public int compare(SeatDTO o1, SeatDTO o2) {
                return o1.getSector().compareTo(o2.getSector());
            }
        });

        List<SeatReservation> seatReservations = new ArrayList<>();
        for (SeatDTO seatDTO : seatsByFree) {
            if (seatDTO.getState() == SeatState.FREE) {
                if (!seatsBySectors.containsKey(seatDTO.getSector())) continue;
                // we could  buyReserved this ticket
                Long demand = seatsBySectors.get(seatDTO.getSector());
                if (demand > 0) {
                    // we buyReserved this ticket because demand > 0
                    seatReservations.add(SeatReservation.newBuilder().setPerformance(performance).setTicket(ticket).setSeat(seatsMapper.seatDTOToSeat(seatDTO)).build());
                    // update demand
                    seatsBySectors.put(seatDTO.getSector(), demand - 1);
                }
            }
        }
        seatReservationRepository.saveAll(seatReservations);
    }


    @Override
    public Long buyTicketsForSectors(boolean buy,Long customerId,Long performanceId, Map<String, Long> seatsBySectors) {
        if (!performanceRepository.existsById(performanceId)) {
            LOGGER.error("Error at buyTicketsForSectors: Not Found - Performance with specified id could not be found.");
            throw new NotFoundException("exception.performance.find.notfound");
        }
        if (!customerRepository.existsById(customerId)) {
            LOGGER.error("Error at buyTicketsForSectors: Not Found - Customer with specified id could not be found.");
            throw new NotFoundException("exception.customer.find.notfound");
        }
        Customer customer = customerRepository.getOne(customerId);
        Performance performance = performanceRepository.getOne(performanceId);

        SortedMap<Sector, Long> seatsBySectorsSorted = validatePurchaseOrReservationForSectors(performance, seatsBySectors);

        // now we know that everything is valid, we can create a ticket and all seatReservations
        Ticket ticket;
        if (buy) {
            ticket = Ticket.newBuilder().state(TicketState.BOUGHT).customer(customer).performance(performance).build();
        }
        else {
            ticket = Ticket.newBuilder().state(TicketState.RESERVED).customer(customer).performance(performance).build();
        }
        ticketRepository.save(ticket);

        createSeatReservationsForTicketSectors(ticket, performance, seatsBySectorsSorted);

        return ticket.getId();
    }

    @Override
    @Transactional
    public void updateTicketState(Long id, TicketState ticketState) {
        Ticket ticket = findOne(id);
        ticket.setState(ticketState);
        ticketRepository.save(ticket);

        seatReservationRepository.updateTicketStatesForTicket(id, ticketState);
    }

    @Override
    public List<Ticket> search(Long customerId, Long eventID, TicketState ticketState) {
        return ticketRepository.findByCustomerAndPerformance(customerId, eventID, ticketState, customerId == null, eventID == null, ticketState == null);
    }

    @Override
    public Ticket search(Long id) {
        return ticketRepository.findById(id).orElseThrow(() -> new NotFoundException("exception.ticket.find.notfound"));
    }

    @Transactional
    private void validateBuyReservedTicket(Long ticketId){
        if(ticketId == null){
            LOGGER.error("Error at validateBuyReservedTicket: Bad Request - icketid has to be set.");
            throw new BadRequestException("exception.ticket.buyreserved.id");
        }
        if(!ticketRepository.existsById(ticketId)){
            LOGGER.error("Error at validateBuyReservedTicket: Not Found - Ticket with specified id could not be found.");
            throw new NotFoundException("exception.ticket.find.notfound");
        }
        Ticket ticket = ticketRepository.findById(ticketId).get();
        if(ticket.getState() != TicketState.RESERVED){
            LOGGER.error("Error at validateBuyReservedTicket: Bad Request - Only reserved tickets can be bought.");
            throw new BadRequestException("exception.ticket.buyreserved.state");
        }
    }

    @Transactional
    @Override
    public Ticket buyReservedSeats(@NotNull Long id, List<SeatDTO> seats) {
        validateBuyReservedTicket(id);

        Ticket ticket = ticketRepository.findById(id).get();
        List<SeatReservation> seatReservations = new ArrayList<>(ticket.getSeatReservations());

        if(seatReservations.size() <= 0){
            LOGGER.error("Internal Error occurred");
            throw new InternalError();
        }

        Performance performance = seatReservations.get(0).getPerformance();
        Event event = performance.getEvent();

        if(event.getType() != EventType.SEATS){
            LOGGER.error("Error at buyReservedSeats-Validation: Bad Request -  Something is invalid.");
            throw new BadRequestException("exception.ticket.buyorreserve.type");
        }
        if(seats.size() > seatReservations.size()){
            LOGGER.error("Error at buyReservedSeats-Validation: Bad Request -  Something is invalid.");
            throw new BadRequestException("exception.ticket.buyreserved.seats.state");
        }
        if(seats.size()<=0){
            LOGGER.error("Error at buyReservedSeats-Validation: Bad Request -  Something is invalid.");
            throw new BadRequestException("exception.ticket.buyorreserve.sector.min");
        }

        if (seats.stream().anyMatch(seatDTO -> {
            return seatDTO.getxCoord() == null || seatDTO.getyCoord() == null || seatDTO.getId_hall() == null;
        })) {
            LOGGER.error("Error at buyReservedSeats-Validation: Bad Request -  Something is invalid.");
            throw new BadRequestException("exception.ticket.buyorreserve.seats.invalid");
        }
        if (seats.stream().anyMatch(seatDTO -> {
            return !seatDTO.getId_hall().equals(performance.getHall().getId());
        })) {
            LOGGER.error("Error at buyReservedSeats-Validation: Bad Request -  Something is invalid.");
            throw new BadRequestException("exception.ticket.buyorreserve.seats.wronghall");
        }
        List<Seat> seatsFound = seatRepository.findAllById(seats.stream().map(seatDTO -> (new SeatPK(seatDTO.getxCoord(), seatDTO.getyCoord(), seatDTO.getId_hall()))).collect(Collectors.toList()));
        if (seatsFound.stream().anyMatch(seat -> seat == null)) {
            LOGGER.error("Error at buyReservedSeats: Not Found - Seat with specified id could not be found.");
            throw new NotFoundException("exception.hall.seat.notfound");
        }

        // Build map for fast check if every seat is in seatReservations
        // We also use this map to find the seats which are removed
        Map<Seat, Boolean> mapSeatsActual = new HashMap<>();
        for (SeatReservation seatReservation : seatReservations) {
            mapSeatsActual.put(seatReservation.getSeat(), true);
        }
        // Now check if every seat is in the map
        for (SeatDTO seatDTO : seats) {
            if (!mapSeatsActual.containsKey(seatsMapper.seatDTOToSeat(seatDTO))) {
                LOGGER.error("Error at buyReservedSeats-Validation: Bad Request -  Something is invalid.");
                throw new BadRequestException("exception.ticket.buyreserved.seats.state");
            }
            else{
                // we dont want to remove this seat
                mapSeatsActual.put(seatsMapper.seatDTOToSeat(seatDTO), false);
            }
        }

        //remove tickets which arent bought
        List<SeatReservation> removeReservations = new ArrayList<>();

        for(Map.Entry<Seat, Boolean> seat : mapSeatsActual.entrySet()){
            if(seat.getValue()){
                SeatReservation seatReservation = SeatReservation.newBuilder().setTicket(ticket).setPerformance(performance).setSeat(seat.getKey()).build();
                removeReservations.add(seatReservation);
            }
        }
        seatReservationRepository.deleteInBatch(removeReservations);
        return this.buyReserved(id);
    }

    @Transactional
    @Override
    public Ticket buyReservedSectors(Long id, Map<String, Long> seatsBySectors) {
        validateBuyReservedTicket(id);
        Ticket ticket = ticketRepository.findById(id).get();
        List<SeatReservation> seatReservations = new ArrayList<>(ticket.getSeatReservations());

        if(seatReservations.size() <= 0){
            LOGGER.error("Error at buyReservedSectors-Validation: InternalError");
            throw new InternalError();
        }

        Event event = seatReservations.get(0).getPerformance().getEvent();
        if(event.getType() != EventType.SECTORS){
            LOGGER.error("Error at buyReservedSectors-Validation: Bad Request -  Something is invalid.");
            throw new BadRequestException("exception.ticket.buyorreserve.type");
        }

        Long sumSeats = seatsBySectors.values().stream().mapToLong(l -> l.longValue()).sum();
        if(sumSeats > seatReservations.size()){
            LOGGER.error("Error at buyReservedSectors-Validation: Bad Request -  Something is invalid.");
            throw new BadRequestException("exception.ticket.buyreserved.seats.state");
        }

        if(sumSeats == 0){
            LOGGER.error("Error at buyReservedSectors-Validation: Bad Request -  Something is invalid.");
            throw new BadRequestException("exception.ticket.buyorreserve.sector.min");
        }

        Map<String, Long> seatsBySectorsActual = new HashMap<>();
        for(SeatReservation seatReservation : seatReservations){
            String sector = seatReservation.getSeat().getSector().toString();
            if(!seatsBySectorsActual.containsKey(sector)){
                seatsBySectorsActual.put(sector, 1L);
            }
            else{
                Long val = seatsBySectorsActual.get(sector);
                seatsBySectorsActual.put(sector, val+1);
            }
        }

        for(Map.Entry<String, Long> seatBySector : seatsBySectors.entrySet()){
            if(!seatsBySectorsActual.containsKey(seatBySector.getKey())){
                LOGGER.error("Error at buyReservedSectors-Validation: Bad Request -  Something is invalid.");
                throw new BadRequestException("exception.ticket.buyorreserve.sector.sectorname");
            }
            else if(seatsBySectorsActual.get(seatBySector.getKey()) < seatBySector.getValue()){
                LOGGER.error("Error at buyReservedSectors-Validation: Bad Request -  Something is invalid.");
                throw new BadRequestException("exception.ticket.buyreserved.seats.state");
            }
        }

        // Actual removement
        List<SeatReservation> removeReservations = new ArrayList<>();

        for(SeatReservation seatReservation : seatReservations){
            String sector = seatReservation.getSeat().getSector().toString();
            if(seatsBySectors.containsKey(sector) && !seatsBySectors.get(sector).equals(seatsBySectorsActual.get(sector))){
                // something to remove
                Long val = seatsBySectors.get(sector);
                seatsBySectors.put(sector, val+1);

                removeReservations.add(seatReservation);
            }
            else if(!seatsBySectors.containsKey(sector)){
                // a sector which is not even in the input, we have to remove this reservation
                removeReservations.add(seatReservation);
            }
        }
        seatReservationRepository.deleteInBatch(removeReservations);
        return this.buyReserved(id);
    }

    @Transactional
    @Override
    public void refundReservedTicket(@NotNull Long ticketID) {
        if(!ticketRepository.existsById(ticketID)){
            LOGGER.error("Error at refundReservedTicket: Not Found - Ticket with specified id could not be found.");
            throw new NotFoundException("exception.ticket.find.notfound");
        }
        Ticket ticket = ticketRepository.findById(ticketID).get();

        if(ticket.getState() != TicketState.RESERVED){
            LOGGER.error("Error at refundReservedTicket: Bad Request - Only reserved tickets can be cancelled.");
            throw new BadRequestException("exception.ticket.refund.reserved.state");
        }

        seatReservationRepository.deleteAll(ticket.getSeatReservations());
        ticketRepository.delete(ticket);
    }

    @Override
    @Transactional
    public List<SeatDTO> getSeats(@NotNull Long ticketID) {
        if(!ticketRepository.existsById(ticketID)){
            LOGGER.error("Error at getSeats: Not Found - Ticket with specified id could not be found.");
            throw new NotFoundException("exception.ticket.find.notfound");
        }
        List<Seat> seats = ticketRepository.findById(ticketID).get().getSeatReservations()
            .stream().map(sr -> sr.getSeat()).collect(Collectors.toList());
        return seatsMapper.seatToSeatDTO(seats);
    }

    @Override
    @Transactional
    public Map<String, Long> getSeatsBySectors(@NotNull Long ticketID) {
        if(!ticketRepository.existsById(ticketID)){
            LOGGER.error("Error at getSeatsBySectors: Not Found - Ticket with specified id could not be found.");
            throw new NotFoundException("exception.ticket.find.notfound");
        }
        List<Seat> seats = ticketRepository.findById(ticketID).get().getSeatReservations()
            .stream().map(sr -> sr.getSeat()).collect(Collectors.toList());
        Map<String, Long> seatsBySectors = new TreeMap<>();

        for(Seat seat : seats){
            String sector = seat.getSector().toString();
            if(!seatsBySectors.containsKey(sector)){
                seatsBySectors.put(sector, 1L);
            }
            else{
                Long val = seatsBySectors.get(sector);
                seatsBySectors.put(sector, val+1);
            }
        }
        return seatsBySectors;
    }

    @Override
    public File getPdf(@NotNull Long ticketID) {
        if(!ticketRepository.existsById(ticketID)){
            LOGGER.error("Error at getPdf: Not Found - Ticket with specified id could not be found.");
            throw new NotFoundException("exception.ticket.find.notfound");
        }
        Ticket ticket = ticketRepository.findById(ticketID).get();
        if(ticket.getState() == TicketState.RESERVED){
            LOGGER.error("Error at getPdf: Bad Request - No bills for reserved tickets.");
            throw new BadRequestException("no bills for reserved tickets!");
        }

        File file = null;
        if(ticket.getState() == TicketState.BOUGHT){
            file = new File("bills" + File.separator + ticket.getId() + "-bought");
        }
        else{
            file = new File("bills" + File.separator + ticket.getId() + "-reserved");
        }

        if(file.exists()){
            // Bill already created
            return file;
        }
        else{
            file.getParentFile().mkdirs();
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new InternalError();
            }
        }

        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream(file));
        } catch (DocumentException e) {
            LOGGER.error("Error at getPdf: Document Exception appeared.");
            throw new InternalError();
        } catch (FileNotFoundException e) {
            LOGGER.error("Error at getPdf: File not Found Exception appeared.");
            throw new InternalError();
        }

        document.open();

        boolean isBought = ticket.getState() == TicketState.BOUGHT;

        try {
            Chunk titleChunk = new Chunk(isBought ? "Rechnung": "Stornorechnung", FontFactory.getFont(FontFactory.COURIER, 30, BaseColor.BLACK));
            Phrase titlePhrase = new Phrase();
            titlePhrase.add(titleChunk);
            Paragraph titleParagraph = new Paragraph();
            titleParagraph.add(titlePhrase);
            titleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(titleParagraph);
            document.add(Chunk.NEWLINE);

            document.add(new Paragraph("Ticketline-GMBH\n" +
                "Wiedner Hauptstraße 76/2/2\n" +
                "1040 Wien\n"));
            document.add(Chunk.NEWLINE);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
            Paragraph dateParagraph = new Paragraph("Datum: " + LocalDate.now().format(formatter));
            dateParagraph.setAlignment(Element.ALIGN_RIGHT);
            document.add(dateParagraph);
            document.add(new Chunk(new LineSeparator()));
            document.add(Chunk.NEWLINE);

            document.add(new Paragraph("Ticket für Veranstaltung \"" + ticket.getPerformance().getEvent().getName() + "\" am " + formatter.format(ticket.getPerformance().getDate()) +
                " um " + timeFormatter.format(ticket.getPerformance().getDate())));
            Hall h = ticket.getPerformance().getHall();
            document.add(new Paragraph("Ort: " + String.format("%s   %s, %s %s,%s", h.getName(), h.getCountry(), h.getCity(), h.getPostcode(), h.getStreet())));

            document.add(Chunk.NEWLINE);

            PdfPTable table = new PdfPTable(4);
            table.addCell("Artikel");
            table.addCell("Bezeichnung");
            table.addCell("Menge");
            table.addCell("Gesamtpreis");

            BigDecimal sumPrice = BigDecimal.ZERO;
            Event event = ticket.getPerformance().getEvent();
            Performance performance = ticket.getPerformance();
            if(event.getType() == EventType.SEATS){
                List<SeatDTO> seats = this.getSeats(ticket.getId());
                for(SeatDTO seatDTO : seats){
                    table.addCell("Sitzplatz");
                    table.addCell("Kateorie " + seatDTO.getSector() + ", Reihe " + (seatDTO.getyCoord()+1) +", Platz " + (seatDTO.getxCoord()+1));
                    table.addCell("1");
                    BigDecimal seatPrice = seatDTO.getSector().getFactor().multiply(performance.getPrice()).round(new MathContext(2));
                    sumPrice = sumPrice.add(seatPrice);
                    table.addCell((isBought ? "€" : "-€") + seatPrice.setScale(2));
                }
            }
            else{
                Map<String, Long> seatsBySectors = this.getSeatsBySectors(ticket.getId());
                for(Map.Entry<String, Long> seatsBySector : seatsBySectors.entrySet()){
                    table.addCell(seatsBySector.getValue() > 1 ? "Sitzplätze" : "Sitzplatz");
                    table.addCell("Sektor " + seatsBySector.getKey());
                    table.addCell(seatsBySector.getValue().toString());
                    BigDecimal price = performance.getPrice().multiply(Sector.valueOf(seatsBySector.getKey()).getFactor()).round(new MathContext(2));
                    price = price.multiply(new BigDecimal(seatsBySector.getValue()));
                    sumPrice = sumPrice.add(price);
                    table.addCell((isBought ? "€" : "-€") + price.setScale(2));
                }
            }

            document.add(table);
            document.add(new Chunk(new LineSeparator()));

            Paragraph sumParagraph = new Paragraph("Summe brutto (inkl 20% USt): " + (isBought ? "€" : "-€") + sumPrice.toString());
            sumParagraph.setAlignment(Element.ALIGN_RIGHT);
            document.add(sumParagraph);
            document.add(Chunk.NEWLINE);

            Paragraph deliveryParagraph = new Paragraph("Lieferdatum:" + formatter.format(ticket.getBillDate()));
            document.add(deliveryParagraph);

        } catch (DocumentException e) {
            LOGGER.error("Error at getPdf: Document Exception appeared.");
            throw new InternalError();
        }
        document.close();

        return file;
    }
}
