package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface NewsService {

    /**
     * Find all news entries ordered by published at date (descending).
     *
     * @return ordered list of al news entries
     */
    List<News> findAll();


    /**
     * Find a single news entry by id.
     *
     * @param id the is of the news entry
     * @return the news entry
     */
    News findOne(@NotNull Long id);

    /**
     * Publish a single news entry
     *
     * @param news to publish
     * @return published news entry
     */
    News publishNews(News news);

    /**
     * Returns a list of news this user has not read.
     *
     * @param user The user to check.
     * @return the news not read by this user.
     */
    List<News> userUnreadNews(User user);

    /**
     * Marks the given news as read by the given user.
     * @param news
     * @param user
     * @return The marked news.
     */
	News markNewsAsRead(News news, User user);

}
