package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;

import java.util.List;

/**
 * The interface Hall service.
 */
public interface HallService {

    /**
     * Finds all halls
     *
     * @return the list of all halls
     */
    List<Hall> findAll();


    /**
     * Search halls which matches text.
     *
     * @param text the text to match
     * @return the list of halls, which match
     */
    List<Hall> search(String text);
}
