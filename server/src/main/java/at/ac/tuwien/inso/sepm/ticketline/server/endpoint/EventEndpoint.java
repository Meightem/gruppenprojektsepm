package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventSearchDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.server.endpoint.base.EndpointBase;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.events.EventsMapper;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.TopEventDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.events.TopEventMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.HeaderTokenAuthenticationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/events")
@Api(value = "events")
public class EventEndpoint extends EndpointBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersEndpoint.class);

    private final EventService eventService;
    private final HeaderTokenAuthenticationService authenticationService;
    private final EventsMapper eventsMapper;
    private final TopEventMapper topEventMapper;

    @Autowired
    public EventEndpoint(EventService eventService, HeaderTokenAuthenticationService authenticationService, EventsMapper eventsMapper, TopEventMapper topEventMapper) {
        this.eventService = eventService;
        this.authenticationService = authenticationService;
        this.eventsMapper = eventsMapper;
        this.topEventMapper = topEventMapper;
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Create a new event")
    public EventDTO createEvent(@RequestBody EventDTO eventDTO) {
        return eventsMapper.eventToEventDTO(eventService.createEvent(eventsMapper.eventDTOtoEvent(eventDTO)));
    }

    @RequestMapping(value = "/{eventId}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Gets a single event")
    public EventDTO findOne(@PathVariable Long eventId) {
        return eventsMapper.eventToEventDTO(eventService.findOne(eventId));
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Get list of event")
    public List<EventDTO> findAll() {
        return eventsMapper.eventToEventDTO(eventService.findAll());
    }

    @RequestMapping(value = "/top/{type}/{month}/{year}",method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Get list of top event")
    public List<TopEventDTO> search(@PathVariable EventType type, @PathVariable int month, @PathVariable int year) {
        return topEventMapper.TopEventToTopEventDTO(eventService.getTopTenEvents(type,month,year));
    }

    @RequestMapping(value = "/artist/{artistID}",method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Get list of events of artist")
    public List<EventDTO> search(@PathVariable long artistID) {
        return eventsMapper.eventToEventDTO(eventService.findEventsOfArtist(artistID));
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Create a new event")
    public List<EventDTO> searchEvent(@RequestBody EventSearchDTO eventSearchDTO) {
        return eventsMapper.eventToEventDTO(eventService.search(eventSearchDTO));
    }

    @RequestMapping(value = "/search/{text}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Create a new event")
    public List<EventDTO> searchEvent(@PathVariable("text") String text) {
        return eventsMapper.eventToEventDTO(eventService.search(text));
    }

}
