package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.seats;

import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring",imports = {Seat.class,Hall.class})
public interface SeatsMapper {
    @Mappings({
        @Mapping(target = "hall", expression = "java(Hall.newBuilder().setId(seatDTO.getId_hall()).build())")
    })
    Seat seatDTOToSeat(SeatDTO seatDTO);

    @Mappings({
        @Mapping(target = "id_hall", expression = "java(seat.getHall()==null?null:seat.getHall().getId())")
    })
    SeatDTO seatToSeatDTO(Seat seat);

    List<SeatDTO> seatToSeatDTO(List<Seat> seat);

    List<Seat> seatDTOToSeat(List<SeatDTO> seatDTO);
}
