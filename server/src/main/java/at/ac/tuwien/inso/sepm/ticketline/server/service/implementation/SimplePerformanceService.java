package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatState;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BadRequestException;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Performance;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ExtendedConstraintViolationException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.HallRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.PerformanceRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.dao.PerformanceSearchDAO;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.queryResults.SeatResult;
import at.ac.tuwien.inso.sepm.ticketline.server.service.PerformanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;

@Service
public class SimplePerformanceService implements PerformanceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimplePerformanceService.class);

    private final PerformanceRepository performanceRepository;
    private final Validator validator;
    private final HallRepository hallRepository;
    private final PerformanceSearchDAO performanceSearchDAO;

    @Autowired
    public SimplePerformanceService(PerformanceRepository performanceRepository, Validator validator, HallRepository hallRepository, PerformanceSearchDAO performanceSearchDAO) {
        this.performanceRepository = performanceRepository;
        this.hallRepository = hallRepository;
        this.validator = validator;
        this.performanceSearchDAO = performanceSearchDAO;
    }

    @Override
    public List<SeatDTO> getSeats(Long performanceId) {
        if(performanceId == null) {
            LOGGER.error("Error at getSeats: Bad Request - ID is null");
            throw new BadRequestException("exception.performance.seats.get.id"); // doesnt exist
        }
        if(!performanceRepository.findById(performanceId).isPresent()){
            LOGGER.error("Error at getSeats: Bad Request - There is no performance with this id.");
            throw new BadRequestException("exception.performance.seats.get.id.notfound");
        }
        List<SeatResult> seats = performanceRepository.getSeats(performanceId);
        //List<SeatResult> seats = seatsByPerformanceDAO.getSeats(performanceId);
        List<SeatDTO> seatDTOS = new ArrayList<>();

        for (int i = 0; i < seats.size(); i++) {
            SeatDTO seatDTO = SeatDTO.newBuilder()
                .sector(seats.get(i).getSector())
                .id_hall(seats.get(i).getId_hall())
                .xCoord(seats.get(i).getxCoord())
                .yCoord(seats.get(i).getyCoord())
                .build();
            if (seats.get(i).getState() == null) {
                seatDTO.setState(SeatState.FREE);
            } else if (seats.get(i).getState() == TicketState.BOUGHT) {
                seatDTO.setState(SeatState.BOUGHT);
            }
            else{
                seatDTO.setState(SeatState.RESERVED);
            }
            seatDTOS.add(seatDTO);

        }
        return seatDTOS;
    }

    @Override
    public SortedMap<Sector, Long> getSeatsBySector(Long performanceId) {
        if(performanceId == null) {
            LOGGER.error("Error at getSeatsBySector: Bad Request - ID is null.");
            throw new BadRequestException("exception.performance.get.id"); // doesnt exist
        }
        if(!performanceRepository.findById(performanceId).isPresent()){
            LOGGER.error("Error at getSeatsBySector: Bad Request - There is no performance with this id.");
            throw new BadRequestException("exception.performance.get.id.notfound");
        }
        SortedMap<Sector, Long> seatsBySectors = new TreeMap<>();
        for(Object[] res : performanceRepository.getSeatsBySectors(performanceId)){
            seatsBySectors.put((Sector)res[0], (Long)res[1]);
        }
        return seatsBySectors;
    }

    @Override
    public List<Performance> findAll() {
        return performanceRepository.findAll();
    }

    @Override
    public Performance findOne(@NotNull Long id) {
        return performanceRepository.findById(id).orElseThrow(() -> {
            LOGGER.error("Error at findOne: Not Found - Performance with specified id could not be found. ");
            throw new NotFoundException("exception.performance.find.notfound");
        });
    }

    @Override
    public Performance createPerformance(Performance performance) {
        Set<ConstraintViolation<Performance>> violations = validator.validate(performance);
        if (!violations.isEmpty()) {
            LOGGER.error("Error at createPerformance: Constraint Violation - Performance contains invalid values.");
            throw new ExtendedConstraintViolationException("exception.performance.create.constraint",violations);
        }

        if (performance.getEvent() == null || performance.getHall() == null || performance.getEvent().getId() == null || performance.getHall().getId() == null) {
            LOGGER.error("Error at createPerformance: Bad Request - The event id and hall id has to be set.");
            throw new BadRequestException("exception.performance.create.hallandevent");
        }

        // Workaround for hibernate, which truncates nanosecond portion but doesn't return truncated version
        if (performance.getDate() != null) {
            performance.setDate(performance.getDate().withNano(0));
        }
        return performanceRepository.save(performance);
    }

    @Override
    public List<Performance> findAllByEventId(Long eventId) {
        return performanceRepository.findAllByEventId(eventId);
    }

    @Override
    public List<Performance> findAllByHallId(Long hallId) {
        return new ArrayList<>(hallRepository.findById(hallId).orElseThrow(() -> new NotFoundException("exception.performance.find.notfound")).getPerformances());
    }

    @Override
    public List<Performance> search(PerformanceDTO performanceSearchDTO) {
        return performanceSearchDAO.search(performanceSearchDTO);
    }
}
