package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.performances;

import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Performance;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PerformancesMapper {
    Performance performanceDTOToPerformance(PerformanceDTO performanceDTO);

    PerformanceDTO performanceToPerformanceDTO(Performance performance);

    List<PerformanceDTO> performanceToPerformanceDTO(List<Performance> performances);
}
