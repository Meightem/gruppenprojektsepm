package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.queryResults.TopEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    @Query(
        "SELECT new at.ac.tuwien.inso.sepm.ticketline.server.repository.queryResults.TopEvent(e, COUNT(e)) " +
            "FROM Event e JOIN e.performances p ON " +
            "  e.id = p.event.id JOIN p.seatReservations sr ON " +
            "  p.id = sr.performance.id " +
            "JOIN sr.ticket t ON " +
            "  t.id = sr.ticket.id WHERE MONTH(p.date) = :month AND " +
            "  YEAR(p.date) = :year AND " +
            "  e.type = :type " +
            "GROUP BY e ORDER BY COUNT(e) DESC"
    )
    List<TopEvent> getTopEvents(@Param("month") int month, @Param("year") int year,@Param("type") EventType type);

    @Query("SELECT e FROM Event e WHERE e.artist.id = :id")
    List<Event> findEventsOfArtist(@Param("id") long id);
}
