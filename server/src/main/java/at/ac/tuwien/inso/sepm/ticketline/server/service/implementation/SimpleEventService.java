package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventSearchDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Artist;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BadRequestException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ExtendedConstraintViolationException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.ArtistRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.EventRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.dao.EventSearchDAO;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.queryResults.TopEvent;
import at.ac.tuwien.inso.sepm.ticketline.server.service.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SimpleEventService implements EventService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleEventService.class);
    private final EventRepository eventRepository;
    private final ArtistRepository artistRepository;
    private final EventSearchDAO eventSearchDAO;
    private final Validator validator;

    @Autowired
    public SimpleEventService(EventRepository eventRepository, ArtistRepository artistRepository, EventSearchDAO eventSearchDAO, Validator validator) {
        this.eventRepository = eventRepository;
        this.artistRepository = artistRepository;
        this.eventSearchDAO = eventSearchDAO;
        this.validator = validator;
    }

    @Override
    public List<Event> findAll() {
        return eventRepository.findAll();
    }

    @Override
    public Event findOne(@NotNull Long id) {
        return eventRepository.findById(id).orElseThrow(() -> {
            throw new NotFoundException("exception.event.find.notfound");
        });
    }

    @Override
    public Event createEvent(Event event) {
        Set<ConstraintViolation<Artist>> artistViolations = validator.validate(event.getArtist());
        if (!artistViolations.isEmpty()) {
            LOGGER.error("Error at createEvent: Constraint Violation - Event could not be created.");
            throw new ExtendedConstraintViolationException("exception.event.create", artistViolations);
        }
        List<Artist> artists = artistRepository.findExact(event.getArtist().getFirstName(), event.getArtist().getLastName());
        Artist artist;
        if (artists.size() == 0) {
            artist = artistRepository.save(event.getArtist());
        } else {
            artist = artists.get(0);
        }
        event.setArtist(artist);
        Set<ConstraintViolation<Event>> eventViolations = validator.validate(event);
        if (!eventViolations.isEmpty()) {
            LOGGER.error("Error at createEvent: Constraint Violation - Event could not be created.");
            throw new ExtendedConstraintViolationException("exception.event.create", eventViolations);
        }
        return eventRepository.save(event);
    }

    @Override
    public List<TopEvent> getTopTenEvents(EventType eventType, int month, int year) {
        if (month > 12 || month < 1) {
            LOGGER.error("Error at getTopTenEvents: Bad Request - Month has to be valid (between 1 and 12).");
            throw new BadRequestException("exception.event.topten.month");
        }
        return eventRepository.getTopEvents(month, year, eventType).stream().limit(10).collect(Collectors.toList());
    }

    @Override
    public List<Event> findEventsOfArtist(long artistID) {
        return eventRepository.findEventsOfArtist(artistID);
    }

    @Override
    public List<Event> search(EventSearchDTO eventSearchDTO) {
        return eventSearchDAO.searchEvent(eventSearchDTO);
    }

    @Override
    public List<Event> search(String text) {
        return eventSearchDAO.searchEvent(text);
    }
}
