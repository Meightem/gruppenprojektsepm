package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.SeatReservation;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.keys.SeatReservationPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;

@Repository
public interface SeatReservationRepository extends JpaRepository<SeatReservation, SeatReservationPK> {

    @Modifying
    @Query("update SeatReservation set state=:ticketState " +
        "where ticket.id = :id_ticket")
    void updateTicketStatesForTicket(@Param("id_ticket") Long id, @Param("ticketState") TicketState ticketState);
}
