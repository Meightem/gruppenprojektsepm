package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * Find a single user by its username
     * @param username
     * @return The user if it was found
     */
    Optional<User> findOneByUsername(String username);

    /**
     * Find a single user by its username
     * @param username
     * @return The user if it was found or null
     */
    User findByUsername(String username) throws EmptyResultDataAccessException;

    Long countByIsAdminAndIsLocked(Boolean isAdmin, Boolean isLocked);
}
