package at.ac.tuwien.inso.sepm.ticketline.server.repository.dao;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;
import org.hibernate.search.bridge.TwoWayFieldBridge;

public class HallFieldBridge implements FieldBridge {
    @Override
    public void set(String s, Object o, Document document, LuceneOptions luceneOptions) {
        Hall hall = (Hall)o;

        luceneOptions.addFieldToDocument(
            s + ".id",
            hall.getId() + "",
            document
        );
    }

}
