package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.artist.ArtistMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.events.EventsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.halls.HallsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.performances.PerformancesMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.*;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;

@Profile("generateData")
@Component
public class EventDataGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventDataGenerator.class);
    private final EventRepository eventRepository;
    private final EventsMapper eventsMapper;
    private static final int NUMBER_OF_EVENTS = 20;
    private final ArtistRepository artistRepository;
    private final ArtistMapper artistMapper;
    private final PerformanceRepository performanceRepository;
    private final PerformancesMapper performancesMapper;
    private final HallRepository hallRepository;
    private final HallsMapper hallsMapper;
    private final SeatRepository seatRepository;

    public EventDataGenerator(EventRepository eventRepository, EventsMapper eventsMapper, ArtistRepository artistRepository, ArtistMapper artistMapper, PerformanceRepository performanceRepository, PerformancesMapper performancesMapper, HallRepository hallRepository, HallsMapper hallsMapper,SeatRepository seatRepository) {
        this.eventRepository = eventRepository;
        this.eventsMapper = eventsMapper;
        this.artistRepository = artistRepository;
        this.artistMapper = artistMapper;
        this.performanceRepository = performanceRepository;
        this.performancesMapper = performancesMapper;
        this.hallRepository = hallRepository;
        this.hallsMapper = hallsMapper;
        this.seatRepository = seatRepository;
    }

    @PostConstruct
    public void generateEventsIncludingAll() {
        Faker faker = new Faker();

        if (eventRepository.count() > 0) {
            LOGGER.info("events and belonging entities already generated");
        } else {
            // generate halls
            HallDTO hall1DTO = HallDTO.newBuilder()
                .city(faker.address().cityName())
                .country(faker.address().country())
                .name(String.format("%s %s Halle", faker.name().firstName(), faker.name().lastName()))
                .street(faker.address().streetName())
                .postcode(faker.address().streetAddressNumber())
                .build();
            Hall hall1 = hallsMapper.HallDTOToHall(hall1DTO);

            HallDTO hall2DTO = HallDTO.newBuilder()
                .city(faker.address().cityName())
                .country(faker.address().country())
                .name(String.format("%s %s Platz", faker.name().firstName(), faker.name().lastName()))
                .street(faker.address().streetName())
                .postcode(faker.address().streetAddressNumber())
                .build();
            Hall hall2 = hallsMapper.HallDTOToHall(hall2DTO);


            hallRepository.save(hall1);
            setLayout1To(hall1);
            hallRepository.save(hall2);
            setLayout2To(hall2);

            generateInsoHall();

            for (int i = 0; i < NUMBER_OF_EVENTS; i++) {
                // generate Artists
                Artist artist = artistMapper.artistDTOToArtist(ArtistDTO.newBuilder()
                    .firstName(faker.name().firstName())
                    .lastName(faker.name().lastName())
                    .build());

                artistRepository.save(artist);

                String eventContent;

                switch(i % 8) {
                    case 0:
                        eventContent = "Im Anschluss an den dramatischen Konflikt zwischen den Mitgliedern der Avengers kehrt T’Challa alias Black Panther in seine Heimat Wakanda zurück.";
                        break;
                    case 1:
                        eventContent = "Eine Verfilmung des gleichnamigen Romans von Bestseller-Autor Ian McEwan über die Beziehung eines jungen Paares, das im Käfig seiner Zeit und seiner Erziehung gefangen ist.";
                        break;
                    case 2:
                        eventContent = "Noch nie kämpften mehr Superhelden in einem Film, noch nie war eine Mission so riskant und noch nie war die Gefahr zu scheitern so greifbar!";
                        break;
                    case 3:
                        eventContent = "In der neusten Produktion des Horrorthriller-Experten Blumhouse Productions (Happy Deathday, Get Out, Split) wird eine Gruppe junger Studenten in ein übernatürliches Spiel mit dem Tod verwickelt.";
                        break;
                    case 4:
                        eventContent = "Auf der Suche nach neuer Schärfe in seinem Leben muss Wade gegen Ninjas, die Yakuza und eine Horde sexuell aggressiver Hunde kämpfen.";
                        break;
                    case 5:
                        eventContent = "Die feinsinnig verfilmte Adaption des Romans Die Buchhandlung, der britischen Schriftstellerin Penelope Fitzgerald, zelebriert die Liebe zur Literatur.";
                        break;
                    case 6:
                        eventContent = "Die auf wahren Ereignissen beruhende Geschichte zeigt wie selbst im Chaos der letzten Kriegstage etablierte Befehlsketten und Machtmechanismen funktionieren.";
                        break;
                    default:
                        eventContent = "Nouvelle-Vague-Legende Agnès Varda und Fotograf JR verbindet nicht nur ihre Leidenschaft für Bilder, sondern auch ein feines Gespür für Menschen und die Poesie des Moments.";
                        break;
                }

                // generate Events
                EventDTO eventDTO = EventDTO.newBuilder()
                    .name(faker.name().name())
                    .artist(artistMapper.artistToArtistDTO(artist))
                    .name(artist.getLastName() + " Tour")
                    .content(eventContent)
                    .length(Duration.ofHours((new Random().nextInt(10)+1)))
                    .type(i % 2==0 ? EventType.SEATS : EventType.SECTORS)
                    .build();

                Event event = (eventsMapper.eventDTOtoEvent(eventDTO));

                eventRepository.save(event);

                for(int j = 0 ; j< new Random().nextInt(5);j++) {
                    Performance performance = performancesMapper.performanceDTOToPerformance(PerformanceDTO.newBuilder()
                        .setEvent(EventDTO.newBuilder().id(event.getId()).build())
                        .setDate(LocalDateTime.now().plusDays(10 * j + 3).plusMinutes(new Random().nextInt(1300)))
                        .setHall(HallDTO.newBuilder().id(j > 5 ? hall1.getId() : hall2.getId()).build())
                        .setPrice(new BigDecimal(new Random().nextInt(100) + 1))
                        .build());
                    performanceRepository.save(performance);
                }
            }
        }
    }

    private Hall generateInsoHall(){
        Hall hall  = Hall.newBuilder()
            .setCity("Wien")
            .setCountry("Österreich")
            .setName("INSO - Saal")
            .setStreet("Wiedner Hauptstraße 76/2/2")
            .setPostcode("1040")
            .build();

        hall = hallRepository.save(hall);

        String[] plan = new String[]{
            "CCCCCCCCCCCCCCCCCCCCCCCCCCCC",
            "C     C  CCCC C      C     C",
            "CCC CCC C CCC C CCCCCC CCC C ",
            "CCC CCC CC CC C      C CCC C",
            "CCC CCC CCC C CCCCCC C CCC C",
            "C     C CCCC  C      C     C",
            "CCCCCCCCCCCCCCCCCCCCCCCCCCCC",
            "",
           /* "        AA      AA",
            "       AAAA    AAAA",
            "      AAAAAA  AAAAAA",
            "     AAAAAAAAAAAAAAAA",
            "     AAAAAAAAAAAAAAAA",
            "      AAAAAAAAAAAAAA",
            "       AAAAAAAAAAAA",
            "        AAAAAAAAAA",
            "         AAAAAAAA",
            "          AAAAAA",
            "           AAAA",
            "            AA",*/
            "",
            "DDDDDDDDDDDDDDDDDDDDDDDDDDDD",
            "D      D     D     D  DDD  D",
            "D DDDDDD DDDDD DDD D D D D D",
            "D      D     D     D DD DD D",
            "DDDDDD D DDDDD DDDDD DDDDD D",
            "D      D     D DDDDD DDDDD D",
            "DDDDDDDDDDDDDDDDDDDDDDDDDDDD",

        };
        long x = 0,y=0;
        for(String row : plan){
            for(char seat:row.toCharArray()){
                if(seat != ' '){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.valueOf(""+seat)).setHall(hall).setXCoord(x).setYCoord(y).build());
                }
                x++;
            }
            y++;
            x=0;
        }
        return hall;
    }

    private void setLayout1To(Hall hall){
        for(long x = 0; x<11;x++){
            for(long y = 0;y<11;y++){
                if(x==5||y==5){
                    continue;
                }
                if(x<5&&y<5){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.A).setHall(hall).setXCoord(x).setYCoord(y).build());
                }else if(y<5){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.B).setHall(hall).setXCoord(x).setYCoord(y).build());
                }else if(x<5){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.C).setHall(hall).setXCoord(x).setYCoord(y).build());
                }else{
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.D).setHall(hall).setXCoord(x).setYCoord(y).build());
                }
            }
        }
    }

    private void setLayout2To(Hall hall) {
        for(long x = 0; x<50;x++){
            for(long y = 0;y<50;y++){
                if(x==25||y%8==7){
                    continue;
                }
                if(y/8==0){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.A).setHall(hall).setXCoord(x).setYCoord(y).build());
                }else if(y/8==1){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.B).setHall(hall).setXCoord(x).setYCoord(y).build());
                }else if(y/8==2){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.C).setHall(hall).setXCoord(x).setYCoord(y).build());
                }else if(y/8==3){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.D).setHall(hall).setXCoord(x).setYCoord(y).build());
                }else if(y/8==4){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.E).setHall(hall).setXCoord(x).setYCoord(y).build());
                }else if(y/8==5){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.F).setHall(hall).setXCoord(x).setYCoord(y).build());
                } else if(y/8==6){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.G).setHall(hall).setXCoord(x).setYCoord(y).build());
                } else if(y/8==7){
                    seatRepository.save(Seat.newBuilder().
                        setSector(Sector.H).setHall(hall).setXCoord(x).setYCoord(y).build());
                }
            }
        }
    }
}
