package at.ac.tuwien.inso.sepm.ticketline.server.service;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Ticket;

public interface TicketService {
    /**
     * Find all tickets.
     *
     * @return A List containing all known tickets.
     */
    List<TicketDTO> findAll();

    /**
     * Find one ticket by id.
     *
     * @param id the id of the ticket to look for.
     * @return The Ticket if such a ticket exists.
     */
    Ticket findOne(@NotNull Long id);

    /**
     * Find all tickets by a customer.
     *
     * @param customerID the id of the ticket to look for.
     * @return A List of Tickets if such a ticket exists.
     */
    List<Ticket> findAllByCustomer(@NotNull Long customerID);


    /**
     * Sets the state of a given ticket to 'BOUGHT'.
     * Also removes Seatreservations which arent bought
     * EventType has to be Seats
     * @param id the id of the ticket to buy.
     * @param seats the seats to be bought (have to be reserved)
     * @return the bought Ticket
     */
    Ticket buyReservedSeats(@NotNull Long id, List<SeatDTO> seats);

    /**
     * Sets the state of a given ticket to 'BOUGHT'.
     * Also removes Seatreservation which arent bought
     * EventType has to be Sectors
     * @param id the id of the ticket to buy
     * @param seatsBySectors the seats by sectors to be bought
     * @return the bought ticket
     */
    Ticket buyReservedSectors(@NotNull Long id, @NotNull Map<String, Long> seatsBySectors);

    /**
     * Sets the state of the given ticket to 'CANCELLED'. Has to be bought first.
     *
     * @param id the id of the ticket to refund.
     */
    Ticket refundTicket(@NotNull Long id);

    /**
     * Removes the given ticket. Has to not be bought yet.
     *
     * @param id the id of the ticket to remove.
     */
    void delete(@NotNull Long id);


    /**
     * validates if the purchase is possible,
     * if so, creates a ticket + seatreservations for the ticket
     *
     * @param buy           true = set status of ticket to buyReserved, else reserved
     * @param customerId    the id of the customer
     * @param performanceId the id of the performance
     * @param seats         list of seats to buyReserved for that performance
     * @return the id of the created ticket
     */
    Long buyTicketForSeats(@NotNull boolean buy, @NotNull Long customerId, @NotNull Long performanceId, List<SeatDTO> seats);

    /**
     * validates if the purchase is possible,
     * if so, creates a ticket + seatreservations for the ticket (note that seatreservations are just created randomly for some sector,
     * such that it is possible to track free seats by sector)
     *
     * @param buy           true = set status of ticket to buyReserved, else reserved
     * @param customerId     the id of the customer
     * @param performanceId  the id of the performance
     * @param seatsBySectors map of seat-sectors to number of seats to buyReserved
     * @return the id of the created ticket
     */
    Long buyTicketsForSectors(@NotNull boolean buy, @NotNull Long customerId, @NotNull Long performanceId, Map<String, Long> seatsBySectors);

    /**
     * updates a ticket state
     * @param id the ticket to be updated
     * @param ticketState the new state of the ticket
     */
    void updateTicketState(Long id, TicketState ticketState);

    /**
     * finds the ticket of the customer for a given performance
     * @param customerId
     * @param eventID
     * @param ticketState
     * @return
     */

    List<Ticket> search(Long customerId, Long eventID, TicketState ticketState);

    /**
     * seaches a Ticket with the following id
     * @param id
     * @return
     */

    Ticket search(@NotNull Long id);

    /**
     * Refunds a reserved ticket
     * @param ticketID the ticket to be refunded
     */
    void refundReservedTicket(@NotNull Long ticketID);

    /**
     *
     * @param ticketID the id of a ticket
     * @return all seats for a ticket
     */
    List<SeatDTO> getSeats(@NotNull Long ticketID);

    /**
     *
     * @param ticketID the id of a ticket
     * @return seats by sectors for a ticket
     */
    Map<String,Long> getSeatsBySectors(@NotNull Long ticketID);

    File getPdf(@NotNull Long ticketID);
}
