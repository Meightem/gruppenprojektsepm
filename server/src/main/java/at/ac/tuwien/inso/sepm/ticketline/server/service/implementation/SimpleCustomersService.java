package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.customers.CustomersMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BadRequestException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ExtendedConstraintViolationException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.CustomerRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.dao.CustomerSearchDAO;
import at.ac.tuwien.inso.sepm.ticketline.server.service.CustomersService;
import org.apache.commons.validator.routines.IntegerValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Validated
public class SimpleCustomersService implements CustomersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCustomersService.class);
    private final CustomerRepository customerRepository;
    private final CustomersMapper customersMapper;
    private final CustomerSearchDAO customerSearchDAO;
    private final Validator validator;

    public SimpleCustomersService(CustomerRepository customerRepository, CustomersMapper customersMapper, CustomerSearchDAO customerSearchDAO, Validator validator) {
        this.customerRepository = customerRepository;
        this.customersMapper = customersMapper;
        this.customerSearchDAO = customerSearchDAO;
        this.validator = validator;
    }

    private List<CustomerDTO> filterCustomers(List<CustomerDTO> customers) {
        return customers
            .stream()
            .filter(customer -> !customer.getIsAnonymousCustomer())
            .collect(Collectors.toList());
    }

    @Override
    public CustomerDTO createCustomer(CustomerDTO customerDTO) {
        if(customerRepository.findOneById(customerDTO.getId()).isPresent()){
            LOGGER.error("Error at createCustomer: Bad Request - Customer with specified id already exists.");
            throw new BadRequestException("exception.customer.create.id");
        }
        if(customerDTO.getIsAnonymousCustomer()){
            LOGGER.error("Error at createCustomer: Bad Request - Customer to create is anonymous.");
            throw new BadRequestException("exception.customer.create.anoymous");
        }
        Customer customer = customersMapper.customerDTOToCustomer(customerDTO);
        Set<ConstraintViolation<Customer>> violations = validator.validate(customer);
        if(!violations.isEmpty()) {
            LOGGER.error("Error at createCustomer: Constraint Violation - Customer contains invalid values.");
            throw new ExtendedConstraintViolationException("exception.customer.create.constraint",violations);
        }
        return customersMapper.customerToCustomerDTO(customerRepository.save(customer));
    }

    @Override
    public List<CustomerDTO> getAll() {
        return filterCustomers(customersMapper.customerToCustomerDTO(customerRepository.findAll()));
    }

    @Override
    public CustomerDTO get(Long id) {
        return customersMapper.customerToCustomerDTO(customerRepository.findById(id).orElseThrow(() -> new NotFoundException("exception.customer.find.notfound")));
    }

    @Override
    public CustomerDTO updateCustomer(CustomerDTO customerDTO) {
        Optional<Customer> existingCustomer = customerRepository.findById(customerDTO.getId());
        if(!existingCustomer.isPresent()){
            LOGGER.error("Error at updateCustomer: Bad Request - Customer doesn't exist.");
            throw new BadRequestException("exception.customer.update.id");
        }
        if(existingCustomer.get().getIsAnonymousCustomer()){
            LOGGER.error("Error at updateCustomer: Bad Request - Update is anonymous.");
            throw new BadRequestException("exception.customer.update.isanonymous");
        }
        if(customerDTO.getIsAnonymousCustomer()!=null&&customerDTO.getIsAnonymousCustomer()){
            LOGGER.error("Error at updateCustomer: Bad Request - Update is anonymous");
            throw new BadRequestException("exception.customer.update.changetoanoymous");
        }
        Customer customer = customersMapper.customerDTOToCustomer(customerDTO);
        Set<ConstraintViolation<Customer>> violations = validator.validate(customer);
        if(!violations.isEmpty()) {
            LOGGER.error("Error at updateCustomer: Constraint Violation - New customerdata contains invalid values.");
            throw new ExtendedConstraintViolationException("exception.customer.update.constraint",violations);
        }
        return customersMapper.customerToCustomerDTO(customerRepository.save(customer));
    }

    @Override
    public List<CustomerDTO> search(String text) {

        IntegerValidator integerValidator = IntegerValidator.getInstance();

        List<Customer> result;
        if(integerValidator.isValid(text)){
            Optional<Customer> customer = customerRepository.findById(Long.valueOf(integerValidator.validate(text)));
            if(!customer.isPresent()){
                return new ArrayList<>();
            }
            result = Arrays.asList(customer.get());
        } else {
            result = customerSearchDAO.searchCustomerByFirstAndLastNamePhrase(text);
        }
        return filterCustomers(customersMapper.customerToCustomerDTO(result));
    }

    @Override
    public CustomerDTO getAnonymousCustomer() {
        return customersMapper.customerToCustomerDTO(customerRepository.findAnonymousCustomer().get());
    }
}
