package at.ac.tuwien.inso.sepm.ticketline.server.repository.dao;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventSearchDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EventSearchDAO {

    private EntityManager entityManager;
    private IndexManager indexManager;

    public EventSearchDAO(EntityManager entityManager, IndexManager indexManager) {
        this.entityManager = entityManager;
        this.indexManager = indexManager;
    }


    @Transactional
    public List<Event> searchEvent(String text){

        indexManager.initSearchIndex();

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);


        Query query = fullTextEntityManager
            .getSearchFactory()
            .buildQueryBuilder()
            .forEntity(Event.class)
            .overridesForField("name", "edgeNGram_query")
            .get()
            .simpleQueryString()
            .onField("name")
            .matching(text)
            .createQuery();

         List<Event> results = getJpaQuery(query).getResultList();
         return results;
    }

    @Transactional
    public List<Event> searchEvent(EventSearchDTO eventSearchDTO){

        indexManager.initSearchIndex();

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        EntityContext entityContext = fullTextEntityManager
            .getSearchFactory()
            .buildQueryBuilder()
            .forEntity(Event.class);

        List<Query> queries = new ArrayList<>();

        if(eventSearchDTO.getName() != null){
            queries.add(
                entityContext
                    .overridesForField("name", "edgeNGram_query")
                    .get()
                    .simpleQueryString()
                    .onField("name")
                    .matching(eventSearchDTO.getName())
                    .createQuery());
        }

        if(eventSearchDTO.getContent() != null){
            queries.add(
                entityContext
                    .get()
                    .phrase()
                    .onField("content")
                    .sentence(eventSearchDTO.getContent())
                    .createQuery()
            );
        }

        if(eventSearchDTO.getType() != null){
            queries.add(
                entityContext
                .get()
                .simpleQueryString()
                .onField("type")
                .matching(eventSearchDTO.getType().name())
                .createQuery()
            );
        }
        if(eventSearchDTO.getLength() != null){
            long hour = 3600000000000L;
            queries.add(
                entityContext
                .get()
                .range()
                .onField("length")
                .from(eventSearchDTO.getLength().minusMinutes(30))
                .to(eventSearchDTO.getLength().plusMinutes(30))
                .createQuery()
            );
        }

        List<Event> events = new ArrayList<>();
        if(queries.size() == 0){
            return events;
        }
        MustJunction booleanQuery = entityContext.get().bool().must(queries.get(0));

        for (int i = 1; i < queries.size(); i++) {
            Query query = queries.get(i);
            booleanQuery = booleanQuery.must(query);
        }

        return getJpaQuery(booleanQuery.createQuery()).getResultList();
    }

    private FullTextQuery getJpaQuery(org.apache.lucene.search.Query luceneQuery) {

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        return fullTextEntityManager.createFullTextQuery(luceneQuery, Event.class);
    }
}
