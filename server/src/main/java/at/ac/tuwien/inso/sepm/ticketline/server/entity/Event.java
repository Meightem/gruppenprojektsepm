package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Duration;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Indexed
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_event_id")
    @SequenceGenerator(name = "seq_event_id", sequenceName = "seq_event_id")
    private Long id;

    @ManyToOne(optional = false, fetch=FetchType.EAGER)
    @NotNull
    @JoinColumn(name="id_artist")
    private Artist artist;

    @Column(nullable = false)
    @NotBlank(message = "exception.event.name.notblank")
    @Size(min=1,max = 100,message = "exception.event.name.size;{min};{max}")
    @Field(analyzer = @Analyzer(definition = "edgeNgram"))
    private String name;

    @Column(nullable=false)
    @NotBlank(message = "exception.event.content.notblank")
    @Size(min=1,max=300,message = "exception.event.content.size;{min};{max}")
    @Field
    private String content;

    @Column(nullable=false)
    @NotNull(message = "exception.event.duration")
    @Field
    private Duration length;

    @Column(nullable=false)
    @NotNull(message = "exception.event.type")
    @Field
    @Enumerated(EnumType.ORDINAL)
    private EventType type;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="event", cascade = CascadeType.ALL)
    private Set<Performance> performances = new HashSet<>();

    private Event(Builder builder) {
        setId(builder.id);
        artist = builder.artist;
        setName(builder.name);
        setContent(builder.content);
        setLength(builder.length);
        setType(builder.type);
        setPerformances(builder.performances);
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Duration getLength() {
        return length;
    }

    public void setLength(Duration length) {
        this.length = length;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    @Transactional
    public Set<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(Set<Performance> performances) {
        this.performances = performances;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(id, event.id) &&
            Objects.equals(artist, event.artist) &&
            Objects.equals(name, event.name) &&
            Objects.equals(content, event.content) &&
            Objects.equals(length, event.length) &&
            type == event.type;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, artist, name, content, length, type, performances);
    }

    @Override
    public String toString() {
        return "Event{" +
            "id=" + id +
            ", artist=" + artist +
            ", name='" + name + '\'' +
            ", content='" + content + '\'' +
            ", length=" + length +
            ", type=" + type +
            ", performances=" + performances +
            '}';
    }

    public Event(Long id, @NotNull @NotBlank Artist artist, @NotNull @NotBlank @Size(max = 100) String name, @NotNull @NotBlank @Size(max = 100) String content, @NotNull Duration length, @NotNull EventType type, Set<Performance> performances) {
        this.id = id;
        this.artist = artist;
        this.name = name;
        this.content = content;
        this.length = length;
        this.type = type;
        this.performances = performances;
    }

    public Event() {
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public static final class Builder {
        private Long id;
        private @NotNull @NotBlank Artist artist;
        private @NotNull @NotBlank @Size(max = 100) String name;
        private @NotNull @NotBlank @Size(max = 100) String content;
        private @NotNull Duration length;
        private @NotNull EventType type;
        private Set<Performance> performances;

        private Builder() {
        }

        public Builder setId(Long val) {
            id = val;
            return this;
        }

        public Builder artist(@NotNull @NotBlank Artist val) {
            artist = val;
            return this;
        }

        public Builder name(@NotNull @NotBlank @Size(max = 100) String val) {
            name = val;
            return this;
        }

        public Builder content(@NotNull @NotBlank @Size(max = 100) String val) {
            content = val;
            return this;
        }

        public Builder length(@NotNull Duration val) {
            length = val;
            return this;
        }

        public Builder type(@NotNull EventType val) {
            type = val;
            return this;
        }

        public Builder performances(Set<Performance> val) {
            performances = val;
            return this;
        }

        public Event build() {
            return new Event(this);
        }
    }
}
