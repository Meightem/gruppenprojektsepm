package at.ac.tuwien.inso.sepm.ticketline.server.repository.dao;

import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Performance;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.EntityContext;
import org.hibernate.search.query.dsl.MustJunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PerformanceSearchDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceSearchDAO.class);

    private EntityManager entityManager;
    private IndexManager indexManager;

    public PerformanceSearchDAO(EntityManager entityManager, IndexManager indexManager) {
        this.entityManager = entityManager;
        this.indexManager = indexManager;
    }

    @Transactional
    public List<Performance> search(PerformanceDTO performanceSearchDTO){

        indexManager.initSearchIndex();

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        EntityContext entityContext = fullTextEntityManager
            .getSearchFactory()
            .buildQueryBuilder()
            .forEntity(Performance.class);

        List<Query> queries = new ArrayList<>();


        if(performanceSearchDTO.getEvent() != null){
            queries.add(
                entityContext
                    .get()
                    .keyword()
                    .onField("event.id")
                    .ignoreFieldBridge()
                    .matching(String.valueOf(performanceSearchDTO.getEvent().getId()))
                    .createQuery()
            );
        }

        if(performanceSearchDTO.getHall()!= null){
            queries.add(
                entityContext
                    .get()
                    .keyword()
                    .onField("hall.id")
                    .ignoreFieldBridge()
                    .matching(String.valueOf(performanceSearchDTO.getHall().getId()))
                    .createQuery()
            );
        }

        if(performanceSearchDTO.getDate() != null){ ;
            queries.add(
                entityContext
                    .get()
                    .range()
                    .onField("date")
                    .from(performanceSearchDTO.getDate().minusHours(1))
                    .to(performanceSearchDTO.getDate().plusHours(1))
                    .createQuery()
            );
        }


        if(performanceSearchDTO.getPrice() != null){
            BigDecimal range = performanceSearchDTO.getPrice().divide(BigDecimal.TEN);
            long from = BigDecimalNumericFieldBridge.toIndexFormat(performanceSearchDTO.getPrice().subtract(range));
            long to = BigDecimalNumericFieldBridge.toIndexFormat(performanceSearchDTO.getPrice().add(range));
            LOGGER.debug("searching from {} to {}", from, to);
            queries.add(
                entityContext
                    .get()
                    .range()
                    .onField("price")
                    .from(from)
                    .to(to)
                    .createQuery()
            );
        }

        List<Performance> performances = new ArrayList<>();
        if(queries.size() == 0){
            return performances;
        }
        MustJunction booleanQuery = entityContext.get().bool().must(queries.get(0));

        for (int i = 1; i < queries.size(); i++) {
            Query query = queries.get(i);
            booleanQuery = booleanQuery.must(query);
        }

        return getJpaQuery(booleanQuery.createQuery()).getResultList();
    }

    private FullTextQuery getJpaQuery(org.apache.lucene.search.Query luceneQuery) {

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        return fullTextEntityManager.createFullTextQuery(luceneQuery, Performance.class);
    }
}
