package at.ac.tuwien.inso.sepm.ticketline.server.entity.keys;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SeatReservationPK implements Serializable {
    private Long id_ticket;

    private Long id_performance;

    private SeatPK id_seat;

    public Long getId_ticket() {
        return id_ticket;
    }

    public void setId_ticket(Long id_ticket) {
        this.id_ticket = id_ticket;
    }

    public Long getId_performance() {
        return id_performance;
    }

    public void setId_performance(Long id_performance) {
        this.id_performance = id_performance;
    }

    public SeatPK getId_seat() {
        return id_seat;
    }

    public void setId_seat(SeatPK id_seat) {
        this.id_seat = id_seat;
    }

    public SeatReservationPK(Long id_ticket, Long id_performance, SeatPK id_seat) {
        this.id_ticket = id_ticket;
        this.id_performance = id_performance;
        this.id_seat = id_seat;
    }

    public SeatReservationPK() {
    }


    @Override
    public String toString() {
        return "SeatReservationPK{" +
            "id_ticket=" + id_ticket +
            ", id_performance=" + id_performance +
            ", id_seat=" + id_seat +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SeatReservationPK that = (SeatReservationPK) o;
        return Objects.equals(id_ticket, that.id_ticket) &&
            Objects.equals(id_performance, that.id_performance) &&
            Objects.equals(id_seat, that.id_seat);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id_ticket, id_performance, id_seat);
    }
}
