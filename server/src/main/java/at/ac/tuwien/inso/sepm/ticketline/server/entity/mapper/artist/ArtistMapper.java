package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.artist;

import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Artist;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ArtistMapper {
    ArtistDTO artistToArtistDTO(Artist artist);
    Artist artistDTOToArtist(ArtistDTO artist);
    List<ArtistDTO> artistToArtistDTO(List<Artist> artist);
    List<Artist> artistDTOToArtist(List<ArtistDTO> artist);
}
