package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.endpoint.base.EndpointBase;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.news.NewsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.users.UsersMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.UserRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.security.AuthenticationConstants;
import at.ac.tuwien.inso.sepm.ticketline.server.service.HeaderTokenAuthenticationService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.NewsService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.UsersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

import org.springframework.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/news")
@Api(value = "news")
public class NewsEndpoint extends EndpointBase {

    @Autowired
    private final NewsService newsService;
    @Autowired
    private final UsersService usersService;

    @Autowired
    private final NewsMapper newsMapper;

    @Autowired
    private final UsersMapper usersMapper;

    @Autowired
    private final HeaderTokenAuthenticationService authenticationService;

    public NewsEndpoint(NewsService newsService, NewsMapper newsMapper, UsersMapper usersMapper, HeaderTokenAuthenticationService authenticationService, UsersService usersService) {
        this.newsService = newsService;
        this.newsMapper = newsMapper;
        this.authenticationService = authenticationService;
        this.usersService = usersService;
        this.usersMapper = usersMapper;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Get list of simple news entries")
    public List<SimpleNewsDTO> findAll(@ApiIgnore @RequestHeader(value = HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        return newsMapper.newsToSimpleNewsDTO(newsService.findAll());
    }

    @RequestMapping(value = "/unread", method = RequestMethod.GET)
    @ApiOperation(value = "Get list of unread news")
    public List<SimpleNewsDTO> findUnreadNews(@ApiIgnore @RequestHeader(value = HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        String authHead = authorizationHeader.substring(AuthenticationConstants.TOKEN_PREFIX.length()).trim();
        AuthenticationTokenInfo authTokenInfo = authenticationService.authenticationTokenInfo(authHead);
        return usersService.getUnreadNews(authTokenInfo.getUsername());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Get detailed information about a specific news entry")
    public DetailedNewsDTO find(@PathVariable Long id, @ApiIgnore @RequestHeader(value = HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        DetailedNewsDTO dto = newsMapper.newsToDetailedNewsDTO(newsService.findOne(id));
        return dto;
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Publish a new news entry")
    public DetailedNewsDTO publishNews(@RequestBody DetailedNewsDTO detailedNewsDTO) {
        News news = newsMapper.detailedNewsDTOToNews(detailedNewsDTO);
        news = newsService.publishNews(news);
        return newsMapper.newsToDetailedNewsDTO(news);
    }

    @RequestMapping(value = "/{id}/read", method = RequestMethod.POST)
    @ApiOperation(value = "Mark the given news as read for the given user.")
    public DetailedNewsDTO markAsRead(@PathVariable Long id, @ApiIgnore @RequestHeader(value = HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        User user = usersService.getByUsername(authenticationService.authenticationTokenInfo(authorizationHeader.substring(AuthenticationConstants.TOKEN_PREFIX.length()).trim()).getUsername());
        News news = newsService.findOne(id);
        news = newsService.markNewsAsRead(news, user);
        DetailedNewsDTO dto = newsMapper.newsToDetailedNewsDTO(news);
        return dto;
    }
}
