package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.news.NewsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.users.UsersMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BadRequestException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ExtendedConstraintViolationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.UserRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.NewsService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

@Service
public class SimpleUsersService implements UsersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleUsersService.class);
    private final UserRepository userRepository;
    private final UsersMapper usersMapper;
    private final NewsMapper newsMapper;
    private final PasswordEncoder passwordEncoder;
    private final Validator validator;
    private final NewsService newsService;

    public SimpleUsersService(UserRepository userRepository, UsersMapper usersMapper, PasswordEncoder passwordEncoder, Validator validator, NewsMapper newsMapper, NewsService newsService) {
        this.userRepository = userRepository;
        this.usersMapper = usersMapper;
        this.newsMapper = newsMapper;
        this.passwordEncoder = passwordEncoder;
        this.validator = validator;
        this.newsService = newsService;
    }

    private User saveUser(User user) {
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        if(!violations.isEmpty()) {
            LOGGER.error("Error at saveUser: Constraint Violation - User contains invalid values.");
            throw new ExtendedConstraintViolationException("exception.user.create.constraint",violations);
        }
        return userRepository.save(user);
    }

    @Override
    public UserDTO createUser(UserDTO userDTO) {
        if(userRepository.findOneByUsername(userDTO.getUsername()).isPresent()) {
            LOGGER.error("Error at createUser: Bad Request - There is already a user with this username.");
            throw new BadRequestException("exception.user.create.taken");
        }
        if(userDTO.getPassword()==null||userDTO.getPassword().length()<1||userDTO.getPassword().length()>100) {
            LOGGER.error("Error at createUser: Bad Request - Password has to be between specific characters.");
            throw new BadRequestException("exception.user.create.passwordsize;1;100");
        }

        User user = usersMapper.userDTOToUser(userDTO);
        if(userDTO.getPassword() != null) {
            user.setPasswordHash(passwordEncoder.encode(userDTO.getPassword()));
        }
        return usersMapper.userToUserDTO(saveUser(user));
    }

    @Override
    public List<UserDTO> getAll() {
        return usersMapper.userToUserDTO(userRepository.findAll());
    }

    @Override
    public UserDTO updateUser(UserDTO userDTO, AuthenticationTokenInfo authenticationTokenInfo) {
        if(userDTO.getId() == null) {
            LOGGER.error("Error at updateUser: Bad Request - Userid has to be set.");
            throw new BadRequestException("exception.user.update.id");
        }
        if(!userRepository.findById(userDTO.getId()).isPresent()){
            LOGGER.error("Error at updateUser: Bad Request - There is no existing user with this id.");
            throw new BadRequestException("exception.user.update.notfound");
        }

        User user = userRepository.getOne(userDTO.getId());

        if((userDTO.getLocked() != null) && (userDTO.getLocked() != user.getLocked()) && (user.getUsername().equals(authenticationTokenInfo.getUsername()))){
            LOGGER.error("Error at updateUser: Bad Request - You cannot lock yourself out.");
            throw new BadRequestException("exception.user.update.lock");
        }
        if(user.getAdmin() == true && userRepository.countByIsAdminAndIsLocked(true, false) <= 1){
            if(userDTO.getLocked() != null && userDTO.getLocked() == true && user.getLocked() == false){
                LOGGER.error("Error at updateUser: Bad Request - You cannnot lock all admins.");
                throw new BadRequestException("exception.user.update.lockall");
            }
            if(userDTO.getAdmin() != null && userDTO.getAdmin() == false){
                LOGGER.error("Error at updateUser: Bad Request - You cannot change all admins to users.");
                throw new BadRequestException("exception.user.update.removeadmin");
            }
        }
        if(userDTO.getUsername() != null) {
            user.setUsername(userDTO.getUsername());
        }
        if(userDTO.getPassword() != null) {
            user.setPasswordHash(passwordEncoder.encode(userDTO.getPassword()));
        }
        if(userDTO.getAdmin() != null) {
            user.setAdmin(userDTO.getAdmin());
        }
        if(userDTO.getFailedAttempts() != null) {
            user.setFailedAttempts(userDTO.getFailedAttempts());
        }
        if(userDTO.getLocked() != null) {
            user.setLocked(userDTO.getLocked());
        }
        return usersMapper.userToUserDTO(saveUser(user));
    }

    @Override
    public User getByUsername(String name) {
        return userRepository.findByUsername(name);
    }

    @Override
    public List<SimpleNewsDTO> getUnreadNews(String username) {
        User user = getByUsername(username);
        return newsMapper.newsToSimpleNewsDTO(newsService.userUnreadNews(user));
    }
}
