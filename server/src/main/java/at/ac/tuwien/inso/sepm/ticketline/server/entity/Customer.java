package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.WhitespaceTokenizerFactory;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilterFactory;
import org.apache.lucene.analysis.ngram.EdgeNGramFilterFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


@Entity
@Indexed
@AnalyzerDef(name = "edgeNgram",
    tokenizer = @TokenizerDef(factory = WhitespaceTokenizerFactory.class),
    filters = {
        @TokenFilterDef(factory = ASCIIFoldingFilterFactory.class), // Replace accented characeters by their simpler counterpart (è => e, etc.)
        @TokenFilterDef(factory = LowerCaseFilterFactory.class), // Lowercase all characters
        @TokenFilterDef(factory = EdgeNGramFilterFactory.class,
            params = {
                @Parameter(name = "minGramSize", value = "1"),
                @Parameter(name = "maxGramSize", value = "100")
            }
        ) // Generate prefix tokens)
    })
@AnalyzerDef(name = "edgeNGram_query",
    tokenizer = @TokenizerDef(factory = WhitespaceTokenizerFactory.class),
    filters = {
        @TokenFilterDef(factory = ASCIIFoldingFilterFactory.class), // Replace accented characeters by their simpler counterpart (è => e, etc.)
        @TokenFilterDef(factory = LowerCaseFilterFactory.class) // Lowercase all characters
    })
public class Customer {
    public static String ANONYMOUS_FIRST_NAME = "Anonymous";
    public static String ANONYMOUS_LAST_NAME = "Anonymous";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_customer_id")
    @SequenceGenerator(name = "seq_customer_id", sequenceName = "seq_customer_id")
    private Long id;

    @Column(nullable = false)
    @Size(min = 1, max = 100, message = "exception.customer.firstname.size;{min};{max}")
    @NotBlank(message = "exception.customer.firstname.notblank")
    @Field(analyzer = @Analyzer(definition = "edgeNgram"))
    private String firstName;

    @Column(nullable = false)
    @Size(min = 1, max = 100, message = "exception.customer.lastname.size;{min};{max}")
    @NotBlank(message = "exception.customer.lastname.notblank")
    @Field(analyzer = @Analyzer(definition = "edgeNgram"))
    private String lastName;

    @Column(nullable = false)
    private Boolean isAnonymousCustomer;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="customer", cascade = CascadeType.ALL)
    private Set<Ticket> tickets = new HashSet<>();

    private Customer(Builder builder) {
        setId(builder.id);
        setFirstName(builder.firstName);
        setLastName(builder.lastName);
        setTickets(builder.tickets);
        setIsAnonymousCustomer(builder.isAnonymousCustomer);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public String toString() {
        return "Customer{" +
            "id=" + id +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(id, customer.id) &&
            Objects.equals(firstName, customer.firstName) &&
            Objects.equals(lastName, customer.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName);
    }

    public Customer() {
    }

    public Boolean getIsAnonymousCustomer() {
        return isAnonymousCustomer;
    }

    public void setIsAnonymousCustomer(Boolean anonymousCustomer) {
        isAnonymousCustomer = anonymousCustomer;
    }


    public static final class Builder {
        private Long id;
        private @Size(max = 100) @NotNull @NotBlank String firstName;
        private @Size(max = 100) @NotNull @NotBlank String lastName;
        private Set<Ticket> tickets;
        private Boolean isAnonymousCustomer;

        private Builder() {
            isAnonymousCustomer = false;
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder firstName(@Size(max = 100) @NotNull @NotBlank String val) {
            firstName = val;
            return this;
        }

        public Builder lastName(@Size(max = 100) @NotNull @NotBlank String val) {
            lastName = val;
            return this;
        }

        public Builder tickets(Set<Ticket> val) {
            tickets = val;
            return this;
        }

        public Builder isAnonymousCustomer(boolean val) {
            isAnonymousCustomer = val;
            return this;
        }

        public Customer build() {
            return new Customer(this);
        }
    }
}