package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventSearchDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.queryResults.TopEvent;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BadRequestException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface EventService {
    /**
     * Find all event.
     *
     * @return list of all event
     */
    List<Event> findAll();

    /**
     * Find a single event by id.
     *
     * @param id of the event
     * @throws BadRequestException when no event could be found with this id
     * @return the event
     */
    Event findOne(@NotNull Long id);

    /**
     * Create a new event
     *
     * @param event to created
     * @return the created event
     */
    Event createEvent(Event event);



    /**
     * Get the topevents
     *
     * @param eventType type to filter
     * @param month the month to filter
     * @param year the year to filter
     * @return the  topEvents
     */
    List<TopEvent> getTopTenEvents(EventType eventType, int month, int year);


    /**
     * get a list of events from specifiic artist
     *
     * @param artistID if of artist
     * @return the evenst of te specified artist
     */
    List<Event> findEventsOfArtist(long artistID);


    /**
     * get a list of events from specifiic search
     *
     * @param eventSearchDTO the specidied search terms
     * @return the events
     */
    List<Event> search(EventSearchDTO eventSearchDTO);

    /**
     * Searches an event for its name
     * @param text the searchtext
     * @return
     */
    List<Event> search(String text);
}
