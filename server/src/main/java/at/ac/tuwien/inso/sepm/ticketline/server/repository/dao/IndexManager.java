package at.ac.tuwien.inso.sepm.ticketline.server.repository.dao;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;

@Component
public class IndexManager {
    private EntityManager entityManager;
    private boolean initSearch = false;

    public IndexManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void initSearchIndex() {
        if(initSearch){
            return;
        }
        FullTextEntityManager fullTextEntityManager
            = Search.getFullTextEntityManager(entityManager);
        try {
            fullTextEntityManager.createIndexer()
                .purgeAllOnStart(true) // required for system tests
                .optimizeAfterPurge(true)
                .optimizeOnFinish(true)
                .batchSizeToLoadObjects( 25 )
                .threadsToLoadObjects( 5 )
                .threadsForSubsequentFetching( 20 )
                .startAndWait();
            initSearch = true;
        } catch (InterruptedException e) {
            e.printStackTrace();
            initSearch = false;
        }
    }
}
