package at.ac.tuwien.inso.sepm.ticketline.server.repository.dao;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.hibernate.search.bridge.LuceneOptions;
import org.hibernate.search.bridge.MetadataProvidingFieldBridge;
import org.hibernate.search.bridge.TwoWayFieldBridge;
import org.hibernate.search.bridge.spi.FieldMetadataBuilder;
import org.hibernate.search.bridge.spi.FieldType;

import java.math.BigDecimal;

public class BigDecimalNumericFieldBridge implements MetadataProvidingFieldBridge, TwoWayFieldBridge {
    /**
     * The default bridge just converts to string and has by-character sort order ('13.8' > '106.6')
     * We need to use NumericBridge, but that one doesn't work out of the box for bigdecimal
     * <p>
     * See https://hibernate.atlassian.net/browse/HSEARCH-678
     */

    private static final BigDecimal storeFactor = BigDecimal.valueOf(100);

    public static long toIndexFormat(BigDecimal value) {
        return value.multiply(storeFactor).longValue();
    }

    @Override
    public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
        if (value == null) {
            if (luceneOptions.indexNullAs() != null) {
                luceneOptions.addFieldToDocument(name, luceneOptions.indexNullAs(), document);
            }
        } else {
            BigDecimal decimalValue = (BigDecimal) value;
            long indexedValue = toIndexFormat(decimalValue);
            luceneOptions.addNumericFieldToDocument(name, indexedValue, document);
        }
    }

    @Override
    public Object get(String name, Document document) {
        final IndexableField field = document.getField(name);
        if (field != null) {
            Number numericValue = field.numericValue();
            BigDecimal bigValue = new BigDecimal(numericValue.longValue());
            return bigValue.divide(storeFactor);
        } else {
            return null;
        }
    }

    @Override
    public final String objectToString(final Object object) {
        return object == null ? null : String.valueOf(object);
    }

    @Override
    public void configureFieldMetadata(String name, FieldMetadataBuilder builder) {
        builder.field(name, FieldType.LONG);
    }
}