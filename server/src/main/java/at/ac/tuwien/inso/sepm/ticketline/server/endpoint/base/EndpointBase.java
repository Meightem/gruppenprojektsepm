package at.ac.tuwien.inso.sepm.ticketline.server.endpoint.base;

import at.ac.tuwien.inso.sepm.ticketline.rest.exception.ExceptionDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BaseExpection;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ExtendedConstraintViolationException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;

public class EndpointBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(EndpointBase.class);

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionDTO validationError(ConstraintViolationException ex) {
        return ExceptionDTO.newBuilder()
            .bundleKey("exception.unknown.constraintviolation")
            .constraintViolations(ExtendedConstraintViolationException.violationToStringSet(ex.getConstraintViolations()))
            .build();
    }

    @ExceptionHandler(ExtendedConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionDTO validationError(ExtendedConstraintViolationException ex) {
        return ExceptionDTO.newBuilder()
            .bundleKey(ex.getBundleKey())
            .constraintViolations(ex.getViolations())
            .build();
    }

    @ExceptionHandler(LockedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ExceptionDTO validationError(LockedException ex) {
        return ExceptionDTO.newBuilder()
            .bundleKey("exception.authentication.locked")
            .details("exception.authentication.locked.hint")
            .build();
    }

    @ExceptionHandler({BadCredentialsException.class, InternalAuthenticationServiceException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ExceptionDTO validationError(AuthenticationException ex) {
        return ExceptionDTO.newBuilder()
            .bundleKey("exception.authentication.wrongcredentials")
            .details("exception.hint.retry")
            .build();
    }


    @ExceptionHandler(CredentialsExpiredException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ExceptionDTO validationError(CredentialsExpiredException ex) {
        return ExceptionDTO.newBuilder()
            .bundleKey("exception.authentication.renewtoken")
            .details("exception.authentication.renewtoken.hint")
            .build();
    }


    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ExceptionDTO validationError(NotFoundException ex) {
        return ExceptionDTO.newBuilder()
            .bundleKey(ex.getBundleKey())
            .details(ex.getDetails())
            .build();
    }

    @ExceptionHandler(BaseExpection.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionDTO validationError(BaseExpection ex) {
        return ExceptionDTO.newBuilder()
            .bundleKey(ex.getBundleKey())
            .details(ex.getDetails())
            .build();
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ExceptionDTO validationError(AccessDeniedException ex) {
        LOGGER.debug(ex.getClass().getName() + "\n" + ex.getMessage());
        return ExceptionDTO.newBuilder()
            .bundleKey("exception.forbidden")
            .build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ExceptionDTO validationError(Exception ex) {
        LOGGER.debug("Unknown Exception occurred:"+ex.getClass().getName() + "\nMessage:\n" + ex.getMessage());
        return ExceptionDTO.newBuilder()
            .bundleKey("exception.internal.server")
            .details("exception.internal.server.hint")
            .build();
    }
}
