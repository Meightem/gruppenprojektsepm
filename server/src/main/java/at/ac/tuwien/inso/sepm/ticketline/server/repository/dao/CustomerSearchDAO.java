package at.ac.tuwien.inso.sepm.ticketline.server.repository.dao;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer.ANONYMOUS_FIRST_NAME;
import static at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer.ANONYMOUS_LAST_NAME;

@Repository
public class CustomerSearchDAO {

    private EntityManager entityManager;
    private IndexManager indexManager;

    public CustomerSearchDAO(EntityManager entityManager, IndexManager indexManager) {
        this.entityManager = entityManager;
        this.indexManager = indexManager;
    }


    @Transactional
    public List<Customer> searchCustomerByFirstAndLastNamePhrase(String text){

        indexManager.initSearchIndex();

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        Query query = fullTextEntityManager
            .getSearchFactory()
            .buildQueryBuilder()
            .forEntity(Customer.class)
            .overridesForField("firstName", "edgeNGram_query")
            .overridesForField("lastName","edgeNGram_query")
            .get()
            .simpleQueryString()
            .onFields("firstName","lastName")
            .withAndAsDefaultOperator()
            .matching(text)
            .createQuery();

        List<Customer> results = getJpaQuery(query).getResultList();

        return results;
    }

    private FullTextQuery getJpaQuery(org.apache.lucene.search.Query luceneQuery) {

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        return fullTextEntityManager.createFullTextQuery(luceneQuery, Customer.class);
    }


}
