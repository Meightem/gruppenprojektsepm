package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.server.datagenerator.ProductionDataGenerator;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/testhelper")
@Api(value = "testhelper")
@Profile({"testing"})
public class TestHelperEndpoint {
    private UserRepository userRepository;
    private NewsRepository newsRepository;
    private CustomerRepository customerRepository;
    private TicketRepository ticketRepository;
    private EventRepository eventRepository;
    private PerformanceRepository performanceRepository;
    private HallRepository hallRepository;
    private SeatRepository seatRepository;
    private ProductionDataGenerator productionDataGenerator;

    public TestHelperEndpoint(UserRepository userRepository, NewsRepository newsRepository, CustomerRepository customerRepository, TicketRepository ticketRepository, EventRepository eventRepository, PerformanceRepository performanceRepository, HallRepository hallRepository, SeatRepository seatRepository, ProductionDataGenerator productionDataGenerator) {
        this.userRepository = userRepository;
        this.newsRepository = newsRepository;
        this.customerRepository = customerRepository;
        this.ticketRepository = ticketRepository;
        this.eventRepository = eventRepository;
        this.performanceRepository = performanceRepository;
        this.hallRepository = hallRepository;
        this.seatRepository = seatRepository;
        this.productionDataGenerator = productionDataGenerator;
    }

    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    @ApiOperation(value = "Remove all entites from database, create test users")
    public void reset() {
        ticketRepository.deleteAll();
        performanceRepository.deleteAll();
        eventRepository.deleteAll();
        newsRepository.deleteAll();
        userRepository.deleteAll();
        customerRepository.deleteAll();
        productionDataGenerator.generateProductionData();

        seatRepository.deleteAllInBatch();
        hallRepository.deleteAllInBatch();
        Hall hall = hallRepository.save(Hall.newBuilder()
            .setName("Ernst-Happel-Stadion")
            .setCity("Wien")
            .setCountry("Austria")
            .setStreet("Kärntner Straße")
            .setPostcode("4020")
            .build());

        seatRepository.save(Seat.newBuilder().
            setSector(Sector.A).setHall(hall).setXCoord(0L).setYCoord(1L).build());

        seatRepository.save(Seat.newBuilder().
            setSector(Sector.B).setHall(hall).setXCoord(0L).setYCoord(2L).build());

        seatRepository.save(Seat.newBuilder().
            setSector(Sector.C).setHall(hall).setXCoord(1L).setYCoord(1L).build());

        seatRepository.save(Seat.newBuilder().
            setSector(Sector.D).setHall(hall).setXCoord(1L).setYCoord(2L).build());
    }
}
