package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.CustomerRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer.ANONYMOUS_FIRST_NAME;
import static at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer.ANONYMOUS_LAST_NAME;

@Component
public class ProductionDataGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductionDataGenerator.class);
    private final UserRepository userRepository;
    private final CustomerRepository customerRepository;
    private final PasswordEncoder passwordEncoder;

    public ProductionDataGenerator(UserRepository userRepository, CustomerRepository customerRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.customerRepository = customerRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void generateProductionData() {
        if(!customerRepository.findAnonymousCustomer().isPresent()) {
            customerRepository.save(Customer.newBuilder()
                .firstName(ANONYMOUS_FIRST_NAME)
                .lastName(ANONYMOUS_LAST_NAME)
                .isAnonymousCustomer(true)
                .build());
        }

        if (userRepository.count() > 0) {
            LOGGER.info("users already generated");
        } else {
            LOGGER.info("generating {} use entries", 2);
            User admin = User.newBuilder().isAdmin(true).failedAttempts(0).isLocked(false).username("admin").passwordHash(passwordEncoder.encode("password")).build();
            User user = User.newBuilder().isAdmin(false).failedAttempts(0).isLocked(false).username("user").passwordHash(passwordEncoder.encode("password")).build();
            userRepository.save(admin);
            userRepository.save(user);
        }
    }
}
