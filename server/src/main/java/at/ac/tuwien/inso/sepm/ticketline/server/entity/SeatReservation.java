package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.keys.SeatReservationPK;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class SeatReservation {
    @EmbeddedId
    private SeatReservationPK id = new SeatReservationPK();

    @MapsId("id_ticket")
    @ManyToOne(fetch=FetchType.EAGER, optional = false)
    @JoinColumns({
        @JoinColumn(name = "id_ticket", referencedColumnName = "id", insertable = false, updatable = false, nullable = false)
    })
    private Ticket ticket;

    @Column(name = "state")
    @Enumerated(EnumType.ORDINAL)
    private TicketState state;


    @ManyToOne(fetch=FetchType.EAGER, optional = false)
    @JoinColumn(name = "id_performance", insertable = false, updatable = false)
    private Performance performance;

    @MapsId("id_seat")
    @ManyToOne(fetch=FetchType.EAGER, optional = false)
    @JoinColumns({
        @JoinColumn(name = "xCoord"),
        @JoinColumn(name = "yCoord"),
        @JoinColumn(name = "id_hall")
    })
    private Seat seat;

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.id.setId_ticket(ticket.getId());
        this.ticket = ticket;
        this.state = ticket.getState();
    }

    public Performance getPerformance() {
        return performance;
    }

    public void setPerformance(Performance performance) {
        this.performance = performance;
        this.id.setId_performance(performance.getId());
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public SeatReservationPK getId() {
        return id;
    }

    public TicketState getState() {
        return state;
    }

    public void setState(TicketState state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SeatReservation that = (SeatReservation) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "SeatReservation{" +
            "id=" + id +
            '}';
    }

    public SeatReservation() {
    }

    public SeatReservation(SeatReservationPK id, Ticket ticket, Performance performance, Seat seat, TicketState state) {
        this.id = id;
        this.ticket = ticket;
        this.performance = performance;
        this.seat = seat;
        this.state = state;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private SeatReservationPK id = new SeatReservationPK();
        private Ticket ticket;
        private Performance performance;
        private Seat seat;
        private TicketState state;

        public Builder setTicket(Ticket ticket) {
            this.ticket = ticket;
            this.id.setId_ticket(ticket.getId());
            this.state = ticket.getState();
            return this;
        }

        public Builder setPerformance(Performance performance) {
            this.performance = performance;
            this.id.setId_performance(performance.getId());
            return this;
        }

        public Builder setSeat(Seat seat) {
            this.seat = seat;
            this.id.setId_seat(seat.getId());
            return this;
        }

        public SeatReservation build() {
            return new SeatReservation(id, ticket, performance, seat, state);
        }
    }
}
