package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Artist;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.ArtistRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.dao.ArtistSearchDAO;
import at.ac.tuwien.inso.sepm.ticketline.server.service.ArtistService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleArtistService implements ArtistService {

    private final ArtistSearchDAO artistSearchDAO;
    private final ArtistRepository artistRepository;

    public SimpleArtistService(ArtistSearchDAO artistSearchDAO, ArtistRepository artistRepository) {
        this.artistSearchDAO = artistSearchDAO;
        this.artistRepository = artistRepository;
    }

    @Override
    public List<Artist> search(String text) {
        return artistSearchDAO.searchArtistByFirstAndLastNamePhrase(text);
    }

    @Override
    public List<Artist> findAll() {
        return artistRepository.findAll();
    }
}
