package at.ac.tuwien.inso.sepm.ticketline.server.repository.dao;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Artist;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ArtistSearchDAO {

    private EntityManager entityManager;
    private IndexManager indexManager;

    public ArtistSearchDAO(EntityManager entityManager, IndexManager indexManager) {
        this.entityManager = entityManager;
        this.indexManager = indexManager;
    }

    @Transactional
    public List<Artist> searchArtistByFirstAndLastNamePhrase(String text){

        indexManager.initSearchIndex();

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        Query query = fullTextEntityManager
            .getSearchFactory()
            .buildQueryBuilder()
            .forEntity(Artist.class)
            .overridesForField("firstName", "edgeNGram_query")
            .overridesForField("lastName","edgeNGram_query")
            .get()
            .simpleQueryString()
            .onFields("firstName","lastName")
            .withAndAsDefaultOperator()
            .matching(text)
            .createQuery();

        List<Artist> results = getJpaQuery(query).getResultList();

        return results;
    }

    private FullTextQuery getJpaQuery(org.apache.lucene.search.Query luceneQuery) {

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        return fullTextEntityManager.createFullTextQuery(luceneQuery, Artist.class);
    }
}
