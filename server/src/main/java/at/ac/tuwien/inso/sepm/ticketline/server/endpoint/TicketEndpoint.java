package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;


import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketSearchDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.endpoint.base.EndpointBase;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Ticket;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.tickets.TicketsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.service.TicketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/tickets")
@Api(value = "tickets")
public class TicketEndpoint extends EndpointBase {

    @Autowired
    private final TicketService ticketService;

    @Autowired
    private final TicketsMapper ticketsMapper;

    @Autowired
    public TicketEndpoint(TicketService ticketService, TicketsMapper ticketsMapper) {
        this.ticketService = ticketService;
        this.ticketsMapper = ticketsMapper;
    }

    @RequestMapping(value = "/reserveseats/{customerId}/{performanceId}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Mark ticket as reserved for performance with seats available, REQUIRES EventType.SEATS")
    public Long reserveTicketForSeats(@RequestBody List<SeatDTO> seats, @PathVariable("customerId") Long customerId, @PathVariable("performanceId") Long performanceId) {
        return ticketService.buyTicketForSeats(false, customerId, performanceId, seats);

    }

    @RequestMapping(value = "/buyseats/{customerId}/{performanceId}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Buys a ticket for some seats of a performance (EventType.SEATS required)")
    public Long buyTicketForSeats(@RequestBody List<SeatDTO> seats, @PathVariable("customerId") Long customerId, @PathVariable("performanceId") Long performanceId) {
        return ticketService.buyTicketForSeats(true, customerId, performanceId, seats);
    }

    @RequestMapping(value = "/reservesectors/{customerId}/{performanceId}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Mark ticket as reserved for performance with sectors available, REQUIRES EventType.SECTORS")
    public Long reserveTicketForSectors(@RequestBody Map<String, Long> seatsBySectors, @PathVariable("customerId") Long customerId, @PathVariable("performanceId") Long performanceId) {
        return ticketService.buyTicketsForSectors(false, customerId, performanceId, seatsBySectors);
    }

    @RequestMapping(value = "/buysectors/{customerId}/{performanceId}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Buys a ticket for some number of seats by sector of a performance (EventType.SECTORS required)")
    public Long buyTicketForSectors(@RequestBody Map<String, Long> seatsBySectors, @PathVariable("customerId") Long customerId, @PathVariable("performanceId") Long performanceId) {
        return ticketService.buyTicketsForSectors(true, customerId, performanceId, seatsBySectors);
    }

    @RequestMapping(value = "/buyreservedseats/{ticketID}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Buys a reserved ticket for eventType seats. Not all seats have to be bought")
    public TicketDTO buyReservedSeats(@PathVariable Long ticketID, @RequestBody List<SeatDTO> seats){
        return ticketsMapper.ticketToTicketDTO(ticketService.buyReservedSeats(ticketID, seats));
    }

    @RequestMapping(value = "/buyreservedsectors/{ticketID}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Buys a reserved ticket for eventType seats. Not all seats have to be bought")
    public TicketDTO buyReservedSectors(@PathVariable Long ticketID, @RequestBody Map<String, Long> seatsBySectors){
        return ticketsMapper.ticketToTicketDTO(ticketService.buyReservedSectors(ticketID, seatsBySectors));
    }

    @RequestMapping(value = "/refund/{ticketID}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Refunds a ticket")
    @ResponseStatus(value = HttpStatus.OK)
    public void refundTicket(@PathVariable("ticketID") Long ticketID) {
        ticketsMapper.ticketToTicketDTO(ticketService.refundTicket(ticketID));
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Get a list of all Tickets")
    public List<TicketDTO> findAll(){
        return this.ticketService.findAll();
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Search tickets")
    public List<TicketDTO> search(@RequestBody TicketSearchDTO ticketSearchDTO) {
        return ticketsMapper.ticketToTicketDTO(ticketService.search(ticketSearchDTO.getCustomerID(), ticketSearchDTO.getEventID(), ticketSearchDTO.getState()));
    }

    @RequestMapping(value = "/search/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Get ticket by ID")
    public TicketDTO search(@PathVariable("id") Long id) {
        return ticketsMapper.ticketToTicketDTO(ticketService.search(id));
    }

    @RequestMapping(value = "/refundreserved/{ticketID}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Refund a reserved ticket")
    @ResponseStatus(value = HttpStatus.OK)
    public void refundReserved(@PathVariable Long ticketID){
        ticketService.refundReservedTicket(ticketID);
    }

    @RequestMapping(value = "/getSeats/{ticketID}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Get all Seats for a ticket")
    public List<SeatDTO> getSeats(@PathVariable("ticketID") Long ticketID){
        return ticketService.getSeats(ticketID);
    }

    @RequestMapping(value = "/getSeatsBySectors/{ticketID}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Get all Seats by Sectors for a ticket")
    public Map<String, Long> getSeatsBySector(@PathVariable("ticketID") Long ticketID){
        return ticketService.getSeatsBySectors(ticketID);
    }

    @RequestMapping(value = "/getBill/{ticketID}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @ApiOperation(value = "Get a bill for a ticket")
    public FileSystemResource getPdf(@PathVariable("ticketID") Long ticketID){
        return new FileSystemResource(ticketService.getPdf(ticketID));
    }
}
