package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatState;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.seats.SeatsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.CustomerRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.PerformanceRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.SeatReservationRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.TicketRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.PerformanceService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.TicketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Profile("generateData")
@Component
public class TicketDataGenerator{
    private static final Logger LOGGER = LoggerFactory.getLogger(TicketDataGenerator.class);
    private final CustomerRepository customerRepository;
    private final TicketService ticketService;
    private final TicketRepository ticketRepository;
    private final CustomerDataGenerator customerDataGenerator;
    private final EventDataGenerator eventDataGenerator;
    private final PerformanceRepository performanceRepository;
    private final PerformanceService performanceService;
    private final SeatReservationRepository seatReservationRepository;
    private final SeatsMapper seatsMapper;

    @Qualifier("transactionManager")
    private final PlatformTransactionManager txManager;

    private List<Customer> customers;
    private List<Performance> performances;

    private final double PERCENT_FILLED = 0.25;
    private final double TICKETSCOUNT = 3000;


    public TicketDataGenerator(CustomerRepository customerRepository, TicketService ticketService, TicketRepository ticketRepository, CustomerDataGenerator customerDataGenerator, EventDataGenerator eventDataGenerator, PerformanceRepository performanceRepository, PerformanceService performanceService, SeatReservationRepository seatReservationRepository, SeatsMapper seatsMapper, PlatformTransactionManager txManager) {
        this.customerRepository = customerRepository;
        this.ticketService = ticketService;
        this.ticketRepository = ticketRepository;
        this.customerDataGenerator = customerDataGenerator;
        this.eventDataGenerator = eventDataGenerator;
        customers = customerRepository.findAll();
        this.performanceRepository = performanceRepository;
        performances = performanceRepository.findAll();
        this.performanceService = performanceService;
        this.seatReservationRepository = seatReservationRepository;
        this.seatsMapper = seatsMapper;
        this.txManager = txManager;
    }

    @Transactional
    @PostConstruct
    private void generateTickets() {
        TransactionTemplate tmpl = new TransactionTemplate(txManager);
        tmpl.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                if (ticketRepository.count() > 0) {
                    LOGGER.info("tickets already generated");
                } else {
                    LOGGER.info("generating {} ticket entries",TICKETSCOUNT);
                    performances = performanceRepository.findAll();
                    customers = customerRepository.findAll();
                    long cntFreeSeats = calculateCountFreeSeats();

                    long seatsPerTicket = (int)(cntFreeSeats / (TICKETSCOUNT/(PERCENT_FILLED*(2/3d))));

                    for(int i = 0; i < TICKETSCOUNT; i++){
                        try{
                            createTicket(TicketState.values()[new Random().nextInt(TicketState.values().length)], seatsPerTicket);
                        }
                        catch(Exception e){
                            e.printStackTrace();
                            throw e;
                        }
                    }
                }
            }
        });
    }

    @Transactional
    private void createTicket(TicketState state, long cntSeats){
        Customer customer = getRandomCustomer();
        Performance performance = null;
        List<SeatDTO> seatDTOS = null;
        while(true){
            performance = getRandomPerformance();
            seatDTOS = performanceService.getSeats(performance.getId()).stream().filter(seatDTO -> seatDTO.getState() == SeatState.FREE).collect(Collectors.toList());
            long freeseats = seatDTOS.size();
            if(freeseats < cntSeats){
                continue;
            }
            else{
                break;
            }
        }
        List<Seat> seats = seatsMapper.seatDTOToSeat(seatDTOS);

        Ticket ticket = Ticket.newBuilder().customer(customer).performance(performance).state(state).build();
        ticketRepository.save(ticket);
        BigDecimal sumPrice = BigDecimal.ZERO;
        int i = 0;
        for(; i < cntSeats; i++){
            SeatReservation sr = SeatReservation.newBuilder().setPerformance(performance).setSeat(getRandomFreeSeat(seats)).setTicket(ticket).build();
            BigDecimal price = performance.getPrice().multiply(sr.getSeat().getSector().getFactor()).round(new MathContext(2));

            sumPrice = sumPrice.add(price);
            if(sumPrice.compareTo(new BigDecimal(400l))>0){
                break;
            }
            seatReservationRepository.save(sr);
        }
        if(i == 0){
            ticketRepository.delete(ticket);
        }
    }

    @Transactional
    private Seat getRandomFreeSeat(List<Seat> seats){
        int i = (int)(Math.random()*seats.size());
        if(i>=seats.size())i--;
        Seat seat = seats.get(i);
        seats.remove(seat);
        return seat;
    }

    @Transactional
    private long calculateCountFreeSeats(){
        long cnt = 0;
        for(Performance performance : performances){
            cnt += performance.getHall().getSeats().size();
        }
        return cnt;
    }

    @Transactional
    private Customer getRandomCustomer(){
        int i = (int)(Math.random()*customers.size());
        if(i>=customers.size())i--;
        return customers.get(i);
    }

    @Transactional
    private Performance getRandomPerformance(){
        int i = (int)(Math.random()*performances.size());
        if(i>=performances.size())i--;
        return performances.get(i);
    }
}
