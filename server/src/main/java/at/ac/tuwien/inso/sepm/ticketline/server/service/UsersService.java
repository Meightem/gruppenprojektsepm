package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;

import java.util.List;

public interface UsersService {
    /**
     * Create a single user
     * @param user The user to create
     * @return The created user
     */
    UserDTO createUser(UserDTO user);

    /**
     * Returns all users
     * @return The List of all teh users
     */
    List<UserDTO> getAll();

    /**
     * Returns the user with the given username.
     * @param username The username to be looked for.
     * @return The user with the given username.
     */
    User getByUsername(String username);

    /**
     * Update the specified user(specified by the id) to the corresponding fields
     * @param user to be updated
     * @param authenticationTokenInfo the info about the user doing the update
     * @return the updated user
     */
    UserDTO updateUser(UserDTO user, AuthenticationTokenInfo authenticationTokenInfo);

    /**
     * Returns the news not read by the given user.
     * @param userId The user id to get the read news for.
     * @return The list of news not read by the given user
     */
    List<SimpleNewsDTO> getUnreadNews(String username);
}
