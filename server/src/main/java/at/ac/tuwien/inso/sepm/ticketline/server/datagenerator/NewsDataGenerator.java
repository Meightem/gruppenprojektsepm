package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.NewsRepository;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

@Profile("generateData")
@Component
public class NewsDataGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsDataGenerator.class);
    private static final int NUMBER_OF_NEWS_TO_GENERATE = 25;

    private final NewsRepository newsRepository;
    private final Faker faker;

    public NewsDataGenerator(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
        faker = new Faker();
    }

    @PostConstruct
    private void generateNews() {
        if (newsRepository.count() > 0) {
            LOGGER.info("news already generated");
        } else {
            LOGGER.info("generating {} news entries", NUMBER_OF_NEWS_TO_GENERATE);
            for (int i = 0; i < NUMBER_OF_NEWS_TO_GENERATE; i++) {
                News news = null;
                String nameFull = faker.name().fullName();
                String nameLast = faker.name().lastName();
                String location = faker.address().cityName();

                if (i % 5 > 2) {
                    news = News.builder()
                        .title(nameFull + " ist neuer Leiter der Filiale in " + location)
                        .text("Nach der Pensionierung der geschätzen Frau " + nameLast + ", die rund 20 Jahre für die Organisation verantwortlich war, " +
                            "folgt nun " + nameFull + ". " + nameFull + " arbeitet bereits seit 15 Jahren als Vize und kennt somit die Strukturen und Arbeitsweisen bestens. Wir alle gratulieren und wünschen viel Erfolg!")
                        .publishedAt(
                            LocalDateTime.ofInstant(
                                faker.date()
                                    .past(365 * 3, TimeUnit.DAYS).
                                    toInstant(),
                                ZoneId.systemDefault()
                            ))
                        .build();
                } else if (i % 3 < 1) {
                    try {
                        File file = new File("client/src/main/resources/image/BSP2_Summer.jpg");
                        Path path = file.toPath();
                        String image = DatatypeConverter.printBase64Binary(Files.readAllBytes(path));
                        LOGGER.debug(image);
                        news = News.builder()
                            .title("Neue Arbeitszeiten im Sommer")
                            .text("Sehr geehrte Mitarbeiterinnen und Mitarbeiter, \n" +
                                "Wie schon in diversen Meetings angekündigt gibt es neue Arbeitszeiten, die in den Monaten Juli und August gelten.\n " +
                                "Aufgrund dessen, dass immer mehr Menschen gerade im Sommer vermehrt Events besuchen möchten und das Angebot dementsprechend " +
                                "auch aufgestockt wurde, gibt es im Juli und August angepasste Öffnungszeiten: \n \n" +
                                "Montag-Mittwoch: 8-17 Uhr \nDonnerstag-Freitag: 8-20 Uhr \nSamstag 11-21 Uhr. \n \n" +
                                "Wir bitten um Ihr Verständnis.")
                            .publishedAt(
                                LocalDateTime.ofInstant(
                                    faker.date()
                                        .past(365 * 3, TimeUnit.DAYS).
                                        toInstant(),
                                    ZoneId.systemDefault()
                                ))
                            .image(image)
                            .build();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        File file = new File("client/src/main/resources/image/BSP1_Ticketline.png");
                        Path path = file.toPath();
                        String image = DatatypeConverter.printBase64Binary(Files.readAllBytes(path));
                        LOGGER.debug(image);
                        news = News.builder()
                            .title("Neue Software")
                            .text("Verehrte Verkäuferinnen und Verkäufer, \n" +
                                "da die alte Software nicht mehr zeitgemäß war, wurde vor rund 3 Monaten eine Software in Auftrag gegeben. Das Team rund um " + nameFull +
                                "hat sich sehr viel Mühe gegeben, diese nach unseren Wünschen zu gestalten! Sie befinden sich gerade in einem Verkaufsprogramm, dass es für Sie leichter machen soll," +
                                " zu arbeiten. Für weitere Hilfe besuchen sie die 'Hilfe', die sich links oben befindet oder fragen sie den zuständigen " +
                                "Technikbeauftragten Ihrer Abteilung. \n" +
                                "Viel Freude wünscht Ihnen Ihre IT-Abteilung.")
                            .publishedAt(
                                LocalDateTime.ofInstant(
                                    faker.date()
                                        .past(365 * 3, TimeUnit.DAYS).
                                        toInstant(),
                                    ZoneId.systemDefault()
                                ))
                            .image(image)
                            .build();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                LOGGER.debug("saving news {}", news);
                newsRepository.save(news);
            }
        }
    }

}
