package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@Entity
@Indexed
@Table(
    uniqueConstraints =
    @UniqueConstraint(columnNames = {"firstName", "lastName"})
)
public class Artist {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_artist_id")
    @SequenceGenerator(name = "seq_artist_id", sequenceName = "seq_artist_id")
    private Long id;

    @Column(nullable = false)
    @NotBlank(message = "exception.artist.fistname.notblank")
    @Size(min = 1, max = 100, message = "exception.artist.fistname;{min};{max}")
    @Field(analyzer = @Analyzer(definition = "edgeNgram"))
    private String firstName;

    @Column(nullable = false)
    @NotBlank(message = "exception.artist.lastname.notblank")
    @Size(min = 1, max = 100, message = "exception.artist.lastname;{min};{max}")
    @Field(analyzer = @Analyzer(definition = "edgeNgram"))
    private String lastName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "artist", cascade = CascadeType.ALL)
    private Set<Event> events;

    public Artist() {
    }

    public Artist(Long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    private Artist(Builder builder) {
        setId(builder.id);
        setFirstName(builder.firstName);
        setLastName(builder.lastName);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artist artist = (Artist) o;
        return Objects.equals(id, artist.id) &&
            Objects.equals(firstName, artist.firstName) &&
            Objects.equals(lastName, artist.lastName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName);
    }


    public static final class Builder {
        private Long id;
        private @NotNull @NotBlank @Size(max = 100) String firstName;
        private @NotNull @NotBlank @Size(max = 100) String lastName;

        private Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder firstName(@NotNull @NotBlank @Size(max = 100) String val) {
            firstName = val;
            return this;
        }

        public Builder lastName(@NotNull @NotBlank @Size(max = 100) String val) {
            lastName = val;
            return this;
        }

        public Artist build() {
            return new Artist(this);
        }
    }
}
