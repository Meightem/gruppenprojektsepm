package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Indexed
public class Hall {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_hall_id")
    @SequenceGenerator(name = "seq_hall_id", sequenceName = "seq_hall_id")
    private Long id;

    @Column(nullable = false)
    @NotNull
    @NotBlank
    @Size(max = 100)
    @Field(analyzer = @Analyzer(definition = "edgeNgram"))
    private String street;

    @Column(nullable = false)
    @NotNull
    @NotBlank
    @Size(max = 100)
    @Field(analyzer = @Analyzer(definition = "edgeNgram"))
    private String city;

    @Column(nullable = false)
    @NotNull
    @NotBlank
    @Size(max = 100)
    @Field(analyzer = @Analyzer(definition = "edgeNgram"))
    private String name;

    @Column(nullable = false)
    @NotNull
    @NotBlank
    @Pattern(regexp="[\\d]*")
    @Size(max = 10)
    @Field(analyzer = @Analyzer(definition = "edgeNgram"))
    private String postcode;

    @Column(nullable = false)
    @NotNull
    @NotBlank
    @Size(max = 100)
    @Field(analyzer = @Analyzer(definition = "edgeNgram"))
    private String country;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="hall", cascade = CascadeType.ALL)
    private Set<Performance> performances = new HashSet<>();

    @OneToMany(fetch=FetchType.LAZY, mappedBy="hall", cascade = CascadeType.ALL)
    private Set<Seat> seats = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Set<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(Set<Performance> performances) {
        this.performances = performances;
    }

    public Set<Seat> getSeats() {
        return seats;
    }

    public void setSeats(Set<Seat> seats) {
        this.seats = seats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hall hall = (Hall) o;
        return Objects.equals(id, hall.id) &&
            Objects.equals(street, hall.street) &&
            Objects.equals(city, hall.city) &&
            Objects.equals(name, hall.name) &&
            Objects.equals(postcode, hall.postcode) &&
            Objects.equals(country, hall.country);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, street, city, name, postcode, country, performances);
    }

    public Hall() {
    }

    public Hall(Long id,@NotNull @NotBlank @Size(max = 100) String street, @NotNull @NotBlank @Size(max = 100) String city, @NotNull @NotBlank @Size(max = 100) String name, @NotNull @NotBlank @Pattern(regexp = "[\\d]*") @Size(max = 10) String postcode, @NotNull @NotBlank @Size(max = 100) String country, Set<Performance> performances, Set<Seat> seats) {
        this.id = id;
        this.street = street;
        this.city = city;
        this.name = name;
        this.postcode = postcode;
        this.country = country;
        this.performances = performances;
        this.seats = seats;
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public static class Builder {
        private Long id;
        private @NotNull @NotBlank @Size(max = 100) String street;
        private @NotNull @NotBlank @Size(max = 100) String city;
        private @NotNull @NotBlank @Size(max = 100) String name;
        private @NotNull @NotBlank @Pattern(regexp = "[\\d]*") @Size(max = 10) String postcode;
        private @NotNull @NotBlank @Size(max = 100) String country;
        private Set<Performance> performances;
        private Set<Seat> seats;

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setStreet(@NotNull @NotBlank @Size(max = 100) String street) {
            this.street = street;
            return this;
        }

        public Builder setCity(@NotNull @NotBlank @Size(max = 100) String city) {
            this.city = city;
            return this;
        }

        public Builder setName(@NotNull @NotBlank @Size(max = 100) String name) {
            this.name = name;
            return this;
        }

        public Builder setPostcode(@NotNull @NotBlank @Pattern(regexp = "[\\d]*") @Size(max = 10) String postcode) {
            this.postcode = postcode;
            return this;
        }

        public Builder setCountry(@NotNull @NotBlank @Size(max = 100) String country) {
            this.country = country;
            return this;
        }

        public Builder setPerformances(Set<Performance> performances) {
            this.performances = performances;
            return this;
        }

        public Builder setSeats(Set<Seat> seats) {
            this.seats = seats;
            return this;
        }

        public Hall build() {
            return new Hall(id, street, city, name, postcode, country, performances, seats);
        }
    }
}
