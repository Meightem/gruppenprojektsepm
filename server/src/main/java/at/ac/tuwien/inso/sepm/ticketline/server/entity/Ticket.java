package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Ticket implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_ticket_id")
    @SequenceGenerator(name = "seq_ticket_id", sequenceName = "seq_ticket_id")
    private Long id;

    @Column(nullable=false)
    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private TicketState state;

    @ManyToOne(fetch=FetchType.EAGER, optional = false)
    @JoinColumn(name="id_customer")
    private Customer customer;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="ticket", cascade = CascadeType.ALL)
    private Set<SeatReservation> seatReservations = new HashSet<>();

    @ManyToOne(fetch=FetchType.EAGER, optional = false)
    @JoinColumn(name="id_performance")
    private Performance performance;

    private LocalDate billDate;

    @PrePersist
    protected void onCreate() {
        billDate = LocalDate.now();
    }

    @PreUpdate
    protected void onUpdate() {
        billDate = LocalDate.now();
    }

    private Ticket(Builder builder) {
        setId(builder.id);
        setState(builder.state);
        setCustomer(builder.customer);
        setSeatReservations(builder.seatReservations);
        setPerformance(builder.performance);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TicketState getState() {
        return state;
    }

    public void setState(TicketState state) {
        this.state = state;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<SeatReservation> getSeatReservations() {
        return seatReservations;
    }

    public void setSeatReservations(Set<SeatReservation> seatReservations) {
        this.seatReservations = seatReservations;
    }

    public Performance getPerformance() {
        return performance;
    }

    public void setPerformance(Performance performance) {
        this.performance = performance;
    }

    public LocalDate getBillDate() {
        return billDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(id, ticket.id) &&
            state == ticket.state;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, state);
    }

    @Override
    public String toString() {
        return "Ticket{" +
            "id=" + id +
            ", state=" + state +
            '}';
    }

    public Ticket() {
    }


    public static final class Builder {
        private Long id;
        private @NotNull TicketState state;
        private Customer customer;
        private Set<SeatReservation> seatReservations;
        private Performance performance;

        private Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder state(@NotNull TicketState val) {
            state = val;
            return this;
        }

        public Builder customer(Customer val) {
            customer = val;
            return this;
        }

        public Builder seatReservations(Set<SeatReservation> val) {
            seatReservations = val;
            return this;
        }

        public Builder performance(Performance val) {
            performance = val;
            return this;
        }

        public Ticket build() {
            return new Ticket(this);
        }
    }
}
