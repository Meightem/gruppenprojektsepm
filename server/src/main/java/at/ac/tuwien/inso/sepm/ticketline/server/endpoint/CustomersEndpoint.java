package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.endpoint.base.EndpointBase;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.customers.CustomersMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.service.CustomersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/customers")
@Api(value = "customers")
public class CustomersEndpoint extends EndpointBase {

    @Autowired
    private final CustomersService customersService;

    @Autowired
    private final CustomersMapper customersMapper;

    public CustomersEndpoint(CustomersService customersService, CustomersMapper customersMapper) {
        this.customersService = customersService;
        this.customersMapper = customersMapper;
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Create a new customer")
    public CustomerDTO createCustomer(@RequestBody CustomerDTO customerDTO) {
        customerDTO = customersService.createCustomer(customerDTO);
        return customerDTO;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Get list of simple customer entries")
    public List<CustomerDTO> findAll() {
        return customersService.getAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Update fields of customer")
    public CustomerDTO updateUser(@RequestBody CustomerDTO userDTO, @PathVariable Long id) {
        userDTO.setId(id);
        return customersService.updateCustomer(userDTO);
    }

    @RequestMapping(value = "/search/{text}",method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ApiOperation(value = "Get list of simple customer entries which match the search")
    public List<CustomerDTO> search(@PathVariable String text) {
        return customersService.search(text);
    }

    @RequestMapping(value = "/anonymous", method = RequestMethod.GET)
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Find single anonymous user")
    public CustomerDTO getAnonymousCustomer() {
        return customersService.getAnonymousCustomer();
    }
}
