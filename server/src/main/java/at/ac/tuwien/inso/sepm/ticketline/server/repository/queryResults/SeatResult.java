package at.ac.tuwien.inso.sepm.ticketline.server.repository.queryResults;

import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;

public class SeatResult {
    private Long xCoord;
    private Long yCoord;
    private Sector sector;
    private Long id_hall;
    private TicketState state;

    public SeatResult(Long xCoord, Long yCoord, Sector sector, Long id_hall, TicketState state) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.sector = sector;
        this.id_hall = id_hall;
        this.state = state;
    }

    public Long getxCoord() {
        return xCoord;
    }

    public Long getyCoord() {
        return yCoord;
    }

    public Sector getSector() {
        return sector;
    }

    public Long getId_hall() {
        return id_hall;
    }

    public TicketState getState() {
        return state;
    }
}
