package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;

import java.util.List;

public interface CustomersService {

    /**
     * Create a single customer
     * @param customerDTO The customer to create
     * @return The created customer
     */
    CustomerDTO createCustomer(CustomerDTO customerDTO);

    /**
     * Get list of customers
     *
     * @return the list of customers
     */
    List<CustomerDTO> getAll();

    /**
     * Get a customer.
     *
     * @param id the id to look for.
     * @return the customer
     */
    CustomerDTO get(Long id);

    /**
     * update a customer
     * @param customerDTO The customer to update
     * @return the updated customer
     */
    CustomerDTO updateCustomer(CustomerDTO customerDTO);

    List<CustomerDTO> search(String text);

    CustomerDTO getAnonymousCustomer();
}
