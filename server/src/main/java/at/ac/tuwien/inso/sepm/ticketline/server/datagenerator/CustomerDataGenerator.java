package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.customers.CustomersMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.CustomerRepository;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Profile("generateData")
@Component
public class CustomerDataGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerDataGenerator.class);
    private static final int NUMBER_OF_CUSTOMERS_TO_GENERATE = 25;
    private final CustomerRepository customerRepository;
    private final CustomersMapper customersMapper;

    public CustomerDataGenerator(CustomerRepository customerRepository, CustomersMapper customersMapper){
        this.customerRepository = customerRepository;
        this.customersMapper = customersMapper;
    }

    @PostConstruct
    public void generateCustomers(){
        int inDB = customerRepository.findAll().size();
        for (int i = 0; i < (NUMBER_OF_CUSTOMERS_TO_GENERATE - inDB); i++) {
            Faker faker = new Faker();
            customerRepository.save(customersMapper.customerDTOToCustomer(CustomerDTO.newBuilder()
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .isAnonymousCustomer(false)
                .build()
            ));
        }
    }
}
