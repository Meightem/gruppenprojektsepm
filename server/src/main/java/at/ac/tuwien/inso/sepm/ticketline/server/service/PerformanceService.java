package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;

import java.util.List;
import java.util.SortedMap;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Performance;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BadRequestException;

import javax.validation.constraints.NotNull;

public interface PerformanceService {
    /**
     *
     * @param performanceId the id of the performance
     * @return All seats for a given Performance + additional information about the state of the seat (bought, reserved or free)
     */
    List<SeatDTO> getSeats(Long performanceId);

    /**
     *
     * @param performanceId the id of the performance
     * @return all the sectors of a hall of a performance + number of free seats sorted by sector
     */
    SortedMap<Sector,Long> getSeatsBySector(Long performanceId);

    /**
     * Find all performances.
     *
     * @return list of all performances
     */
    List<Performance> findAll();


    /**
     * Find a single performance by id.
     *
     * @param id of the performance
     * @throws BadRequestException when no performance could be found with this id
     * @return the performance
     */
    Performance findOne(@NotNull Long id);

    /**
     * Create a new performance
     *
     * @param performance to created
     * @return the created performance
     */
    Performance createPerformance(Performance performance);

    /**
     * Find all performances that belong to the given event
     * @param eventId the event id to search for
     * @return the performances
     */
    List<Performance> findAllByEventId(Long eventId);

    /**
     * Find all performances that belong to the given hall
     * @param hallId the hall id to search for
     * @return the performances
     */
    List<Performance> findAllByHallId(Long hallId);

    /**
     * Find all performances that belong to the given search
     * @param performanceSearchDTO the search paramters
     * @return the performances that match the search
     */
    List<Performance> search(PerformanceDTO performanceSearchDTO);
}
