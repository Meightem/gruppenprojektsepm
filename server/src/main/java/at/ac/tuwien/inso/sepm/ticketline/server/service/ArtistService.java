package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Artist;

import java.util.List;

/**
 * The interface Artist service.
 */
public interface ArtistService {
    /**
     * Search artist by text.
     *
     * @param text the text
     * @return the artist which matches the text
     */
    List<Artist> search(String text);

    /**
     * List all artists
     *
     * @return a list of all artists
     */
    List<Artist> findAll();
}
