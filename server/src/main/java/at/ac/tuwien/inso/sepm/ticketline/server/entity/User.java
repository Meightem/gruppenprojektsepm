package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_user_id")
    @SequenceGenerator(name = "seq_user_id", sequenceName = "seq_user_id")
    private Long id;

    @Column(nullable = false, unique = true)
    @NotBlank(message = "excpetion.user.username.notblank")
    @Size(min = 1, max = 100,message = "exception.user.username.size;{min};{max}")
    private String username;

    @Column(nullable = false)
    @NotBlank
    @Size(min = 1, max = 100)
    private String passwordHash;

    @Column(nullable = false)
    @NotNull
    private Boolean isAdmin;

    @Column(nullable = false)
    @NotNull
    private Integer failedAttempts;

    @Column(nullable = false)
    @NotNull
    private Boolean isLocked;


    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "users", fetch = FetchType.LAZY)
    private Set<News> news;

    private User(Builder builder) {
        setId(builder.id);
        setUsername(builder.username);
        setPasswordHash(builder.passwordHash);
        setAdmin(builder.isAdmin);
        setFailedAttempts(builder.failedAttempts);
        setLocked(builder.isLocked);
        setNews(builder.readNews);
    }

    public User() {
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public Integer getFailedAttempts() {
        return failedAttempts;
    }

    public void setFailedAttempts(Integer failedAttempts) {
        this.failedAttempts = failedAttempts;
    }

    public Boolean getLocked() {
        return isLocked;
    }

    public void setLocked(Boolean locked) {
        isLocked = locked;
    }

    public Set<News> getNews() {
        return news;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    public static final class Builder {
        private Long id;
        private @Size(max = 100) String username;
        private @Size(max = 100) String passwordHash;
        private Boolean isAdmin;
        private Integer failedAttempts;
        private Boolean isLocked;
        private Set<News> readNews;

        private Builder() {
            isAdmin = false;
            failedAttempts = 0;
            isLocked = false;
            readNews = new HashSet<News>();
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder username(@Size(max = 100) String val) {
            username = val;
            return this;
        }

        public Builder passwordHash(@Size(max = 100) String val) {
            passwordHash = val;
            return this;
        }

        public Builder isAdmin(boolean val) {
            isAdmin = val;
            return this;
        }

        public Builder failedAttempts(int val) {
            failedAttempts = val;
            return this;
        }

        public Builder isLocked(boolean val) {
            isLocked = val;
            return this;
        }

        public Builder readNews(Set<News> val) {
            readNews = val;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
            Objects.equals(username, user.username) &&
            Objects.equals(passwordHash, user.passwordHash) &&
            Objects.equals(isAdmin, user.isAdmin) &&
            Objects.equals(failedAttempts, user.failedAttempts) &&
            Objects.equals(isLocked, user.isLocked);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, passwordHash, isAdmin, failedAttempts, isLocked);
    }

    @Override
    public String toString() {
        return "User{" +
            "id='" + id + '\'' +
            ", username='" + username + '\'' +
            ", isAdmin='" + isAdmin + '\'' +
            ", failedAttempts='" + failedAttempts + '\'' +
            ", isLocked='" + isLocked + '\'' +
            '}';
    }
}