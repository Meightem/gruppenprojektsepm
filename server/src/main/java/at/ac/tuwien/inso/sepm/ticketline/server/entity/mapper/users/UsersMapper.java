package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.users;

import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsersMapper {
    User userDTOToUser(UserDTO userDTO);
    UserDTO userToUserDTO(User user);
    List<UserDTO> userToUserDTO(List<User> user);
}
