package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Artist;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Performance;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.events.EventsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.halls.HallsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.performances.PerformancesMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class PerformancesMapperTest {
    @Configuration
    @ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
    public static class PerformancesMapperTestContextConfiguration {
    }

    @Autowired
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    // Suppress warning cause inspection does not know that the cdi annotations are added in the code generation step
    private PerformancesMapper performancesMapper;
    @Autowired
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    private HallsMapper hallsMapper;
    @Autowired
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    private EventsMapper eventsMapper;
    @Test
    public void shouldMapPerformanceToPerformanceDTO() {
        Event event = Event.newBuilder()
            .setId(123L)
            .name("asdf")
            .artist(Artist.newBuilder().firstName("firstname").build())
            .content("CONTENT")
            .type(EventType.SECTORS)
            .length(Duration.ofMinutes(210))
            .build();
        Hall hall = Hall.newBuilder()
            .setId(1123L)
            .setName("asdf")
            .setCity("wien")
            .setCountry("austria")
            .setPostcode("1001")
            .setStreet("street")
            .build();
        Performance performance = Performance.newBuilder()
            .setId(1L)
            .setPrice(new BigDecimal("10.15"))
            .setDate(LocalDateTime.now())
            .setEvent(event)
            .setHall(hall).build();

        PerformanceDTO performanceDTO = performancesMapper.performanceToPerformanceDTO(performance);

        assertThat(performanceDTO.getId()).isEqualTo(performance.getId());
        assertThat(performanceDTO.getPrice()).isEqualTo(performance.getPrice());
        assertThat(performanceDTO.getDate()).isEqualTo(performance.getDate());
        assertThat(performanceDTO.getEvent()).isEqualTo(eventsMapper.eventToEventDTO(event));
        assertThat(performanceDTO.getHall()).isEqualTo(hallsMapper.hallToHallDTO(hall));
    }

    @Test
    public void shouldMapPerformanceDTOToPerformance() {
        HallDTO hallDTO = HallDTO.newBuilder()
            .id(123L)
            .name("test")
            .city("Wien")
            .street("asdfasdf")
            .postcode("1001")
            .country("austria")
            .build();
        EventDTO eventDTO = EventDTO.newBuilder()
            .id(142L)
            .name("asdfasdf")
            .type(EventType.SECTORS)
            .artist(ArtistDTO.newBuilder().firstName("asdfasdf").build())
            .content("asdfasdf")
            .length(Duration.ofMinutes(120))
            .build();

        PerformanceDTO performanceDTO = PerformanceDTO.newBuilder()
            .setId(112L)
            .setDate(LocalDateTime.now())
            .setPrice(new BigDecimal("154.4"))
            .setHall(hallDTO)
            .setEvent(eventDTO)
            .build();

        Performance performance = performancesMapper.performanceDTOToPerformance(performanceDTO);

        assertThat(performanceDTO.getId()).isEqualTo(performance.getId());
        assertThat(performanceDTO.getDate()).isEqualTo(performance.getDate());
        assertThat(performanceDTO.getPrice()).isEqualTo(performance.getPrice());
        assertThat(performanceDTO.getHall()).isEqualTo(hallsMapper.hallToHallDTO(performance.getHall()));
        assertThat(performanceDTO.getEvent()).isEqualTo(eventsMapper.eventToEventDTO(performance.getEvent()));

    }
}
