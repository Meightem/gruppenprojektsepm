package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.configuration.JacksonConfiguration;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.users.UsersMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BadRequestException;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.UserRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.security.AuthenticationConstants;
import at.ac.tuwien.inso.sepm.ticketline.server.service.implementation.SimpleHeaderTokenAuthenticationService;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.assertj.core.util.Strings;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;

import static org.codehaus.groovy.runtime.InvokerHelper.asList;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Matchers.any;

public class UserLockingEndpointTest extends BaseIntegrationTest {
    private static final String SERVER_HOST = "http://localhost";
    private static final String USER_USERNAME = "user";
    private static final String USER_PASSWORD = "password";
    private static final String USER_PASSWORD_WRONG = USER_PASSWORD + "asdf";
    private static final String ADMIN_PASSWORD = "password";
    private static final String ADMIN_USERNAME = "admin";

    private static final String USERS_ENDPOINT = "/users";
    private static final String USERS_ENDPOINT_ID = "/users/";

    @Value("${server.context-path}")
    private String contextPath;

    @LocalServerPort
    private int port;

    @Autowired
    private SimpleHeaderTokenAuthenticationService simpleHeaderTokenAuthenticationService;

    @Autowired
    private JacksonConfiguration jacksonConfiguration;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UsersMapper usersMapper;

    @Autowired
    private UserRepository userRepository;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    private void tryUserLoginWithWrongCredentials() throws BadCredentialsException, LockedException{
        RestAssured.baseURI = SERVER_HOST;
        RestAssured.basePath = contextPath;
        RestAssured.port = port;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.config = RestAssuredConfig.config().
            objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory((aClass, s) ->
                jacksonConfiguration.jackson2ObjectMapperBuilder().build()));
        simpleHeaderTokenAuthenticationService.authenticate(USER_USERNAME, USER_PASSWORD_WRONG);
    }

    private void tryAdminLoginWithWrongCredentials() throws BadCredentialsException, LockedException{
        RestAssured.baseURI = SERVER_HOST;
        RestAssured.basePath = contextPath;
        RestAssured.port = port;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.config = RestAssuredConfig.config().
            objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory((aClass, s) ->
                jacksonConfiguration.jackson2ObjectMapperBuilder().build()));
        simpleHeaderTokenAuthenticationService.authenticate(ADMIN_USERNAME, USER_PASSWORD_WRONG);
    }

    @Test
    public void testUpdateFailedAttempts() {
        Integer initFailedAttempts = userRepository.findByUsername(USER_USERNAME).getFailedAttempts();
        try{
            tryUserLoginWithWrongCredentials();
        }
        catch(Exception e){
        }
        Integer afterFailedAttempts = userRepository.findByUsername(USER_USERNAME).getFailedAttempts();

        Assert.assertEquals((long)afterFailedAttempts, initFailedAttempts+1);
    }

    @Test
    public void testOnlyAdminCannotBeLocked(){
        Assert.assertEquals(userRepository.findByUsername(ADMIN_USERNAME).getLocked(), false);
        for(int i = 0; i < 6; i++){
            try{
                tryUserLoginWithWrongCredentials();
            }
            catch(Exception e){
            }
        }
        Assert.assertEquals(userRepository.findByUsername(ADMIN_USERNAME).getLocked(), false);
    }

    @Test
    public void testLockedAfterFiveTrys(){
        Assert.assertEquals(userRepository.findByUsername(USER_USERNAME).getLocked(), false);
        for(int i = 0; i < 5; i++){
            try{
                tryUserLoginWithWrongCredentials();
            }
            catch(Exception e){
            }
        }
        Assert.assertEquals(userRepository.findByUsername(USER_USERNAME).getLocked(), true);

        expectedEx.expect(LockedException.class);
        tryUserLoginWithWrongCredentials();
    }

    @Test
    public void testResetFailedAttempts(){
        Assert.assertEquals((long)userRepository.findByUsername(USER_USERNAME).getFailedAttempts(), 0);
        for(int i = 0; i < 4; i++){
            try{
                tryUserLoginWithWrongCredentials();
            }
            catch(Exception e){}
        }
        Assert.assertEquals((long)userRepository.findByUsername(USER_USERNAME).getFailedAttempts(), 4);
        Assert.assertEquals(userRepository.findByUsername(USER_USERNAME).getLocked(), false);

        simpleHeaderTokenAuthenticationService.authenticate(USER_USERNAME, USER_PASSWORD);

        Assert.assertEquals((long)userRepository.findByUsername(USER_USERNAME).getFailedAttempts(), 0);
    }

    @Test
    public void testLockAllAdmins(){
        List<User> users = userRepository.findAll();
        for(User user : users){
            UserDTO userDTO = usersMapper.userToUserDTO(user);
            userDTO.setLocked(true);

            boolean isCheck = userRepository.countByIsAdminAndIsLocked(true, false) == 1 && user.getAdmin() == true;

            Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .body(userDTO)
                .when().patch(USERS_ENDPOINT_ID+user.getId())
                .then().extract().response();
            if(isCheck){
                Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST.value());
            }
        }
    }
}
