package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatState;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketSearchDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.keys.SeatPK;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.keys.SeatReservationPK;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.customers.CustomersMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.performances.PerformancesMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.tickets.TicketsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.persistence.EntityManagerFactory;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

import static org.hamcrest.core.Is.is;

public class TicketEndpointTest extends BaseIntegrationTest {
    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private SeatReservationRepository seatReservationRepository;

    @Autowired
    private SeatRepository seatRepository;

    @Autowired
    private PerformanceRepository performanceRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private HallRepository hallRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private CustomersMapper customersMapper;

    @Autowired
    private PerformancesMapper performancesMapper;

    @Autowired
    private EntityManagerFactory entityManager;

    @Autowired
    private TicketsMapper ticketsMapper;

    private final String TICKET_ENDPOINT = "/tickets";
    private static Long lastPerformanceId;
    private static Long lastHallId;
    private static Long lastTicketId;
    private static Long lastCustomerId;
    private static Long firstCustomerIDOfGen;
    private static Long firstEventIDOfGen;

    private static boolean generatedTestData = false;

    @Before
    @Override
    public void beforeBase() {
        if (generatedTestData) {
            setTokens();
        } else {
            super.beforeBase();
            generateTestData();
            generatedTestData = true;
        }
    }

    public Response responseBuyTicketsSeats(List<SeatDTO> seats, Long customerId, Long performanceId) {
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(seats)
            .when().post(TICKET_ENDPOINT + "/buyseats/" + customerId.toString() + "/" + performanceId.toString())
            .then().extract().response();
        return response;
    }

    public Response responseBuyTicketsSectors(Map<String, Long> seatsBySectors, Long customerId, Long performanceId) {
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(seatsBySectors)
            .when().post(TICKET_ENDPOINT + "/buysectors/" + customerId.toString() + "/" + performanceId.toString())
            .then().extract().response();
        return response;
    }

    public Response responseBuyReservedSeats(List<SeatDTO> seats, Long ticketId){
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(seats)
            .when().post(TICKET_ENDPOINT + "/buyreservedseats/" + ticketId.toString())
            .then().extract().response();
        return response;
    }

    public Response responseBuyReservedSectors(Map<String, Long> seatsBySectors, Long ticketId) {
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(seatsBySectors)
            .when().post(TICKET_ENDPOINT + "/buyreservedsectors/" + ticketId.toString())
            .then().extract().response();
        return response;
    }


    public Response responseReserveTicketsSeats(List<SeatDTO> seats, Long customerId, Long performanceId) {
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(seats)
            .when().post(TICKET_ENDPOINT + "/reserveseats/" + customerId.toString() + "/" + performanceId.toString())
            .then().extract().response();
        return response;
    }

    public Response responseReserveTicketsSectors(Map<String, Long> seatsBySectors, Long customerId, Long performanceId) {
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(seatsBySectors)
            .when().post(TICKET_ENDPOINT + "/reservesectors/" + customerId.toString() + "/" + performanceId.toString())
            .then().extract().response();
        return response;
    }

    @Test
    public void testBuyAllFreeAndCancelledTicketsSeats() {
        generatedTestData = false;
        beforeBase();

        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seats = new ArrayList<>();
        seats.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(2L).id_hall(lastHallId).build());
        seats.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.B).xCoord(0L).yCoord(5L).id_hall(lastHallId).build());
        seats.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(6L).id_hall(lastHallId).build());
        seats.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.B).xCoord(0L).yCoord(7L).id_hall(lastHallId).build());
        seats.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(10L).id_hall(lastHallId).build());

        Response response = responseBuyTicketsSeats(seats, lastCustomerId, lastPerformanceId - 3);

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 3, new SeatPK(0L, 2L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 3, new SeatPK(0L, 5L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 3, new SeatPK(0L, 6L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 3, new SeatPK(0L, 7L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 3, new SeatPK(0L, 10L, lastHallId))));

        Assert.assertThat(seatReservationRepository.count(), is(37L));
    }

    @Test
    public void testBuyAllFreeAndCancelledTicketsSectors() {
        generatedTestData = false;
        beforeBase();

        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Map<String, Long> seatsBySectors = new HashMap<>();
        seatsBySectors.put("A", 3L);
        seatsBySectors.put("B", 2L);

        Response response = responseBuyTicketsSectors(seatsBySectors, lastCustomerId, lastPerformanceId - 1);

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 1, new SeatPK(0L, 2L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 1, new SeatPK(0L, 5L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 1, new SeatPK(0L, 6L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 1, new SeatPK(0L, 7L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 1, new SeatPK(0L, 10L, lastHallId))));

        Assert.assertThat(seatReservationRepository.count(), is(37L));
    }

    @Test
    public void testBuyFreeTicketBeforeNowSeats() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 2).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isBefore(LocalDateTime.now()));

        List<SeatDTO> seats = new ArrayList<>();
        seats.add(SeatDTO.newBuilder()
                      .state(SeatState.FREE)
                      .sector(Sector.A)
                      .xCoord(0L).yCoord(2L)

                      .id_hall(lastHallId)
                      .build());
        Response response = responseBuyTicketsSeats(seats, lastCustomerId, lastPerformanceId - 2);

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void testBuyFreeTicketBeforeNowSectors() {
        Performance performance = performanceRepository.findById(lastPerformanceId).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isBefore(LocalDateTime.now()));
        Map<String, Long> seatsBySectors = new HashMap<>();
        seatsBySectors.put("A", 1L);
        seatsBySectors.put("B", 0L);

        Response response = responseBuyTicketsSectors(seatsBySectors, lastCustomerId, lastPerformanceId);

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void testBuyTicketSeatsValidation1() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seatBought = new ArrayList<>();
        seatBought.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(4L).id_hall(lastHallId).build());
        long beforeCount = seatReservationRepository.count();
        Response responseBought = responseBuyTicketsSeats(seatBought, lastCustomerId, lastPerformanceId - 3);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseBought.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketSeatsValidation2() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seatReserved = new ArrayList<>();
        seatReserved.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.B).xCoord(0L).yCoord(1L).id_hall(lastHallId).build());
        long beforeCount = seatReservationRepository.count();
        Response responseReserved = responseBuyTicketsSeats(seatReserved, lastCustomerId, lastPerformanceId - 3);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseReserved.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }


    @Test
    public void testBuyTicketSeatsValidation3() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seatCancelledAndBought = new ArrayList<>();
        seatCancelledAndBought.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(0L).id_hall(lastHallId).build());
        long beforeCount = seatReservationRepository.count();
        Response responseCancelledAndBought = responseBuyTicketsSeats(seatCancelledAndBought, lastCustomerId, lastPerformanceId - 3);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseCancelledAndBought.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketSeatsValidation4() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seatEmpty = new ArrayList<>();
        long beforeCount = seatReservationRepository.count();
        Response responseEmpty = responseBuyTicketsSeats(seatEmpty, lastCustomerId, lastPerformanceId - 3);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseEmpty.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }


    @Test
    public void testBuyTicketSeatsValidation5() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seatWrongEventType = new ArrayList<>();
        seatWrongEventType.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(2L).id_hall(lastHallId).build());
        long beforeCount = seatReservationRepository.count();
        Response responseWrongEventType = responseBuyTicketsSeats(seatWrongEventType, lastCustomerId, lastPerformanceId - 1);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseWrongEventType.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }


    @Test
    public void testBuyTicketSeatsValidation6() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seatXCoordNull = new ArrayList<>();
        seatXCoordNull.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(null).yCoord(2L).id_hall(lastHallId).build());
        long beforeCount = seatReservationRepository.count();
        Response responseXCoordNull = responseBuyTicketsSeats(seatXCoordNull, lastCustomerId, lastPerformanceId - 3);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseXCoordNull.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketSeatsValidation7() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seatYCoordNull = new ArrayList<>();
        seatYCoordNull.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(null).id_hall(lastHallId).build());
        long beforeCount = seatReservationRepository.count();
        Response responseYCoordNull = responseBuyTicketsSeats(seatYCoordNull, lastCustomerId, lastPerformanceId - 3);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseYCoordNull.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketSeatsValidation8() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seatHallNull = new ArrayList<>();
        seatHallNull.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(2L).id_hall(null).build());
        long beforeCount = seatReservationRepository.count();
        Response responseHallNull = responseBuyTicketsSeats(seatHallNull, lastCustomerId, lastPerformanceId - 3);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseHallNull.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }


    @Test
    public void testBuyTicketSeatsValidation9() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seatNotExist = new ArrayList<>();
        seatNotExist.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(Long.MAX_VALUE).yCoord(2L).id_hall(lastHallId).build());
        long beforeCount = seatReservationRepository.count();
        Response responseNotExist = responseBuyTicketsSeats(seatNotExist, lastCustomerId, lastPerformanceId - 3);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseNotExist.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketSeatsValidation10() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Hall hall = hallRepository.save(Hall.newBuilder().setCity("city").setCountry("country").setName("name").setPostcode("1234").setStreet("street").build());
        seatRepository.save(Seat.newBuilder().setHall(hall).setSector(Sector.A).setXCoord(0L).setYCoord(0L).build());

        List<SeatDTO> seatWrongHall = new ArrayList<>();
        seatWrongHall.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(0L).id_hall(lastHallId + 1).build());
        long beforeCount = seatReservationRepository.count();
        Response responseWrongHall = responseBuyTicketsSeats(seatWrongHall, lastCustomerId, lastPerformanceId - 3);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseWrongHall.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketSeatsValidation11() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seatPerformanceNotFound = new ArrayList<>();
        seatPerformanceNotFound.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(2L).id_hall(lastHallId).build());
        long beforeCount = seatReservationRepository.count();
        Response responsePerformanceNotFound = responseBuyTicketsSeats(seatPerformanceNotFound, lastCustomerId, lastPerformanceId + 1);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responsePerformanceNotFound.getStatusCode(), is(HttpStatus.NOT_FOUND.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketSeatsValidation12() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seatCustomerNotFound = new ArrayList<>();
        seatCustomerNotFound.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(2L).id_hall(lastHallId).build());
        long beforeCount = seatReservationRepository.count();
        Response responseCustomerNotFound = responseBuyTicketsSeats(seatCustomerNotFound, lastCustomerId + 1, lastPerformanceId - 3);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseCustomerNotFound.getStatusCode(), is(HttpStatus.NOT_FOUND.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketsValidationSectors1() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));
    }

    @Test
    public void testBuyTicketsValidationSectors2() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Map<String, Long> tooHighDemand1 = new HashMap<>();
        tooHighDemand1.put("A", 4L);
        tooHighDemand1.put("B", 0L);
        long beforeCount = seatReservationRepository.count();
        Response responseTooHighDemand1 = responseBuyTicketsSectors(tooHighDemand1, lastCustomerId, lastPerformanceId - 1);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseTooHighDemand1.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketsValidationSectors3() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Map<String, Long> tooHighDemand2 = new HashMap<>();
        tooHighDemand2.put("A", 0L);
        tooHighDemand2.put("B", 3L);
        long beforeCount = seatReservationRepository.count();
        Response responseTooHighDemand2 = responseBuyTicketsSectors(tooHighDemand2, lastCustomerId, lastPerformanceId - 1);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseTooHighDemand2.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketsValidationSectors4() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Map<String, Long> tooLowDemand1 = new HashMap<>();
        tooLowDemand1.put("A", 0L);
        tooLowDemand1.put("B", 0L);
        long beforeCount = seatReservationRepository.count();
        Response responseTooLowDemand1 = responseBuyTicketsSectors(tooLowDemand1, lastCustomerId, lastPerformanceId - 1);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseTooLowDemand1.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketsValidationSectors5() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Map<String, Long> tooLowDemand2 = new HashMap<>();
        tooLowDemand2.put("A", 1L);
        tooLowDemand2.put("B", -1L);
        long beforeCount = seatReservationRepository.count();
        Response responseTooLowDemand2 = responseBuyTicketsSectors(tooLowDemand2, lastCustomerId, lastPerformanceId - 1);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseTooLowDemand2.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketsValidationSectors6() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Map<String, Long> wrongSectorDemand = new HashMap<>();
        wrongSectorDemand.put("A", 1L);
        wrongSectorDemand.put("B", 0L);
        wrongSectorDemand.put("C", 0L);
        long beforeCount = seatReservationRepository.count();
        Response responseWrongSector = responseBuyTicketsSectors(wrongSectorDemand, lastCustomerId, lastPerformanceId - 1);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseWrongSector.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketsValidationSectors7() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Map<String, Long> validDemand = new HashMap<>();
        validDemand.put("A", 1L);
        validDemand.put("B", 0L);

        long beforeCount = seatReservationRepository.count();
        Response responseInvalidPerformance = responseBuyTicketsSectors(validDemand, lastCustomerId, lastPerformanceId + 1);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseInvalidPerformance.getStatusCode(), is(HttpStatus.NOT_FOUND.value()));
        Assert.assertTrue(beforeCount == afterCount);
    }

    @Test
    public void testBuyTicketsValidationSectors8() {
        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Map<String, Long> validDemand = new HashMap<>();
        validDemand.put("A", 1L);
        validDemand.put("B", 0L);

        long beforeCount = seatReservationRepository.count();
        Response responseInvalidCustomer = responseBuyTicketsSectors(validDemand, lastCustomerId + 1, lastPerformanceId - 1);
        long afterCount = seatReservationRepository.count();
        Assert.assertThat(responseInvalidCustomer.getStatusCode(), is(HttpStatus.NOT_FOUND.value()));
        Assert.assertTrue(beforeCount == afterCount);

    }

    // only two tests for reserving, because reserving and buying uses same methods
    @Test
    public void testIndividualReserveTicketsSeatsAll() {
        generatedTestData = false;
        beforeBase();

        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        List<SeatDTO> seats = new ArrayList<>();
        seats.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(2L).id_hall(lastHallId).build());
        seats.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.B).xCoord(0L).yCoord(5L).id_hall(lastHallId).build());
        seats.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(6L).id_hall(lastHallId).build());
        seats.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.B).xCoord(0L).yCoord(7L).id_hall(lastHallId).build());
        seats.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.A).xCoord(0L).yCoord(10L).id_hall(lastHallId).build());

        Response response = responseReserveTicketsSeats(seats, lastCustomerId, lastPerformanceId - 3);

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        List<Ticket> list = ticketRepository.findAllByCustomer(customerRepository.getOne(lastCustomerId));
        Assert.assertThat(list.get(1).getState(), is(TicketState.RESERVED));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 3, new SeatPK(0L, 2L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 3, new SeatPK(0L, 5L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 3, new SeatPK(0L, 6L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 3, new SeatPK(0L, 7L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 3, new SeatPK(0L, 10L, lastHallId))));

        Assert.assertThat(seatReservationRepository.count(), is(37L));

    }

    @Test
    public void testIndividualReserveTicketsSectorsAll() {
        generatedTestData = false;
        beforeBase();

        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Map<String, Long> seatsBySectors = new HashMap<>();
        seatsBySectors.put("A", 3L);
        seatsBySectors.put("B", 2L);

        Response response = responseReserveTicketsSectors(seatsBySectors, lastCustomerId, lastPerformanceId - 1);

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        List<Ticket> tickets = ticketRepository.findAllByCustomer(customerRepository.getOne(lastCustomerId));
        Assert.assertThat(tickets.get(1).getState(), is(TicketState.RESERVED));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 1, new SeatPK(0L, 2L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 1, new SeatPK(0L, 5L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 1, new SeatPK(0L, 6L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 1, new SeatPK(0L, 7L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId + 1, lastPerformanceId - 1, new SeatPK(0L, 10L, lastHallId))));

        Assert.assertThat(seatReservationRepository.count(), is(37L));
    }

    private void generateTestData() {
        /**
         * Y-Coordinates:
         *
         * 0: Cancelled and Bought
         * 1: Bought
         * 2: Free
         * 3: Bought
         * 4: Reserved
         * 5: Cancelled
         * 6: Free
         * 7: Free
         * 8: Reserved
         * 9: Reserved
         * 10: Free
         */

        Customer customer = Customer.newBuilder().firstName("fn").lastName("ln").build();
        customerRepository.save(customer);
        lastCustomerId = customer.getId();
        Artist artist = Artist.newBuilder().lastName("ln").firstName("fn").build();
        artistRepository.save(artist);
        Hall hall = Hall.newBuilder().setCity("city").setCountry("country").setName("name").setPostcode("1234").setStreet("street").build();
        hallRepository.save(hall);
        lastHallId = hall.getId();
        Event eventSeats = Event.newBuilder().artist(artist).content("content").
            length(Duration.ofDays(10L)).name("name").type(EventType.SEATS).build();
        Event eventSectors = Event.newBuilder().artist(artist).content("content").
            length(Duration.ofDays(10L)).name("name").type(EventType.SECTORS).build();
        Event event = eventRepository.save(eventSeats);
        firstEventIDOfGen = event.getId();
        eventRepository.save(eventSectors);
        Performance performanceSeatAfter = Performance.newBuilder().setDate(LocalDateTime.now().plusDays(1)).setEvent(eventSeats).setHall(hall).setPrice(BigDecimal.TEN).build();
        Performance performanceSeatBefore = Performance.newBuilder().setDate(LocalDateTime.now().minusDays(1)).setEvent(eventSeats).setHall(hall).setPrice(BigDecimal.TEN).build();
        Performance performanceSectorsAfter = Performance.newBuilder().setDate(LocalDateTime.now().plusDays(1)).setEvent(eventSectors).setHall(hall).setPrice(BigDecimal.TEN).build();
        Performance performanceSectorsBefore = Performance.newBuilder().setDate(LocalDateTime.now().minusDays(1)).setEvent(eventSectors).setHall(hall).setPrice(BigDecimal.TEN).build();
        performanceRepository.save(performanceSeatAfter);
        performanceRepository.save(performanceSeatBefore);
        performanceRepository.save(performanceSectorsAfter);
        performanceRepository.save(performanceSectorsBefore);

        lastPerformanceId = performanceSectorsBefore.getId();

        for (int i = 0; i < 11; i++) {
            Seat seat = Seat.newBuilder().setXCoord(0L).setYCoord((long)i).setHall(hall).setSector(i % 2 == 0 ? Sector.A : Sector.B).build();
            seatRepository.save(seat);
        }

        for (int i = 0; i < 4; i++) {


            Performance performance;
            if (i == 0)
                performance = performanceSeatAfter;
            else if (i == 1)
                performance = performanceSeatBefore;
            else if (i == 2)
                performance = performanceSectorsAfter;
            else
                performance = performanceSectorsBefore;

            Ticket ticketBought = Ticket.newBuilder().customer(customer).state(TicketState.BOUGHT).performance(performance).build();
            Ticket ticketReserved = Ticket.newBuilder().customer(customer).state(TicketState.RESERVED).performance(performance).build();
            Ticket ticketCancelled = Ticket.newBuilder().customer(customer).state(TicketState.CANCELLED).performance(performance).build();
            ticketRepository.save(ticketBought);
            ticketRepository.save(ticketReserved);
            ticketRepository.save(ticketCancelled);

            if (i == 3) {
                lastTicketId = ticketCancelled.getId();
            } else if (i == 0) {
                firstCustomerIDOfGen = ticketBought.getId();
            }

            SeatReservation sr1Bought = SeatReservation.newBuilder().setPerformance(performance).setTicket(ticketBought).setSeat(seatRepository.findById(new SeatPK(0L, 1L, hall.getId())).get()).build();
            SeatReservation sr2Bought = SeatReservation.newBuilder().setPerformance(performance).setTicket(ticketBought).setSeat(seatRepository.findById(new SeatPK(0L, 3L, hall.getId())).get()).build();
            SeatReservation sr3Bought = SeatReservation.newBuilder().setPerformance(performance).setTicket(ticketBought).setSeat(seatRepository.findById(new SeatPK(0L, 0L, hall.getId())).get()).build();
            SeatReservation sr1Reserved = SeatReservation.newBuilder().setPerformance(performance).setTicket(ticketReserved).setSeat(seatRepository.findById(new SeatPK(0L, 4L, hall.getId())).get()).build();
            SeatReservation sr2Reserved = SeatReservation.newBuilder().setPerformance(performance).setTicket(ticketReserved).setSeat(seatRepository.findById(new SeatPK(0L, 8L, hall.getId())).get()).build();
            SeatReservation sr3Reserved = SeatReservation.newBuilder().setPerformance(performance).setTicket(ticketReserved).setSeat(seatRepository.findById(new SeatPK(0L, 9L, hall.getId())).get()).build();
            SeatReservation sr1Cancelled = SeatReservation.newBuilder().setPerformance(performance).setTicket(ticketCancelled).setSeat(seatRepository.findById(new SeatPK(0L, 0L, hall.getId())).get()).build();
            SeatReservation sr2Cancelled = SeatReservation.newBuilder().setPerformance(performance).setTicket(ticketCancelled).setSeat(seatRepository.findById(new SeatPK(0L, 5L, hall.getId())).get()).build();

            seatReservationRepository.save(sr1Bought);
            seatReservationRepository.save(sr2Bought);
            seatReservationRepository.save(sr3Bought);
            seatReservationRepository.save(sr1Reserved);
            seatReservationRepository.save(sr2Reserved);
            seatReservationRepository.save(sr3Reserved);
            seatReservationRepository.save(sr1Cancelled);
            seatReservationRepository.save(sr2Cancelled);
        }
    }

    @Test
    public void refundTicketWorks() {
        generatedTestData = false;
        beforeBase();
        Ticket ticket = ticketRepository.findById(firstCustomerIDOfGen).get();
        Assert.assertThat(ticket.getState(), is(TicketState.BOUGHT));

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().post(TICKET_ENDPOINT + "/refund/" + firstCustomerIDOfGen)
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        Ticket ticketRet = ticketRepository.findById(firstCustomerIDOfGen).get();
        Assert.assertThat(ticketRet.getState(), is(TicketState.CANCELLED));
    }

    @Test
    public void refundReservedTicketWorks() {
        generatedTestData = false;
        beforeBase();
        Ticket ticket = ticketRepository.findById(firstCustomerIDOfGen+1).get();
        Assert.assertThat(ticket.getState(), is(TicketState.RESERVED));

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().post(TICKET_ENDPOINT + "/refundreserved/" + (firstCustomerIDOfGen+1))
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        Assert.assertTrue(!ticketRepository.existsById(firstCustomerIDOfGen+1));
        Assert.assertTrue(seatReservationRepository.count()<32);
    }

    @Test
    public void refundTicketDoesNotWork() {
        generatedTestData = false;
        beforeBase();
        Ticket ticket = ticketRepository.findById(firstCustomerIDOfGen + 1L).get();
        Assert.assertTrue(ticket.getState() != (TicketState.BOUGHT));

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().post(TICKET_ENDPOINT + "/refund/" + (firstCustomerIDOfGen + 1L))
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void buyReservedSeatsOk() {
        generatedTestData = false;
        beforeBase();

        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Ticket ticket = ticketRepository.findById(lastTicketId - 10).get();
        Assert.assertThat(ticket.getState(), is(TicketState.RESERVED));

        List<SeatDTO> seats = new ArrayList<>();
        seats.add(SeatDTO.newBuilder().state(SeatState.FREE).sector(Sector.B).xCoord(0L).yCoord(9L).id_hall(lastHallId).build());

        Response response = responseBuyReservedSeats(seats, lastTicketId - 10);

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertTrue(!seatReservationRepository.existsById(new SeatReservationPK(lastTicketId - 10, lastPerformanceId - 3, new SeatPK(0L, 4L, lastHallId))));
        Assert.assertTrue(!seatReservationRepository.existsById(new SeatReservationPK(lastTicketId - 10, lastPerformanceId - 3, new SeatPK(0L, 8L, lastHallId))));
        Assert.assertTrue(seatReservationRepository.existsById(new SeatReservationPK(lastTicketId - 10, lastPerformanceId - 3, new SeatPK(0L, 9L, lastHallId))));

    }

    @Test
    public void testBuyReservedSeatsTooLittle() {
        generatedTestData = false;
        beforeBase();

        Performance performance = performanceRepository.findById(lastPerformanceId - 3).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SEATS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Ticket ticket = ticketRepository.findById(lastTicketId - 10).get();
        Assert.assertThat(ticket.getState(), is(TicketState.RESERVED));

        List<SeatDTO> seats = new ArrayList<>();

        Response response = responseBuyReservedSeats(seats, lastTicketId - 10);

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void testBuyReservedSectorsOK() {
        generatedTestData = false;
        beforeBase();

        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Map<String, Long> seatsBySectors = new HashMap<>();
        seatsBySectors.put("A", 1L);
        seatsBySectors.put("B", 0L);

        Response response = responseBuyReservedSectors(seatsBySectors, lastTicketId - 4);

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(seatReservationRepository.count(), is(30L));
    }

    @Test
    public void testBuyReservedSectorsTooLittle() {
        generatedTestData = false;
        beforeBase();

        Performance performance = performanceRepository.findById(lastPerformanceId - 1).get();
        Assert.assertThat(performance.getEvent().getType(), is(EventType.SECTORS));
        Assert.assertTrue(performance.getDate().isAfter(LocalDateTime.now()));

        Map<String, Long> seatsBySectors = new HashMap<>();
        seatsBySectors.put("A", 0L);
        seatsBySectors.put("B", 0L);

        Response response = responseBuyReservedSectors(seatsBySectors, lastTicketId - 4);

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void searchTicketWorks(){

        Long customerID = firstCustomerIDOfGen;
        Long eventID = firstEventIDOfGen;
        TicketState state = TicketState.BOUGHT;

        TicketSearchDTO ticketSearchDTO = TicketSearchDTO
            .newBuilder()
            .customerID(customerID)
            .eventID(eventID)
            .state(state)
            .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(ticketSearchDTO)
            .when().post(TICKET_ENDPOINT + "/search")
            .then().extract().response();

        List<TicketDTO> ticketDTO = Arrays.asList(response.as(TicketDTO[].class));

        for (TicketDTO dto : ticketDTO) {
            Assert.assertThat(dto.getCustomer().getId(), is(customerID));
            Assert.assertThat(dto.getState(), is(state));
        }
    }

    @Test
    public void searchTicketWorksWithouteventID(){

        Long customerID = firstCustomerIDOfGen;
        Long eventID = null;
        TicketState state = TicketState.BOUGHT;

        TicketSearchDTO ticketSearchDTO = TicketSearchDTO
            .newBuilder()
            .customerID(customerID)
            .eventID(eventID)
            .state(state)
            .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(ticketSearchDTO)
            .when().post(TICKET_ENDPOINT + "/search")
            .then().extract().response();

        List<TicketDTO> ticketDTO = Arrays.asList(response.as(TicketDTO[].class));

        for (TicketDTO dto : ticketDTO) {
            Assert.assertThat(dto.getCustomer().getId(), is(customerID));
            Assert.assertThat(dto.getState(), is(state));
            System.out.println(dto);
        }


    }

    @Test
    public void searchTicketWorksWithouteventIdAndState(){


        Long customerID = firstCustomerIDOfGen;
        Long eventID = null;
        TicketState state = null;

        TicketSearchDTO ticketSearchDTO = TicketSearchDTO
            .newBuilder()
            .customerID(customerID)
            .eventID(eventID)
            .state(state)
            .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(ticketSearchDTO)
            .when().post(TICKET_ENDPOINT + "/search")
            .then().extract().response();

        List<TicketDTO> ticketDTO = Arrays.asList(response.as(TicketDTO[].class));

        for (TicketDTO dto : ticketDTO) {
            Assert.assertThat(dto.getCustomer().getId(), is(customerID));
            System.out.println(dto);
        }
    }

    @Test
    public void searchTicketWorksWithoutAll(){

        Long customerID = null;
        Long eventID = null;
        TicketState state = null;

        TicketSearchDTO ticketSearchDTO = TicketSearchDTO
            .newBuilder()
            .customerID(customerID)
            .eventID(eventID)
            .state(state)
            .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(ticketSearchDTO)
            .when().post(TICKET_ENDPOINT + "/search")
            .then().extract().response();

        List<TicketDTO> ticketDTO = Arrays.asList(response.as(TicketDTO[].class));

        List<TicketDTO> tickets = ticketsMapper.ticketToTicketDTO(ticketRepository.findAll());

        Assert.assertThat(ticketDTO.size(), is(tickets.size()));

        for (TicketDTO dto : ticketDTO) {
            Assert.assertThat(tickets.contains(dto), is(true));
        }
    }

    @Test
    public void searchWorksWithID(){
        TicketDTO ticket = ticketsMapper.ticketToTicketDTO(ticketRepository.findById(lastTicketId).get());

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(TICKET_ENDPOINT + "/search/" + lastTicketId)
            .then().extract().response();

        Assert.assertThat(response.as(TicketDTO.class) , is(ticket));
    }

    @Test
    public void testGetSeats(){
        generatedTestData = false;
        beforeBase();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(TICKET_ENDPOINT + "/getSeats/" + (lastTicketId-11))
            .then().extract().response();
        List<SeatDTO> seats = Arrays.asList(response.as(SeatDTO[].class));

        Assert.assertThat(response.statusCode(), is(HttpStatus.OK.value()));
        Assert.assertTrue(seats.size()>0);
    }

    @Test
    public void testGetSeatsBySectors(){
        generatedTestData = false;
        beforeBase();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(TICKET_ENDPOINT + "/getSeatsBySectors/" + (lastTicketId-5))
            .then().extract().response();

        Map<String, Long> seatsBySectors = response.jsonPath().getMap("", String.class, Long.class);
        Assert.assertThat(response.statusCode(), is(HttpStatus.OK.value()));
        Assert.assertThat((seatsBySectors.get("A")), is(1L));
        Assert.assertThat((seatsBySectors.get("B")), is(2L));
    }

    @Test
    public void testGetPdf(){
        generatedTestData = false;
        beforeBase();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(TICKET_ENDPOINT + "/getBill/" + (lastTicketId-11))
            .then().extract().response();

        byte[] bytes = response.asByteArray();

        Assert.assertThat(response.statusCode(), is(HttpStatus.OK.value()));
        Assert.assertTrue(bytes.length > 0);
    }
}
