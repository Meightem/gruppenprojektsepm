package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.*;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

import static org.hamcrest.core.Is.is;

public class TicketRepositoryTest extends BaseIntegrationTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private PerformanceRepository performanceRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private HallRepository hallRepository;

    @Autowired
    private SeatReservationRepository seatReservationRepository;

    @Autowired
    private SeatRepository seatRepository;

    @Test
    public void testTicketRepository() {
        Customer customer = new Customer();
        customer.setFirstName("franz");
        customer.setLastName("huber");
        customer.setIsAnonymousCustomer(false);

        Customer newCustomer = customerRepository.save(customer);

        Artist artist = artistRepository.save(Artist.newBuilder().firstName("test").lastName("test").build());
        Event event = Event.newBuilder().artist(artist).content("content").
            length(Duration.ofDays(10L)).name("Eventname").type(EventType.SEATS).build();
        event = eventRepository.save(event);

        Hall hall = Hall.newBuilder().setCity("city").setCountry("country").setName("Hallname").setPostcode("1234").setStreet("street").build();
        hall = hallRepository.save(hall);

        Performance performance = Performance.newBuilder().setHall(hall).setDate(LocalDateTime.now().plusDays(1)).setPrice(BigDecimal.TEN)
            .setEvent(event).build();
        performance = performanceRepository.save(performance);

        Ticket ticket = Ticket.newBuilder().customer(newCustomer).state(TicketState.BOUGHT).performance(performance).build();
        Ticket newTicket = ticketRepository.save(ticket);

        Assert.assertThat(ticket, is(ticketRepository.findById(newTicket.getId()).get()));
    }


    @Test
    public void testSeatReservation() {
        Customer customer = new Customer();
        customer.setFirstName("franz");
        customer.setLastName("huber");
        customer.setIsAnonymousCustomer(false);

        Customer newCustomer = customerRepository.save(customer);

        Artist artist = artistRepository.save(Artist.newBuilder().firstName("test").lastName("test").build());
        Event event = Event.newBuilder().artist(artist).content("content").
            length(Duration.ofDays(10L)).name("Eventname").type(EventType.SEATS).build();
        event = eventRepository.save(event);

        Hall hall = Hall.newBuilder().setCity("city").setCountry("country").setName("Hallname").setPostcode("1234").setStreet("street").build();
        hall = hallRepository.save(hall);

        Performance performance = Performance.newBuilder().setHall(hall).setDate(LocalDateTime.now().plusDays(1)).setPrice(BigDecimal.TEN)
            .setEvent(event).build();
        performance = performanceRepository.save(performance);

        Ticket ticket = Ticket.newBuilder().customer(newCustomer).state(TicketState.BOUGHT).performance(performance).build();
        Ticket savedTicket = ticketRepository.save(ticket);
        Hall savedHall = hallRepository.save(hall);

        Seat seat = Seat.newBuilder().setSector(Sector.A).setHall(savedHall).setXCoord(1L).setYCoord(0L).build();
        Seat savedSeat = seatRepository.save(seat);

        SeatReservation seatReservation = SeatReservation.newBuilder().setSeat(savedSeat).setPerformance(performance).setTicket(savedTicket).build();

        SeatReservation savedSeatReservation = seatReservationRepository.save(seatReservation);

        Assert.assertThat(seatReservation, is(seatReservationRepository.findById(savedSeatReservation.getId()).get()));
    }
}
