package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventSearchDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.artist.ArtistMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.events.EventsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.ArtistRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.EventRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.EventService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.time.Duration;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.codehaus.groovy.runtime.InvokerHelper.asList;
import static org.hamcrest.core.Is.is;


public class EventEndpointTest extends BaseIntegrationTest {
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private EventsMapper eventsMapper;
    @Autowired
    private EventService eventService;
    @Autowired
    private ArtistRepository artistRepository;
    @Autowired
    private ArtistMapper artistMapper;

    private static final String EVENTS_ENDPOINT = "/events";
    private static final String SPECIFIC_EVENTS_PATH = "/{eventId}";
    private static final String ARTIST_PATH = "/artist";

    private int getRandomNumber() {
        return (int) Math.round(Math.random() * 1000);
    }

    private EventDTO getRandomNewEvent() {
        ArtistDTO artistDTO = ArtistDTO
            .newBuilder()
            .firstName("ArtistsFirstname" + getRandomNumber())
            .lastName("ArtistsLastname" + getRandomNumber())
            .build();

        return getEvent(artistDTO);
    }

    private EventDTO getEvent(ArtistDTO artistDTO){
        return EventDTO.newBuilder()
            .artist(artistDTO)
            .name("SuperKrasseVeranstaltung" + getRandomNumber())
            .length(Duration.ofMinutes(getRandomNumber()))
            .content("Konzert")
            .type(EventType.SEATS)
            .build();
    }

    private EventDTO getEvent(String name, String content, Duration duration, EventType eventType, ArtistDTO artistDTO){
        return EventDTO.newBuilder()
            .artist(artistDTO)
            .name(name)
            .length(duration)
            .content(content)
            .type(eventType)
            .build();
    }

    private Response getAndSaveEvent(String name, String content, Duration duration, EventType eventType){
        return createEvent(validAdminTokenWithPrefix,getEvent(name,content,duration,eventType,ArtistDTO.newBuilder().firstName("test").lastName("test").build()));
    }

    private Response createRandomEvent(String token) {
        return createEvent(token, getRandomNewEvent());
    }

    private Response createEvent(String token,EventDTO eventDTO){
        return RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, token)
            .body(eventDTO)
            .when().post(EVENTS_ENDPOINT)
            .then().extract().response();
    }

    private List<EventDTO> saveRandomEvents() {
        List<EventDTO> retVal = new LinkedList<>();
        for (int i = 0; i < 5; i++) {
            EventDTO randomNewEvent = getRandomNewEvent();
            randomNewEvent = eventsMapper.eventToEventDTO(eventService.createEvent(eventsMapper.eventDTOtoEvent(randomNewEvent)));
            retVal.add(randomNewEvent);
        }
        return retVal;
    }

    private Response findAllEvents(String token) {
        return RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, token)
            .when().get(EVENTS_ENDPOINT)
            .then().extract().response();
    }

    private void testFindAllEvents(String token) {
        List<EventDTO> savedevents = saveRandomEvents();
        Response response = findAllEvents(token);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        List<EventDTO> foundevents = asList(response.as(EventDTO[].class));
        Assert.assertEquals(savedevents.size(), foundevents.size());
        for (EventDTO event : foundevents) {
            Assert.assertTrue(savedevents.contains(event));
        }
    }

    private Response findEvent(Long eventId, String token) {
        return RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, token)
            .when().get(EVENTS_ENDPOINT + SPECIFIC_EVENTS_PATH, eventId)
            .then().extract().response();
    }

    private void assertEventSameFindEvent(String token) {
        Response response = createRandomEvent(validAdminTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        EventDTO savedPer = response.as(EventDTO.class);

        response = findEvent(savedPer.getId(), token);
        Assert.assertThat(response.as(EventDTO.class), is(savedPer));
    }


    @Test
    public void testEventRepository() {
        Event event = eventsMapper.eventDTOtoEvent(getRandomNewEvent());
        event.setArtist(artistRepository.save(event.getArtist()));
        Event savedEvent = eventRepository.save(event);
        Assert.assertThat(event, is(eventRepository.findById(savedEvent.getId()).get()));
    }

    @Test
    public void createEventWorksAsAdmin() {
        Response response = createRandomEvent(validAdminTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
    }

    @Test
    public void createEventDoesNotWorkAsUser() {
        Response response = createRandomEvent(validUserTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN.value()));
    }

    @Test
    public void createEventDoesNotWorkAsAnonymous() {
        Response response = createRandomEvent("");
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
    }

    @Test
    public void findAllEventsWorksAsAdmin() {
        testFindAllEvents(validAdminTokenWithPrefix);
    }

    @Test
    public void findAllEventsWorksAsUser() {
        testFindAllEvents(validUserTokenWithPrefix);
    }

    @Test
    public void findAllEventsDoesNotWorkAsAnonymous() {
        Response response = findAllEvents("");
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
    }

    @Test
    public void findOneEventWorksAsAdmin() {
        assertEventSameFindEvent(validAdminTokenWithPrefix);
    }

    @Test
    public void findOneEventWorksAsUser() {
        assertEventSameFindEvent(validUserTokenWithPrefix);
    }

    @Test
    public void findOneEventDoesNotWorkAsAnonymous() {
        Response response = findEvent(100L, "");
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
    }

    @Test
    public void createArtistWithEventWorks(){
        Assert.assertThat(artistRepository.findAll().size(), is(0));
        EventDTO eventDTO = getRandomNewEvent();
        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .body(eventDTO)
                .when().post(EVENTS_ENDPOINT)
                .then().extract().response();
        Assert.assertThat(artistRepository.findAll().size(), is(1));
        Assert.assertThat(response.as(EventDTO.class).getArtist(), is(artistMapper.artistToArtistDTO(artistRepository.findAll().get(0))));
    }

    @Test
    public void MapArtistWithEventWorksIfArtistAlreadyExists(){
        Assert.assertThat(artistRepository.findAll().size(), is(0));
        createRandomEvent(validAdminTokenWithPrefix);
        Assert.assertThat(artistRepository.findAll().size(), is(1));

        EventDTO eventDTO = getRandomNewEvent();
        eventDTO.setArtist(artistMapper.artistToArtistDTO(artistRepository.findAll().get(0)));
        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .body(eventDTO)
                .when().post(EVENTS_ENDPOINT)
                .then().extract().response();
        Assert.assertThat(artistRepository.findAll().size(), is(1));
        Assert.assertThat(response.as(EventDTO.class).getArtist(), is(artistMapper.artistToArtistDTO(artistRepository.findAll().get(0))));
    }

    @Test
    public void findALlEventsByArtistWorks(){
        ArtistDTO artistDTO =
            ArtistDTO
                .newBuilder()
                .firstName("Peter")
                .lastName("Lustig")
                .build();
        for (int i = 0; i < 10; i++) {
            createEvent(validAdminTokenWithPrefix,getEvent(artistDTO));
        }

        long id = artistRepository.findExact("Peter","Lustig").get(0).getId();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().get(EVENTS_ENDPOINT+ARTIST_PATH+"/"+id)
                .then().extract().response();
        List<EventDTO> eventDTOS = Arrays.asList(response.as(EventDTO[].class));
        Assert.assertThat(eventDTOS.size(), is(10));

        for (int i = 0; i < 10; i++) {
            Assert.assertThat(eventDTOS.get(i).getArtist().getId(), is(id));
        }
    }

    @Test
    public void findALlEventsByArtistWorksWithInterference(){
        ArtistDTO artistDTO =
            ArtistDTO
                .newBuilder()
                .firstName("Peter")
                .lastName("Lustig")
                .build();
        for (int i = 0; i < 10; i++) {
            createEvent(validAdminTokenWithPrefix,getEvent(artistDTO));
        }

        ArtistDTO artistDTO2 =
            ArtistDTO
                .newBuilder()
                .firstName("Peter")
                .lastName("Lustiger")
                .build();
        for (int i = 0; i < 10; i++) {
            createEvent(validAdminTokenWithPrefix,getEvent(artistDTO2));
        }

        long id = artistRepository.findExact("Peter","Lustig").get(0).getId();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().get(EVENTS_ENDPOINT+ARTIST_PATH+"/"+id)
                .then().extract().response();
        List<EventDTO> eventDTOS = Arrays.asList(response.as(EventDTO[].class));
        Assert.assertThat(eventDTOS.size(), is(10));

        for (int i = 0; i < 10; i++) {
            Assert.assertThat(eventDTOS.get(i).getArtist().getId(), is(id));
        }
    }

    @Test
    public void searchNameWorks(){
        Response createResponse = getAndSaveEvent("EventName","test",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .name("EventName")
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).get(0), is(createResponse.as(EventDTO.class)));

    }

    @Test
    public void searchContentWorks(){
        Response createResponse = getAndSaveEvent("EventName","This is just test a sentence",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .content("this is just")
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).get(0), is(createResponse.as(EventDTO.class)));

    }

    @Test
    public void searchDurationWorks(){
        Response createResponse = getAndSaveEvent("EventName","This is just test a sentence",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .length(Duration.ofMinutes(10L))
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).get(0), is(createResponse.as(EventDTO.class)));
    }

    @Test
    public void searchDurationWorksInRange(){
        Response createResponse = getAndSaveEvent("EventName","This is just test a sentence",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .length(Duration.ofMinutes(40L))
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).get(0), is(createResponse.as(EventDTO.class)));
    }
    @Test
    public void searchDurationDoesNotWorkOutsideRange(){
        Response createResponse = getAndSaveEvent("EventName","This is just test a sentence",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .length(Duration.ofMinutes(41L))
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).size(), is(0));
    }

    @Test
    public void searchTypeWorks(){
        Response createResponse = getAndSaveEvent("EventName","This is just test a sentence",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .type(EventType.SECTORS)
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).get(0), is(createResponse.as(EventDTO.class)));
    }
    @Test
    public void searchTypeDoewsNotWork(){
        Response createResponse = getAndSaveEvent("EventName","This is just test a sentence",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .type(EventType.SEATS)
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).size(), is(0));
    }

    @Test
    public void CombinedSearchWorks(){
        Response createResponse = getAndSaveEvent("EventName","This is just test a sentence",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .name("EventName")
            .content("This is just test a sentence")
            .type(EventType.SECTORS)
            .length(Duration.ofMinutes(10L))
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).get(0), is(createResponse.as(EventDTO.class)));
    }

    @Test
    public void CombinedSearchDoesNotWorkWithOneCriteraNotMet1(){
        Response createResponse = getAndSaveEvent("EventName","This is just test a sentence",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .name("EventName")
            .content("This is just test a sentence")
            .type(EventType.SECTORS)
            .length(Duration.ofMinutes(1000L))
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).size(), is(0));
    }

    @Test
    public void CombinedSearchDoesNotWorkWithOneCriteraNotMet2(){
        Response createResponse = getAndSaveEvent("EventName","This is just test a sentence",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .name("EventName")
            .content("This is just test a sentence")
            .type(EventType.SEATS)
            .length(Duration.ofMinutes(10L))
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).size(), is(0));
    }

    @Test
    public void CombinedSearchDoesNotWorkWithOneCriteraNotMet3(){
        Response createResponse = getAndSaveEvent("EventName","This is just test a sentence",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .name("EventName")
            .content("This is just test a sentenceasdasddaaa")
            .type(EventType.SECTORS)
            .length(Duration.ofMinutes(10L))
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).size(), is(0));
    }

    @Test
    public void CombinedSearchDoesNotWorkWithOneCriteraNotMet4(){
        Response createResponse = getAndSaveEvent("EventName","This is just test a sentence",Duration.ofMinutes(10L),EventType.SECTORS);


        EventSearchDTO eventSearchDTO = EventSearchDTO
            .newBuilder()
            .name("EventNameaasdasd")
            .content("This is just test a sentence")
            .type(EventType.SECTORS)
            .length(Duration.ofMinutes(10L))
            .build();

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(eventSearchDTO)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().post(EVENTS_ENDPOINT+"/search")
                .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).size(), is(0));
    }

    @Test
    public void nameSeachWorks(){
        EventDTO eventDTO = createRandomEvent(validAdminTokenWithPrefix).as(EventDTO.class);

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().get(EVENTS_ENDPOINT+"/search/"+eventDTO.getName())
                .then().extract().response();

        Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).get(0).getId(), is(eventDTO.getId()));
    }

    @Test
    public void nameSeachWorksNot(){
        EventDTO eventDTO = createRandomEvent(validAdminTokenWithPrefix).as(EventDTO.class);

        Response response =
            RestAssured
                .given()
                .contentType(ContentType.JSON)
                .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
                .when().get(EVENTS_ENDPOINT+"/search/"+eventDTO.getName()+"test")
                .then().extract().response();

        Assert.assertTrue(Arrays.asList(response.as(EventDTO[].class)).size() == 0);
    }

}
