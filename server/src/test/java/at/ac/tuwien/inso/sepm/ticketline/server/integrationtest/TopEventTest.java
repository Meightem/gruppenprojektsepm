package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.TopEventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.events.EventsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.events.TopEventMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.*;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.queryResults.TopEvent;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;

public class TopEventTest extends BaseIntegrationTest {

    @Autowired
    HallRepository hallRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    PerformanceRepository performanceRepository;

    @Autowired
    SeatReservationRepository seatReservationRepository;

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    SeatRepository seatRepository;

    @Autowired
    TopEventMapper topEventMapper;

    @Autowired
    EventsMapper eventsMapper;

    @Autowired
    ArtistRepository artistRepository;

    private static final int MONTH = 11;
    private static final int YEAR = 1996;
    private static final String EVENT_ENDPOINT = "events/";
    private int counter =0;

    @Test
    public void repositoryWorks(){

        ArrayList<Event> events = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            events.add(generateTestData(EventType.SEATS,i+1,11,1996));
        }
        List<TopEvent> list =  eventRepository.getTopEvents(MONTH, YEAR, EventType.SEATS);
        Assert.assertThat(list.size(), is(11));

        for (int i = 0; i < 11; i++) {
            Assert.assertThat(list.get(i).getEvent(), is(events.get(10-i)));
            Assert.assertThat(list.get(i).getCount(), is((long) 11 - i));
        }
    }

    @Test
    public void repositoryWorksWithInterferenceOfOtherCategories(){

        ArrayList<Event> events = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            events.add(generateTestData(EventType.SEATS,i+1,11,1996));
        }
        for (int i = 0; i < 20; i++) {
            generateTestData(EventType.SECTORS,i+1,MONTH,YEAR);
        }
        List<TopEvent> list =  eventRepository.getTopEvents(MONTH, YEAR, EventType.SEATS);
        Assert.assertThat(list.size(), is(11));

        for (int i = 0; i < 11; i++) {
            Assert.assertThat(list.get(i).getEvent(), is(events.get(10-i)));
            Assert.assertThat(list.get(i).getCount(), is((long) 11 - i));
        }
    }

    @Test
    public void repositoryWorksWithInterferenceOfSameCategoryOfOtherMonth(){

        ArrayList<Event> events = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            events.add(generateTestData(EventType.SEATS,i+1,11,1996));
        }
        generateTestData(EventType.SEATS,100,MONTH+1,YEAR);
        List<TopEvent> list =  eventRepository.getTopEvents(MONTH, YEAR, EventType.SEATS);
        Assert.assertThat(list.size(), is(11));

        for (int i = 0; i < 11; i++) {
            Assert.assertThat(list.get(i).getEvent(), is(events.get(10-i)));
            Assert.assertThat(list.get(i).getCount(), is((long) 11 - i));
        }
    }

    @Test
    public void repositoryOnlyReturnsEventsWithTickets(){
        ArrayList<Event> events = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            events.add(generateTestData(EventType.SEATS,i-3,11,1996));
        }
        List<TopEvent> list =  eventRepository.getTopEvents(MONTH, YEAR, EventType.SEATS);
        Assert.assertThat(list.size(), is(7));
    }

    @Transactional
    public Event generateTestData(EventType eventType,int seatNum, int month, int year) {

        Artist artist = artistRepository.save(Artist.newBuilder().firstName("test" + counter++).lastName("test" + counter).build());

        Event event = Event.newBuilder()
            .artist(artist)
            .content("test")
            .name("test")
            .type(eventType)
            .length(Duration.ZERO)
            .performances(null)
            .build();
        event = eventRepository.save(event);

        Customer customer = new Customer();
        customer.setFirstName("test");
        customer.setLastName("lastName");
        customer.setIsAnonymousCustomer(false);
        customer = customerRepository.save(customer);


        Hall hall =
            Hall.newBuilder()
                .setCity("test")
                .setCountry("test")
                .setName("test")
                .setPerformances(null)
                .setPostcode("3011")
                .setSeats(null)
                .setStreet("test")
                .build();
        hall = hallRepository.save(hall);

        Performance performance =
            Performance.newBuilder()
                .setDate(LocalDateTime.of(LocalDate.of(year,month,15),LocalTime.NOON))
                .setEvent(event)
                .setHall(hall)
                .setPrice(new BigDecimal(1))
                .setSeatReservations(null)
                .build();
        performance = performanceRepository.save(performance);

        for (int i = 0; i < seatNum; i++) {
            Seat seat =
                Seat.newBuilder()
                    .setSeatReservations(null)
                    .setHall(hall)
                    .setSector(Sector.A)
                    .setXCoord(i+0l)
                    .setYCoord(0l)
                    .build();
            seat = seatRepository.save(seat);

            Ticket ticket =
                Ticket.newBuilder()
                    .customer(customer)
                    .seatReservations(null)
                    .state(TicketState.BOUGHT)
                    .performance(performance)
                    .build();
            ticket = ticketRepository.save(ticket);

            SeatReservation seatReservation =
                SeatReservation.newBuilder()
                    .setSeat(seat)
                    .setPerformance(performance)
                    .setTicket(ticket)
                    .build();
            seatReservation = seatReservationRepository.save(seatReservation);
        }
        return event;
    }


    @Test
    public void endpointTruncatesResponse(){

        ArrayList<Event> events = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            events.add(generateTestData(EventType.SEATS,i+1,MONTH,YEAR));
        }

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(EVENT_ENDPOINT+"top/"+EventType.SEATS +"/"+MONTH+"/"+YEAR)
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        List<TopEventDTO> topEventDTOSResponse = Arrays.asList(response.as(TopEventDTO[].class));

        Assert.assertThat(topEventDTOSResponse.size(), is(10));
    }

    @Test
    public void endpointSortsResponse(){

        ArrayList<Event> events = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            events.add(generateTestData(EventType.SEATS,i+1,MONTH,YEAR));
        }

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(EVENT_ENDPOINT+"top/"+EventType.SEATS +"/"+MONTH+"/"+YEAR)
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        List<TopEventDTO> topEventDTOSResponse = Arrays.asList(response.as(TopEventDTO[].class));

        for (int i = 0; i < topEventDTOSResponse.size()-1; i++) {
            Assert.assertTrue(topEventDTOSResponse.get(i).getCount() > topEventDTOSResponse.get(i+1).getCount());
        }
    }

    @Test
    public void endpointGetsRightTopEvents(){
        ArrayList<Event> events = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            events.add(generateTestData(EventType.SEATS,i+1,MONTH,YEAR));
        }

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(EVENT_ENDPOINT+"top/"+EventType.SEATS +"/"+MONTH+"/"+YEAR)
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        List<TopEventDTO> topEventDTOSResponse = Arrays.asList(response.as(TopEventDTO[].class));

        for (int i = 0; i < topEventDTOSResponse.size(); i++) {
            Assert.assertThat(topEventDTOSResponse.get(i).getEvent(), is(eventsMapper.eventToEventDTO(events.get(10-i))));
            Assert.assertThat(topEventDTOSResponse.get(i).getCount(), is(11-i));
        }
    }
}
