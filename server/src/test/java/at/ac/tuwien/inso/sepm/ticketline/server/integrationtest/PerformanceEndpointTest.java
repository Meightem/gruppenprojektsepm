package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatState;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Artist;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Performance;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.events.EventsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.halls.HallsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.performances.PerformancesMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.ArtistRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.EventRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.HallRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.PerformanceRepository;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;

public class PerformanceEndpointTest extends BaseIntegrationTest {
    @Autowired
    private PerformanceRepository performanceRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private HallRepository hallRepository;

    @Autowired
    private SeatRepository seatRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private SeatReservationRepository seatReservationRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private PerformancesMapper performancesMapper;
    @Autowired
    private EventsMapper eventsMapper;
    @Autowired
    private HallsMapper hallsMapper;

    private static final String BY_HALL_PATH = "/byHall/";
    private static final String PERFORMANCES_ENDPOINT = "/performances";
    private static final String PATH_SEATS = "/seats/{performanceId}";
    private static final String PATH_SEATS_BY_SECTORS = "/seats/bysector/{performanceId}";

    @Autowired
    private ArtistRepository artistRepository;

    private int count = 0;

    private static final String PERFORMANCE_ENDPOINT = "/performances";
    private static final String SPECIFIC_PERFORMANCE_PATH = "/{performanceId}";
    private static final String BY_EVENT_PATH = "/byEvent/{eventId}";


    private int getRandomNumber() {
        return (int) Math.round(Math.random() * 1000);
    }

    private PerformanceDTO getRandomPerformanceDTO() {

        Artist artist = artistRepository.save(Artist.newBuilder().firstName("test"+count++).lastName("test").build());
        Event event = Event.newBuilder().artist(artist).content("content").
            length(Duration.ofDays(10L)).name("Eventname" + getRandomNumber()).type(EventType.SEATS).build();
        event = eventRepository.save(event);

        Hall hall = Hall.newBuilder().setCity("city").setCountry("country").setName("Hallname" + getRandomNumber()).setPostcode("1234").setStreet("street").build();
        hall = hallRepository.save(hall);

        return PerformanceDTO.newBuilder()
            .setDate(LocalDateTime.now())
            .setPrice(new BigDecimal("99.99"))
            .setEvent(eventsMapper.eventToEventDTO(event))
            .setHall(hallsMapper.hallToHallDTO(hall))
            .build();
    }

    private Response createPerformance(String token) {
        return RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, token)
            .body(getRandomPerformanceDTO())
            .when().post(PERFORMANCE_ENDPOINT)
            .then().extract().response();
    }

    private Response findPerformance(Long performanceId, String token) {
        return RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, token)
            .when().get(PERFORMANCE_ENDPOINT + SPECIFIC_PERFORMANCE_PATH, performanceId)
            .then().extract().response();
    }

    private Response findByEvent(Long eventId, String token) {
        return RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, token)
            .when().get(PERFORMANCE_ENDPOINT + BY_EVENT_PATH, eventId)
            .then().extract().response();
    }

    private void assertPerformanceSameFindPerformance(String token) {
        Response response = createPerformance(validAdminTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        PerformanceDTO savedPer = response.as(PerformanceDTO.class);

        response = findPerformance(savedPer.getId(), token);
        Assert.assertThat(response.as(PerformanceDTO.class), is(savedPer));
    }

    @Test
    public void testPerformanceRepository() {

        Artist artist = artistRepository.save(Artist.newBuilder().firstName("test").lastName("test").build());
        Event event = Event.newBuilder().artist(artist).content("content").
            length(Duration.ofDays(10L)).name("Eventname" + getRandomNumber()).type(EventType.SEATS).build();
        Event savedEvent = eventRepository.save(event);

        Hall hall = Hall.newBuilder().setCity("city").setCountry("country").setName("name").setPostcode("1234").setStreet("street").build();

        Hall savedHall = hallRepository.save(hall);
        Performance performance = Performance.newBuilder().setDate(LocalDateTime.of(2018, 5, 12, 12, 55)).setPrice(BigDecimal.TEN.setScale(2)).setEvent(savedEvent).setHall(savedHall).build();
        Performance savedPerformance = performanceRepository.save(performance);
        Assert.assertThat(performance, is(performanceRepository.findById(savedPerformance.getId()).get()));
    }

    private Performance generateTestData(){
        Customer customer = new Customer();
        customer.setLastName("lastname");
        customer.setFirstName("fistname");
        customer.setIsAnonymousCustomer(false);
        customerRepository.save(customer);



        Artist artist = Artist.newBuilder().firstName("fn").lastName("ln").build();
        artistRepository.save(artist);

        Event event = Event.newBuilder().type(EventType.SEATS).name("name").length(Duration.ofDays(2)).content("content").artist(artist).build();
        eventRepository.save(event);

        Hall hall = Hall.newBuilder().setStreet("street").setPostcode("1234").setName("name")
            .setCountry("country").setCity("city").build();

        hallRepository.save(hall);

        Performance performance = Performance.newBuilder().setEvent(event).setPrice(BigDecimal.ONE)
            .setDate(LocalDateTime.of(1234,12,12,12,12))
            .setHall(hall).build();

        performanceRepository.save(performance);

        Ticket ticketBought = Ticket.newBuilder().customer(customer).state(TicketState.BOUGHT).performance(performance).build();
        Ticket ticketReserved = Ticket.newBuilder().customer(customer).state(TicketState.RESERVED).performance(performance).build();
        Ticket ticketCancelled = Ticket.newBuilder().customer(customer).state(TicketState.CANCELLED).performance(performance).build();
        ticketRepository.save(ticketBought);
        ticketRepository.save(ticketReserved);
        ticketRepository.save(ticketCancelled);

        Seat seatBought = Seat.newBuilder().setSector(Sector.A).setHall(hall)
            .setXCoord(0L).setYCoord(1L).build();
        Seat seatReserved = Seat.newBuilder().setSector(Sector.B).setHall(hall)
            .setXCoord(1L).setYCoord(1L).build();
        Seat seatCancelled = Seat.newBuilder().setSector(Sector.A).setHall(hall)
            .setXCoord(1L).setYCoord(0L).build();
        Seat seatFree = Seat.newBuilder().setSector(Sector.B).setHall(hall)
            .setXCoord(0L).setYCoord(0L).build();
        seatRepository.save(seatBought);
        seatRepository.save(seatReserved);
        seatRepository.save(seatCancelled);
        seatRepository.save(seatFree);

        SeatReservation seatReservationBought = SeatReservation.newBuilder().setSeat(seatBought).setTicket(ticketBought).setPerformance(performance).build();
        SeatReservation seatReservationReserved = SeatReservation.newBuilder().setSeat(seatReserved).setTicket(ticketReserved).setPerformance(performance).build();
        SeatReservation seatReservationCancelled = SeatReservation.newBuilder().setSeat(seatCancelled).setTicket(ticketCancelled).setPerformance(performance).build();

        seatReservationRepository.save(seatReservationBought);
        seatReservationRepository.save(seatReservationCancelled);
        seatReservationRepository.save(seatReservationReserved);

        return performance;
    }

    @Test
    public void testGetSeatsForPerformance(){
        Performance performance = generateTestData();

        Response response = RestAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .contentType(ContentType.JSON)
            .when().get(PERFORMANCES_ENDPOINT + PATH_SEATS,performance.getId())
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        List<SeatDTO> seats = response.jsonPath().getList("", SeatDTO.class);
        List<SeatDTO> seatsExpected = new ArrayList<>();
        seatsExpected.add(SeatDTO.newBuilder().sector(Sector.B).id_hall(performance.getHall().getId()).xCoord(0L).yCoord(0L).state(SeatState.FREE).build());
        seatsExpected.add(SeatDTO.newBuilder().sector(Sector.A).id_hall(performance.getHall().getId()).xCoord(1L).yCoord(0L).state(SeatState.FREE).build());
        seatsExpected.add(SeatDTO.newBuilder().sector(Sector.A).id_hall(performance.getHall().getId()).xCoord(0L).yCoord(1L).state(SeatState.BOUGHT).build());
        seatsExpected.add(SeatDTO.newBuilder().sector(Sector.B).id_hall(performance.getHall().getId()).xCoord(1L).yCoord(1L).state(SeatState.RESERVED).build());

        Assert.assertThat(seats, is(seatsExpected));
    }

    @Test
    public void testGetSeatsForOnlyThisPerformance(){
        Artist artist = Artist.newBuilder().firstName("Alex").lastName("SuperCOOL").build();
        artistRepository.save(artist);

        Event event = Event.newBuilder().type(EventType.SEATS).name("DESGEILEEVENT").length(Duration.ofDays(14)).content("14 tage vollgas").artist(artist).build();
        eventRepository.save(event);

        Hall hall = Hall.newBuilder().setStreet("Hauptstraße").setPostcode("1234").setName("DieSauCooleHalle")
            .setCountry("Österreich").setCity("Wien").build();

        hall = hallRepository.save(hall);

        Performance performance = Performance.newBuilder().setEvent(event).setPrice(BigDecimal.ONE)
            .setDate(LocalDateTime.of(2019,8,12,12,12))
            .setHall(hall).build();

        performance =  performanceRepository.save(performance);


        Hall hall2 = Hall.newBuilder().setStreet("Hauptstraße").setPostcode("5678").setName("DieSauSchlechteHalle")
            .setCountry("Deutschland").setCity("Berlin").build();

       hall2 = hallRepository.save(hall2);

        Performance performance2 = Performance.newBuilder().setEvent(event).setPrice(BigDecimal.ONE)
            .setDate(LocalDateTime.of(2019,8,19,12,12))
            .setHall(hall2).build();
        performance2 =  performanceRepository.save(performance2);

        Seat seat = Seat.newBuilder().setSector(Sector.A).setHall(hall)
            .setXCoord(0L).setYCoord(0L).build();
        seat =  seatRepository.save(seat);

        Seat seat2 = Seat.newBuilder().setSector(Sector.B).setHall(hall2)
            .setXCoord(12L).setYCoord(12L).build();
        seat2 =  seatRepository.save(seat2);


        Response response = RestAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .contentType(ContentType.JSON)
            .when().get(PERFORMANCES_ENDPOINT + PATH_SEATS,performance.getId())
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        List<SeatDTO> seats = response.jsonPath().getList("", SeatDTO.class);
        List<SeatDTO> seatsExpected = new ArrayList<>();
        seatsExpected.add(SeatDTO.newBuilder().sector(Sector.A).id_hall(performance.getHall().getId()).xCoord(0L).yCoord(0L).state(SeatState.FREE).build());

        Assert.assertThat(seats, is(seatsExpected));
    }

    @Test
    public void testGetSeatsForPerformanceBySector(){
        Performance performance = generateTestData();

        Response response = RestAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .contentType(ContentType.JSON)
            .when().get(PERFORMANCES_ENDPOINT + PATH_SEATS_BY_SECTORS,performance.getId())
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        Map<String, Long> seatsBySector = response.jsonPath().getMap("", String.class, Long.class);

        Map<String, Long> seatsBySectorExpected = new TreeMap<>();
        seatsBySectorExpected.put("A", 1L);
        seatsBySectorExpected.put("B", 1L);

        Assert.assertThat(seatsBySector, is(seatsBySectorExpected));
    }

    @Test
    public void createPerformanceWorksAsAdmin() {
        Response response = createPerformance(validAdminTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        PerformanceDTO savedPer = response.as(PerformanceDTO.class);

        response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .when().get(PERFORMANCE_ENDPOINT)
            .then().extract().response();
        Assert.assertThat(Arrays.asList(response.as(PerformanceDTO[].class)), is(singletonList(savedPer)));
    }

    @Test
    public void createPerformanceDoesNotWorkAsUser() {
        Response response = createPerformance(validUserTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN.value()));
    }

    @Test
    public void createPerformanceDoesNotWorkAsAnonymous() {
        Response response = createPerformance("");
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
    }

    @Test
    public void findOnePerformanceWorksAsAdmin() {
        assertPerformanceSameFindPerformance(validAdminTokenWithPrefix);
    }

    @Test
    public void findOnePerformanceWorksAsUser() {
        assertPerformanceSameFindPerformance(validUserTokenWithPrefix);
    }

    @Test
    public void findOnePerformanceDoesNotWorkAsAnonymous() {
        Response response = findPerformance(100L, "");
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
    }

    @Test
    public void findByEventIdWorks() {
        Response response = createPerformance(validAdminTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        response = createPerformance(validAdminTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        PerformanceDTO savedPer = response.as(PerformanceDTO.class);

        response = findByEvent(savedPer.getEvent().getId(), validAdminTokenWithPrefix);
        List<PerformanceDTO> pers = Arrays.asList(response.as(PerformanceDTO[].class));

        Assert.assertThat(pers, is(singletonList(savedPer)));
    }

    private int counter = 0;

    private class Tuple<X, Y> {
        public final X x;
        public final Y y;
        public Tuple(X x, Y y) {
            this.x = x;
            this.y = y;
        }
    }

    @Transactional
    public Tuple<Performance,Hall> generateTestData(Hall hall) {

        Artist artist = artistRepository.save(Artist.newBuilder().firstName("test" + counter++).lastName("test" + counter).build());

        Event event = Event.newBuilder()
            .artist(artist)
            .content("test")
            .name("test")
            .type(EventType.SEATS)
            .length(Duration.ZERO)
            .performances(null)
            .build();
        event = eventRepository.save(event);

        Customer customer = new Customer();
        customer.setFirstName("test");
        customer.setLastName("lastName");
        customer.setIsAnonymousCustomer(false);
        customer = customerRepository.save(customer);

        if(hall == null) {
            hall =
                Hall.newBuilder()
                    .setCity("test")
                    .setCountry("test")
                    .setName("test")
                    .setPerformances(null)
                    .setPostcode("3011")
                    .setSeats(null)
                    .setStreet("test")
                    .build();
            hall = hallRepository.save(hall);
        }

        Performance performance =
            Performance.newBuilder()
                .setDate(LocalDateTime.of(LocalDate.of(1000,10,15),LocalTime.NOON))
                .setEvent(event)
                .setHall(hall)
                .setPrice(new BigDecimal(1))
                .setSeatReservations(null)
                .build();
        performance = performanceRepository.save(performance);

        for (int i = 0; i < 1; i++) {
            Seat seat =
                Seat.newBuilder()
                    .setSeatReservations(null)
                    .setHall(hall)
                    .setSector(Sector.A)
                    .setXCoord(i+0l)
                    .setYCoord(0l)
                    .build();
            seat = seatRepository.save(seat);

            Ticket ticket =
                Ticket.newBuilder()
                    .customer(customer)
                    .seatReservations(null)
                    .state(TicketState.BOUGHT)
                    .performance(performance)
                    .build();
            ticket = ticketRepository.save(ticket);

            SeatReservation seatReservation =
                SeatReservation.newBuilder()
                    .setSeat(seat)
                    .setPerformance(performance)
                    .setTicket(ticket)
                    .build();
            seatReservation = seatReservationRepository.save(seatReservation);
        }
        return new Tuple<>(performance,hall);
    }


    @Test
    public void getPerformancesOfHallWorks(){
        List<Performance> performances = new ArrayList<>();
        Hall hall = null;

        for (int i = 0; i < 10; i++) {
            var res = generateTestData(hall);
            hall = res.y;
            performances.add(res.x);
        }

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .when().get(PERFORMANCE_ENDPOINT + BY_HALL_PATH + hall.getId())
            .then().extract().response();

        List<PerformanceDTO> results = Arrays.asList(response.as(PerformanceDTO[].class));

        Assert.assertThat(results.size(), is(10));
    }

    @Test
    public void getPerformancesOfHallWorksWithInteference(){
        List<Performance> performances = new ArrayList<>();
        Hall hall = null;

        for (int i = 0; i < 10; i++) {
            var res = generateTestData(hall);
            hall = res.y;
            performances.add(res.x);
        }

        for (int i = 0; i < 2; i++) {
            generateTestData(null);
        }

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .when().get(PERFORMANCE_ENDPOINT + BY_HALL_PATH + hall.getId())
            .then().extract().response();

        List<PerformanceDTO> results = Arrays.asList(response.as(PerformanceDTO[].class));

        Assert.assertThat(results.size(), is(10));
    }

    @Test
    public void getPerformancesOfHallWorksWithOnePerformance(){
        Performance performance;
        Hall hall = null;
            var res = generateTestData(hall);
            hall = res.y;
            performance = res.x;

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .when().get(PERFORMANCE_ENDPOINT + BY_HALL_PATH + hall.getId())
            .then().extract().response();

        PerformanceDTO result = Arrays.asList(response.as(PerformanceDTO[].class)).get(0);

        Assert.assertThat(result.getDate(), is(performance.getDate()));
        Assert.assertThat(result.getEvent(), is(eventsMapper.eventToEventDTO(performance.getEvent())));
        Assert.assertThat(result.getHall(), is(hallsMapper.hallToHallDTO(performance.getHall())));
        Assert.assertThat(result.getPrice().doubleValue(), is(performance.getPrice().doubleValue()));
        Assert.assertThat(result.getId(), is(performance.getId()));
    }

    @Test
    public void searchWorksWithDate(){
        Performance performance = generateTestData();
        PerformanceDTO performanceDTO =
            PerformanceDTO.newBuilder()
            .setDate(performance.getDate())
            .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(performanceDTO)
            .when().post(PERFORMANCE_ENDPOINT + "/search")
            .then().extract().response();

        List<PerformanceDTO> performanceDTOList = Arrays.asList(response.as(PerformanceDTO[].class));
        Assert.assertThat(performanceDTOList.get(0).getId(), is(performance.getId()));
    }

    @Test
    public void searchWorksWithDateNot(){
        Performance performance = generateTestData();
        PerformanceDTO performanceDTO =
            PerformanceDTO.newBuilder()
                .setDate(performance.getDate().plusHours(2))
                .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(performanceDTO)
            .when().post(PERFORMANCE_ENDPOINT + "/search")
            .then().extract().response();

        List<PerformanceDTO> performanceDTOList = Arrays.asList(response.as(PerformanceDTO[].class));
        Assert.assertThat(performanceDTOList.size(), is(0));
    }

    @Test
    public void searchWorksWithPrice(){
        Performance performance = generateTestData();
        PerformanceDTO performanceDTO =
            PerformanceDTO.newBuilder()
                .setPrice(performance.getPrice())
                .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(performanceDTO)
            .when().post(PERFORMANCE_ENDPOINT + "/search")
            .then().extract().response();

        List<PerformanceDTO> performanceDTOList = Arrays.asList(response.as(PerformanceDTO[].class));
        Assert.assertThat(performanceDTOList.get(0).getId(), is(performance.getId()));
    }

    @Test
    public void searchWorksWithPriceNot(){
        Performance performance = generateTestData();
        PerformanceDTO performanceDTO =
            PerformanceDTO.newBuilder()
                .setPrice(performance.getPrice().add(performance.getPrice()))
                .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(performanceDTO)
            .when().post(PERFORMANCE_ENDPOINT + "/search")
            .then().extract().response();

        List<PerformanceDTO> performanceDTOList = Arrays.asList(response.as(PerformanceDTO[].class));
        Assert.assertThat(performanceDTOList.size(), is(0));
    }

    @Test
    public void searchWorksWithEvent(){
        Performance performance = generateTestData();
        PerformanceDTO performanceDTO =
            PerformanceDTO.newBuilder()
                .setEvent(eventsMapper.eventToEventDTO(performance.getEvent()))
                .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(performanceDTO)
            .when().post(PERFORMANCE_ENDPOINT + "/search")
            .then().extract().response();

        List<PerformanceDTO> performanceDTOList = Arrays.asList(response.as(PerformanceDTO[].class));
        Assert.assertThat(performanceDTOList.get(0).getId(), is(performance.getId()));
    }

    @Test
    public void searchWorksWithEventNot(){
        Performance performance = generateTestData();
        performance.getEvent().setId(performance.getEvent().getId()+1);
        PerformanceDTO performanceDTO =
            PerformanceDTO.newBuilder()
                .setEvent(eventsMapper.eventToEventDTO(performance.getEvent()))
                .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(performanceDTO)
            .when().post(PERFORMANCE_ENDPOINT + "/search")
            .then().extract().response();

        List<PerformanceDTO> performanceDTOList = Arrays.asList(response.as(PerformanceDTO[].class));
        Assert.assertThat(performanceDTOList.size(), is(0));
    }


    @Test
    public void searchWorksWithHall(){
        Performance performance = generateTestData();
        PerformanceDTO performanceDTO =
            PerformanceDTO.newBuilder()
                .setHall(hallsMapper.hallToHallDTO(performance.getHall()))
                .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(performanceDTO)
            .when().post(PERFORMANCE_ENDPOINT + "/search")
            .then().extract().response();

        List<PerformanceDTO> performanceDTOList = Arrays.asList(response.as(PerformanceDTO[].class));
        Assert.assertThat(performanceDTOList.get(0).getId(), is(performance.getId()));
    }

    @Test
    public void searchWorksWithHallNot(){
        Performance performance = generateTestData();
        performance.getHall().setId(performance.getHall().getId()+1);
        PerformanceDTO performanceDTO =
            PerformanceDTO.newBuilder()
                .setHall(hallsMapper.hallToHallDTO(performance.getHall()))
                .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(performanceDTO)
            .when().post(PERFORMANCE_ENDPOINT + "/search")
            .then().extract().response();

        List<PerformanceDTO> performanceDTOList = Arrays.asList(response.as(PerformanceDTO[].class));
        Assert.assertThat(performanceDTOList.size(), is(0));
    }

    @Test
    public void searchWorksWithAllCombined(){
        Performance performance = generateTestData();
        PerformanceDTO performanceDTO =
            PerformanceDTO.newBuilder()
                .setHall(hallsMapper.hallToHallDTO(performance.getHall()))
                .setEvent(eventsMapper.eventToEventDTO(performance.getEvent()))
                .setDate(performance.getDate())
                .setPrice(performance.getPrice())
                .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(performanceDTO)
            .when().post(PERFORMANCE_ENDPOINT + "/search")
            .then().extract().response();

        List<PerformanceDTO> performanceDTOList = Arrays.asList(response.as(PerformanceDTO[].class));
        Assert.assertThat(performanceDTOList.get(0).getId(), is(performance.getId()));
    }

    @Test
    public void searchWorksWithAllCombinedNot(){
        Performance performance = generateTestData();
        performance.getEvent().setId(performance.getEvent().getId()+1);
        PerformanceDTO performanceDTO =
            PerformanceDTO.newBuilder()
                .setHall(hallsMapper.hallToHallDTO(performance.getHall()))
                .setEvent(eventsMapper.eventToEventDTO(performance.getEvent()))
                .setDate(performance.getDate())
                .setPrice(performance.getPrice())
                .build();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(performanceDTO)
            .when().post(PERFORMANCE_ENDPOINT + "/search")
            .then().extract().response();

        List<PerformanceDTO> performanceDTOList = Arrays.asList(response.as(PerformanceDTO[].class));
        Assert.assertThat(performanceDTOList.size(), is(0));
    }
}
