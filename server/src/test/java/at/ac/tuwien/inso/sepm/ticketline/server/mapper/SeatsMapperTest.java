package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatState;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.seats.SeatsMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class SeatsMapperTest {
    @Configuration
    @ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
    public static class PerformancesMapperTestContextConfiguration {
    }

    @Autowired
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    // Suppress warning cause inspection does not know that the cdi annotations are added in the code generation step
    private SeatsMapper seatsMapper;

    @Test
    public void shouldMapSeatToSeatDTO() {
        Seat seat = Seat.newBuilder().setHall(Hall.newBuilder().setId(1L).build()).setSector(Sector.B).setXCoord(0L).setYCoord(20L).build();
        SeatDTO seatDTO = seatsMapper.seatToSeatDTO(seat);
        assertThat(seatDTO.getSector()).isEqualTo(seat.getSector());
        assertThat(seatDTO.getxCoord()).isEqualTo(seat.getxCoord());
        assertThat(seatDTO.getyCoord()).isEqualTo(seat.getyCoord());
        assertThat(seatDTO.getId_hall()).isEqualTo(seat.getHall().getId());

    }

    @Test
    public void shouldMapSeatDTOToSeat() {
        SeatDTO seatDTO = SeatDTO.newBuilder().sector(Sector.A).id_hall(1L).xCoord(0L).yCoord(20L).state(SeatState.BOUGHT).build();
        Seat seat = seatsMapper.seatDTOToSeat(seatDTO);
        assertThat(seatDTO.getSector()).isEqualTo(seat.getSector());
        assertThat(seatDTO.getxCoord()).isEqualTo(seat.getxCoord());
        assertThat(seatDTO.getyCoord()).isEqualTo(seat.getyCoord());
        assertThat(seatDTO.getId_hall()).isEqualTo(seat.getHall().getId());
    }
}
