package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.tickets.TicketsMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class TicketsMapperTest {
    @Configuration
    @ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
    public static class TicketsMapperTestContextConfiguration {
    }

    @Autowired
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    // Suppress warning cause inspection does not know that the cdi annotations are added in the code generation step
    private TicketsMapper ticketsMapper;

    @Test
    public void shouldMapTicketToTicketDTO() {
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setFirstName("Hans");
        customer.setLastName("Gans");

        Artist artist = Artist.newBuilder().firstName("test").lastName("test").build();
        Event event = Event.newBuilder().artist(artist).content("content").
            length(Duration.ofDays(10L)).name("Eventname").type(EventType.SEATS).build();

        Hall hall = Hall.newBuilder().setCity("city").setCountry("country").setName("name").setPostcode("1234").setStreet("street").build();

        Performance performance = Performance.newBuilder().setId(1L).setDate(LocalDateTime.of(2018, 5, 12, 12, 55)).setPrice(BigDecimal.TEN.setScale(2)).setEvent(event).setHall(hall).build();


        Ticket ticket = Ticket.newBuilder()
            .id(1L)
            .state(TicketState.BOUGHT)
            .customer(customer)
            .seatReservations(new HashSet<>())
            .performance(performance)
            .build();

        TicketDTO ticketDTO = ticketsMapper.ticketToTicketDTO(ticket);
        assertThat(ticketDTO.getId()).isEqualTo(ticket.getId());
        assertThat(ticketDTO.getState()).isEqualTo(ticket.getState());
        assertThat(ticketDTO.getPerformance().getId()).isEqualTo(ticket.getPerformance().getId());
        assertThat(ticketDTO.getCustomer().getId()).isEqualTo(ticket.getCustomer().getId());
        assertThat(ticketDTO.getCustomer().getFirstName()).isEqualTo(ticket.getCustomer().getFirstName());
        assertThat(ticketDTO.getCustomer().getLastName()).isEqualTo(ticket.getCustomer().getLastName());
    }

    @Test
    public void shouldMapTicketDTOToTicket() {
        CustomerDTO customer = new CustomerDTO();
        customer.setId(1L);
        customer.setFirstName("Hans");
        customer.setLastName("Gans");

        TicketDTO ticketDTO = TicketDTO.builder()
            .id(1L)
            .state(TicketState.BOUGHT)
            .customer(customer)
            .build();

        Ticket ticket = ticketsMapper.ticketDTOToTicket(ticketDTO);
        assertThat(ticketDTO.getId()).isEqualTo(ticket.getId());
        assertThat(ticketDTO.getState()).isEqualTo(ticket.getState());
        assertThat(ticketDTO.getCustomer().getId()).isEqualTo(ticket.getCustomer().getId());
        assertThat(ticketDTO.getCustomer().getFirstName()).isEqualTo(ticket.getCustomer().getFirstName());
        assertThat(ticketDTO.getCustomer().getLastName()).isEqualTo(ticket.getCustomer().getLastName());
    }
}
