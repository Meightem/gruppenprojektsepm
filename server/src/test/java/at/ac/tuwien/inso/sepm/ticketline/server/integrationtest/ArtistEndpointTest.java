package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Artist;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.artist.ArtistMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.ArtistRepository;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;

public class ArtistEndpointTest extends BaseIntegrationTest {
    private static final String ARTISTS_ENDPOINT = "/artists";

    private static final String TEST_FIRST_NAME = "Häns";
    private static final String TEST_LAST_NAME = "Gäns";
    private static Long TEST_ID = 1L;

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private ArtistMapper artistMapper;

    private Artist createArtist(String first, String last) {
        Artist artist = Artist.newBuilder().firstName(first).lastName(last).build();
        return artistRepository.save(artist);
    }


    @Test
    public void searchWorksWithOneUser() {
        ArtistDTO exArtist
            = artistMapper.artistToArtistDTO(createArtist(TEST_FIRST_NAME, TEST_LAST_NAME));
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(ARTISTS_ENDPOINT + "/search/" + TEST_FIRST_NAME + " " + TEST_LAST_NAME)
            .then().extract().response();

        List<ArtistDTO> artists = Arrays.asList(response.as(ArtistDTO[].class));

        Assert.assertThat(artists.get(0), is(exArtist));
    }

    @Test
    public void searchDoesntWorkBecauseThereIsNoUser() {
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(ARTISTS_ENDPOINT + "/search/" + TEST_FIRST_NAME + " " + TEST_LAST_NAME)
            .then().extract().response();

        List<ArtistDTO> artists = Arrays.asList(response.as(ArtistDTO[].class));

        Assert.assertTrue(artists.size() == 0);
    }

    @Test
    public void serachDoentWorkBecauseSearchedTextDoesntExist() {
        ArtistDTO exArtist = artistMapper.artistToArtistDTO(createArtist(TEST_FIRST_NAME, TEST_LAST_NAME));

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(ARTISTS_ENDPOINT + "/search/" + "TEST")
            .then().extract().response();

        List<ArtistDTO> artists
            = Arrays.asList(response.as(ArtistDTO[].class));

        Assert.assertTrue(artists
            .size() == 0);
    }

    @Test
    public void searchArtistwithPartialFirstName() {
        ArtistDTO exArtist = artistMapper.artistToArtistDTO(createArtist("Benjamin", "Strasser"));
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(ARTISTS_ENDPOINT + "/search/" + "Ben")
            .then().extract().response();

        List<ArtistDTO> responseList = Arrays.asList(response.as(ArtistDTO[].class));

        Assert.assertThat(responseList.get(0), is(exArtist
        ));
    }

    @Test
    public void searchArtistwithPartialFirstNameAndLastName() {
        ArtistDTO exArtist = artistMapper.artistToArtistDTO(createArtist("Benjamin", "Strasser"));
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(ARTISTS_ENDPOINT + "/search/" + "Ben Stra")
            .then().extract().response();

        List<ArtistDTO> responseList = Arrays.asList(response.as(ArtistDTO[].class));

        Assert.assertThat(responseList.get(0), is(exArtist
        ));
    }

    @Test
    public void searchArtistDoesNotFitBecauseofPartialFirstName() {
        ArtistDTO exArtist = artistMapper.artistToArtistDTO(createArtist("Benjamin", "Strasser"));
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(ARTISTS_ENDPOINT + "/search/" + "Benk" + " " + "Stra")
            .then().extract().response();

        List<ArtistDTO> responseList = Arrays.asList(response.as(ArtistDTO[].class));
        Assert.assertThat(responseList.size(), is(0));
    }

    @Test
    public void searchArtistDoesWorkwithJustFirstName() {
        ArtistDTO exArtist = artistMapper.artistToArtistDTO(createArtist("Benjamin", "Strasser"));
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(ARTISTS_ENDPOINT + "/search/" + "Ben")
            .then().extract().response();

        List<ArtistDTO> responseList = Arrays.asList(response.as(ArtistDTO[].class));
        Assert.assertThat(responseList.get(0), is(exArtist
        ));
    }

    @Test
    public void searchArtistDoesWorkwithJustLastName() {
        ArtistDTO exArtist = artistMapper.artistToArtistDTO(createArtist("Benjamin", "Strasser"));
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(ARTISTS_ENDPOINT + "/search/" + "Stra")
            .then().extract().response();

        List<ArtistDTO> responseList = Arrays.asList(response.as(ArtistDTO[].class));
        Assert.assertThat(responseList.get(0), is(exArtist
        ));
    }

    @Test
    public void searchArtistwithMatchInFirstAndLastName() {
        ArtistDTO exArtist = artistMapper.artistToArtistDTO(createArtist("Benjamin", "Benjamin"));
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(ARTISTS_ENDPOINT + "/search/" + "Ben")
            .then().extract().response();

        List<ArtistDTO> responseList = Arrays.asList(response.as(ArtistDTO[].class));

        Assert.assertThat(responseList.get(0), is(exArtist
        ));
    }

    @Test
    public void searchArtistDoesNotWorkWithOneMatchInFirstAndLastName() {
        ArtistDTO exArtist = artistMapper.artistToArtistDTO(createArtist("Benjamin", "Benjamin"));
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(ARTISTS_ENDPOINT + "/search/" + "Ben Te")
            .then().extract().response();

        List<ArtistDTO> responseList = Arrays.asList(response.as(ArtistDTO[].class));

        Assert.assertThat(responseList.size(), is(0));
    }
}
