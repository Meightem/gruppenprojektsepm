package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Hall;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.halls.HallsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.HallRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.SeatRepository;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;

public class HallEndpointTest extends BaseIntegrationTest {

    @Autowired
    private HallRepository hallRepository;

    @Autowired
    private SeatRepository seatRepository;

    @Autowired
    private HallsMapper hallsMapper;

    private final String HALL_ENDPOINT = "/halls";
    private final String HALL_ENDPOINT_SEARCH = "/search/";


    private final String HALL_NAME = "HALLNAME";
    private final String CITY = "CITYNAME";
    private final String STREET = "STREET";
    private final String POSTCODE = "3011";
    private final String COUNTRY = "Country";

    private Hall createHall(String name, String country, String city, String postcode, String street) {
        Hall hall =
            Hall
                .newBuilder()
                .setCity(city)
                .setName(name)
                .setCountry(country)
                .setPostcode(postcode)
                .setStreet(street)
                .build();
        return hallRepository.save(hall);
    }

    private Hall createHall() {
        return createHall(HALL_NAME, COUNTRY, CITY, POSTCODE, STREET);
    }

    @Test
    public void testHallRepository() {
        Hall hall = Hall.newBuilder().setCity("city").setCountry("country").setName("name").setPostcode("1234").setStreet("street").build();

        Hall savedHall = hallRepository.save(hall);
        Assert.assertThat(hall, is(hallRepository.findById(savedHall.getId()).get()));
    }

    @Test
    public void testSeatRepository() {
        Hall hall = Hall.newBuilder().setCity("city").setCountry("country").setName("name").setPostcode("1234").setStreet("street").build();
        Hall savedHall = hallRepository.save(hall);

        Seat seat = Seat.newBuilder().setSector(Sector.A).setHall(savedHall).setXCoord(1L).setYCoord(0L).build();
        Seat savedSeat = seatRepository.save(seat);

        Assert.assertThat(seat, is(seatRepository.findById(savedSeat.getId()).get()));
    }

    @Test
    public void searchHallWorksWithAllCriteria() {

        Hall hall = createHall();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(HALL_ENDPOINT + HALL_ENDPOINT_SEARCH + HALL_NAME + " " + COUNTRY + " " + POSTCODE + " " + STREET + " " + CITY)
            .then().extract().response();

        List<HallDTO> hallDTOS = Arrays.asList(response.as(HallDTO[].class));

        Assert.assertThat(hallDTOS.get(0), is(hallsMapper.hallToHallDTO(hall)));
    }

    @Test
    public void searchHallWorksWithSomeCriteria() {

        Hall hall = createHall();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(HALL_ENDPOINT + HALL_ENDPOINT_SEARCH + HALL_NAME + " " + POSTCODE + " " + STREET + " " + CITY)
            .then().extract().response();

        List<HallDTO> hallDTOS = Arrays.asList(response.as(HallDTO[].class));

        Assert.assertThat(hallDTOS.get(0), is(hallsMapper.hallToHallDTO(hall)));
    }

    @Test
    public void searchHallDoesNotWorkWithSomeCriteriaNotMet() {

        Hall hall = createHall();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(HALL_ENDPOINT + HALL_ENDPOINT_SEARCH + HALL_NAME+HALL_NAME + " " + POSTCODE + " " + STREET + " " + CITY)
            .then().extract().response();

        List<HallDTO> hallDTOS = Arrays.asList(response.as(HallDTO[].class));

        Assert.assertThat(hallDTOS.size(), is(0));
    }

    @Test
    public void searchHallFindsMultipleHalls() {

        Hall hall = createHall();
        Hall hall1 = createHall();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(HALL_ENDPOINT + HALL_ENDPOINT_SEARCH + HALL_NAME + " " + POSTCODE + " " + STREET + " " + CITY)
            .then().extract().response();

        List<HallDTO> hallDTOS = Arrays.asList(response.as(HallDTO[].class));

        Assert.assertThat(hallDTOS.size(), is(2));
    }

    @Test
    public void searchHallWorksWithAllCriteriaWithInterference() {

        Hall hall = createHall();
        Hall hall1 = createHall("test", "test","test","2000","test");

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(HALL_ENDPOINT + HALL_ENDPOINT_SEARCH + HALL_NAME + " " + COUNTRY + " " + POSTCODE + " " + STREET + " " + CITY)
            .then().extract().response();

        List<HallDTO> hallDTOS = Arrays.asList(response.as(HallDTO[].class));

        Assert.assertThat(hallDTOS.size(), is(1));
        Assert.assertThat(hallDTOS.get(0), is(hallsMapper.hallToHallDTO(hall)));
    }
}
