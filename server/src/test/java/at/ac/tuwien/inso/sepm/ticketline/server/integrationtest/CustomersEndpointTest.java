package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.datagenerator.ProductionDataGenerator;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.customers.CustomersMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.CustomerRepository;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;


import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;

public class CustomersEndpointTest extends BaseIntegrationTest {

    private static final String CUSTOMERS_ENDPOINT = "/customers";

    private static final String TEST_FIRST_NAME = "Häns";
    private static final String TEST_LAST_NAME = "Gäns";
    private static final Long TEST_ID = 1L;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomersMapper customersMapper;

    @Autowired
    private ProductionDataGenerator productionDataGenerator;

    private Response createCustomer(String token) {
        return createCustomer(token,TEST_FIRST_NAME,TEST_LAST_NAME);
    }

    private Response createCustomer(String token,String first,String last){
        CustomerDTO dto = new CustomerDTO();
        dto.setFirstName(first);
        dto.setLastName(last);
        dto.setIsAnonymousCustomer(false);
        return RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, token)
            .body(dto)
            .when().post(CUSTOMERS_ENDPOINT)
            .then().extract().response();
    }

    @Test
    public void getAnonymousCustomerWorks() {
        productionDataGenerator.generateProductionData();

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT + "/anonymous")
            .then().extract().response();
        Assert.assertThat(response.as(CustomerDTO.class).getIsAnonymousCustomer(), is(true));
    }

    @Test
    public void createCustomerWorksAsUser() {
        Response response = createCustomer(validUserTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        CustomerDTO dto = new CustomerDTO();
        dto.setFirstName(TEST_FIRST_NAME);
        dto.setLastName(TEST_LAST_NAME);

        CustomerDTO responseDTO = response.as(CustomerDTO.class);
        responseDTO.setId(null);
        Assert.assertThat(responseDTO, is(dto));
    }

    @Test
    public void createCustomerWorksAsAdmin(){
        Response response = createCustomer(validAdminTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        CustomerDTO dto = new CustomerDTO();
        dto.setFirstName(TEST_FIRST_NAME);
        dto.setLastName(TEST_LAST_NAME);

        CustomerDTO responseDTO = response.as(CustomerDTO.class);
        responseDTO.setId(null);
        Assert.assertThat(responseDTO, is(dto));
    }

    @Test
    public void createCustomerRejectsInvalidDTOs(){
        Response response = createCustomer(validAdminTokenWithPrefix, TEST_FIRST_NAME, "");
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));

        response = createCustomer(validAdminTokenWithPrefix, "", TEST_LAST_NAME);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));

        response = createCustomer(validAdminTokenWithPrefix, null, null);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void createCustomerDoesNotWorkAsAnonym(){
        CustomerDTO dto = new CustomerDTO();
        dto.setFirstName(TEST_FIRST_NAME);
        dto.setLastName(TEST_LAST_NAME);
        Response response =  RestAssured
            .given()
            .contentType(ContentType.JSON)
            .body(dto)
            .when().post(CUSTOMERS_ENDPOINT)
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
    }

    @Test
    public void updateCustomerWorks(){
        CustomerDTO customerDTO = createCustomer(validUserTokenWithPrefix).as(CustomerDTO.class);

        customerDTO.setFirstName("Changed");

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(customerDTO)
            .when().patch(CUSTOMERS_ENDPOINT+"/"+customerDTO.getId())
            .then().extract().response();

        CustomerDTO dbCustomer = customersMapper.customerToCustomerDTO(customerRepository.findOneById(customerDTO.getId()).get());
        CustomerDTO responseCustomer = response.as(CustomerDTO.class);

        Assert.assertEquals(customerDTO,dbCustomer);
        Assert.assertEquals(customerDTO,responseCustomer);
    }

    @Test
    public void updateCustomerRejectsInvalidDTOs(){
        CustomerDTO customerDTO = createCustomer(validUserTokenWithPrefix).as(CustomerDTO.class);

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(new CustomerDTO())
            .when().patch(CUSTOMERS_ENDPOINT+"/"+customerDTO.getId())
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));

        CustomerDTO dbCustomer = customersMapper.customerToCustomerDTO(customerRepository.findOneById(customerDTO.getId()).get());
        Assert.assertEquals(customerDTO,dbCustomer);
    }

    @Test
    public void updateCustomerDoesntWorkBecauseCustomerDoesntExist(){
        CustomerDTO customerDTO = createCustomer(validUserTokenWithPrefix).as(CustomerDTO.class);

        customerDTO.setFirstName("Changed");

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(customerDTO)
            .when().patch(CUSTOMERS_ENDPOINT+"/"+customerDTO.getId()+1)
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void createCustomerDoesntWorkBecauseCustomerAlreadyExists(){

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(CustomerDTO.newBuilder().firstName(TEST_FIRST_NAME).lastName(TEST_LAST_NAME).isAnonymousCustomer(false).build())
            .when().post(CUSTOMERS_ENDPOINT)
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        CustomerDTO customerDTO = response.as(CustomerDTO.class);

        response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(customerDTO)
            .when().post(CUSTOMERS_ENDPOINT)
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void searchWorksWithOneUser(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix).as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+TEST_FIRST_NAME+" "+ TEST_LAST_NAME)
            .then().extract().response();

        List<CustomerDTO> customers = Arrays.asList(response.as(CustomerDTO[].class));

        Assert.assertThat(customers.get(0),is(exCustomer));
    }

    @Test
    public void searchDoesntWorkBecauseThereIsNoUser(){
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+TEST_FIRST_NAME+" "+ TEST_LAST_NAME)
            .then().extract().response();

        List<CustomerDTO> customers = Arrays.asList(response.as(CustomerDTO[].class));

        Assert.assertTrue(customers.size()== 0);
    }

    @Test
    public void serachDoentWorkBecauseSearchedTextDoesntExist(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix).as(CustomerDTO.class);

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+"TEST")
            .then().extract().response();

        List<CustomerDTO> customers = Arrays.asList(response.as(CustomerDTO[].class));

        Assert.assertTrue(customers.size()== 0);
    }

    @Test
    public void findMultipleCustomersWithSameFirstName(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix).as(CustomerDTO.class);
        CustomerDTO exCustomer2 = createCustomer(validUserTokenWithPrefix).as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+TEST_FIRST_NAME+" "+ TEST_LAST_NAME)
            .then().extract().response();

        List<CustomerDTO> customers = Arrays.asList(response.as(CustomerDTO[].class));

        Assert.assertTrue(customers.contains(exCustomer));
        Assert.assertTrue(customers.contains(exCustomer2));
        Assert.assertThat(customers.size(),is(2));
    }

    @Test
    public void searchCustomerwithPartialFirstName(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix,"Benjamin","Strasser").as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+"Ben")
            .then().extract().response();

        List<CustomerDTO> responseList = Arrays.asList(response.as(CustomerDTO[].class));

        Assert.assertThat(responseList.get(0),is(exCustomer));
    }

    @Test
    public void searchCustomerwithPartialFirstNameAndLastName(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix,"Benjamin","Strasser").as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+"Ben Stra")
            .then().extract().response();

        List<CustomerDTO> responseList = Arrays.asList(response.as(CustomerDTO[].class));

        Assert.assertThat(responseList.get(0),is(exCustomer));
    }

    @Test
    public void searchCustomerDoesNotFitBecauseofPartialFirstName(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix,"Benjamin","Strasser").as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+"Benk"+" "+ "Stra")
            .then().extract().response();

        List<CustomerDTO> responseList = Arrays.asList(response.as(CustomerDTO[].class));
        Assert.assertThat(responseList.size() , is(0));
    }

    @Test
    public void searchCustomerDoesWorkwithJustFirstName(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix,"Benjamin","Strasser").as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+"Ben")
            .then().extract().response();

        List<CustomerDTO> responseList = Arrays.asList(response.as(CustomerDTO[].class));
        Assert.assertThat(responseList.get(0),is(exCustomer));
    }

    @Test
    public void searchCustomerDoesWorkwithJustLastName(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix,"Benjamin","Strasser").as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+"Stra")
            .then().extract().response();

        List<CustomerDTO> responseList = Arrays.asList(response.as(CustomerDTO[].class));
        Assert.assertThat(responseList.get(0),is(exCustomer));
    }

    @Test
    public void searchCustomerwithMatchInFirstAndLastName(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix,"Benjamin","Benjamin").as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+"Ben")
            .then().extract().response();

        List<CustomerDTO> responseList = Arrays.asList(response.as(CustomerDTO[].class));

        Assert.assertThat(responseList.get(0),is(exCustomer));
    }

    @Test
    public void searchCustomerDoesNotWorkWithOneMatchInFirstAndLastName(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix,"Benjamin","Benjamin").as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+"Ben Te")
            .then().extract().response();

        List<CustomerDTO> responseList = Arrays.asList(response.as(CustomerDTO[].class));

        Assert.assertThat(responseList.size(), is(0));
    }

    @Test
    public void receiveCustomerWorks(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix,"Benjamin","Benjamin").as(CustomerDTO.class);
        CustomerDTO exCustomer2 = createCustomer(validUserTokenWithPrefix,"Benjamin","Benjamin").as(CustomerDTO.class);
        CustomerDTO exCustomer3 = createCustomer(validUserTokenWithPrefix,"Benjamin","Benjamin").as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT)
            .then().extract().response();

        List<CustomerDTO> responseList = Arrays.asList(response.as(CustomerDTO[].class));

        Assert.assertTrue(responseList.contains(exCustomer));
        Assert.assertTrue(responseList.contains(exCustomer2));
        Assert.assertTrue(responseList.contains(exCustomer3));
    }

    @Test
    public void searchForIDWorks(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix,"Benjamin","Strasser").as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+exCustomer.getId())
            .then().extract().response();

        List<CustomerDTO> responseList = Arrays.asList(response.as(CustomerDTO[].class));

        Assert.assertThat(responseList.get(0),is(exCustomer));
    }

    @Test
    public void searchWithWrongIDDoesNotWork(){
        CustomerDTO exCustomer = createCustomer(validUserTokenWithPrefix,"Benjamin","Strasser").as(CustomerDTO.class);
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(CUSTOMERS_ENDPOINT+"/search/"+exCustomer.getId()+1)
            .then().extract().response();

        List<CustomerDTO> responseList = Arrays.asList(response.as(CustomerDTO[].class));

        Assert.assertThat(responseList.size(),is(0));
    }


}
