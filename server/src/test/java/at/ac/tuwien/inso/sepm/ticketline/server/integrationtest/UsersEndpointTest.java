package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationToken;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.users.UsersMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.UserRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.security.AuthenticationConstants;
import at.ac.tuwien.inso.sepm.ticketline.server.service.HeaderTokenAuthenticationService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.UsersService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.assertj.core.util.Strings;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;

import static org.codehaus.groovy.runtime.InvokerHelper.asList;
import static org.hamcrest.core.Is.is;

public class UsersEndpointTest extends BaseIntegrationTest {

    private static final String USERS_ENDPOINT = "/users";
    private static final String USERS_ENDPOINT_ID = "/users/";

    private static final String TEST_USERNAME = "hansgans";
    private static final String TEST_PASSWORD = "PASSWORD HERE"; // might be password or hash depending on context

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UsersMapper usersMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UsersService usersService;

    @Autowired
    private HeaderTokenAuthenticationService authenticationService;

    private Response createUser(String token) {
        UserDTO dto = UserDTO.newBuilder().username(TEST_USERNAME).password(TEST_PASSWORD).build();
        return RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, token)
            .body(dto)
            .when().post(USERS_ENDPOINT)
            .then().extract().response();
    }

    @Test
    public void createUserRejectsInvalidDTO() {
        UserDTO dto = UserDTO.newBuilder().username(TEST_USERNAME).build();
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(dto)
            .when().post(USERS_ENDPOINT)
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void createUserWorksAsAdmin() {
        Response response = createUser(validAdminTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));

        UserDTO dto = UserDTO
            .newBuilder()
            .username(TEST_USERNAME)
            .password(response.as(UserDTO.class).getPassword())
            .id(response.as(UserDTO.class).getId())
            .build();
        Assert.assertThat(
            response.as(UserDTO.class),
            is(dto)
        );
    }

    @Test
    public void createUserDoesNotWorkAsUser() {
        Response response = createUser(validUserTokenWithPrefix);
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN.value()));
    }

    @Test
    public void getUsersWorksAsAdmin() {
        User user = userRepository.save(User
            .newBuilder()
            .username(TEST_USERNAME)
            .passwordHash(TEST_PASSWORD)
            .build());

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .when().get(USERS_ENDPOINT)
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        List<UserDTO> dtos = asList(response.as(UserDTO[].class));
        Assert.assertEquals(3, dtos.size());
        Assert.assertTrue(dtos.contains(UserDTO.newBuilder()
            .username(TEST_USERNAME)
            .id(user.getId())
            .build()));
    }

    @Test
    public void getUsersDoesNotWorksAsUser() {
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(USERS_ENDPOINT)
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN.value()));
    }

    @Test
    public void updateUserDoesWorkAsAdmin() {
        User user = userRepository.save(User
            .newBuilder()
            .username(TEST_USERNAME)
            .passwordHash(TEST_PASSWORD)
            .failedAttempts(2)
            .build());

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(UserDTO.newBuilder().username(TEST_USERNAME + TEST_USERNAME).password(TEST_PASSWORD + TEST_PASSWORD).build())
            .when().patch(USERS_ENDPOINT_ID + user.getId())
            .then().extract().response();

        Optional<User> optionalUser = userRepository.findById(user.getId());
        if (!optionalUser.isPresent()) {
            Assert.fail();
        }
        User dbUser = optionalUser.get();
        User responseUser = usersMapper.userDTOToUser(response.as(UserDTO.class));

        Assert.assertTrue(passwordEncoder.matches(TEST_PASSWORD + TEST_PASSWORD, dbUser.getPasswordHash()));
        Assert.assertTrue(responseUser.getPasswordHash() == null);

        dbUser.setPasswordHash(null);

        User expected = User.newBuilder().id(user.getId()).username(TEST_USERNAME + TEST_USERNAME).build();
        Assert.assertEquals(expected, responseUser);
        Assert.assertEquals(expected, dbUser);
    }

    @Test
    public void updateUserDoesntWorkBecauseIDDoesNotExist() {
        User user = userRepository.save(User
            .newBuilder()
            .username(TEST_USERNAME)
            .passwordHash(TEST_PASSWORD)
            .failedAttempts(2)
            .build());

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(UserDTO.newBuilder().username(TEST_USERNAME + TEST_USERNAME).password(TEST_PASSWORD + TEST_PASSWORD).build())
            .when().patch(USERS_ENDPOINT_ID + "-1")
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }


    @Test
    public void updateUserRejectsInvalidDTOs() {
        User user = userRepository.save(User
            .newBuilder()
            .username(TEST_USERNAME)
            .passwordHash(TEST_PASSWORD)
            .failedAttempts(2)
            .build());

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(UserDTO.newBuilder().username("").password("").build())
            .when().patch(USERS_ENDPOINT_ID + user.getId())
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void selfLockDoesNotWork() {

        UserDTO user = usersService.createUser(
            UserDTO.newBuilder()
                .username(TEST_USERNAME)
                .password(TEST_PASSWORD)
                .isAdmin(true)
                .build()
        );

        String authenticationToken = Strings
            .join(
                AuthenticationConstants.TOKEN_PREFIX,
                authenticationService.authenticate(TEST_USERNAME, TEST_PASSWORD).getCurrentToken())
            .with(" ");

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, authenticationToken)
            .body(UserDTO.newBuilder().username(TEST_USERNAME).password(TEST_PASSWORD).isLocked(true).build())
            .when().patch(USERS_ENDPOINT_ID + user.getId())
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void selfUpdateDoesWork(){
        UserDTO user = usersService.createUser(
            UserDTO.newBuilder()
                .username(TEST_USERNAME)
                .password(TEST_PASSWORD)
                .isAdmin(true)
                .build()
        );

        String authenticationToken = Strings
            .join(
                AuthenticationConstants.TOKEN_PREFIX,
                authenticationService.authenticate(TEST_USERNAME, TEST_PASSWORD).getCurrentToken())
            .with(" ");

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, authenticationToken)
            .body(UserDTO.newBuilder().username(TEST_USERNAME+TEST_USERNAME).password(TEST_PASSWORD+TEST_PASSWORD).isLocked(false).build())
            .when().patch(USERS_ENDPOINT_ID + user.getId())
            .then().extract().response();

        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
    }
}
