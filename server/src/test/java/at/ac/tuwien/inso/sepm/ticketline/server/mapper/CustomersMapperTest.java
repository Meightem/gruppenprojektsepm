package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.customers.CustomersMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomersMapperTest {

    @Configuration
    @ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
    public static class CustomersMapperTestContextConfiguration {
    }

    @Autowired
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    // Suppress warning cause inspection does not know that the cdi annotations are added in the code generation step
    private CustomersMapper customersMapper;

    private static final long CUSTOMER_ID = 1L;
    private static final String CUSTOMER_FIRST_NAME = "Hans";
    private static final String CUSTOMER_LAST_NAME = "Gans";

    @Test
    public void shouldMapCustomerToDTO() {
        Customer customer = new Customer();
        customer.setId(CUSTOMER_ID);
        customer.setFirstName(CUSTOMER_FIRST_NAME);
        customer.setLastName(CUSTOMER_LAST_NAME);
        CustomerDTO customerDTO = customersMapper.customerToCustomerDTO(customer);
        assertThat(customerDTO.getId()).isEqualTo(CUSTOMER_ID);
        assertThat(customerDTO.getFirstName()).isEqualTo(CUSTOMER_FIRST_NAME);
        assertThat(customerDTO.getLastName()).isEqualTo(CUSTOMER_LAST_NAME);
    }

}
