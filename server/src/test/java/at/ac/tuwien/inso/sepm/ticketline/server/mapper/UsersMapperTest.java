package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.users.UsersMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class UsersMapperTest {

    @Configuration
    @ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
    public static class UsersMapperTestContextConfiguration {
    }

    @Autowired
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    // Suppress warning cause inspection does not know that the cdi annotations are added in the code generation step
    private UsersMapper usersMapper;

    private static final long USER_ID = 1L;
    private static final String USER_NAME = "Hans";
    private static final String USER_PASSWORD = "Gans";

    @Test
    public void shouldMapUserToDTO() {
        User user = new User();
        user.setId(USER_ID);
        user.setUsername(USER_NAME);
        user.setPasswordHash(USER_PASSWORD);
        UserDTO userDTO = usersMapper.userToUserDTO(user);
        assertThat(userDTO.getId()).isEqualTo(USER_ID);
        assertThat(userDTO.getUsername()).isEqualTo(USER_NAME);
        assertThat(userDTO.getPassword()).isNull();
    }

}
