package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Artist;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.events.EventsMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;

@RunWith(SpringJUnit4ClassRunner.class)
public class EventsMapperTest {
    @Configuration
    @ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
    public static class EventsMapperTestContextConfiguration {
    }

    @Autowired
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    // Suppress warning cause inspection does not know that the cdi annotations are added in the code generation step
    private EventsMapper eventsMapper;

    @Test
    public void shouldMapEventToEventDTO() {


        Artist artist = Artist.newBuilder().firstName("hans").lastName("gurtner").build();

        Event event = Event.newBuilder().artist(artist).content("content").setId(1L).
            length(Duration.ofDays(2)).name("name").type(EventType.SEATS).build();
        EventDTO eventDTO = eventsMapper.eventToEventDTO(event);
        assertThat(eventDTO.getId()).isEqualTo(event.getId());
        assertThat(eventDTO.getArtist().getFirstName()).isEqualTo(event.getArtist().getFirstName());
        assertThat(eventDTO.getArtist().getLastName()).isEqualTo(event.getArtist().getLastName());
        assertThat(eventDTO.getContent()).isEqualTo(event.getContent());
        assertThat(eventDTO.getName()).isEqualTo(event.getName());
        assertThat(eventDTO.getLength()).isEqualTo(event.getLength());
        assertThat(eventDTO.getType()).isEqualTo(event.getType());


    }

    @Test
    public void shouldMapEventDTOToEvent() {

        ArtistDTO artistDTO = ArtistDTO.newBuilder().firstName("hans").lastName("gurtner").build();

        EventDTO eventDTO = EventDTO.newBuilder().artist(artistDTO).content("content").id(1L).
            length(Duration.ofDays(2)).name("name").type(EventType.SEATS).build();
        Event event = eventsMapper.eventDTOtoEvent(eventDTO);
        assertThat(eventDTO.getId()).isEqualTo(event.getId());
        assertThat(eventDTO.getArtist().getFirstName()).isEqualTo(event.getArtist().getFirstName());
        assertThat(eventDTO.getArtist().getLastName()).isEqualTo(event.getArtist().getLastName());
        assertThat(eventDTO.getContent()).isEqualTo(event.getContent());
        assertThat(eventDTO.getName()).isEqualTo(event.getName());
        assertThat(eventDTO.getLength()).isEqualTo(event.getLength());
        assertThat(eventDTO.getType()).isEqualTo(event.getType());

    }
}
