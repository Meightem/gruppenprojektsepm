package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.news.NewsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.NewsRepository;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class NewsEndpointTest extends BaseIntegrationTest {
    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsMapper newsMapper;

    private static final String NEWS_ENDPOINT = "/news";
    private static final String SPECIFIC_NEWS_PATH = "/{newsId}";
    private static final String READ_NEWS_PATH = "/read";
    private static final String UNREAD_NEWS_PATH = "/unread";
    private static final String TEST_NEWS_TEXT = "TestNewsText";
    private static final String TEST_NEWS_TITLE = "title";

    private static final LocalDateTime TEST_NEWS_PUBLISHED_AT =
        LocalDateTime.of(2016, 11, 13, 12, 15, 0, 0);

    @Test
    public void publishNewsUnauthorizedAsAnonymous() {
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .body(DetailedNewsDTO.builder()
                .title(TEST_NEWS_TITLE)
                .text(TEST_NEWS_TEXT)
                .build())
            .when().post(NEWS_ENDPOINT)
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
    }

    @Test
    public void publishNewsUnauthorizedAsUser() {
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .body(DetailedNewsDTO.builder()
                .title(TEST_NEWS_TITLE)
                .text(TEST_NEWS_TEXT)
                .build())
            .when().post(NEWS_ENDPOINT)
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN.value()));
    }

    @Test
    public void publishNewsAsAdmin() {
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(DetailedNewsDTO.builder()
                .title(TEST_NEWS_TITLE)
                .text(TEST_NEWS_TEXT)
                .build())
            .when().post(NEWS_ENDPOINT)
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        DetailedNewsDTO dto = response.as(DetailedNewsDTO.class);
        Assert.assertThat(dto.getTitle(), is(TEST_NEWS_TITLE));
        Assert.assertThat(dto.getText(), is(TEST_NEWS_TEXT));
    }

    @Test
    public void publishInvalidNews() {
        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(new DetailedNewsDTO())
            .when().post(NEWS_ENDPOINT)
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }


    @Test
    public void readNews() {
        News news = News.builder()
            .title(TEST_NEWS_TITLE)
            .text(TEST_NEWS_TEXT)
            .publishedAt(TEST_NEWS_PUBLISHED_AT)
            .image("lol")
            .build();
        news = newsRepository.save(news);

        Response response = RestAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().post(NEWS_ENDPOINT + SPECIFIC_NEWS_PATH + READ_NEWS_PATH, news.getId())
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
        DetailedNewsDTO markAsReadResponse = response.as(DetailedNewsDTO.class);
        Assert.assertThat(markAsReadResponse, is(DetailedNewsDTO.builder()
            .id(news.getId())
            .title(TEST_NEWS_TITLE)
            .text(TEST_NEWS_TEXT)
            .publishedAt(TEST_NEWS_PUBLISHED_AT)
            .image("lol")
            .build()));

        DetailedNewsDTO findResponse = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(NEWS_ENDPOINT + SPECIFIC_NEWS_PATH, news.getId())
            .then().extract().response().as(DetailedNewsDTO.class);
        Assert.assertThat(findResponse, is(markAsReadResponse));
    }

    @Test
    public void unreadNews() {
        News news = News.builder()
            .title(TEST_NEWS_TITLE)
            .text(TEST_NEWS_TEXT)
            .publishedAt(TEST_NEWS_PUBLISHED_AT)
            .build();
        news = newsRepository.save(news);

        Response response = RestAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
            .when().get(NEWS_ENDPOINT + UNREAD_NEWS_PATH)
            .then().extract().response();

        List<SimpleNewsDTO> unreadNews = response.jsonPath().getList("", SimpleNewsDTO.class);
        assertThat(unreadNews.size(), is(1));
        assertThat(unreadNews, is(Collections.singletonList(SimpleNewsDTO.builder()
            .id(news.getId())
            .publishedAt(TEST_NEWS_PUBLISHED_AT)
            .summary(TEST_NEWS_TEXT)
            .title(TEST_NEWS_TITLE).build())));
    }

    @Test
    public void PublishAlreadyExistingNewsDoesNotWork(){
        DetailedNewsDTO newsDTO = DetailedNewsDTO.builder()
            .title(TEST_NEWS_TITLE)
            .text(TEST_NEWS_TEXT)
            .publishedAt(LocalDateTime.now())
            .build();
        News news = newsRepository.save(newsMapper.detailedNewsDTOToNews(newsDTO));

        Response response = RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
            .body(newsMapper.newsToDetailedNewsDTO(news))
            .when().post(NEWS_ENDPOINT)
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST.value()));
    }
}
