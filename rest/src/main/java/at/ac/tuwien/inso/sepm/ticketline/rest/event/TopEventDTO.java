package at.ac.tuwien.inso.sepm.ticketline.rest.event;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

@ApiModel(value = "TopEventDTO", description = "represents an event and the number of tickets sold during this event in timespan")
public class TopEventDTO {
    @ApiModelProperty(required = true, name = "The Event")
    private EventDTO event;

    @ApiModelProperty(required = true, name = "number of tickets sold")
    private int count;

    private TopEventDTO(Builder builder) {
        event = builder.event;
        count = builder.count;
    }

    public TopEventDTO(EventDTO eventDTO, int count) {
        this.event = event;
        this.count = count;
    }

    public TopEventDTO(){}

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopEventDTO that = (TopEventDTO) o;
        return count == that.count &&
            Objects.equals(event, that.event);
    }

    @Override
    public int hashCode() {
        return Objects.hash(event, count);
    }

    @Override
    public String toString() {
        return "TopEventDTO{" +
            "eventDTO=" + event+
            ", count=" + count +
            '}';
    }

    public EventDTO getEvent() {
        return event;
    }

    public void setEvent(EventDTO event) {
        this.event = event;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static final class Builder {
        private EventDTO event;
        private int count;

        private Builder() {
        }

        public Builder event(EventDTO val) {
            event = val;
            return this;
        }

        public Builder count(int val) {
            count = val;
            return this;
        }

        public TopEventDTO build() {
            return new TopEventDTO(this);
        }
    }
}
