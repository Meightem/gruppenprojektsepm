package at.ac.tuwien.inso.sepm.ticketline.rest.ticket;

import java.util.Objects;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "TicketDTO", description = "Represents a ticket")
public class TicketDTO {
    public TicketDTO() {}

    private TicketDTO(Builder builder) {
        setId(builder.id);
        setState(builder.state);
        setCustomer(builder.customer);
        setPerformance(builder.performance);
    }

    public TicketDTO(Long id, TicketState state, CustomerDTO customer, PerformanceDTO performance) {
        this.id = id;
        this.state = state;
        this.customer = customer;
        this.performance = performance;
    }

    @ApiModelProperty(readOnly = true, name = "The automatically generated user ID")
    private Long id;

    @ApiModelProperty(required = true, name = "The state of the ticket")
    private TicketState state;

    @ApiModelProperty(required = true, name = "The customer the ticket belongs to.")
    private CustomerDTO customer;

    @ApiModelProperty(required = true, name = "The performance of the ticket")
    private PerformanceDTO performance;

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId()
	{
		return this.id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public TicketState getState()
	{
		return this.state;
	}

	public void setState(TicketState state)
	{
		this.state = state;
	}

	public CustomerDTO getCustomer()
	{
		return this.customer;
	}

	public void setCustomer(CustomerDTO customer)
	{
		this.customer = customer;
    }

    public static Builder builder() {
        return new Builder();
    }

    public PerformanceDTO getPerformance() {
        return performance;
    }

    public void setPerformance(PerformanceDTO performance) {
        this.performance = performance;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketDTO ticketDTO = (TicketDTO) o;
        return Objects.equals(id, ticketDTO.id) &&
            state == ticketDTO.state &&
            Objects.equals(customer, ticketDTO.customer) &&
            Objects.equals(performance, ticketDTO.performance);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, state, customer, performance);
    }

    @Override
    public String toString() {
        return "TicketDTO{" +
            "id=" + id +
            ", state=" + state +
            ", customer=" + customer +
            ", performance=" + performance +
            '}';
    }


    public static final class Builder {
        private Long id;
        private TicketState state;
        private CustomerDTO customer;
        private PerformanceDTO performance;

        private Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder state(TicketState val) {
            state = val;
            return this;
        }

        public Builder customer(CustomerDTO val) {
            customer = val;
            return this;
        }

        public Builder performance(PerformanceDTO val) {
            performance = val;
            return this;
        }

        public TicketDTO build() {
            return new TicketDTO(this);
        }
    }
}
