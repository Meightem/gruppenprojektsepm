package at.ac.tuwien.inso.sepm.ticketline.rest.seat;

public enum SeatState {
    RESERVED, BOUGHT, FREE
}
