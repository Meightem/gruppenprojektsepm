package at.ac.tuwien.inso.sepm.ticketline.rest.seat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

@ApiModel(value = "EventDTO", description = "Represents a seat")
public class SeatDTO {
    @ApiModelProperty(readOnly = true, required = true, name = "The x coordinate of the seat")
    private Long xCoord;
    @ApiModelProperty(readOnly =  true, required = true, name = "The y coordinate of the sea")
    private Long yCoord;
    @ApiModelProperty(required = true, name = "The id of the hall, where the seat is placed")
    private Long id_hall;
    @ApiModelProperty(readOnly = true, name = "The sector of the seat")
    private Sector sector;
    @ApiModelProperty(readOnly = true, name = "The state of the seat")
    private SeatState state;

    private SeatDTO(Builder builder) {
        setxCoord(builder.xCoord);
        setyCoord(builder.yCoord);
        setId_hall(builder.id_hall);
        setSector(builder.sector);
        setState(builder.state);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getxCoord() {
        return xCoord;
    }

    public void setxCoord(Long xCoord) {
        this.xCoord = xCoord;
    }

    public Long getyCoord() {
        return yCoord;
    }

    public void setyCoord(Long yCoord) {
        this.yCoord = yCoord;
    }

    public Long getId_hall() {
        return id_hall;
    }

    public void setId_hall(Long id_hall) {
        this.id_hall = id_hall;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public SeatState getState() {
        return state;
    }

    public void setState(SeatState state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SeatDTO)) return false;
        SeatDTO seatDTO = (SeatDTO) o;
        return Objects.equals(getxCoord(), seatDTO.getxCoord()) &&
            Objects.equals(getyCoord(), seatDTO.getyCoord()) &&
            Objects.equals(getId_hall(), seatDTO.getId_hall()) &&
            getSector() == seatDTO.getSector();
        // getSector() == seatDTO.getSector() &&
        // getState() == seatDTO.getState();    // state is not reliable - we don't get it out of the database via the mapper, it's an emergent property of the data.
    }

    @Override
    public int hashCode() {

        return Objects.hash(getxCoord(), getyCoord(), getId_hall(), getSector());
    }

    @Override
    public String toString() {
        return "SeatDTO{" +
            "xCoord=" + xCoord +
            ", yCoord=" + yCoord +
            ", id_hall=" + id_hall +
            ", sector=" + sector +
            ", state=" + state +
            '}';
    }

    public SeatDTO() {
    }


    public static final class Builder {
        private Long xCoord;
        private Long yCoord;
        private Long id_hall;
        private Sector sector;
        private SeatState state;

        private Builder() {
        }

        public Builder xCoord(Long val) {
            xCoord = val;
            return this;
        }

        public Builder yCoord(Long val) {
            yCoord = val;
            return this;
        }

        public Builder id_hall(Long val) {
            id_hall = val;
            return this;
        }

        public Builder sector(Sector val) {
            sector = val;
            return this;
        }

        public Builder state(SeatState val) {
            state = val;
            return this;
        }

        public SeatDTO build() {
            return new SeatDTO(this);
        }
    }
}
