package at.ac.tuwien.inso.sepm.ticketline.rest.event;

public enum EventType {
    SECTORS, SEATS
}
