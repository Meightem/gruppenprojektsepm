package at.ac.tuwien.inso.sepm.ticketline.rest.event;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.Duration;
import java.util.Objects;

@ApiModel(value = "EventSearchDTO", description = "Represents a event")
public class EventSearchDTO {
    @ApiModelProperty(required = true, name = "Name of the event")
    private String name;

    @ApiModelProperty(required = true, name = "Content of the event")
    private String content;

    @ApiModelProperty(required = true, name = "Length of the event")
    private Duration length;

    @ApiModelProperty(required = true, name = "Type of the Event")
    private EventType type;

    private EventSearchDTO(Builder builder) {
        setName(builder.name);
        setContent(builder.content);
        setLength(builder.length);
        setType(builder.type);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Duration getLength() {
        return length;
    }

    public void setLength(Duration length) {
        this.length = length;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "EventSeatchDTO{" +
            "name='" + name + '\'' +
            ", content='" + content + '\'' +
            ", length=" + length +
            ", type=" + type +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventSearchDTO that = (EventSearchDTO) o;
        return Objects.equals(name, that.name) &&
            Objects.equals(content, that.content) &&
            Objects.equals(length, that.length) &&
            type == that.type;
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, content, length, type);
    }

    public EventSearchDTO() {
    }

    public static final class Builder {
        private String name;
        private String content;
        private Duration length;
        private EventType type;

        private Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder content(String val) {
            content = val;
            return this;
        }

        public Builder length(Duration val) {
            length = val;
            return this;
        }

        public Builder type(EventType val) {
            type = val;
            return this;
        }

        public EventSearchDTO build() {
            return new EventSearchDTO(this);
        }
    }
}
