package at.ac.tuwien.inso.sepm.ticketline.rest.event;

import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.Duration;
import java.util.Objects;

@ApiModel(value = "EventDTO", description = "Represents a event")
public class EventDTO {
    @ApiModelProperty(readOnly = true, name = "The automatically generated event ID")
    private Long id;

    @ApiModelProperty(required = true, name = "The artist")
    private ArtistDTO artist;

    @ApiModelProperty(required = true, name = "Name of the event")
    private String name;

    @ApiModelProperty(required = true, name = "Content of the event")
    private String content;

    @ApiModelProperty(required = true, name = "Length of the event")
    private Duration length;

    @ApiModelProperty(required = true, name = "Type of the Event")
    private EventType type;

    private EventDTO(Builder builder) {
        setId(builder.id);
        setArtist(builder.artist);
        setName(builder.name);
        setContent(builder.content);
        setLength(builder.length);
        setType(builder.type);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ArtistDTO getArtist() {
        return artist;
    }

    public void setArtist(ArtistDTO artist) {
        this.artist = artist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Duration getLength() {
        return length;
    }

    public void setLength(Duration length) {
        this.length = length;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventDTO eventDTO = (EventDTO) o;
        return Objects.equals(id, eventDTO.id) &&
            Objects.equals(artist, eventDTO.artist) &&
            Objects.equals(name, eventDTO.name) &&
            Objects.equals(content, eventDTO.content) &&
            Objects.equals(length, eventDTO.length) &&
            type == eventDTO.type;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, artist, name, content, length, type);
    }

    public EventDTO() {
    }

    public static final class Builder {
        private Long id;
        private ArtistDTO artist;
        private String name;
        private String content;
        private Duration length;
        private EventType type;

        private Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder artist(ArtistDTO val) {
            artist = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder content(String val) {
            content = val;
            return this;
        }

        public Builder length(Duration val) {
            length = val;
            return this;
        }

        public Builder type(EventType val) {
            type = val;
            return this;
        }

        public EventDTO build() {
            return new EventDTO(this);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;
            Builder builder = (Builder)o;
            return Objects.equals(id, builder.id) &&
                Objects.equals(artist, builder.artist) &&
                Objects.equals(name, builder.name) &&
                Objects.equals(content, builder.content) &&
                Objects.equals(length, builder.length) &&
                type == builder.type;
        }

        @Override
        public int hashCode() {

            return Objects.hash(id, artist, name, content, length, type);
        }
    }
}
