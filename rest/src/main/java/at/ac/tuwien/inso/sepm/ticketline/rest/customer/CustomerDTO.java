package at.ac.tuwien.inso.sepm.ticketline.rest.customer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

@ApiModel(value = "CustomerDTO", description = "Represents a customer")
public class CustomerDTO {
    @ApiModelProperty(readOnly = true, name = "The automatically generated customer ID")
    private Long id;

    @ApiModelProperty(required = true, name = "First name")
    private String firstName;

    @ApiModelProperty(required = true, name = "Last name")
    private String lastName;

    @ApiModelProperty(required = true, name = "Whether the provided user is the single anonymous user")
    private Boolean isAnonymousCustomer;

    private CustomerDTO(Builder builder) {
        setId(builder.id);
        setFirstName(builder.firstName);
        setLastName(builder.lastName);
        setIsAnonymousCustomer(builder.isAnonymousCustomer);
    }

    public CustomerDTO(){}

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getIsAnonymousCustomer() {
        return isAnonymousCustomer;
    }

    public void setIsAnonymousCustomer(Boolean isAnonymousCustomer) {
        this.isAnonymousCustomer = isAnonymousCustomer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerDTO customerDTO = (CustomerDTO) o;
        return Objects.equals(id, customerDTO.id) &&
            Objects.equals(firstName, customerDTO.firstName) &&
            Objects.equals(lastName, customerDTO.lastName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName);
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
            "id=" + id +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            '}';
    }

    public static final class Builder {
        private Long id;
        private String firstName;
        private String lastName;
        private Boolean isAnonymousCustomer;

        private Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder firstName(String val) {
            firstName = val;
            return this;
        }

        public Builder lastName(String val) {
            lastName = val;
            return this;
        }

        public Builder isAnonymousCustomer(boolean val) {
            isAnonymousCustomer = val;
            return this;
        }

        public CustomerDTO build() {
            return new CustomerDTO(this);
        }
    }
}
