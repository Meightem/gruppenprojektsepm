package at.ac.tuwien.inso.sepm.ticketline.rest.ticket;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

@ApiModel(value = "TicketSearchDTO", description = "Represents a ticketsearch request")
public class TicketSearchDTO {

    @ApiModelProperty(required = true, name = "The state of the ticket, which shall be searched")
    private TicketState state;

    @ApiModelProperty(required = true, name = "The customerID the ticketsearch belongs to.")
    private Long customerID;

    @ApiModelProperty(required = true, name = "The eventID the ticketsearch belongs to.")
    private Long eventID;

    private TicketSearchDTO(Builder builder) {
        setState(builder.state);
        setCustomerID(builder.customerID);
        setEventID(builder.eventID);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public TicketState getState() {
        return state;
    }

    public void setState(TicketState state) {
        this.state = state;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Long getEventID() {
        return eventID;
    }

    public void setEventID(Long eventID) {
        this.eventID = eventID;
    }

    public TicketSearchDTO() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        TicketSearchDTO that = (TicketSearchDTO)o;
        return state == that.state &&
            Objects.equals(customerID, that.customerID) &&
            Objects.equals(eventID, that.eventID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(state, customerID, eventID);
    }


    public static final class Builder {
        private TicketState state;
        private Long customerID;
        private Long eventID;

        public Builder() {}

        public Builder state(TicketState val) {
            state = val;
            return this;
        }

        public Builder customerID(Long val) {
            customerID = val;
            return this;
        }

        public Builder eventID(Long val) {
            eventID = val;
            return this;
        }

        public TicketSearchDTO build() {
            return new TicketSearchDTO(this);
        }
    }
}
