package at.ac.tuwien.inso.sepm.ticketline.rest.seat;

import javafx.scene.paint.Color;

import java.math.BigDecimal;

public enum Sector {
    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P;

    public BigDecimal getFactor(){
        switch (this){
            case A: return new BigDecimal(2);
            case B: return new BigDecimal(1.5);
            case C: return new BigDecimal(1.4);
            case D: return new BigDecimal(1.3);
            case E: return new BigDecimal(1.25);
            case F: return new BigDecimal(1.2);
            case G: return new BigDecimal(1.1);
            case H: return new BigDecimal(1); // normal price
            case I: return new BigDecimal(0.75); //reduced prices
            case J: return new BigDecimal(0.7);
            case K: return new BigDecimal(0.6);
            case L: return new BigDecimal(0.5);
            case M: return new BigDecimal(0.4);
            case O: return new BigDecimal(0.3);
            case P: return new BigDecimal(0.2);
        }

        return new BigDecimal(1);
    }

    public Color getColor() {
        switch (this) {
            case A:
                return Color.valueOf("#dc0d0e");
            case B:
                return Color.valueOf("#3fa45b");
            case C:
                return Color.valueOf("#de890d");
            case D:
                return Color.valueOf("#27a3dd");
            case E:
                return Color.valueOf("#9dc62d");
            case F:
                return Color.valueOf("#69767c");
            case G:
                return Color.valueOf("#416ae0");
            case H:
                return Color.valueOf("#2052e2");
            case I:
                return Color.valueOf("#ff6bf0");
            case J:
                return Color.valueOf("#ff6a8b");
            case K:
                return Color.valueOf("#69ffa7");
            case L:
                return Color.valueOf("#69ffff");
            case M:
                return Color.valueOf("#9a69ff");
            case N:
                return Color.valueOf("#a09500");
            case O:
                return Color.valueOf("#a0003a");
            case P:
                return Color.valueOf("#a04a00");
            default:
                return null;
        }
    }
}
