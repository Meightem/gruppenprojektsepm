package at.ac.tuwien.inso.sepm.ticketline.rest.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

@ApiModel(value = "UserDTO", description = "Represents a user")
public class UserDTO {
    @ApiModelProperty(readOnly = true, name = "The automatically generated user ID")
    private Long id;

    @ApiModelProperty(required = true, name = "User name")
    private String username;

    @ApiModelProperty(required = true, name = "Password")
    private String password;

    @ApiModelProperty(required = true, name = "Whether the user is admin")
    private Boolean isAdmin;

    @ApiModelProperty(required = true, name = "Failed attempts")
    private Integer failedAttempts;

    @ApiModelProperty(required = true, name = "Whether the user is locked")
    private Boolean isLocked;

    private UserDTO(Builder builder) {
        setId(builder.id);
        setUsername(builder.username);
        setPassword(builder.password);
        setAdmin(builder.isAdmin);
        setFailedAttempts(builder.failedAttempts);
        setLocked(builder.isLocked);
    }

    public UserDTO(){}

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public Integer getFailedAttempts() {
        return failedAttempts;
    }

    public void setFailedAttempts(Integer failedAttempts) {
        this.failedAttempts = failedAttempts;
    }

    public Boolean getLocked() {
        return isLocked;
    }

    public void setLocked(Boolean locked) {
        isLocked = locked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO user = (UserDTO) o;
        return Objects.equals(id, user.id) &&
            Objects.equals(username, user.username) &&
            Objects.equals(password, user.password) &&
            Objects.equals(isAdmin, user.isAdmin) &&
            Objects.equals(failedAttempts, user.failedAttempts) &&
            Objects.equals(isLocked, user.isLocked);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, username, password, isAdmin, failedAttempts, isLocked);
    }

    @Override
    public String toString() {
        return "User{" +
            "id=" + id +
            ", username='" + username + '\'' +
            ", isAdmin=" + isAdmin +
            ", failedAttempts=" + failedAttempts +
            ", isLocked=" + isLocked +
            '}';
    }


    public static final class Builder {
        private Long id;
        private String username;
        private String password;
        private Boolean isAdmin;
        private Integer failedAttempts;
        private Boolean isLocked;

        private Builder() {
            isAdmin = false;
            failedAttempts = 0;
            isLocked = false;
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder username(String val) {
            username = val;
            return this;
        }

        public Builder password(String val) {
            password = val;
            return this;
        }

        public Builder isAdmin(boolean val) {
            isAdmin = val;
            return this;
        }

        public Builder failedAttempts(int val) {
            failedAttempts = val;
            return this;
        }

        public Builder isLocked(boolean val) {
            isLocked = val;
            return this;
        }

        public UserDTO build() {
            return new UserDTO(this);
        }
    }
}
