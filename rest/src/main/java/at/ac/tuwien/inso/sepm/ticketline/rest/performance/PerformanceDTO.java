package at.ac.tuwien.inso.sepm.ticketline.rest.performance;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@ApiModel(value = "PerformanceDTO", description = "Represents a performance")
public class PerformanceDTO {
    @ApiModelProperty(readOnly = true, name = "The automatically generated performance ID")
    private Long id;

    @ApiModelProperty(required = true, name = "Date of the performance")
    private LocalDateTime date;

    @ApiModelProperty(required = true, name = "Price of the performance in the form Decimal(10,2)")
    private BigDecimal price;

    @ApiModelProperty(required = true, name = "Ehe event for this performance")
    private EventDTO event;

    @ApiModelProperty(required = true, name = "The hall in which this performance takes place")
    private HallDTO hall;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public EventDTO getEvent() {
        return event;
    }

    public void setEvent(EventDTO event) {
        this.event = event;
    }

    public HallDTO getHall() {
        return hall;
    }

    public void setHall(HallDTO hall) {
        this.hall = hall;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public PerformanceDTO(){

    }

    public PerformanceDTO(Long id, LocalDateTime date, BigDecimal price, EventDTO event, HallDTO hall) {
        this.id = id;
        this.date = date;
        this.price = price;
        this.event = event;
        this.hall = hall;
    }


    @Override
    public String toString() {
        return "PerformanceDTO{" +
            "id=" + id +
            ", date=" + date +
            ", price=" + price +
            ", event=" + event +
            ", hallId=" + hall +
            '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, price, event, hall);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformanceDTO that = (PerformanceDTO) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(date, that.date) &&
            Objects.equals(price, that.price) &&
            Objects.equals(event, that.event) &&
            Objects.equals(hall, that.hall);
    }

    public static class Builder {
        private Long id;
        private LocalDateTime date;
        private BigDecimal price;
        private HallDTO hall;
        private EventDTO event;

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setDate(LocalDateTime date) {
            this.date = date;
            return this;
        }

        public Builder setPrice(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder setHall(HallDTO hall) {
            this.hall = hall;
            return this;
        }

        public Builder setEvent(EventDTO event) {
            this.event = event;
            return this;
        }

        public PerformanceDTO build() {
            return new PerformanceDTO(id, date, price, event, hall);
        }

    }
}
