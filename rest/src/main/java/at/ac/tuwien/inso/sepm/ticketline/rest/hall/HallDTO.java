package at.ac.tuwien.inso.sepm.ticketline.rest.hall;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

@ApiModel(value = "HallDTO", description = "Represents a hall")
public class HallDTO {

    @ApiModelProperty(readOnly = true, name = "The automatically generated hall ID")
    private Long id;

    @ApiModelProperty(readOnly = true, name = "the street of teh hall")
    private String street;

    @ApiModelProperty(readOnly = true, name = "the city of the hall")
    private String city;

    @ApiModelProperty(readOnly = true, name = "teh name of the hall")
    private String name;

    @ApiModelProperty(readOnly = true, name = "the postcod of teh hall")
    private String postcode;

    @ApiModelProperty(readOnly = true, name = "teh country of the hall")
    private String country;

    private HallDTO(Builder builder) {
        setId(builder.id);
        setStreet(builder.street);
        setCity(builder.city);
        setName(builder.name);
        setPostcode(builder.postcode);
        setCountry(builder.country);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HallDTO hallDTO = (HallDTO) o;
        return Objects.equals(id, hallDTO.id) &&
            Objects.equals(street, hallDTO.street) &&
            Objects.equals(city, hallDTO.city) &&
            Objects.equals(name, hallDTO.name) &&
            Objects.equals(postcode, hallDTO.postcode) &&
            Objects.equals(country, hallDTO.country);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, street, city, name, postcode, country);
    }

    @Override
    public String toString() {
        return "HallDTO{" +
            "id=" + id +
            ", street='" + street + '\'' +
            ", city='" + city + '\'' +
            ", name='" + name + '\'' +
            ", postcode='" + postcode + '\'' +
            ", country='" + country + '\'' +
            '}';
    }

    public HallDTO() {
    }

    public HallDTO(Long id, String street, String city, String name, String postcode, String country) {
        this.id = id;
        this.street = street;
        this.city = city;
        this.name = name;
        this.postcode = postcode;
        this.country = country;
    }

    public static final class Builder {
        private Long id;
        private String street;
        private String city;
        private String name;
        private String postcode;
        private String country;

        private Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder street(String val) {
            street = val;
            return this;
        }

        public Builder city(String val) {
            city = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder postcode(String val) {
            postcode = val;
            return this;
        }

        public Builder country(String val) {
            country = val;
            return this;
        }

        public HallDTO build() {
            return new HallDTO(this);
        }
    }
}
