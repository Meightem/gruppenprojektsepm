package at.ac.tuwien.inso.sepm.ticketline.rest.ticket;

public enum TicketState {
    RESERVED, BOUGHT, CANCELLED
}
