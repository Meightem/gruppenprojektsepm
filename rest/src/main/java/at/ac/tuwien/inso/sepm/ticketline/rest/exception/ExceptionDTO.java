package at.ac.tuwien.inso.sepm.ticketline.rest.exception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Set;

@ApiModel(value = "ExceptionDTO", description = "Represents an exception")
public class ExceptionDTO {

    @ApiModelProperty(required = true, name = "bundleKey")
    private String bundleKey;
    @ApiModelProperty(name = "details")
    private String details;
    @ApiModelProperty(name = "constraintViolations")
    private Set<String> constraintViolations;

    public ExceptionDTO() {
    }

    private ExceptionDTO(Builder builder) {
        setBundleKey(builder.bundleKey);
        setDetails(builder.details);
        setConstraintViolations(builder.constraintViolations);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getBundleKey() {
        return bundleKey;
    }

    public void setBundleKey(String bundleKey) {
        this.bundleKey = bundleKey;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Set<String> getConstraintViolations() {
        return constraintViolations;
    }

    public void setConstraintViolations(Set<String> constraintViolations) {
        this.constraintViolations = constraintViolations;
    }

    public static final class Builder {
        private String bundleKey;
        private String details;
        private Set<String> constraintViolations;
        private Builder() {
        }

        public Builder bundleKey(String var) {
            bundleKey = var;
            return this;
        }
        public Builder details(String var) {
            details = var;
            return this;
        }
        public Builder constraintViolations(Set<String> var) {
            constraintViolations = var;
            return this;
        }

        public ExceptionDTO build() {
            return new ExceptionDTO(this);
        }
    }
}

