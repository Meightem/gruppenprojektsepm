package at.ac.tuwien.inso.sepm.ticketline.client.gui.hall;

import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatState;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SeatControl extends BorderPane {
    private static final Logger LOGGER = LoggerFactory.getLogger(SeatControl.class);

    private Color sectorColor;
    private double size;
    private CornerRadii cornerRadii;
    private static FontAwesome fontAwesome = new FontAwesome();
    private SimpleBooleanProperty selected;
    private SeatDTO seat;

    public SeatControl(SeatDTO seatDTO, double size) {
        this.seat = seatDTO;
        this.size = size;
        this.sectorColor = seatDTO.getSector().getColor();

        this.getStyleClass().setAll("seat");
        selected = new SimpleBooleanProperty(false);

        initUI();
        initListeners();
    }

    private void initUI() {
        //set size
        setMinSize(size, size);
        setPrefSize(size, size);
        setMaxSize(size, size);

        cornerRadii = new CornerRadii(size / 3, size / 3, size / 8, size / 8, false);

        //set background when seat is reserved
        if (this.seat.getState() != SeatState.FREE) {
            setBackground(
                new Background(new BackgroundFill(
                    this.sectorColor.darker().darker().darker(),
                    cornerRadii,
                    Insets.EMPTY)));
        }

        setBorder(
            new Border(new BorderStroke(sectorColor,
                BorderStrokeStyle.SOLID,
                cornerRadii,
                new BorderWidths((int) (1.5 + size / 24)))));
    }

    private void initListeners() {
        //bind PropertyWithUI
        selected.addListener((observable, oldValue, newValue) -> {
            if(seat.getState() != SeatState.FREE){
                selected.set(false);
                return;
            }
            if (newValue) {
                Glyph glyphNode = fontAwesome
                    .create(FontAwesome.Glyph.CHECK)
                    .size(this.size * 0.75)
                    .color(this.sectorColor);
                glyphNode.setMinSize(size, size);
                glyphNode.setAlignment(Pos.CENTER);
                glyphNode.setTextAlignment(TextAlignment.CENTER);
                setCenter(glyphNode);
            } else {
                setCenter(null);
            }
        });
        //Change state
        if(seat.getState()==SeatState.FREE) {
            setOnMouseClicked(event -> {
                selected.set(!selected.get());
            });
        }
    }

    public SeatDTO getSeat() {
        return seat;
    }

    public boolean isSelected() {
        return selected.get();
    }

    public SimpleBooleanProperty getSelectedProperty() {
        return selected;
    }
}
