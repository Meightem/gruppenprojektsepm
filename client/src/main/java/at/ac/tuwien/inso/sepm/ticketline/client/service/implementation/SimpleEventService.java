package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.EventRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventSearchDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.TopEventDTO;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.List;

@Service
public class SimpleEventService implements EventService {

    private final EventRestClient eventRestClient;

    public SimpleEventService(EventRestClient eventRestClient) {
        this.eventRestClient = eventRestClient;
    }


    @Override
    public EventDTO create(EventDTO dto) throws DataAccessException {
        return eventRestClient.create(dto);
    }

    @Override
    public List<EventDTO> findAll() throws DataAccessException {
        return eventRestClient.findAll();
    }

    @Override
    public List<EventDTO> searchEvent(String name, EventType type, String content, Duration length) throws DataAccessException {
        EventSearchDTO eventSearchDTO = EventSearchDTO.newBuilder()
            .name(name)
            .type(type)
            .content(content)
            .length(length)
           .build();

        return eventRestClient.searchEvent(eventSearchDTO);
    }

    @Override
    public List<EventDTO> findByArtist(ArtistDTO artistDTO) throws DataAccessException {
        return eventRestClient.findByArtist(artistDTO);
    }

    @Override
    public List<TopEventDTO> findTopTenEvents(@NotNull EventType type, @NotNull Integer month, @NotNull Integer year) throws DataAccessException {

        return eventRestClient.getTopTenEvents(type, month, year);
    }
}
