package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.UsersRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.UsersService;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleUsersService implements UsersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleUsersService.class);

    private final UsersRestClient usersRestClient;

    public SimpleUsersService(UsersRestClient usersRestClient) {
        this.usersRestClient = usersRestClient;
    }

    @Override
    public UserDTO create(UserDTO dto) throws DataAccessException {
        return usersRestClient.createUser(dto);
    }

    @Override
    public List<UserDTO> findAll() throws DataAccessException {
        return usersRestClient.findAll();
    }

    @Override
    public UserDTO update(UserDTO dto) throws DataAccessException {
        return usersRestClient.updateUser(dto);
    }

    @Override
    public UserDTO resetPassword(UserDTO dto) throws DataAccessException {
        return usersRestClient.updateUser(dto);
    }


}
