package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;

import java.util.List;

public interface UsersRestClient {
    UserDTO createUser(UserDTO dto) throws DataAccessException;

    List<UserDTO> findAll() throws DataAccessException;

    UserDTO updateUser(UserDTO dto) throws DataAccessException;
}
