package at.ac.tuwien.inso.sepm.ticketline.client.gui.news;

import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Component
public class NewsDetailedController {
    private static final Logger LOGGER = LoggerFactory.getLogger(NewsDetailedController.class);

    @FXML
    private VBox newsDetailedMainNode;

    @FXML
    private TabHeaderController detailedNewsHeaderController;

    @FXML
    private Label lblDate;

    @FXML
    private Text txtText;

    @FXML
    private Button btnClose;

    @FXML
    private Button btnMarkAsRead;

    @FXML
    private ImageView image;

    private EventHandler<ActionEvent> onClosedClick;

    private EventHandler<ActionEvent> onMarkAsReadClick;

    private static final DateTimeFormatter NEWS_DTF =
        DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG, FormatStyle.SHORT);

    private final static boolean enableBigImageView = true;

    private final static double normalImageWidth = 300;
    private final static double normalImageHeight = 100;

    private final static double bigImageWidth = 1200;
    private final static double bigImageHeight = 700;

    private final static double animationduration = 200;  //in ms

    public void setDetailedNewsDTO(DetailedNewsDTO detailedNewsDTO) {
        if (detailedNewsDTO == null) {
            return;
        }
        image.setFitHeight(normalImageHeight);
        image.setFitWidth(normalImageWidth);
        detailedNewsHeaderController.setTitle(detailedNewsDTO.getTitle());
        lblDate.setText(NEWS_DTF.format(detailedNewsDTO.getPublishedAt()));
        txtText.setText(detailedNewsDTO.getText());
        if (detailedNewsDTO.getImage() != null) {
            image.setImage(new Image(new ByteArrayInputStream(DatatypeConverter.parseBase64Binary(detailedNewsDTO.getImage()))));
        }else{
            image.setImage(null);
        }

    }

    @FXML
    private void initialize() {
        initializeTextBindings();
        detailedNewsHeaderController.setIcon(FontAwesome.Glyph.NEWSPAPER_ALT);

        if (enableBigImageView) {
            image.setOnMouseClicked(event -> {
                Timeline timeline = new Timeline();
                timeline.setCycleCount(1);
                timeline.setAutoReverse(false);
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(animationduration),
                    new KeyValue(image.fitWidthProperty(),
                        image.getFitWidth() <= normalImageWidth ? bigImageWidth : normalImageWidth
                    )
                ));
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(animationduration),
                    new KeyValue(image.fitHeightProperty(),
                        image.getFitHeight() <= normalImageHeight ? bigImageHeight : normalImageHeight
                    )
                ));
                timeline.play();
            });
        }
    }

    private void initializeTextBindings() {
        btnClose.textProperty().bind(BundleManager.getStringBinding("news.create.close"));
        btnMarkAsRead.textProperty().bind(BundleManager.getStringBinding("news.read.mark"));
    }

    @FXML
    public void onCloseClicked(ActionEvent event) {
        LOGGER.info("Clicked on close detailed news");
        if (onClosedClick != null) {
            onClosedClick.handle(event);
        }
    }

    @FXML
    public void onMarkAsReadClicked(ActionEvent event) {
        LOGGER.info("Clicked on mark as read");
        if (onMarkAsReadClick != null) {
            onMarkAsReadClick.handle(event);
        }
    }


    public void setOnClosedClick(EventHandler<ActionEvent> onClosedClick) {
        this.onClosedClick = onClosedClick;
    }

    public void setOnMarkAsReadClick(EventHandler<ActionEvent> onMarkAsReadClick) {
        this.onMarkAsReadClick = onMarkAsReadClick;
    }
}
