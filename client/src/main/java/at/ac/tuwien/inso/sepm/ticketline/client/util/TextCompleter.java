package at.ac.tuwien.inso.sepm.ticketline.client.util;

import javafx.scene.control.TextField;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import java.util.Collection;

/**
 * Wrapper around TextFields.bindAutocompletion to provide support for optional values.
 * @param <T> the value to autocomplete
 */
public class TextCompleter<T> {
    private final StringConverter<T> converter;
    private final Callback<AutoCompletionBinding.ISuggestionRequest, Collection<T>> suggestionProvider;
    private final TextField textField;
    private final AutoCompletionBinding<T> binding;

    private T value;

    public TextCompleter(TextField textField, Callback<AutoCompletionBinding.ISuggestionRequest, Collection<T>> suggestionProvider, StringConverter<T> converter) {
        this.converter = converter;
        this.suggestionProvider = suggestionProvider;
        this.textField = textField;
        textField.focusedProperty().addListener((state, wasFocused, isFocused) -> {
            setValue(getValue());
        });
        textField.setOnMouseClicked(event -> setValue(value));
        this.binding = TextFields.bindAutoCompletion(
            textField,
            suggestionProvider,
            converter
        );
        this.binding.setOnAutoCompleted(completion -> {
            this.value = completion.getCompletion();
        });
    }

    public T getValue() {
        if(textField.getText().isEmpty()) return null;
        return value;
    }

    public void setValue(T value) {
        textField.setText(value != null ? converter.toString(value) : "");
        this.value = value;
    }
}
