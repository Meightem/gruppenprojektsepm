package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.NewsRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.AuthenticationInformationService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.util.List;

@Component
public class SimpleNewsRestClient implements NewsRestClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleNewsRestClient.class);
    private static final String NEWS_URL = "/news";

    private final RestClient restClient;
    private final AuthenticationInformationService authInfoService;

    public SimpleNewsRestClient(RestClient restClient, AuthenticationInformationService authInfoService) {
        this.restClient = restClient;
        this.authInfoService = authInfoService;
    }

    @Override
    public List<SimpleNewsDTO> findAll() throws DataAccessException {
        try {
            LOGGER.debug("Retrieving all news from {}", restClient.getServiceURI(NEWS_URL));
            ResponseEntity<List<SimpleNewsDTO>> news =
                restClient.parseExceptionIfPossibleExchange(
                    restClient.getServiceURI(NEWS_URL),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<SimpleNewsDTO>>() {
                    });
            LOGGER.debug("Result status was {} with content {}", news.getStatusCode(), news.getBody());
            return news.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.news.findAll",e);
        }
    }

    @Override
    public void createNews(DetailedNewsDTO dto) throws DataAccessException {
        try {
            LOGGER.debug("Create News {} at {}", dto, restClient.getServiceURI(NEWS_URL));
            ResponseEntity<DetailedNewsDTO> response =
                restClient.parseExceptionIfPossibleExchange(
                    restClient.getServiceURI(NEWS_URL),
                    HttpMethod.POST,
                    new HttpEntity<Object>(dto),
                    new ParameterizedTypeReference<DetailedNewsDTO>() {
                    });
            LOGGER.debug("Result status was {} with content {}", response.getStatusCode(), response.getBody());
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.news.persistence",e);
        }
    }

    @Override
    public DetailedNewsDTO findDetailed(Long id) throws DataAccessException {
        try {
            LOGGER.debug("Search News with id {} at {}", id, restClient.getServiceURI(NEWS_URL + "/" + id));
            ResponseEntity<DetailedNewsDTO> response =
                restClient.parseExceptionIfPossibleExchange(
                    restClient.getServiceURI(NEWS_URL + "/" + id),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<DetailedNewsDTO>() {
                    });
            LOGGER.debug("Result status was {} with content {}", response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.news.detailed",e);
        }
    }

    @Override
    public void markAsRead(DetailedNewsDTO newsDTO) throws DataAccessException {
        try {
            LOGGER.debug("Marking news with id {} as read by user {}", newsDTO.getId(), authInfoService.getCurrentAuthenticationTokenInfo().get().getUsername());
            ResponseEntity<DetailedNewsDTO> response =
                restClient.parseExceptionIfPossibleExchange(
                    restClient.getServiceURI(NEWS_URL + '/' + newsDTO.getId() + "/read"),
                    HttpMethod.POST,
                    null,
                    new ParameterizedTypeReference<DetailedNewsDTO>() {
                    }
                );
            LOGGER.debug("Result status was {} with content {}", response.getStatusCode(), response.getBody());
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.news.read",e);
        }
    }

    @Override
    public List<SimpleNewsDTO> getUnreadNews() throws DataAccessException {
        try {
            LOGGER.debug("Gettin unread news for user {}", authInfoService.getCurrentAuthenticationTokenInfo().get().getUsername());
            ResponseEntity<List<SimpleNewsDTO>> response =
                restClient.parseExceptionIfPossibleExchange(
                    restClient.getServiceURI(NEWS_URL + "/unread"),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<SimpleNewsDTO>>() {
                    }
                );
            LOGGER.debug("Result status was {} with content {}", response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.news.unread",e);
        }
    }
}
