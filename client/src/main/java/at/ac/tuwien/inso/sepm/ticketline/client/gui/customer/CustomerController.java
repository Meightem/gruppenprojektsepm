package at.ac.tuwien.inso.sepm.ticketline.client.gui.customer;

import at.ac.tuwien.inso.sepm.ticketline.client.TicketlineClientApplication;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import com.sun.javafx.tools.packager.bundlers.Bundler;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.css.PseudoClass;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    private final MainController mainController;
    private final SpringFxmlLoader springFxmlLoader;
    private final CustomerService customerService;

    private final PseudoClass PSEUDO_CLASS_HOVER = PseudoClass.getPseudoClass("hover");


    @FXML
    public Glyph glyphSearch;
    public Label lblName;
    public Label lblCustomerIDHeading;
    public Label lblCustomerFirstnameHeading;
    public Label lblCustomerLastnameHeading;
    @FXML
    private TextField txtSearch;
    @FXML
    private Button btnSearch;
    @FXML
    private Button btnCreateCustomer;
    @FXML
    private Button btnClear;
    @FXML
    private Label lblCustomerEmpty;


    @FXML
    private ListView<CustomerDTO> lvCustomerOverview;

    @FXML
    private TabHeaderController tabHeaderController;

    public CustomerController(MainController mainController, SpringFxmlLoader springFxmlLoader, CustomerService customerService) {
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.customerService = customerService;
    }

    private final ObservableList<CustomerDTO> customerObservableList = FXCollections.observableArrayList();


    @FXML
    private void initialize() {
        tabHeaderController.setIcon(FontAwesome.Glyph.USERS);
        initializeTextBindings();
        initializeListView();
        initializeGlyph();
    }

    private void initializeGlyph() {
        btnSearch.setOnMouseEntered(event -> glyphSearch.pseudoClassStateChanged(PSEUDO_CLASS_HOVER, true));
        btnSearch.setOnMouseExited(event -> glyphSearch.pseudoClassStateChanged(PSEUDO_CLASS_HOVER, false));
        glyphSearch.setOnMouseExited(event -> glyphSearch.pseudoClassStateChanged(PSEUDO_CLASS_HOVER, btnSearch.isHover()));
    }

    private void initializeTextBindings() {
        lblName.textProperty().bind(BundleManager.getStringBinding("customer.name"));
        txtSearch.promptTextProperty().bind(BundleManager.getStringBinding("customer.name"));
        tabHeaderController.setTitleBinding(BundleManager.getStringBinding("tab.customer"));
        lblCustomerEmpty.textProperty().bind(BundleManager.getStringBinding("customer.empty"));
        btnCreateCustomer.textProperty().bind(BundleManager.getStringBinding("customer.create"));
        btnSearch.textProperty().bind(BundleManager.getStringBinding("customer.search"));
        btnClear.textProperty().bind(BundleManager.getStringBinding("customer.search.clear"));
        lblCustomerIDHeading.textProperty().bind(BundleManager.getStringBinding("customer.id"));
        lblCustomerFirstnameHeading.textProperty().bind(BundleManager.getStringBinding("general.firstName"));
        lblCustomerLastnameHeading.textProperty().bind(BundleManager.getStringBinding("general.lastName"));
    }

    private void initializeListView() {
        lvCustomerOverview.setCellFactory(lv -> {
            return new ListCell<CustomerDTO>() {
                @Override
                public void updateItem(CustomerDTO customer, boolean empty) {
                    super.updateItem(customer, empty);

                    if (empty || customer == null) {
                        setGraphic(null);
                    } else {
                        SpringFxmlLoader.Wrapper<CustomerElementController> wrapper = springFxmlLoader.loadAndWrap("/fxml/customer/customerElement.fxml");
                        wrapper.getController().setCustomer(customer);
                        setGraphic(wrapper.getLoadedObject());
                        getStyleClass().add("CustomerRow");
                        //ContextMenu
                        final ContextMenu contextMenu = new ContextMenu();
                        MenuItem editMenuItem = new MenuItem();
                        editMenuItem.textProperty().bind(BundleManager.getStringBinding("general.edit"));
                        editMenuItem.setId("menuItemEdit");
                        editMenuItem.setOnAction(event -> {
                            updateCustomer(customer);
                        });
                        contextMenu.getItems().add(editMenuItem);
                        contextMenu.getStyleClass().add("brightContextMenu");
                        setContextMenu(contextMenu);
                    }
                }
            };
        });
    }


    public void loadCustomers() {
        Task<List<CustomerDTO>> task = new Task<List<CustomerDTO>>() {
            @Override
            protected List<CustomerDTO> call() throws DataAccessException {
                return customerService.findAll();
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: loaded Customers");
                customerObservableList.setAll(this.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                JavaFXUtils.createExceptionDialog(getException(),
                    lvCustomerOverview.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();
    }

    public void createCustomer() {
        VBox box = springFxmlLoader.load("/fxml/customer/createCustomerComponent.fxml");
        Stage stage = new Stage();
        stage.setScene(new Scene(box));
        stage.titleProperty().bind(BundleManager.getStringBinding("customer.create"));
        stage.getIcons().add(new Image(TicketlineClientApplication.class.getResourceAsStream("/image/ticketlineIcon.png")));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(lvCustomerOverview.getScene().getWindow());
        stage.showAndWait();
        searchCustomers();
    }

    private void updateCustomer(CustomerDTO customer) {
        SpringFxmlLoader.AnyWrapper<CreateCustomerController, VBox> wrapper = springFxmlLoader.loadAndWrapAny("/fxml/customer/createCustomerComponent.fxml");
        VBox box = wrapper.getLoadedObject();
        Stage stage = new Stage();
        stage.setScene(new Scene(box));
        stage.titleProperty().bind(BundleManager.getStringBinding("customer.edit"));
        stage.getIcons().add(new Image(TicketlineClientApplication.class.getResourceAsStream("/image/ticketlineIcon.png")));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(lvCustomerOverview.getScene().getWindow());
        wrapper.getController().setCustomer(customer);
        stage.showAndWait();
        searchCustomers();
    }

    public ObservableList<CustomerDTO> getCustomerObservableList() {
        return customerObservableList;
    }

    public void searchCustomers() {
        String text = txtSearch.getText();
        if (text.isEmpty()) {
            loadCustomers();
            return;
        }

        Task<List<CustomerDTO>> task = new Task<List<CustomerDTO>>() {
            @Override
            protected List<CustomerDTO> call() throws DataAccessException {
                return customerService.search(text);
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: searched Customers");
                customerObservableList.setAll(this.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: searching customers. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    lvCustomerOverview.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();
    }

    public void clearSearch() {
        LOGGER.info("Clicked on: clear search");
        txtSearch.clear();
        searchCustomers();
    }
}
