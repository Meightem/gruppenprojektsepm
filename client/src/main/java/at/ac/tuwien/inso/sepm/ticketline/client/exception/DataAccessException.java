package at.ac.tuwien.inso.sepm.ticketline.client.exception;

import at.ac.tuwien.inso.sepm.ticketline.rest.exception.ExceptionDTO;

import java.util.Set;

public class DataAccessException extends Exception {
    private final String bundleKey;
    private final String details;
    private final Set<String> violations;

    public DataAccessException(String bundleKey) {
        this(bundleKey, "", null, null);
    }

    public DataAccessException(ExceptionDTO exceptionDTO, Throwable cause) {
        this(exceptionDTO.getBundleKey(), exceptionDTO.getDetails(), cause, exceptionDTO.getConstraintViolations());
    }

    public DataAccessException(String bundleKey, Throwable cause) {
        this(bundleKey, "", cause, null);
    }

    public DataAccessException(String bundleKey, String details, Throwable cause) {
        this(bundleKey, details, cause, null);
    }

    public DataAccessException(String bundleKey, String details, Throwable cause, Set<String> violations) {
        super(cause);
        this.bundleKey = bundleKey;
        this.details = details;
        this.violations = violations;
    }

    public String getBundleKey() {
        return bundleKey;
    }

    public String getDetails() {
        return details;
    }

    public Set<String> getViolations() {
        return violations;
    }
}
