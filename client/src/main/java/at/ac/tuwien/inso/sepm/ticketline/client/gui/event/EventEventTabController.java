package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import javafx.concurrent.Task;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.controlsfx.glyphfont.Glyph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import java.time.Duration;
import java.util.List;

@Controller
public class EventEventTabController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventEventTabController.class);
    @FXML
    private Label lblName;
    @FXML
    private TextField tfName;
    @FXML
    private Label lblTime;
    @FXML
    private TextField tfTime;
    @FXML
    private Label lblType;
    @FXML
    private ComboBox<String> cbType;
    @FXML
    private Label lblContent;
    @FXML
    private TextField tfContent;

    @FXML
    public Glyph glyphSearch;

    private final PseudoClass PSEUDO_CLASS_HOVER = PseudoClass.getPseudoClass("hover");

    @FXML
    private EventListController eventEventTabListController;

    @FXML
    private Button eventEventTabSearchBtn;

    private final EventService eventService;
    private final MainController mainController;

    public EventEventTabController(MainController mainController, EventService eventService) {
        this.eventService = eventService;
        this.mainController = mainController;
    }

    @FXML
    private void initialize(){
        initializeTextBindings();
        initializeGlyph();
        loadEvents();
    }

    private void initializeGlyph() {
        eventEventTabSearchBtn.setOnMouseEntered(event -> glyphSearch.pseudoClassStateChanged(PSEUDO_CLASS_HOVER, true));
        eventEventTabSearchBtn.setOnMouseExited(event -> glyphSearch.pseudoClassStateChanged(PSEUDO_CLASS_HOVER, false));
        glyphSearch.setOnMouseExited(event -> glyphSearch.pseudoClassStateChanged(PSEUDO_CLASS_HOVER, eventEventTabSearchBtn.isHover()));
    }

    private void initializeTextBindings(){
        lblName.textProperty().bind(BundleManager.getStringBinding("event.name"));
        tfName.promptTextProperty().bind(BundleManager.getStringBinding("event.name"));
        lblTime.textProperty().bind(BundleManager.getStringBinding("event.length"));
        tfTime.promptTextProperty().bind(BundleManager.getStringBinding("event.length"));
        lblType.textProperty().bind(BundleManager.getStringBinding("event.type"));
        lblContent.textProperty().bind(BundleManager.getStringBinding("event.content"));
        tfContent.promptTextProperty().bind(BundleManager.getStringBinding("event.content"));
        eventEventTabSearchBtn.textProperty().bind(BundleManager.getStringBinding("customer.search"));

        BundleManager.getBundleProperty().addListener(observable -> {
            cbType.getItems().setAll(BundleManager.getStringBinding("event.type.seats").get(),
                BundleManager.getStringBinding("event.type.sectors").get(), " ");
        });
        cbType.getItems().setAll(BundleManager.getStringBinding("event.type.seats").get(),
            BundleManager.getStringBinding("event.type.sectors").get(), " ");
    }

    public void searchButtonClicked(){
        LOGGER.info("Clicked on search button");
        Task<List<EventDTO>> task = new Task<List<EventDTO>>() {
            @Override
            protected List<EventDTO> call() throws DataAccessException {
                String name = null;
                Duration duration = null;
                String content = null;
                EventType eventType = null;

                if (!tfName.getText().equals("")){
                    name = tfName.getText();
                }

                if (!tfTime.getText().equals("")) {
                    try{
                        duration = Duration.ofHours(Long.parseLong(tfTime.getText()));
                    }
                    catch(NumberFormatException e){
                        throw new DataAccessException("exception.event.length");
                    }
                }

                if (!tfContent.getText().equals("")){
                    content = tfContent.getText();
                }

                if (cbType != null && cbType.getValue() != null) {
                    if (cbType.getValue().equals(" ")){
                        eventType = null;
                    } else if (cbType.getValue().equals(BundleManager.getStringBinding("event.type.seats").get())) {
                        eventType = EventType.SEATS;
                    } else if (cbType.getValue().equals(BundleManager.getStringBinding("event.type.sectors").get())) {
                        eventType = EventType.SECTORS;
                    }
                }

                if (name == null && duration == null && content == null && eventType == null){
                    return eventService.findAll();
                }

                return eventService.searchEvent(name, eventType, content, duration);
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: searched for events.");
                eventEventTabListController.getEventObservableList().setAll(this.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: searching events. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    eventEventTabSearchBtn.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();
    }

    public void loadEvents() {
        LOGGER.debug("Loading events");
        Task<List<EventDTO>> task = new Task<List<EventDTO>>() {
            @Override
            protected List<EventDTO> call() throws DataAccessException {
                return eventService.findAll();
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: loaded events");
                eventEventTabListController.getEventObservableList().setAll(this.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                JavaFXUtils.createExceptionDialog(getException(),
                    eventEventTabSearchBtn.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();
    }
}
