package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.ArtistRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.util.List;

@Component
public class SimpleArtistRestClient implements ArtistRestClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleEventRestClient.class);
    private static final String ARTISTS_URL = "/artists";

    public SimpleArtistRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    private final RestClient restClient;

    @Override
    public List<ArtistDTO> search(String text) throws DataAccessException {
        try {
            LOGGER.debug("Finding Artists by text");
            ResponseEntity<List<ArtistDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(ARTISTS_URL + String.format("/search/%s", text)),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ArtistDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            String msg = String.format(BundleManager.getExceptionBundle().getString("exception.artists.search"), e.getStatusCode().toString());
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(msg);
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(e.getMessage(), e);
        }
    }

    @Override
    public List<ArtistDTO> findAll() throws DataAccessException {
        try {
            LOGGER.debug("fetching all artists");
            ResponseEntity<List<ArtistDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(ARTISTS_URL),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ArtistDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            String msg = String.format(BundleManager.getExceptionBundle().getString("exception.artists.find"), e.getStatusCode().toString());
            throw new DataAccessException(msg);
        } catch (RestClientException e) {
            throw new DataAccessException(e.getMessage(), e);
        }
    }
}
