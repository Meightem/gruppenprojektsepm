package at.ac.tuwien.inso.sepm.ticketline.client.gui.customer;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Controller
@Scope("prototype")
public class CustomerElementController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerElementController.class);

    @FXML
    private Node mainNode;

    @FXML
    private Label lblCustomerID;

    @FXML
    private Label lblCustomerFirstname;

    @FXML
    private Label lblCustomerLastname;

    private CustomerDTO user;

    @Autowired
    public CustomerElementController() {

    }

    @FXML
    private void initialize() {
        updateView();
    }

    public void setCustomer(CustomerDTO user) {
        this.user = user;
        updateView();
    }

    private void updateView() {
        if (user == null) return;
        lblCustomerID.setText("#" + user.getId());
        lblCustomerFirstname.setText(user.getFirstName());
        lblCustomerLastname.setText(user.getLastName());
    }
}
