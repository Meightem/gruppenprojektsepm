package at.ac.tuwien.inso.sepm.ticketline.client.gui.news;

import at.ac.tuwien.inso.sepm.ticketline.client.TicketlineClientApplication;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.AuthenticationInformationService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.NewsService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class NewsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsController.class);

    @FXML
    private ListView<SimpleNewsDTO> lvNewsElements;

    @FXML
    private Button btnCreateNews;

    @FXML
    private Label lblNewsEmpty;

    @FXML
    private TabHeaderController tabHeaderController;

    @FXML
    private CheckBox cbShowReadNews;

    @FXML
    private SplitPane newsSplitPane;

    @FXML
    private Node newsDetailedComponent;

    @FXML
    private NewsDetailedController newsDetailedComponentController;

    public static final long TIMEOUTASREAD = 5000; //in millisecounds
    private long startTimeOfSelectedItem = 0;

    private final MainController mainController;
    private final SpringFxmlLoader springFxmlLoader;
    private final NewsService newsService;
    private final AuthenticationInformationService authenticationInformationService;

    private final ObservableList<SimpleNewsDTO> newsObservableList = FXCollections.observableArrayList();

    public NewsController(AuthenticationInformationService authenticationInformationService, MainController mainController, SpringFxmlLoader springFxmlLoader, NewsService newsService) {
        this.authenticationInformationService = authenticationInformationService;
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.newsService = newsService;
    }

    @FXML
    private void initialize() {
        tabHeaderController.setIcon(FontAwesome.Glyph.NEWSPAPER_ALT);
        initializeTextBindings();
        intializeListView();
        initCreateNewsBtn();
        filterNews();
        addReadChecker();
    }


    private void initializeTextBindings() {
        tabHeaderController.setTitleBinding(BundleManager.getStringBinding("tab.news"));
        btnCreateNews.textProperty().bind(BundleManager.getStringBinding("news.create"));
        lblNewsEmpty.textProperty().bind(BundleManager.getStringBinding("news.empty"));
        cbShowReadNews.textProperty().bind(BundleManager.getStringBinding("news.filter.read"));
    }

    private void intializeListView() {
        lvNewsElements.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        lvNewsElements.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
        {
            if (newValue != null) {
                setDetailedNews(newValue);
                newsSplitPane.setDividerPositions(0.3);
            } else {
                newsDetailedComponentController.setDetailedNewsDTO(null);
                newsSplitPane.setDividerPositions(1.0);
            }
        });

        lvNewsElements.setCellFactory(lv -> {
            return new ListCell<SimpleNewsDTO>() {
                @Override
                public void updateItem(SimpleNewsDTO simpleNewsDTO, boolean empty) {
                    super.updateItem(simpleNewsDTO, empty);
                    if (empty || simpleNewsDTO == null) {
                        setGraphic(null);
                    } else {
                        SpringFxmlLoader.Wrapper<NewsElementController> wrapper = springFxmlLoader.loadAndWrap("/fxml/news/newsElement.fxml");
                        wrapper.getController().initializeData(simpleNewsDTO);
                        newsDetailedComponentController.setOnClosedClick(event -> {
                            onNewsClosedClicked();
                        });
                        newsDetailedComponentController.setOnMarkAsReadClick(event -> {
                            setMarkAsRead(simpleNewsDTO);
                        });
                        setGraphic(wrapper.getLoadedObject());
                        getStyleClass().add("NewsRow");

                        final ContextMenu contextMenu = new ContextMenu();
                        MenuItem markAsRead = new MenuItem();

                        markAsRead.textProperty().bind(BundleManager.getStringBinding("news.read.mark"));
                        markAsRead.setId("markAsRead");
                        markAsRead.setOnAction(event -> {
                            setMarkAsRead(simpleNewsDTO);
                        });

                        contextMenu.getItems().addAll(markAsRead);
                        contextMenu.getStyleClass().add("brightContextMenu");
                        setContextMenu(contextMenu);

                    }
                }
            };
        });
    }

    private void onNewsClosedClicked() {
        newsSplitPane.setDividerPositions(1.0);
        lvNewsElements.getSelectionModel().clearSelection();
    }

    private void initCreateNewsBtn() {
        Optional<AuthenticationTokenInfo> authInfo = authenticationInformationService.getCurrentAuthenticationTokenInfo();
        btnCreateNews.setVisible(!(!authInfo.isPresent() || !authInfo.get().getRoles().contains("ADMIN")));
    }

    @FXML
    private void onCreateNewsClicked() {
        LOGGER.info("Clicked on create news");
        // open new window for creating news
        VBox vBox = springFxmlLoader.load("/fxml/news/newsCreationComponent.fxml");
        Stage stage = new Stage();
        stage.setScene(new Scene(vBox));
        stage.titleProperty().bind(BundleManager.getStringBinding("news.create"));
        stage.getIcons().add(new Image(TicketlineClientApplication.class.getResourceAsStream("/image/ticketlineIcon.png")));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(lvNewsElements.getScene().getWindow());
        stage.showAndWait();

        filterNews();
    }

    private void setDetailedNews(SimpleNewsDTO simpleNewsDTO) {
        Task<DetailedNewsDTO> task = new Task<DetailedNewsDTO>() {
            @Override
            protected DetailedNewsDTO call() throws Exception {
                return newsService.findDetailed(simpleNewsDTO.getId());
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                newsDetailedComponentController.setDetailedNewsDTO(super.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                JavaFXUtils.createExceptionDialog(getException(),
                    lvNewsElements.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    private void addReadChecker() {
        lvNewsElements.getSelectionModel().selectedItemProperty().addListener( (observable, oldValue, newValue) -> {
            long timeDelta = System.currentTimeMillis() - startTimeOfSelectedItem;
            if(timeDelta >=TIMEOUTASREAD){
                if(oldValue != null){
                    LOGGER.info("Marking {} as read after {} milliseconds",oldValue,timeDelta);
                    setMarkAsRead(oldValue);
                }
            }
            startTimeOfSelectedItem = System.currentTimeMillis();
        });
    }

    // mark news as read without detailed view
    public void setMarkAsRead(SimpleNewsDTO simpleNewsDTO) {
        Task<DetailedNewsDTO> task = new Task<DetailedNewsDTO>() {
            @Override
            protected DetailedNewsDTO call() throws Exception {
                DetailedNewsDTO detailedNewsDTO = newsService.findDetailed(simpleNewsDTO.getId());
                newsService.markAsRead(detailedNewsDTO);
                return detailedNewsDTO;
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                LOGGER.debug("Succeeded: Marked as read by detailed view: {}", super.getValue());
                filterNews();
            }

            @Override
            protected void failed() {
                super.failed();
                JavaFXUtils.createExceptionDialog(getException(),
                    lvNewsElements.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();
    }


    public void loadAllNews() {
        LOGGER.debug("Loading all news...");
        Task<List<SimpleNewsDTO>> task = new Task<List<SimpleNewsDTO>>() {
            @Override
            protected List<SimpleNewsDTO> call() throws DataAccessException {
                return newsService.findAll();
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                SimpleNewsDTO selected = lvNewsElements.getSelectionModel().getSelectedItem();
                newsObservableList.setAll(this.getValue());
                if(selected != null){
                    lvNewsElements.getSelectionModel().select(selected);
                }
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: to load all news.");
                JavaFXUtils.createExceptionDialog(getException(),
                    lvNewsElements.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    public void loadUnreadNews() {
        LOGGER.debug("Loading unread news...");
        Task<List<SimpleNewsDTO>> task = new Task<List<SimpleNewsDTO>>() {
            @Override
            protected List<SimpleNewsDTO> call() throws DataAccessException {
                return newsService.getUnreadNews();
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                LOGGER.debug("Succeeded: loaded unread news.");
                SimpleNewsDTO selected = lvNewsElements.getSelectionModel().getSelectedItem();
                newsObservableList.setAll(this.getValue());
                if(selected != null){
                    lvNewsElements.getSelectionModel().select(selected);
                }
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: to load unread news. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    lvNewsElements.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).run();
    }

    public ObservableList<SimpleNewsDTO> getNewsObservableList() {
        return newsObservableList;
    }

    public void filterNews() {
        LOGGER.debug("show read news: {}", cbShowReadNews.isSelected());
        if (cbShowReadNews.isSelected()) {
            loadAllNews();
        } else {
            loadUnreadNews();
        }
    }
}
