package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.client.TicketlineClientApplication;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("prototype")
public class EventListController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventListController.class);
    public Label lblNameHeading;
    public Label lblTypeHeading;
    public Label lblDurationHeading;

    @FXML
    private ListView<EventDTO> lvEventElements;

    @FXML
    private Label lblNoEvents;


    private final SpringFxmlLoader springFxmlLoader;
    private final ObservableList<EventDTO> eventObservableList;

    public ObservableList<EventDTO> getEventObservableList() {
        return eventObservableList;
    }

    public EventListController(SpringFxmlLoader springFxmlLoader) {
        this.eventObservableList = FXCollections.observableArrayList();
        this.springFxmlLoader = springFxmlLoader;
    }

    @FXML
    private void initialize() {
        initializeTextBindings();
        initializeListView();
    }

    private void initializeTextBindings() {
        lblNoEvents.textProperty().bind(BundleManager.getStringBinding("events.empty"));
        lblNameHeading.textProperty().bind(BundleManager.getStringBinding("event.name"));
        lblDurationHeading.textProperty().bind(BundleManager.getStringBinding("event.length"));
        lblTypeHeading.textProperty().bind(BundleManager.getStringBinding("event.type"));
    }

    private void initializeListView() {
        lvEventElements.setCellFactory(lv -> {
            return new ListCell<EventDTO>() {
                @Override
                public void updateItem(EventDTO eventDTO, boolean empty) {
                    super.updateItem(eventDTO, empty);
                    if (empty || eventDTO == null) {
                        setGraphic(null);
                    } else {
                        SpringFxmlLoader.Wrapper<EventElementController> wrapper = springFxmlLoader.loadAndWrap("/fxml/events/eventElement.fxml");
                        wrapper.getController().setEvent(eventDTO);

                        //DoubleClick to open detailview
                        wrapper.getLoadedObject().setOnMouseClicked(event -> {
                            if(event.getButton().equals(MouseButton.PRIMARY)&&event.getClickCount()>1){
                                onDetailedClicked(eventDTO);
                            }
                        });
                        setGraphic(wrapper.getLoadedObject());
                        getStyleClass().add("EventRow");

                        final ContextMenu contextMenu = new ContextMenu();
                        MenuItem showDetailed = new MenuItem();

                        showDetailed.textProperty().bind(BundleManager.getStringBinding("news.detailed.clicked"));
                        showDetailed.setId("showDetailedEvent");
                        showDetailed.setOnAction(event -> {
                            onDetailedClicked(eventDTO);
                        });

                        contextMenu.getStyleClass().add("brightContextMenu");
                        contextMenu.getItems().addAll(showDetailed);
                        setContextMenu(contextMenu);
                    }
                }
            };
        });
    }

    public void onDetailedClicked(EventDTO eventDTO) {
        LOGGER.info("Detailed event clicked {}", eventDTO);
        SpringFxmlLoader.AnyWrapper<EventDetailedController, VBox> objectWrapper = springFxmlLoader.loadAndWrapAny("/fxml/events/eventDetailedComponent.fxml");
        objectWrapper.getController().setEventDTO(eventDTO);
        Stage stage = new Stage();
        stage.titleProperty().bind(BundleManager.getStringBinding("news.detailed.header"));
        stage.getIcons().add(new Image(TicketlineClientApplication.class.getResourceAsStream("/image/ticketlineIcon.png")));
        stage.setScene(new Scene(objectWrapper.getLoadedObject()));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(lvEventElements.getScene().getWindow());
        stage.showAndWait();
    }
}
