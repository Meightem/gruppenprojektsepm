package at.ac.tuwien.inso.sepm.ticketline.client.gui.news;

import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NewsElementController {

    private static final DateTimeFormatter NEWS_DTF =
        DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT, FormatStyle.SHORT);

    @FXML
    private Label lblElementDate;

    @FXML
    private Label lblElementTitle;

    @FXML
    private Label lblElementText;

    public void initializeData(SimpleNewsDTO simpleNewsDTO) {
        lblElementDate.setText(NEWS_DTF.format(simpleNewsDTO.getPublishedAt()));
        lblElementTitle.setText(simpleNewsDTO.getTitle());
        lblElementText.setText(simpleNewsDTO.getSummary());
    }

}
