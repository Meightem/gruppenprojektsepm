package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;

import java.util.List;

public interface ArtistRestClient {
    /**
     * Search artists by name
     * @param text the search text
     * @return search results
     */
    List<ArtistDTO> search(String text) throws DataAccessException;

    /**
     * List all artists
     * @return list of all artists in db
     */
    List<ArtistDTO> findAll() throws DataAccessException;
}
