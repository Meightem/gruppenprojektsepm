package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.PerformanceRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PerformanceService;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SimplePerformanceService implements PerformanceService {

    private final PerformanceRestClient performanceRestClient;

    public SimplePerformanceService(PerformanceRestClient performanceRestClient) {
        this.performanceRestClient = performanceRestClient;
    }


    @Override
    public PerformanceDTO create(PerformanceDTO dto) throws DataAccessException {
        return performanceRestClient.create(dto);
    }

    @Override
    public List<PerformanceDTO> findByEvent(EventDTO eventDTO) throws DataAccessException {
        return performanceRestClient.findByEvent(eventDTO);
    }

    @Override
    public List<PerformanceDTO> findByHall(HallDTO hallDTO) throws DataAccessException {
        return performanceRestClient.findByHall(hallDTO);
    }

    @Override
    public List<SeatDTO> getSeatsByPerformance(PerformanceDTO performanceDTO) throws DataAccessException {
        return performanceRestClient.getSeatsByPerformance(performanceDTO);
    }

    @Override
    public Map<String, Long> getSeatsPerSectorByPerformance(PerformanceDTO performanceDTO) throws DataAccessException {
        return performanceRestClient.getSeatsPerSectorByPerformance(performanceDTO);
    }

    @Override
    public List<PerformanceDTO> search(PerformanceDTO criteria) throws DataAccessException {
        return performanceRestClient.search(criteria);
    }
}
