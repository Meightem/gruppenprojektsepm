package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.TicketRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketSearchDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class SimpleTicketRestClient implements TicketRestClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleTicketRestClient.class);
    private static final String TICKETS_URL = "/tickets";
    private static final String REFUND_RESERVED_URL = "/refundreserved/";
    private static final String REFUND_URL = "/refund/";
    private static final String SEATS_BY_SECTORS_URL = "/getSeatsBySectors/";
    private static final String SEATS_URL = "/getSeats/";
    private static final String BILL_URL = "/getBill/";
    private static final String SEARCH_URL = "/search";

    private final RestClient restClient;

    public SimpleTicketRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    @Override
    public List<TicketDTO> findAll() throws DataAccessException {
        try {
            LOGGER.debug("Finding all tickets");
            ResponseEntity<List<TicketDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<TicketDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.ticket.find",e);
        }
    }

    @Override
    public Long buyTicketForSeats(CustomerDTO customer, PerformanceDTO performance, List<SeatDTO> seats) throws DataAccessException {
        try {
            LOGGER.debug("Buying tickets");
            ResponseEntity<Long> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + String.format("/buyseats/%s/%s", customer.getId(), performance.getId())),
                HttpMethod.POST,
                new HttpEntity<>(seats),
                new ParameterizedTypeReference<Long>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.ticket.buy",e);
        }
    }

    @Override
    public Long buyTicketForSectors(CustomerDTO customer, PerformanceDTO performance, Map<String, Long> sectors) throws DataAccessException {
        try {
            LOGGER.debug("Buying tickets");
            ResponseEntity<Long> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + String.format("/buysectors/%s/%s", customer.getId(), performance.getId())),
                HttpMethod.POST,
                new HttpEntity<>(sectors),
                new ParameterizedTypeReference<Long>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.ticket.buy",e);
        }
    }

    @Override
    public TicketDTO buyReservedTicketForSectors(TicketDTO ticket, Map<String, Long> sectors) throws DataAccessException {
        try {
            LOGGER.debug("Buying reserved tickets for sector");
            ResponseEntity<TicketDTO> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + String.format("/buyreservedsectors/%s/", ticket.getId())),
                HttpMethod.POST,
                new HttpEntity<>(sectors),
                new ParameterizedTypeReference<TicketDTO>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            String msg = String.format(BundleManager.getExceptionBundle().getString("exception.ticket.buy"), e.getStatusCode().toString());
            throw new DataAccessException(msg + e.getResponseBodyAsString());
        } catch (RestClientException e) {
            throw new DataAccessException(e.getMessage(), e);
        }
    }

    @Override
    public TicketDTO buyReservedTicketForSeats(TicketDTO ticket, List<SeatDTO> seats) throws DataAccessException {
        try {
            LOGGER.debug("Buying tickets");
            ResponseEntity<TicketDTO> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + String.format("/buyreservedseats/%s", ticket.getId())),
                HttpMethod.POST,
                new HttpEntity<>(seats),
                new ParameterizedTypeReference<TicketDTO>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            String msg = String.format(BundleManager.getExceptionBundle().getString("exception.ticket.buy"), e.getStatusCode().toString());
            throw new DataAccessException(msg + e.getResponseBodyAsString());
        } catch (RestClientException e) {
            throw new DataAccessException(e.getMessage(), e);
        }
    }

    @Override
    public Long reserveTicketForSeats(CustomerDTO customer, PerformanceDTO performance, List<SeatDTO> seats) throws DataAccessException {
        try {
            LOGGER.debug("Reserving tickets");
            ResponseEntity<Long> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + String.format("/reserveseats/%s/%s", customer.getId(), performance.getId())),
                HttpMethod.POST,
                new HttpEntity<>(seats),
                new ParameterizedTypeReference<Long>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            LOGGER.error("Error: {}", e.getMessage());
            String msg = String.format(BundleManager.getExceptionBundle().getString("exception.ticket.reserve"), e.getStatusCode().toString());
            throw new DataAccessException(msg + e.getResponseBodyAsString());
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(e.getMessage(), e);
        }
    }

    @Override
    public Long reserveTicketForSectors(CustomerDTO customer, PerformanceDTO performance, Map<String, Long> sectors) throws DataAccessException {
        try {
            LOGGER.debug("Reserving tickets");
            ResponseEntity<Long> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + String.format("/reservesectors/%s/%s", customer.getId(), performance.getId())),
                HttpMethod.POST,
                new HttpEntity<>(sectors),
                new ParameterizedTypeReference<Long>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            LOGGER.error("Error: {}", e.getMessage());
            String msg = String.format(BundleManager.getExceptionBundle().getString("exception.ticket.reserve"), e.getStatusCode().toString());
            throw new DataAccessException(msg + e.getResponseBodyAsString());
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(e.getMessage(), e);
        }
    }


    @Override
    public void refundReserved(Long ticketId) throws DataAccessException {
        try {
            LOGGER.debug("Refund reserved Ticket {}", ticketId);
            ResponseEntity<Void> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + REFUND_RESERVED_URL + ticketId),
                HttpMethod.POST,
                null,
                new ParameterizedTypeReference<Void>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.ticket.refund.reserved",e);
        }
    }

    @Override
    public void refund(Long ticketId) throws DataAccessException {
        try {
            LOGGER.debug("Refund reserved Ticket {}", ticketId);
            ResponseEntity<Void> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + REFUND_URL + ticketId),
                HttpMethod.POST,
                null,
                new ParameterizedTypeReference<Void>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.ticket.refund",e);
        }
    }

    @Override
    public Map<String, Long> getSeatsBySector(Long id) throws DataAccessException {
        try {
            LOGGER.debug("Get Seats for ticket {}", id);
            ResponseEntity<Map<String, Long>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + SEATS_BY_SECTORS_URL + id),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Map<String, Long>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            String msg = String.format(BundleManager.getExceptionBundle().getString("exception.ticket.refund"), e.getStatusCode().toString());
            throw new DataAccessException(msg);
        } catch (RestClientException e) {
            throw new DataAccessException(e.getMessage(), e);
        }
    }

    @Override
    public List<SeatDTO> getSeats(Long id) throws DataAccessException {
        try {
            LOGGER.debug("Get Seats for ticket {}", id);
            ResponseEntity<List<SeatDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + SEATS_URL + id),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<SeatDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            String msg = String.format(BundleManager.getExceptionBundle().getString("exception.ticket.refund"), e.getStatusCode().toString());
            throw new DataAccessException(msg);
        }
    }

    @Override
    public byte[] getBill(Long ticketId) throws DataAccessException {
        try {
            LOGGER.debug("Getting bill for ticket {}", ticketId);
            ResponseEntity<byte[]> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + BILL_URL + ticketId),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<byte[]>() {
                }
            );
            LOGGER.debug("Result status was {}",
                response.getStatusCode());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(e.getMessage(), e);
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(e.getMessage(), e);
        }
    }

    @Override
    public List<TicketDTO> search(TicketSearchDTO ticketSearchDTO) throws DataAccessException {
        try {
            LOGGER.debug("Getting Ticket for TicketSearch");
            ResponseEntity<List<TicketDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + SEARCH_URL),
                HttpMethod.POST,
                new HttpEntity<>(ticketSearchDTO),
                new ParameterizedTypeReference<List<TicketDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(e.getMessage(), e);
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(e.getMessage(), e);
        }
    }

    @Override
    public TicketDTO search(Long id) throws DataAccessException {
        try {
            LOGGER.debug("Getting Ticket for id " + id);
            ResponseEntity<TicketDTO> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(TICKETS_URL + SEARCH_URL + "/" + id),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<TicketDTO>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            LOGGER.error("Error: {}", e.getMessage());
            if(e.getStatusCode() == HttpStatus.NOT_FOUND){
                return null;
            }
            throw new DataAccessException(e.getMessage(), e);
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(e.getMessage(), e);
        }
    }
}
