package at.ac.tuwien.inso.sepm.ticketline.client.gui.ticket;

import at.ac.tuwien.inso.sepm.ticketline.client.TicketlineClientApplication;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.client.util.TextCompleter;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketSearchDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.util.StringConverter;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;

@Component
public class TicketController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TicketController.class);
    public ListView lvTicketElements;
    public Label lblTicketsEmpty;

    private final ObservableList<TicketDTO> ticketsObservableList = FXCollections.observableArrayList();

    private final MainController mainController;
    private final SpringFxmlLoader springFxmlLoader;
    private final TicketService ticketService;
    private final Application application;
    private final EventService eventService;
    private final CustomerService customerService;
    public TextField txtSearchId;
    public Button btnSearch;
    public Button btnClear;
    public Label lblTicketId;
    public Label lblCustomer;
    public TextField txtCustomer;
    public Label lblEvent;
    public TextField txtEvent;
    public Label lblType;
    public ComboBox cbState;
    public Label lblTicketIdHeading;
    public Label lblTicketStateHeading;
    public Label lblCustomerFirstnameHeading;
    public Label lblCustomerLastnameHeading;
    public Label lblEventNameHeading;
    public Label lblPerformanceDateHeading;

    @FXML
    private Glyph glyphSearch;

    private TextCompleter<CustomerDTO> customerCompletionBinding;
    private TextCompleter<EventDTO> eventCompletionBinding;


    private final PseudoClass PSEUDO_CLASS_HOVER = PseudoClass.getPseudoClass("hover");
    @FXML
    private TabHeaderController tabHeaderController;

    @Autowired
    public TicketController(MainController mainController, SpringFxmlLoader springFxmlLoader, TicketService ticketService, Application application, EventService eventService, CustomerService customerService) {
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.ticketService = ticketService;
        this.application = application;
        this.eventService = eventService;
        this.customerService = customerService;
    }

    @FXML
    private void initialize() {
        tabHeaderController.setIcon(FontAwesome.Glyph.TICKET);
        initCustomerCompleter();
        initEventCompleter();
        initializeTextBindings();
        intializeListView();
        initializeGlyph();
    }

    private void initializeGlyph() {
        btnSearch.setOnMouseEntered(event -> glyphSearch.pseudoClassStateChanged(PSEUDO_CLASS_HOVER, true));
        btnSearch.setOnMouseExited(event -> glyphSearch.pseudoClassStateChanged(PSEUDO_CLASS_HOVER, false));
        glyphSearch.setOnMouseExited(event -> glyphSearch.pseudoClassStateChanged(PSEUDO_CLASS_HOVER, btnSearch.isHover()));
    }

    private void initCustomerCompleter() {
        customerCompletionBinding = new TextCompleter<>(
            txtCustomer,
            (suggestion) -> {
                if (suggestion.getUserText().isEmpty()) return Collections.emptyList();
                try {
                    return customerService.search(suggestion.getUserText());
                } catch (DataAccessException e) {
                    return Collections.emptyList();
                }
            },
            new StringConverter<>() {
                @Override
                public String toString(CustomerDTO object) {
                    return object.getId() + " " + object.getFirstName() + " " + object.getLastName();
                }

                @Override
                public CustomerDTO fromString(String string) {
                    try {
                        return customerService.search(string).get(0);
                    } catch (DataAccessException e) {
                        return null;
                    }
                }
            }
        );
    }

    private void initEventCompleter() {
        eventCompletionBinding = new TextCompleter<>(
            txtEvent,
            (suggestion) -> {
                if (suggestion.getUserText().isEmpty()) return Collections.emptyList();
                try {
                    return eventService.searchEvent(suggestion.getUserText(), null, null, null);
                } catch (DataAccessException e) {
                    return Collections.emptyList();
                }
            },
            new StringConverter<>() {
                @Override
                public String toString(EventDTO object) {
                    return object.getName();
                }

                @Override
                public EventDTO fromString(String string) {
                    try {
                        return eventService.searchEvent(string, null, null, null).get(0);
                    } catch (DataAccessException e) {
                        return null;
                    }
                }
            }
        );
    }

    private void intializeListView() {
        lvTicketElements.setCellFactory(lv -> {
            return new ListCell<TicketDTO>() {
                @Override
                public void updateItem(TicketDTO ticket, boolean empty) {
                    super.updateItem(ticket, empty);

                    if (empty || ticket == null) {
                        setGraphic(null);
                    } else {
                        SpringFxmlLoader.Wrapper<TicketElementController> wrapper = springFxmlLoader.loadAndWrap("/fxml/tickets/ticketElement.fxml");
                        wrapper.getController().setTicket(ticket);
                        setGraphic(wrapper.getLoadedObject());
                        getStyleClass().add("TicketRow");
                        //ContextMenu
                        final ContextMenu contextMenu = new ContextMenu();
                        if (ticket.getState() == TicketState.RESERVED) {
                            MenuItem refundReservedMenuItem = new MenuItem();
                            refundReservedMenuItem.textProperty().bind(BundleManager.getStringBinding("ticket.refund.reserved"));
                            refundReservedMenuItem.setId("RefundReservedMenuItem");
                            refundReservedMenuItem.setOnAction(event -> {
                                refundReserved(ticket.getId());
                            });

                            MenuItem buyReservedMenuItem = new MenuItem();
                            buyReservedMenuItem.textProperty().bind(BundleManager.getStringBinding("ticket.buy.reserved"));
                            buyReservedMenuItem.setId("buyReservedMenuItem");
                            buyReservedMenuItem.setOnAction(event -> {
                                buyReserved(ticket);
                            });

                            contextMenu.getItems().addAll(refundReservedMenuItem, buyReservedMenuItem);
                        }
                        if (ticket.getState() == TicketState.BOUGHT) {
                            MenuItem refundBoughtMenuItem = new MenuItem();
                            refundBoughtMenuItem.textProperty().bind(BundleManager.getStringBinding("ticket.refund"));
                            refundBoughtMenuItem.setId("RefundBoughtMenuItem");
                            refundBoughtMenuItem.setOnAction(event -> {
                                refundBought(ticket.getId());
                            });
                            contextMenu.getItems().add(refundBoughtMenuItem);
                        }
                        if(ticket.getState() != TicketState.RESERVED){
                            MenuItem printBillMenuItem = new MenuItem();
                            if(ticket.getState() == TicketState.BOUGHT)
                                printBillMenuItem.textProperty().bind(BundleManager.getStringBinding("ticket.print.bill"));
                            else
                                printBillMenuItem.textProperty().bind(BundleManager.getStringBinding("ticket.print.bill.cancelled"));
                            printBillMenuItem.setId("RefundBoughtMenuItem");
                            printBillMenuItem.setOnAction(event -> {
                                printBill(ticket.getId());
                            });
                            contextMenu.getItems().add(printBillMenuItem);
                        }
                        contextMenu.getStyleClass().add("brightContextMenu");
                        setContextMenu(contextMenu);
                    }
                }
            };
        });
    }

    private void printBill(Long id){
        LOGGER.debug("Printing bill");
        Task<byte[]> task = new Task<>() {
            @Override
            protected byte[] call() throws DataAccessException {
                return ticketService.getBill(id);
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: printed bill.");
                byte[] bytes = this.getValue();
                OutputStream out = null;
                try {
                    File file = new File("bills" + File.separator + id + ".pdf");
                    if(!file.exists()){
                        file.getParentFile().mkdirs();
                        file.createNewFile();
                    }
                    out = new FileOutputStream("bills" + File.separator + id + ".pdf");
                    out.write(bytes);
                    out.close();
                    application.getHostServices().showDocument(file.getAbsolutePath());

                } catch (FileNotFoundException e) {
                    JavaFXUtils.createExceptionDialog(e, lvTicketElements.getScene().getWindow()).showAndWait();
                } catch (IOException e) {
                    JavaFXUtils.createExceptionDialog(e, lvTicketElements.getScene().getWindow()).showAndWait();
                } finally {
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e) {
                            // ???
                        }
                    }
                }

            }

            @Override
            protected void failed() {
                super.failed();
                JavaFXUtils.createExceptionDialog(getException(),
                    lvTicketElements.getScene().getWindow()).showAndWait();
            }
        };
        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
        //application.getHostServices().showDocument(restClient.getServiceURI("/getBill/"+id).toString());
    }

    private void refundBought(Long id) {
        LOGGER.debug("Refunding Bought Ticket");
        Task<Void> task = new Task<>() {
            @Override
            protected Void call() throws DataAccessException {
                ticketService.refund(id);
                return null;
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: refund bought ticket.");
                updateListView();
            }

            @Override
            protected void failed() {
                super.failed();
                JavaFXUtils.createExceptionDialog(getException(),
                    lvTicketElements.getScene().getWindow()).showAndWait();
            }
        };
        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    private void refundReserved(Long id) {
        Task<Void> task = new Task<>() {
            @Override
            protected Void call() throws DataAccessException {
                ticketService.refundReserved(id);
                return null;
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: refund reserved ticket");
                updateListView();
            }

            @Override
            protected void failed() {
                super.failed();
                JavaFXUtils.createExceptionDialog(getException(),
                    lvTicketElements.getScene().getWindow()).showAndWait();
            }
        };
        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    protected void buyReserved(TicketDTO ticket) {
        LOGGER.info("Buying reserved ticket {}", ticket);

        Task<Object> task = new Task<>() {
            @Override
            protected Object call() throws DataAccessException {
                if (ticket.getPerformance().getEvent().getType() == EventType.SEATS) {
                    return (Object) ticketService.getSeats(ticket.getId());
                } else { // Sectors
                    return (Object) ticketService.getSeatsBySector(ticket.getId());
                }
            }

            @Override
            protected void succeeded() {
                SpringFxmlLoader.AnyWrapper<TicketSellController, AnchorPane> objectWrapper = springFxmlLoader.loadAndWrapAny("/fxml/ticket/ticketSellComponent.fxml");
                objectWrapper.getController().setTicket(ticket);
                if (ticket.getPerformance().getEvent().getType() == EventType.SEATS) {
                    objectWrapper.getController().setSelectedSeats((List<SeatDTO>) getValue());
                } else {
                    objectWrapper.getController().setExistingSeats((Map<String, Long>) getValue());
                }
                objectWrapper.getController().initializeHallPlan();

                Stage stage = new Stage();

                stage.titleProperty().bind(BundleManager.getStringBinding("ticket.buy.reserved"));
                stage.setMaximized(true);
                stage.getIcons().add(new Image(TicketlineClientApplication.class.getResourceAsStream("/image/ticketlineIcon.png")));
                stage.setOnHiding(we -> updateListView());
                stage.setScene(new Scene(objectWrapper.getLoadedObject()));
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(lvTicketElements.getScene().getWindow());
                stage.showAndWait();
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Could not open buy dialog", getException());
                JavaFXUtils.createExceptionDialog(getException(),
                    lvTicketElements.getScene().getWindow()).showAndWait();
            }
        };
        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    private void initializeTextBindings() {
        tabHeaderController.setTitleBinding(BundleManager.getStringBinding("tab.tickets"));
        btnSearch.textProperty().bind(BundleManager.getStringBinding("customer.search"));
        lblTicketId.textProperty().bind(BundleManager.getStringBinding("ticket.search.id"));
        txtSearchId.promptTextProperty().bind(BundleManager.getStringBinding("ticket.search.id"));
        lblCustomer.textProperty().bind(BundleManager.getStringBinding("ticket.search.customer"));
        txtCustomer.promptTextProperty().bind(BundleManager.getStringBinding("ticket.search.customer"));
        lblEvent.textProperty().bind(BundleManager.getStringBinding("ticket.search.event"));
        txtEvent.promptTextProperty().bind(BundleManager.getStringBinding("ticket.search.event"));
        lblType.textProperty().bind(BundleManager.getStringBinding("ticket.search.state"));
        lblTicketsEmpty.textProperty().bind(BundleManager.getStringBinding("tickets.empty"));
        BundleManager.getBundleProperty().addListener(observable -> {
            cbState.getItems().setAll(BundleManager.getStringBinding("ticketstate.reserved").get(),
                BundleManager.getStringBinding("ticketstate.bought").get(),
                BundleManager.getStringBinding("ticketstate.cancelled").get(), " ");
        });
        cbState.getItems().setAll(BundleManager.getStringBinding("ticketstate.reserved").get(),
            BundleManager.getStringBinding("ticketstate.bought").get(),
            BundleManager.getStringBinding("ticketstate.cancelled").get(), " ");
        lblTicketIdHeading.textProperty().bind(BundleManager.getStringBinding("ticket.id"));
        lblCustomerFirstnameHeading.textProperty().bind(BundleManager.getStringBinding("general.firstName"));
        lblCustomerLastnameHeading.textProperty().bind(BundleManager.getStringBinding("general.lastName"));
        lblEventNameHeading.textProperty().bind(BundleManager.getStringBinding("ticket.event"));
        lblPerformanceDateHeading.textProperty().bind(BundleManager.getStringBinding("ticket.date"));
        lblTicketStateHeading.textProperty().bind(BundleManager.getStringBinding("ticket.state"));
    }

    public ObservableList<TicketDTO> getTicketsObservableList() {
        return ticketsObservableList;
    }

    public void updateListView() {
        String idStr = txtSearchId.getText();
        if(idStr.length() > 0){
            Task<TicketDTO> task = new Task<TicketDTO>() {
                @Override
                protected TicketDTO call() throws DataAccessException {
                    try{
                        Long id = Long.parseLong(idStr);
                        return ticketService.search(id);
                    }
                    catch(NumberFormatException e){
                        txtSearchId.setText("");
                        throw new DataAccessException("exception.ticket.id",e);
                    }
                }

                @Override
                protected void succeeded() {
                    LOGGER.debug("Succeeded: updated Listview.");
                    if(this.getValue()==null){
                        ticketsObservableList.clear();
                    }
                    else {
                        ticketsObservableList.setAll(this.getValue());
                    }
                }

                @Override
                protected void failed() {
                    super.failed();
                    LOGGER.error("Failed: to update Listview.");
                    JavaFXUtils.createExceptionDialog(getException(),
                        lvTicketElements.getScene().getWindow()).showAndWait();
                }
            };
            task.runningProperty().addListener((observable, oldValue, running) ->
                mainController.setProgressbarProgress(
                    running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
            );
            new Thread(task).start();

        }
        else{
            Task<List<TicketDTO>> task = new Task<List<TicketDTO>>() {
                @Override
                protected List<TicketDTO> call() throws DataAccessException {
                    CustomerDTO customer = customerCompletionBinding.getValue();
                    EventDTO event = eventCompletionBinding.getValue();
                    TicketState ticketState = null;
                    if (cbState != null && cbState.getValue() != null) {
                        if (cbState.getValue().equals(" ")){
                            ticketState = null;
                        } else if (cbState.getValue().equals(BundleManager.getStringBinding("ticketstate.bought").get())) {
                            ticketState = TicketState.BOUGHT;
                        } else if (cbState.getValue().equals(BundleManager.getStringBinding("ticketstate.reserved").get())) {
                            ticketState = TicketState.RESERVED;
                        } else{
                            ticketState = TicketState.CANCELLED;
                        }
                    }
                    TicketSearchDTO ticketSearchDTO = TicketSearchDTO.newBuilder()
                        .customerID(customer==null?null:customer.getId())
                        .eventID(event==null?null:event.getId())
                        .state(ticketState).build();
                    if(ticketSearchDTO.getCustomerID() == null && ticketSearchDTO.getEventID() == null && ticketSearchDTO.getState() == null){
                        return ticketService.findAll();
                    }
                    return ticketService.search(ticketSearchDTO);
                }

                @Override
                protected void succeeded() {
                    LOGGER.debug("Succeeded: updated Listview.");
                    ticketsObservableList.setAll(this.getValue());
                }

                @Override
                protected void failed() {
                    super.failed();
                    LOGGER.error("Failed: to update Listview. ({}) ", getException().getMessage());
                    JavaFXUtils.createExceptionDialog(getException(),
                        lvTicketElements.getScene().getWindow()).showAndWait();
                }
            };
            task.runningProperty().addListener((observable, oldValue, running) ->
                mainController.setProgressbarProgress(
                    running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
            );
            new Thread(task).start();
        }
    }

    public void searchTickets(ActionEvent actionEvent) {
        this.updateListView();
    }
}
