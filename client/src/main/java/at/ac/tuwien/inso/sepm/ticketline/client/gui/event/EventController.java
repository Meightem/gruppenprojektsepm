package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.client.TicketlineClientApplication;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.EventRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.AuthenticationInformationService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class EventController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventController.class);

    @FXML
    private TabHeaderController tabHeaderController;

    @FXML
    private Tab tabTopTen;

    @FXML
    private Tab tabArtists;

    @FXML
    private Tab tabLocations;

    @FXML
    private Tab tabEvents;

    @FXML
    private Tab tabTime;

    @FXML
    private Button btnCreateEvent;

    @FXML
    private EventEventTabController eventEventTabController;

    @FXML
    private EventTopTenTabController eventTopTenTabController;

    @FXML
    private EventTimeTabController eventTimeTabController;

    @FXML
    private EventLocationTabController eventLocationTabController;

    private final AuthenticationInformationService authenticationInformationService;

    private final MainController mainController;
    private final SpringFxmlLoader springFxmlLoader;
    private final EventRestClient eventService;


    public EventController(AuthenticationInformationService authenticationInformationService, MainController mainController, SpringFxmlLoader springFxmlLoader, EventRestClient eventService) {
        this.authenticationInformationService = authenticationInformationService;
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.eventService = eventService;
    }

    @FXML
    private void initialize() {
        if (!isAdmin()) btnCreateEvent.setVisible(false);
        btnCreateEvent.toFront();
        tabHeaderController.setIcon(FontAwesome.Glyph.CALENDAR);
        initializeTextBindings();
    }

    private void initializeTextBindings() {
        tabHeaderController.setTitleBinding(BundleManager.getStringBinding("tab.events"));
        tabTopTen.textProperty().bind(BundleManager.getStringBinding("events.tab.topten"));
        tabArtists.textProperty().bind(BundleManager.getStringBinding("events.tab.artists"));
        tabLocations.textProperty().bind(BundleManager.getStringBinding("events.tab.locations"));
        tabEvents.textProperty().bind(BundleManager.getStringBinding("events.tab.events"));
        tabTime.textProperty().bind(BundleManager.getStringBinding("events.tab.time"));
        btnCreateEvent.textProperty().bind(BundleManager.getStringBinding("event.create"));
    }

    @FXML
    public void createEvent() {
        VBox box = springFxmlLoader.load("/fxml/events/createEventComponent.fxml");
        Stage stage = new Stage();
        stage.setScene(new Scene(box));
        stage.titleProperty().bind(BundleManager.getStringBinding("event.create"));
        stage.getIcons().add(new Image(TicketlineClientApplication.class.getResourceAsStream("/image/ticketlineIcon.png")));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(btnCreateEvent.getScene().getWindow());
        stage.showAndWait();

        eventEventTabController.loadEvents();
        //TODO: Later update each tablist
    }

    private boolean isAdmin() {
        Optional<AuthenticationTokenInfo> authInfo = authenticationInformationService.getCurrentAuthenticationTokenInfo();
        if (!authInfo.isPresent()) return false;
        return authInfo.get().getRoles().contains("ADMIN");
    }
}
