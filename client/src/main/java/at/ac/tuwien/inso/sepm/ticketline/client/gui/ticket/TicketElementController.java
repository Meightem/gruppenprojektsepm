package at.ac.tuwien.inso.sepm.ticketline.client.gui.ticket;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketState;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Component
public class TicketElementController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TicketElementController.class);
    public Label lblEventName;
    public Label lblPerformanceDate;
    private TicketDTO ticket;
    public Label lblTicketId;
    public Label lblTicketState;
    public Label lblCustomerFirstname;
    public Label lblCustomerLastname;
    public AnchorPane mainNode;

    private static final DateTimeFormatter TICKET_DTF =
        DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT, FormatStyle.SHORT);

    @Autowired
    public TicketElementController() {

    }

    @FXML
    private void initialize() {
        updateView();
    }

    public void setTicket(TicketDTO ticket) {
        this.ticket = ticket;
        updateView();
    }

    private void updateView() {
        if (ticket == null) return;
        lblTicketId.setText("#" + ticket.getId());
        if(ticket.getState() == TicketState.BOUGHT){
            lblTicketState.textProperty().bind(BundleManager.getStringBinding("ticketstate.bought"));
        }
        else if(ticket.getState() == TicketState.RESERVED){
            lblTicketState.textProperty().bind(BundleManager.getStringBinding("ticketstate.reserved"));
        }
        else{
            lblTicketState.textProperty().bind(BundleManager.getStringBinding("ticketstate.cancelled"));
        }
        lblCustomerFirstname.setText(ticket.getCustomer().getFirstName());
        lblCustomerLastname.setText(ticket.getCustomer().getLastName());
        lblEventName.setText(ticket.getPerformance().getEvent().getName());
        lblPerformanceDate.setText(TICKET_DTF.format(ticket.getPerformance().getDate()));
    }
}
