package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Duration;

import static at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType.SEATS;
import static at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType.SECTORS;

@Component
public class CreateEventController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateEventController.class);

    @FXML
    private TabHeaderController headerController;

    private final MainController mainController;
    private final SpringFxmlLoader springFxmlLoader;
    private EventService eventService;

    @FXML
    private TextField txtArtistFN;
    @FXML
    private TextField txtArtistLN;
    @FXML
    private TextField txtName;
    @FXML
    private TextField txtContent;
    @FXML
    public TextField txtLength;

    @FXML
    private Label lblArtistFN;
    @FXML
    private Label lblArtistLN;
    @FXML
    public Label lblName;
    @FXML
    public Label lblContent;
    @FXML
    public Label lblLength;
    @FXML
    public Label lblType;

    @FXML
    public RadioButton radioTypeSectors;
    @FXML
    public ToggleGroup radioType;
    @FXML
    public RadioButton radioTypeSeats;

    @FXML
    private Button btnCancel;
    @FXML
    private Button btnSubmit;

    public CreateEventController(MainController mainController, SpringFxmlLoader springFxmlLoader, EventService eventService) {
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.eventService = eventService;
    }

    @FXML
    private void initialize() {
        updateView();
    }

    private void initializeTextBindings() {
        headerController.setTitleBinding(BundleManager.getStringBinding("event.create"));
        lblArtistLN.textProperty().bind(BundleManager.getStringBinding("event.artistLN"));
        lblArtistFN.textProperty().bind(BundleManager.getStringBinding("event.artistFN"));
        lblName.textProperty().bind(BundleManager.getStringBinding("event.name"));
        lblContent.textProperty().bind(BundleManager.getStringBinding("event.content"));
        lblLength.textProperty().bind(BundleManager.getStringBinding("event.length"));
        lblType.textProperty().bind(BundleManager.getStringBinding("event.type"));
        radioTypeSeats.textProperty().bind(BundleManager.getStringBinding("event.type.seats"));
        radioTypeSectors.textProperty().bind(BundleManager.getStringBinding("event.type.sectors"));
        btnCancel.textProperty().bind(BundleManager.getStringBinding("general.cancel"));
        btnSubmit.textProperty().bind(BundleManager.getStringBinding("general.submit"));
    }

    public void submit() {
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws DataAccessException {
                Duration length;
                try {
                    length = Duration.ofHours(Integer.parseInt(txtLength.getText()));
                } catch(NumberFormatException e) {
                    throw new DataAccessException("exception.event.create.length",e);
                }
                EventDTO event = EventDTO.newBuilder()
                    .artist(ArtistDTO.newBuilder().firstName(txtArtistFN.getText()).lastName(txtArtistLN.getText()).build())
                    .name(txtName.getText())
                    .content(txtContent.getText())
                    .length(length)
                    .type(radioType.getSelectedToggle() == radioTypeSeats ? SEATS : SECTORS)
                    .build();

                eventService.create(event);
                return null; // We need to return an instance of Void, just return null.
                // Is `(Void) null` more void than Void?
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: created Event");
                ((Stage) txtArtistLN.getScene().getWindow()).close();
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: Event was not created ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    txtArtistLN.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    @FXML
    private void cancel() {
        ((Stage) txtArtistLN.getScene().getWindow()).close();
    }

    private void updateView() {
        headerController.setIcon(FontAwesome.Glyph.CALENDAR);
        initializeTextBindings();
    }
}
