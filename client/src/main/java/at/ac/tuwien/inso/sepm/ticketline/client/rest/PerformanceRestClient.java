package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;

import java.util.List;
import java.util.Map;

public interface PerformanceRestClient {
    /**
     * Create a single performance
     *
     * @param dto The performance to create
     * @return The created performance
     * @throws DataAccessException when event couldn't be created
     */
    PerformanceDTO create(PerformanceDTO dto) throws DataAccessException;

    /**
     * finds performances belonging to an event
     *
     * @param eventDTO event from which to find belonging performances
     * @return list of all performances
     * @throws DataAccessException when performances could not be readout
     */
    List<PerformanceDTO> findByEvent(EventDTO eventDTO) throws DataAccessException;

    /**
     * finds performances belonging to a hall
     *
     * @param hallDTO hall from which to find belonging performances
     * @return list of all performances
     * @throws DataAccessException when performances could not be readout
     */
    List<PerformanceDTO> findByHall(HallDTO hallDTO) throws DataAccessException;

    /**
     * gets seats belonging to an performance
     *
     * @param performanceDTO performance which the seats belong to
     * @return list of seats
     * @throws DataAccessException when seats could not be read out
     */
    List<SeatDTO> getSeatsByPerformance(PerformanceDTO performanceDTO) throws DataAccessException;

    /**
     * gets the available seats per sector belonging to an performance
     *
     * @param performanceDTO performance which the sectormap belongs to
     * @return map from sector to available seatscount
     * @throws DataAccessException when seatscount could not be read out
     */
    Map<String, Long> getSeatsPerSectorByPerformance(PerformanceDTO performanceDTO) throws DataAccessException;

    /**
     * Search performances by the criteria given in the dto
     * @param criteria The search criteria. Leave null for wildcard
     * @return a list of search results
     */
    List<PerformanceDTO> search(PerformanceDTO criteria) throws DataAccessException;
}
