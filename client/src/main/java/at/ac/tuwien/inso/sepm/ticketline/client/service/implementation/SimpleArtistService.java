package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.ArtistRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.ArtistService;
import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleArtistService implements ArtistService {
    private ArtistRestClient artistRestClient;

    public SimpleArtistService(ArtistRestClient artistRestClient) {
        this.artistRestClient = artistRestClient;
    }

    @Override
    public List<ArtistDTO> search(String text) throws DataAccessException {
        return artistRestClient.search(text);
    }

    @Override
    public List<ArtistDTO> findAll() throws DataAccessException {
        return artistRestClient.findAll();
    }
}
