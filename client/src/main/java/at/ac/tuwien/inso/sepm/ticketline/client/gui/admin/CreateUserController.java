package at.ac.tuwien.inso.sepm.ticketline.client.gui.admin;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.UsersService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CreateUserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateUserController.class);

    @FXML
    private TabHeaderController headerController;

    private final MainController mainController;
    private final SpringFxmlLoader springFxmlLoader;
    private final UsersService usersService;

    @FXML
    private TextField txtUsername;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private PasswordField txtConfirmPassword;
    @FXML
    private CheckBox cbIsAdmin;
    @FXML
    private Label lblUsername;
    @FXML
    private Label lblPassword;
    @FXML
    private Label lblPasswordConfirm;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnSubmit;


    public CreateUserController(MainController mainController, SpringFxmlLoader springFxmlLoader, UsersService usersService) {
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.usersService = usersService;
    }

    @FXML
    private void initialize() {
        headerController.setIcon(FontAwesome.Glyph.USER_PLUS);
        initializeTextBindings();
    }

    private void initializeTextBindings() {
        headerController.setTitleBinding(BundleManager.getStringBinding("user.create"));
        lblUsername.textProperty().bind(BundleManager.getStringBinding("general.username"));
        lblPassword.textProperty().bind(BundleManager.getStringBinding("general.password"));
        lblPasswordConfirm.textProperty().bind(BundleManager.getStringBinding("general.password.confirm"));
        cbIsAdmin.textProperty().bind(BundleManager.getStringBinding("user.create.isAdmin"));
        btnCancel.textProperty().bind(BundleManager.getStringBinding("general.cancel"));
        btnSubmit.textProperty().bind(BundleManager.getStringBinding("general.submit"));
    }

    @FXML
    private void cancel() {
        ((Stage) txtUsername.getScene().getWindow()).close();
    }

    @FXML
    private void submit() {
        if (!txtPassword.getText().equals(txtConfirmPassword.getText())) {
            JavaFXUtils.createErrorDialog(BundleManager.getStringBinding("user.create.passwordMismatch"),
                cbIsAdmin.getScene().getWindow()).showAndWait();
            return;
        }

        Task<UserDTO> task = new Task<UserDTO>() {
            @Override
            protected UserDTO call() throws DataAccessException {
                UserDTO dto = UserDTO.newBuilder()
                    .username(txtUsername.getText())
                    .password(txtPassword.getText())
                    .isAdmin(cbIsAdmin.isSelected())
                    .build();
                return usersService.create(dto);
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: User created");
                Alert a = new Alert(Alert.AlertType.INFORMATION);
                a.setHeaderText(BundleManager.getBundle().getString("user.create.success"));
                a.setContentText(String.format("%s: %s - %s", BundleManager.getBundle().getString("user.name"), getValue().getId(), getValue().getUsername()));
                a.showAndWait();
                ((Stage) cbIsAdmin.getScene().getWindow()).close();
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: User not created. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    cbIsAdmin.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }
}
