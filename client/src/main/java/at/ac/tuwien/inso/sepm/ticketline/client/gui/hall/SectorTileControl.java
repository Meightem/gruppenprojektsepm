package at.ac.tuwien.inso.sepm.ticketline.client.gui.hall;

import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import javafx.geometry.Insets;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class SectorTileControl extends Pane {

    private double size;
    private SeatDTO Left;
    private SeatDTO Top;
    private SeatDTO Right;
    private SeatDTO Bottom;
    private Color sectorColor;
    private SeatDTO seat;

    public SectorTileControl(SeatDTO seatDTO, SeatDTO Left, SeatDTO Top, SeatDTO Right, SeatDTO Bottom, double size) {
        this.size = size;
        this.Left = Left;
        this.Top = Top;
        this.Right = Right;
        this.Bottom = Bottom;
        this.sectorColor = seatDTO.getSector().getColor();
        this.seat = seatDTO;

        initUI();
    }

    private void initUI() {
        //set size
        setMinSize(size, size);
        setPrefSize(size, size);
        setMaxSize(size, size);

        double radius = size / 2;
        CornerRadii cornerRadii = new CornerRadii(
            (Left == null || !Left.getSector().equals(seat.getSector())) && (Top == null || !Top.getSector().equals(seat.getSector())) ? radius : 0,
            (Right == null || !Right.getSector().equals(seat.getSector())) && (Top == null || !Top.getSector().equals(seat.getSector())) ? radius : 0,
            (Right == null || !Right.getSector().equals(seat.getSector())) && (Bottom == null || !Bottom.getSector().equals(seat.getSector())) ? radius : 0,
            (Left == null || !Left.getSector().equals(seat.getSector())) && (Bottom == null || !Bottom.getSector().equals(seat.getSector())) ? radius : 0,
            false);

        setBackground(
            new Background(new BackgroundFill(
                this.sectorColor,
                cornerRadii,
                Insets.EMPTY)));


        double borderwidth = (int) (1.5 + size / 24);
        setBorder(
            new Border(new BorderStroke(sectorColor.darker(),
                BorderStrokeStyle.SOLID,
                cornerRadii,
                new BorderWidths(
                    (Top == null || !Top.getSector().equals(seat.getSector())) ? borderwidth : 0,
                    (Right == null || !Right.getSector().equals(seat.getSector())) ? borderwidth : 0,
                    (Bottom == null || !Bottom.getSector().equals(seat.getSector())) ? borderwidth : 0,
                    (Left == null || !Left.getSector().equals(seat.getSector())) ? borderwidth : 0
                ))));
    }
}

