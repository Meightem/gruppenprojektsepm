package at.ac.tuwien.inso.sepm.ticketline.client.util;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import javafx.beans.binding.StringBinding;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Window;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * This class provides helper methods for JavaFX.
 */
public class JavaFXUtils {

    /**
     * An empty private constructor to prevent the creation of an JavaFXUtils Instance.
     */
    private JavaFXUtils() {

    }

    /**
     * Creates a very basic dialog which can be used to display diffrent kinds of errors
     *
     * @return the dialog with set basics
     */
    private static Alert getBasicErrorAlert(Window parent) {
        Alert alert = new Alert(AlertType.ERROR);
        if (parent != null) {
            alert.initOwner(parent);
            alert.initModality(Modality.WINDOW_MODAL);
        }
        alert.titleProperty().bind(BundleManager.getExceptionStringBinding("error"));
        return alert;
    }

    /**
     * Creates a dialog specific for DataAccessExceptions
     *
     * @param dataAccessException the DataAccessException to display
     * @return the dialog which shows the DataAccessException
     */
    public static Alert createDataExceptionDialog(DataAccessException dataAccessException, Window parentWindow) {
        Alert alert = getBasicErrorAlert(parentWindow);
        String bundleKey = dataAccessException.getBundleKey();
        if (bundleKey == null) {
            bundleKey = "exception.unexpected";
        }
        String headerBundleKey = bundleKey.substring(0, bundleKey.lastIndexOf('.'));
        if (BundleManager.getExceptionBundle().containsKey(headerBundleKey)) {
            alert.headerTextProperty().bind(BundleManager.getExceptionStringBinding(headerBundleKey));
        } else {
            alert.headerTextProperty().bind(BundleManager.getExceptionStringBinding("exception.dataaccess"));
        }
        alert.setContentText(parseMessage(dataAccessException.getBundleKey()));
        String details = parseMessage(dataAccessException.getDetails());
        String hints = violationsToString(dataAccessException.getViolations());
        if ((details == null || details.isEmpty()) && (hints == null || hints.isEmpty())) {
            hints = BundleManager.getExceptionStringBinding("exception.moredetails.empty").get();
        }

        alert.getDialogPane().setExpandableContent(
            getExpContent(BundleManager.getExceptionStringBinding("exception.moredetails"),
                String.format("%s%s", (hints == null ? "" : hints+"\n"), details == null ? "" : details)
            )
        );
        return alert;
    }

    /**
     * Creates a dialog for an exception.
     *
     * @param throwable the throwable
     * @return the dialog which shows the exception
     */
    public static Alert createExceptionDialog(Throwable throwable, Window parentWindow) {
        Alert alert;
        if (throwable instanceof DataAccessException) {
            return createDataExceptionDialog((DataAccessException) throwable, parentWindow);
        }
        alert = getBasicErrorAlert(parentWindow);
        alert.headerTextProperty().bind(BundleManager.getExceptionStringBinding("exception.unexpected"));
        alert.setContentText(throwable.getLocalizedMessage());

        alert.getDialogPane().setExpandableContent(
            getExpContent(BundleManager.getExceptionStringBinding("exception.stacktrace"),
                stacktraceToString(throwable)
            )
        );
        return alert;
    }

    /**
     * Parses a set of violation strings to 1 string message
     *
     * @param violations sets of violation strings
     * @return the parsed message
     */
    private static String violationsToString(Set<String> violations) {
        if (violations == null)
            return null;
        StringBuilder details = new StringBuilder();
        for (String violationKey : violations) {
            String parsedMessage = parseMessage(violationKey);
            if (!parsedMessage.isEmpty()) {
                details.append(parsedMessage);
                details.append("\n");
            }
        }
        return details.toString();
    }

    /**
     * Parses one bundleKey (with optional formatting required)
     * E.g "exception.news.persistence.title;1;100" returns "Newstitle has to be between 1 and 100 characters."
     * Language will also be taken into consideration
     *
     * @param bundleKey single bundleKey with maybe some parameters split by ;
     * @return the parsed message
     */
    private static String parseMessage(String bundleKey) {
        if (bundleKey == null || bundleKey.isEmpty())
            return "";

        String[] violationParts = bundleKey.split(";");

        ResourceBundle exceptionBundle = BundleManager.getExceptionBundle();
        if (!exceptionBundle.containsKey(violationParts[0])) {
            return exceptionBundle.getString("exception.unexpected");
        }

        if (violationParts.length <= 1) {
            return exceptionBundle.getString(violationParts[0]);
        }

        String[] params = new String[violationParts.length - 1];
        Arrays.asList(violationParts).subList(1, violationParts.length).toArray(params);
        try {
            return String.format(exceptionBundle.getString(violationParts[0]), params);
        } catch (Exception e) {
            return exceptionBundle.getString(violationParts[0]);
        }
    }

    private static String stacktraceToString(Throwable throwable) {
        StringWriter sw = new StringWriter();
        throwable.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

    private static GridPane getExpContent(StringBinding header, String details) {
        Label label = new Label();
        label.textProperty().bind(header);
        TextArea textArea = new TextArea();
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMinSize(0.0, 0.0);
        textArea.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        textArea.setText(details);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);
        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);
        return expContent;
    }

    /**
     * Creates a standard error dialog.
     *
     * @param contentText  of the error dialog
     * @param parentWindow or null if not modal
     * @return the dialog which shows the error
     */
    public static Alert createErrorDialog(StringBinding contentText, Window parentWindow) {
        Alert alert = getBasicErrorAlert(parentWindow);
        alert.headerTextProperty().bind(BundleManager.getExceptionStringBinding("exception.default"));
        alert.contentTextProperty().bind(contentText);
        return alert;
    }
}
