package at.ac.tuwien.inso.sepm.ticketline.client.gui.ticket;

import at.ac.tuwien.inso.sepm.ticketline.client.TicketlineClientApplication;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.customer.CreateCustomerController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.hall.HallController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PerformanceService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.client.util.TextCompleter;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatState;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.BatchUpdateException;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;

import javafx.css.PseudoClass;

@Controller
@Scope("prototype")
public class TicketSellController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TicketSellController.class);

    private final PseudoClass PSEUDO_CLASS_HOVER = PseudoClass.getPseudoClass("hover");

    private TicketDTO existingTicket = null;
    private Map<String, Long> existingSeats = null;
    private List<SeatDTO> selectedSeats = null;

    @FXML
    private Node hallPlan;
    @FXML
    private HallController hallPlanController;

    private PerformanceDTO performance;

    private final MainController mainController;
    private final PerformanceService performanceService;
    private final CustomerService customerService;
    private final TicketService ticketService;
    private final SpringFxmlLoader springFxmlLoader;


    @FXML
    private Label lblSelectedCustomer;

    @FXML
    private Label lblPriceSum;

    @FXML
    private ListView lvSelectedSeatsOverview;
    private ObservableList<String> selectedSeatsObservableList;

    @FXML
    private ListView lvPriceOverview;
    private ObservableList<String> priceObservableList;

    @FXML
    private GridPane gpSellingSectorsUI;

    private HashMap<String, ComboBox<Integer>> mapSectorWithSelected;

    @FXML
    private Label lblCustomer;

    @FXML
    private Button btnSearchCustomer;

    @FXML
    private Label lblEmptySelected;

    @FXML
    private Label lblEmptyPrice;

    @FXML
    private Button btnBuy;

    @FXML
    private Button btnReserve;

    @FXML
    private TextField txtCustomerId;

    @FXML
    private Button btnCreateCustomer2;
    private TextCompleter<CustomerDTO> customerCompletionBinding;

    @FXML
    private Label lbTitle;

    @FXML
    private Label lbChosen;


    public TicketSellController(MainController mainController, PerformanceService performanceService, CustomerService customerService, TicketService ticketService, SpringFxmlLoader springFxmlLoader) {
        this.mainController = mainController;
        this.performanceService = performanceService;
        this.customerService = customerService;
        this.ticketService = ticketService;
        this.springFxmlLoader = springFxmlLoader;
        selectedSeatsObservableList = FXCollections.observableArrayList();
        priceObservableList = FXCollections.observableArrayList();
        mapSectorWithSelected = new HashMap<>();
    }

    public void setPerformance(PerformanceDTO performance) {
        this.performance = performance;
        lbTitle.setText(performance.getEvent().getName() + ", " + performance.getDate().format(DateTimeFormatter.ofPattern("dd.MM.YYYY")));
    }

    @FXML
    private void initialize() {
        initializeTextBindings();
        initializeCustomerSearch();
    }

    public static String stringifyCustomer(CustomerDTO customerDTO) {
        return String.format("%s - %s %s", customerDTO.getId(), customerDTO.getFirstName(), customerDTO.getLastName());
    }

    private void initializeCustomerSearch() {
        customerCompletionBinding = new TextCompleter<CustomerDTO>(
            txtCustomerId,
            (suggestion) -> {
                if (suggestion.getUserText().isEmpty()) return Collections.emptyList();
                try {
                    return customerService.search(txtCustomerId.getText());
                } catch (DataAccessException e) {
                    return Collections.emptyList();
                }
            },
            new StringConverter<>() {
                @Override
                public String toString(CustomerDTO object) {
                    return stringifyCustomer(object);
                }

                @Override
                public CustomerDTO fromString(String string) {
                    try {
                        String[] parts = string.split(" ");
                        if (parts.length >= 1 && parts[0].matches("^[0-9]+$")) {
                            // hack to get exact match by customer id
                            return customerService.search(parts[0]).get(0);
                        }
                        return customerService.search(string).get(0);
                    } catch (DataAccessException e) {
                        return null;
                    }
                }
            }
        );
    }

    private void initializeTextBindings() {

        lbChosen.textProperty().bind(BundleManager.getStringBinding("selling.chosen"));
        lblSelectedCustomer.textProperty().bind(BundleManager.getStringBinding("selling.customer.choose"));
        lblEmptySelected.textProperty().bind(BundleManager.getStringBinding("selling.empty"));
        lblEmptyPrice.textProperty().bind(BundleManager.getStringBinding("selling.empty"));
        btnBuy.textProperty().bind(BundleManager.getStringBinding("ticket.buy.reserved"));
        btnReserve.textProperty().bind(BundleManager.getStringBinding("selling.reserve"));
        btnCreateCustomer2.textProperty().bind(BundleManager.getStringBinding("customer.create"));
    }

    private void initializeListView() {
        LOGGER.info("Initialize Listview");
        hallPlanController.getSelectedSeats().addListener((ListChangeListener<SeatDTO>) c -> {
            if (performance.getEvent().getType() == EventType.SEATS) {
                adaptOverviewToSelectedSeats(hallPlanController.getSelectedSeats());
            }
        });
        adaptOverviewToSelectedSeats(hallPlanController.getSelectedSeats());
    }


    private void initializeUIforSectors() {
        LOGGER.debug("Initialize UI for Sectors only");

        try {
            Map<String, Long> map;
            LOGGER.debug("Seats from ticket: {}", existingSeats);
            if (existingSeats != null) { // are we changing an existing ticket?
                map = existingSeats; // assumed to be set earlier via setExistingSeats()
            } else { // no, so get all seats for the performance
                map = performanceService.getSeatsPerSectorByPerformance(this.performance);
            }

            int rowIndex = 1;

            for (Map.Entry<String, Long> entry : map.entrySet()) {
                Label lbSector = new Label();
                lbSector.getStyleClass().add("sectorlabel");
                lbSector.setText(BundleManager.getBundle().getString("event.type.sector") + " " + entry.getKey());
                lbSector.setAlignment(Pos.CENTER_RIGHT);
                ComboBox<Integer> cbSelectedTickets = new ComboBox<>();
                cbSelectedTickets.getStyleClass().add("cbSelectedTickets");
                cbSelectedTickets.setPrefWidth(250);


                List<Integer> availableTickets = new ArrayList<>();

                for (int i = 0; i <= entry.getValue(); i++) { // beware, can overflow!
                    availableTickets.add(i);
                }

                cbSelectedTickets.getItems().setAll(availableTickets);
                if (existingSeats != null) {
                    cbSelectedTickets.getSelectionModel().selectLast();
                } else {
                    cbSelectedTickets.getSelectionModel().select(0);
                }

                mapSectorWithSelected.put(entry.getKey(), cbSelectedTickets);

                cbSelectedTickets.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                    mapSectorWithSelected.put(entry.getKey(), cbSelectedTickets); //old value is replaced
                    adaptOverviewToSelectedSectors();
                });

                GridPane.setConstraints(lbSector, 1, rowIndex);
                GridPane.setConstraints(cbSelectedTickets, 2, rowIndex);

                gpSellingSectorsUI.getChildren().addAll(lbSector, cbSelectedTickets);

                rowIndex++;
            }

            adaptOverviewToSelectedSectors();

        } catch (DataAccessException e) {
            JavaFXUtils.createExceptionDialog(e,
                lvPriceOverview.getScene().getWindow()).showAndWait();
        }

    }


    public void initializeHallPlan() {
        Task<List<SeatDTO>> task = new Task<List<SeatDTO>>() {
            @Override
            protected List<SeatDTO> call() throws DataAccessException {
                return performanceService.getSeatsByPerformance(performance);
            }

            @Override
            protected void succeeded() {
                hallPlanController.setPerformanceDTO(performance);
                if (selectedSeats != null) { // we already have seats
                    List<SeatDTO> seats = this.getValue();
                    for (SeatDTO s : seats) {
                        if (selectedSeats.contains(s))
                            s.setState(SeatState.FREE);
                        else
                            s.setState(SeatState.BOUGHT);
                    }
                    hallPlanController.setSeats(seats);
                } else { // we're making a new reservation
                    hallPlanController.setSeats(this.getValue());
                }

                if (performance.getEvent().getType().equals(EventType.SECTORS)) {
                    initializeUIforSectors();
                }else{
                    gpSellingSectorsUI.setDisable(true);
                }
                initializeListView();
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: Initializing Hallplan. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    hallPlan.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();
    }

    public void adaptOverviewToSelectedSeats(List<SeatDTO> seatDTOS) {
        LOGGER.debug("Seats adaption called");
        selectedSeatsObservableList.clear();
        priceObservableList.clear();

        HashMap<Sector, Integer> mapSeatsToSectors = new HashMap<>();

        for (SeatDTO seatDTO : seatDTOS) {
            Sector belongingSector = seatDTO.getSector();
            if (mapSeatsToSectors.containsKey(belongingSector)) {
                mapSeatsToSectors.put(belongingSector, mapSeatsToSectors.get(belongingSector) + 1);

            } else mapSeatsToSectors.put(belongingSector, 1);
        }
        // sum of prices
        BigDecimal sumOfAllSeats = new BigDecimal(0);

        for (Map.Entry<Sector, Integer> entry : mapSeatsToSectors.entrySet()) {
            //if (entry.getValue() == 0) {
            //    continue;
            //}

            int categoryCount = entry.getValue();

            BigDecimal priceWithoutCategories = performance.getPrice().multiply(entry.getKey().getFactor()).round(new MathContext(2));
            BigDecimal priceWithCategories = priceWithoutCategories.multiply(new BigDecimal(categoryCount));

            sumOfAllSeats = sumOfAllSeats.add(priceWithCategories);
            selectedSeatsObservableList.add(String.format("%d x %s %s", categoryCount, BundleManager.getBundle().getString("event.type.category"), entry.getKey().toString()));
            priceObservableList.add(String.format("%d x %.2f€ = %.2f€", categoryCount, priceWithoutCategories, priceWithCategories));
        }

        lblPriceSum.setText(String.format("%.2f €", sumOfAllSeats));
    }

    public void adaptOverviewToSelectedSectors() {
        LOGGER.debug("Sector adaption called");
        selectedSeatsObservableList.clear();
        priceObservableList.clear();
        BigDecimal sumOfAllSeats = new BigDecimal(0);

        for (Map.Entry<String, ComboBox<Integer>> entry : mapSectorWithSelected.entrySet()) {
            int sectorCount = 0;

            if (!entry.getValue().getSelectionModel().isEmpty()) { //if combobox is not empty
                sectorCount = entry.getValue().getValue();
            }


            BigDecimal priceWithoutSectors = performance.getPrice().multiply(Sector.valueOf(entry.getKey()).getFactor()).round(new MathContext(2));
            BigDecimal priceWithSectors = priceWithoutSectors.multiply(new BigDecimal(sectorCount));
            sumOfAllSeats = sumOfAllSeats.add(priceWithSectors);

            selectedSeatsObservableList.add(String.format("%d x %s %s", sectorCount, BundleManager.getBundle().getString("event.type.category"), entry.getKey().toString()));
            priceObservableList.add(String.format("%d x %.2f = %.2f", sectorCount, priceWithoutSectors, priceWithSectors));
        }

        lblPriceSum.setText(String.format("%.2f Euro", sumOfAllSeats));
    }


    public ObservableList<String> getSelectedSeatsObservableList() {
        return selectedSeatsObservableList;
    }

    public ObservableList<String> getPriceObservableList() {
        return priceObservableList;
    }

    @FXML
    public void buyTicket() {
        LOGGER.info("Clicked on buy ticket");
        Task<Long> task = new Task<>() {
            @Override
            protected Long call() throws DataAccessException {
                Long ticketNumber;
                if (performance.getEvent().getType() == EventType.SEATS) {
                    if (existingTicket != null) {
                        ticketNumber = ticketService.buyReservedTicketForSeats(existingTicket, hallPlanController.getSelectedSeats()).getId();
                    } else {
                        ticketNumber = ticketService.buyTicketForSeats(customerCompletionBinding.getValue(), performance, hallPlanController.getSelectedSeats());
                    }
                } else {
                    HashMap<String, Long> sectors = new HashMap<>();
                    for (var e : mapSectorWithSelected.entrySet()) {
                        sectors.put(e.getKey(), Long.valueOf(e.getValue().getValue()));
                    }
                    if (existingTicket != null) {
                        ticketNumber = ticketService.buyReservedTicketForSectors(existingTicket, sectors).getId();
                    } else {
                        ticketNumber = ticketService.buyTicketForSectors(customerCompletionBinding.getValue(), performance, sectors);
                    }
                }
                return ticketNumber;
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Suceeded: bought ticket.");
                Alert a = new Alert(Alert.AlertType.INFORMATION);
                a.setHeaderText(BundleManager.getBundle().getString("buying.success"));
                a.setContentText(String.format(BundleManager.getBundle().getString("buying.yourid"), getValue()));
                a.showAndWait();
                ((Stage) btnBuy.getScene().getWindow()).close();
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: Ticket was not bought. {} ", getException());
                JavaFXUtils.createExceptionDialog(getException(),
                    btnBuy.getScene().getWindow()).showAndWait();
            }
        };

        Alert confirm = new Alert(Alert.AlertType.CONFIRMATION);
        confirm.setHeaderText(BundleManager.getBundle().getString("selling.confirm"));
        Optional<ButtonType> res = confirm.showAndWait();
        if (res.isPresent() && res.get() == ButtonType.OK) {
            task.runningProperty().addListener((observable, oldValue, running) ->
                mainController.setProgressbarProgress(
                    running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
            );

            new Thread(task).start();
        }
    }

    @FXML
    public void createCustomer() {
        LOGGER.info("Clicked on create customer");
        SpringFxmlLoader.Wrapper<CreateCustomerController> wrapper = springFxmlLoader.loadAndWrap("/fxml/customer/createCustomerComponent.fxml");
        Stage stage = new Stage();
        stage.setScene(new Scene((VBox) wrapper.getLoadedObject()));
        stage.titleProperty().bind(BundleManager.getStringBinding("customer.create"));
        stage.getIcons().add(new Image(TicketlineClientApplication.class.getResourceAsStream("/image/ticketlineIcon.png")));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(btnCreateCustomer2.getScene().getWindow());
        stage.showAndWait();
        CustomerDTO customer = wrapper.getController().getCustomerDTO();
        if (customer.getId() != null) {
            customerCompletionBinding.setValue(customer);
        }
    }

    @FXML
    public void reserveTicket() {
        LOGGER.info("Clicked on reserve tickets");

        Alert confirm = new Alert(Alert.AlertType.CONFIRMATION);
        confirm.setHeaderText(BundleManager.getBundle().getString("reserving.confirm"));
        Optional<ButtonType> res = confirm.showAndWait();

        LOGGER.debug("res: {}", res);
        if (res.isPresent() && res.get() == ButtonType.OK) {
            LOGGER.debug("Got reserving confirmation");
            Task<Long> task = new Task<Long>() {
                @Override
                protected Long call() throws DataAccessException {
                    Long reservationNumber;
                    if (performance.getEvent().getType() == EventType.SEATS) {
                        return ticketService.reserveTicketForSeats(customerCompletionBinding.getValue(), performance, hallPlanController.getSelectedSeats());
                    } else {
                        HashMap<String, Long> sectors = new HashMap<>();
                        for (var e : mapSectorWithSelected.entrySet()) {
                            sectors.put(e.getKey(), Long.valueOf(e.getValue().getValue()));
                        }
                        reservationNumber = ticketService.reserveTicketForSectors(
                            customerCompletionBinding.getValue(),
                            performance,
                            sectors
                        );
                    }

                    return reservationNumber;
                }

                @Override
                protected void succeeded() {
                    LOGGER.debug("Succeeded: reserved ticket.");
                    Alert a = new Alert(Alert.AlertType.INFORMATION);
                    a.setHeaderText(BundleManager.getBundle().getString("reserving.success"));
                    a.setContentText(String.format(BundleManager.getBundle().getString("reserving.yourid"), getValue()));
                    a.showAndWait();
                    ((Stage) btnBuy.getScene().getWindow()).close();
                }


                @Override
                protected void failed() {
                    super.failed();
                    LOGGER.error("Failed: Ticket was not reserved.({}) ", getException().getMessage());
                    JavaFXUtils.createExceptionDialog(getException(),
                        btnBuy.getScene().getWindow()).showAndWait();
                }
            };

            task.runningProperty().addListener((observable, oldValue, running) ->
                mainController.setProgressbarProgress(
                    running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
            );

            new Thread(task).start();
        } else {
            LOGGER.error("SOMETHING WENT WRONG");
        }
    }

    public void setSelectedSeats(List<SeatDTO> seats) {
        this.selectedSeats = seats;
        hallPlanController.getSelectedSeats().setAll(seats);
        btnReserve.setDisable(true);
    }

    public void setExistingSeats(Map<String, Long> seatsBySector) {
        this.existingSeats = seatsBySector;
        btnReserve.setDisable(true);
    }

    public void setTicket(TicketDTO ticket) {
        this.existingTicket = ticket;
        setCustomer(ticket.getCustomer());
        setPerformance(ticket.getPerformance());
    }

    public void setCustomer(CustomerDTO customer) {
        this.customerCompletionBinding.setValue(customer);
        this.btnCreateCustomer2.setDisable(true);
        this.txtCustomerId.setDisable(true);
        this.txtCustomerId.setText(customer.getId() + " - " + customer.getFirstName() + " " + customer.getLastName());
    }
}
