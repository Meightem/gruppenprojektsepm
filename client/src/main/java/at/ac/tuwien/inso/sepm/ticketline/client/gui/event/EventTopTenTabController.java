package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.TopEventDTO;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class EventTopTenTabController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventTopTenTabController.class);

    public Label lblEventType;
    public Label lblMonth;
    public Label lblYear;
    @FXML
    private ComboBox<String> cbEventTypeChooser;

    @FXML
    private ComboBox<Integer> cbYearChooser;

    @FXML
    private ComboBox<Integer> cbMonthChooser;

    @FXML
    private PieChart pcTopTen;

    @FXML
    private Node eventTopTenTabList;

    @FXML
        private Button btnUpdate;



    ObservableList<PieChart.Data> pieChartDataObservableList = FXCollections.observableArrayList();

    @FXML
    private EventListController eventTopTenTabListController;

    private final EventService eventService;
    private final MainController mainController;

    public EventTopTenTabController(MainController mainController, EventService eventService) {
        this.eventService = eventService;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        initializeTextBindings();
        initializeComboBoxes();

    }

    private void initializeTextBindings() {
        BundleManager.getBundleProperty().addListener(observable -> {
            cbEventTypeChooser.getItems().setAll(BundleManager.getStringBinding("event.type.seats").get(),
                BundleManager.getStringBinding("event.type.sectors").get());
        });
        cbEventTypeChooser.getItems().setAll(BundleManager.getStringBinding("event.type.seats").get(),
            BundleManager.getStringBinding("event.type.sectors").get());
        cbMonthChooser.getItems().addAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        int currentYear = LocalDate.now().getYear();
        cbYearChooser.getItems().addAll(currentYear, currentYear - 1, currentYear - 2, currentYear - 3, currentYear - 4);
        lblEventType.textProperty().bind(BundleManager.getStringBinding("event.type"));
        lblMonth.textProperty().bind(BundleManager.getStringBinding("event.month"));
        lblYear.textProperty().bind(BundleManager.getStringBinding("event.year"));
        pcTopTen.titleProperty().bind(BundleManager.getStringBinding("events.tab.topten.piechart.header"));
        btnUpdate.textProperty().bind(BundleManager.getStringBinding("general.update"));
    }

    private void initializeComboBoxes() { // if one combobox changes, load data
        cbEventTypeChooser.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                loadEvents();
            }
        });

        cbMonthChooser.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Integer>() {
            @Override
            public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
                loadEvents();
            }
        });

        cbYearChooser.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Integer>() {
            @Override
            public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
                loadEvents();
            }
        });
    }

    public void loadEvents() {
        LOGGER.debug("Loading Top Ten Events");
        if (cbMonthChooser.getValue() == null || cbYearChooser.getValue() == null || cbEventTypeChooser.getValue() == null) {
            return;
        }
        Task<List<TopEventDTO>> task = new Task<List<TopEventDTO>>() {
            @Override
            protected List<TopEventDTO> call() throws DataAccessException {
                if (cbEventTypeChooser.getValue().equals(BundleManager.getStringBinding("event.type.seats").get())) {
                    return eventService.findTopTenEvents(EventType.SEATS, cbMonthChooser.getValue(), cbYearChooser.getValue());
                } else {
                    return eventService.findTopTenEvents(EventType.SECTORS, cbMonthChooser.getValue(), cbYearChooser.getValue());
                }
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: loaded Top Ten Events.");
                eventTopTenTabListController.getEventObservableList().
                    setAll(this.getValue().stream().map(TopEventDTO::getEvent).collect(Collectors.toList()));

                pieChartDataObservableList.clear();

                for (TopEventDTO topEventDTO : this.getValue()) {
                    PieChart.Data data = new PieChart.Data(topEventDTO.getEvent().getName() + " (" + topEventDTO.getCount() + ")", topEventDTO.getCount());
                    pieChartDataObservableList.add(data);

                }
                pcTopTen.setData(pieChartDataObservableList);
                pcTopTen.setLabelsVisible(true);
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: to load Top Ten Events. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    eventTopTenTabList.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();
    }

}
