package at.ac.tuwien.inso.sepm.ticketline.client.gui.performance;

import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Controller
@Scope("prototype")
public class PerformanceElementController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceElementController.class);

    public Label lblPrice;
    @FXML
    private Node mainNode;
    @FXML
    private Label lblEvent;
    @FXML
    private Label lblDate;
    @FXML
    private Label lblLocation;

    private PerformanceDTO performance;

    private static final DateTimeFormatter PERF_DTF =
        DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT, FormatStyle.SHORT);



    private BooleanProperty showEventName;

    public BooleanProperty showEventNameProperty() {
        return showEventName;
    }

    @Autowired
    public PerformanceElementController() {
        showEventName = new SimpleBooleanProperty();
    }

    @FXML
    private void initialize() {
        updateView();
        updateEventShow();
        showEventName.addListener((observable, oldValue, newValue) -> {
            updateEventShow();
        });
    }

    public void setPerformance(PerformanceDTO performance) {
        this.performance = performance;
        updateView();
    }

    private void updateEventShow() {
        /*if (showEventName.getValue()) {
            mainNode.getChildren().add(0, lblEvent);
        } else {
            mainNode.getChildren().remove(lblEvent);
        }*/
    }

    private void updateView() {
        if (this.performance == null) return;
        lblDate.setText(PERF_DTF.format(performance.getDate()));
        HallDTO h = performance.getHall();
        lblEvent.setText(performance.getEvent().getName());
        lblLocation.setText(String.format("%s   %s, %s %s,%s", h.getName(), h.getCountry(), h.getCity(), h.getPostcode(), h.getStreet()));
        lblPrice.setText("€" + performance.getPrice().setScale(2).toString());
    }
}
