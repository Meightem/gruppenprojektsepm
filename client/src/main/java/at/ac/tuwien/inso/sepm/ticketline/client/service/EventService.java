package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.TopEventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;

import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.List;

public interface EventService {
    /**
     * Create a single event
     *
     * @param dto The event to create
     * @return The created event
     * @throws DataAccessException when event couldn't be created
     */
    EventDTO create(EventDTO dto) throws DataAccessException;

    /**
     * Find all customers
     *
     * @return List of all customers
     * @throws DataAccessException when event couldn't be readout
     */
    List<EventDTO> findAll() throws DataAccessException;

    /**
     * Find top events
     *
     * @param type Type of event to show
     * @param month of statistic
     * @param year of statistic
     * @return list of top events
     * @throws DataAccessException when events could not be read
     */
    List<TopEventDTO> findTopTenEvents(@NotNull EventType type, @NotNull Integer month, @NotNull Integer year) throws DataAccessException;

    List<EventDTO> searchEvent(String name, EventType type, String content, Duration length) throws DataAccessException;

    /**
     * Get all events for one artist
     * @param artistDTO the artist to search for
     * @return the artist's events
     */
    List<EventDTO> findByArtist(ArtistDTO artistDTO) throws DataAccessException;
}
