package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;

import java.util.List;

public interface NewsService {

    /**
     * Find all news entries.
     *
     * @return list of news entries
     * @throws DataAccessException in case something went wrong
     */
    List<SimpleNewsDTO> findAll() throws DataAccessException;

    public void createNews(DetailedNewsDTO newsDTO) throws DataAccessException;

    /**
     * Finds detailed news with id "id"
     *
     * @param id from news
     * @return detailed news
     * @throws DataAccessException in case something went wrong
     */
    DetailedNewsDTO findDetailed(Long id) throws DataAccessException;

    /**
     * Marks the given news as read by the current user.
     *
     * @param newsDTO The news to mark as read.
     * @throws DataAccessException in case something went wrong.
     */
    public void markAsRead(DetailedNewsDTO newsDTO) throws DataAccessException;

    /**
     * Gets the news read by the current user.
     */
    public List<SimpleNewsDTO> getUnreadNews() throws DataAccessException;

}
