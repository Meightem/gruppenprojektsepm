package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.CustomerRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.TicketRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.TicketService;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketSearchDTO;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SimpleTicketService implements TicketService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleUsersService.class);

    private final TicketRestClient ticketRestClient;
    private CustomerRestClient customerRestClient;

    public SimpleTicketService(TicketRestClient ticketRestClient, CustomerRestClient customerRestClient) {
        this.ticketRestClient = ticketRestClient;
        this.customerRestClient = customerRestClient;
    }

    @Override
    public List<TicketDTO> findAll() throws DataAccessException {
        return ticketRestClient.findAll();
    }

    private CustomerDTO mapCustomer(CustomerDTO customer) throws DataAccessException {
        if(customer != null) return customer;
        return customerRestClient.getAnonymousCustomer();
    }

    @Override
    public Long buyTicketForSeats(CustomerDTO customer, PerformanceDTO performance, List<SeatDTO> seats) throws DataAccessException {
        customer = mapCustomer(customer);
        return ticketRestClient.buyTicketForSeats(customer, performance, seats);
    }

    @Override
    public Long buyTicketForSectors(CustomerDTO customer, PerformanceDTO performance, Map<String, Long> sectors) throws DataAccessException {
        customer = mapCustomer(customer);
        return ticketRestClient.buyTicketForSectors(customer, performance, sectors);
    }

    @Override
    public TicketDTO buyReservedTicketForSectors(TicketDTO ticket, Map<String, Long> sectors) throws DataAccessException {
        return ticketRestClient.buyReservedTicketForSectors(ticket, sectors);
    }

    @Override
    public TicketDTO buyReservedTicketForSeats(TicketDTO ticket, List<SeatDTO> seats) throws DataAccessException {
        return ticketRestClient.buyReservedTicketForSeats(ticket, seats);
    }

    @Override
    public void refundReserved(@NotNull Long ticketId) throws DataAccessException {
        ticketRestClient.refundReserved(ticketId);
    }

    @Override
    public void refund(@NotNull Long ticketId) throws DataAccessException {
        ticketRestClient.refund(ticketId);
    }

    @Override
    public List<TicketDTO> search(@NotNull TicketSearchDTO ticketSearchDTO) throws DataAccessException {
        return ticketRestClient.search(ticketSearchDTO);
    }

    @Override
    public TicketDTO search(@NotNull Long id) throws DataAccessException {
        return ticketRestClient.search(id);
    }

    @Override
    public Long reserveTicketForSectors(CustomerDTO customer, PerformanceDTO performance, Map<String, Long> sectors) throws DataAccessException {
        customer = mapCustomer(customer);
        return ticketRestClient.reserveTicketForSectors(customer, performance, sectors);
    }

    @Override
    public Long reserveTicketForSeats(CustomerDTO customer, PerformanceDTO performance, List<SeatDTO> selectedSeats) throws DataAccessException {
        customer = mapCustomer(customer);
        return ticketRestClient.reserveTicketForSeats(customer, performance, selectedSeats);
    }

    @Override
    public List<SeatDTO> getSeats(Long id) throws DataAccessException {
        return ticketRestClient.getSeats(id);
    }

    @Override
    public Map<String, Long> getSeatsBySector(Long id) throws DataAccessException {
        return ticketRestClient.getSeatsBySector(id);
    }

    @Override
    public byte[] getBill(@NotNull Long ticketId) throws DataAccessException {
        return ticketRestClient.getBill(ticketId);
    }
}
