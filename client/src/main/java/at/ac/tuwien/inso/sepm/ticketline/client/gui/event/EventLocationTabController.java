package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.performance.PerformanceListController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.HallService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PerformanceService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class EventLocationTabController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventLocationTabController.class);
    public Label lblNoLocations;
    public Label lblNameHeading;
    public Label lblCountryHeading;
    public Label lblCityPostcodeHeading;
    public Label lblStreetHeading;
    @FXML
    Label lblName;
    @FXML
    TextField tfName;
    @FXML
    Label lblCity;
    @FXML
    TextField tfCity;
    @FXML
    Label lblStreet;
    @FXML
    TextField tfStreet;
    @FXML
    Label lblCountry;
    @FXML
    TextField tfCountry;
    @FXML
    Label lblPostcode;
    @FXML
    TextField tfPostcode;
    @FXML
    Button eventLocationTabSearchBtn;

    @FXML
    private ListView<HallDTO> lvLocation;

    @FXML
    private PerformanceListController locationPerformanceListController;


    private final HallService hallService;
    private final PerformanceService performanceService;
    private final MainController mainController;
    private final ObservableList<HallDTO> locationObservableList;
    private final Property<HallDTO> searchParameters;
    private final SpringFxmlLoader springFxmlLoader;

    public EventLocationTabController(SpringFxmlLoader springFxmlLoader, MainController mainController, HallService hallService,PerformanceService performanceService) {
        this.springFxmlLoader = springFxmlLoader;
        this.searchParameters = new SimpleObjectProperty<HallDTO>();
        this.locationObservableList = FXCollections.observableArrayList();
        this.hallService = hallService;
        this.performanceService = performanceService;
        this.mainController = mainController;

    }
    @FXML
    private void initialize(){
        this.locationPerformanceListController.showEventNameProperty().setValue(true);
        initSearchParametersBindings();
        initializeTextBindings();
        initializeListView();
        loadLocations();
    }

    private void initializeTextBindings(){
        lblNoLocations.textProperty().bind(BundleManager.getStringBinding("location.empty"));
        lblName.textProperty().bind(BundleManager.getStringBinding("hall.name"));
        tfName.promptTextProperty().bind(BundleManager.getStringBinding("hall.name"));
        lblCity.textProperty().bind(BundleManager.getStringBinding("hall.city"));
        tfCity.promptTextProperty().bind(BundleManager.getStringBinding("hall.city"));
        lblPostcode.textProperty().bind(BundleManager.getStringBinding("hall.postcode"));
        tfPostcode.promptTextProperty().bind(BundleManager.getStringBinding("hall.postcode"));
        lblCountry.textProperty().bind(BundleManager.getStringBinding("hall.country"));
        tfCountry.promptTextProperty().bind(BundleManager.getStringBinding("hall.country"));
        lblStreet.textProperty().bind(BundleManager.getStringBinding("hall.street"));
        tfStreet.promptTextProperty().bind(BundleManager.getStringBinding("hall.street"));
        eventLocationTabSearchBtn.textProperty().bind(BundleManager.getStringBinding("hall.search"));
        lblCityPostcodeHeading.textProperty().bind(BundleManager.getStringBinding("hall.postcode"));
        lblCountryHeading.textProperty().bind(BundleManager.getStringBinding("hall.country"));
        lblStreetHeading.textProperty().bind(BundleManager.getStringBinding("hall.street"));
        lblNameHeading.textProperty().bind(BundleManager.getStringBinding("hall.name"));
    }

    private void initSearchParametersBindings() {
        searchParameters.bind(javafx.beans.binding.Bindings.createObjectBinding(() -> HallDTO.newBuilder()
                .name(tfName.getText())
                .city(tfCity.getText())
                .street(tfStreet.getText())
                .country(tfCountry.getText())
                .postcode(tfPostcode.getText())
                .build(),
            tfName.textProperty(), tfCity.textProperty(), tfStreet.textProperty(), tfCountry.textProperty(), tfPostcode.textProperty()));
    }

    @FXML
    private void searchClicked() {
        LOGGER.info("On search clicked");
        loadLocations();
    }

    private void loadLocations() {
        LOGGER.debug("Loading locations");
        Task<List<HallDTO>> task = new Task<List<HallDTO>>() {
            @Override
            protected List<HallDTO> call() throws DataAccessException {
                HallDTO h = searchParameters.getValue();
                String searchText = String.format("%s %s %s %s %s", h.getName(),h.getCity(),h.getPostcode(),h.getStreet(),h.getCountry()).trim().replaceAll("( )+"," ");
                return hallService.search(searchText);
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: locations loaded.");
                locationObservableList.setAll(this.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                JavaFXUtils.createExceptionDialog(getException(),
                    eventLocationTabSearchBtn.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();
    }

    private void initializeListView() {
        lvLocation.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        lvLocation.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
        {
            if(newValue != null){
                loadAllPerformances(newValue);
            }else{
                locationPerformanceListController.getPerformanceObservableList().clear();
            }
        });

        lvLocation.setCellFactory(lv -> {
            return new ListCell<HallDTO>() {
                @Override
                public void updateItem(HallDTO location, boolean empty) {
                    super.updateItem(location, empty);
                    if (empty || location == null) {
                        setGraphic(null);
                    } else {
                        SpringFxmlLoader.Wrapper<LocationElementController> wrapper = springFxmlLoader.loadAndWrap("/fxml/events/locationElement.fxml");
                        wrapper.getController().setLocation(location);
                        setGraphic(wrapper.getLoadedObject());
                        getStyleClass().add("LocationRow");
                    }
                }
            };
        });

    }

    private void loadAllPerformances(HallDTO hallDTO){
        LOGGER.debug("Loading Performances from {}", hallDTO);

        Task<List<PerformanceDTO>> task = new Task<List<PerformanceDTO>>() {
            @Override
            protected List<PerformanceDTO> call() throws DataAccessException {
                return performanceService.findByHall(hallDTO);
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: loaded performances.");
                super.succeeded();
                locationPerformanceListController.getPerformanceObservableList().setAll(this.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: loading performances. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    lvLocation.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    public ObservableList<HallDTO> getLocationObservableList() {
        return locationObservableList;
    }
}
