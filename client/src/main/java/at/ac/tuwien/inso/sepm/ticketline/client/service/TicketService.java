package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketSearchDTO;
import javafx.collections.ObservableList;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface TicketService {

    /**
     *
     * @return all Tickets
     */
    List<TicketDTO> findAll() throws DataAccessException;

    /**
     * Buy a ticket
     * @param customer
     * @param performance
     */
    Long buyTicketForSeats(CustomerDTO customer, PerformanceDTO performance, List<SeatDTO> seats) throws DataAccessException;

    /**
     * Buy a ticket
     * @param customer
     * @param performance
     */
    Long buyTicketForSectors(CustomerDTO customer, PerformanceDTO performance, Map<String, Long> sectors) throws DataAccessException;

    /**
     * Buys seats in the sector for the given reserved ticket.
     *
     * @param ticket  The ticket.
     * @param sectors The Sectors containing the seats.
     */
    TicketDTO buyReservedTicketForSectors(TicketDTO ticket, Map<String, Long> sectors) throws DataAccessException;

    /**
     * Buys the seats for the given reserved ticket.
     *
     * @param ticket The ticket.
     * @param seats  The Seats to be bought.
     */
    TicketDTO buyReservedTicketForSeats(TicketDTO ticket, List<SeatDTO> seats) throws DataAccessException;

    /**
     * refunds a reserved ticket
     * @param ticketId the ticket to be refunded
     */
    void refundReserved(@NotNull Long ticketId) throws DataAccessException;

    /**
     * refunds a bought ticket
     * @param ticketId the ticket to be refunded
     * @throws DataAccessException
     */
    void refund(@NotNull Long ticketId) throws DataAccessException;

    /**
     * Reserve a ticket
     * @param customer
     * @param performance
     * @param sectors
     */
    Long reserveTicketForSectors(CustomerDTO customer, PerformanceDTO performance, Map<String,Long> sectors) throws DataAccessException;

    /**
     * Reserve a ticket
     * @param customer
     * @param performance
     * @param selectedSeats
     */
    Long reserveTicketForSeats(CustomerDTO customer, PerformanceDTO performance, List<SeatDTO> selectedSeats) throws DataAccessException;

    List<SeatDTO> getSeats(Long id) throws DataAccessException;

    Map<String, Long> getSeatsBySector(Long id) throws DataAccessException;

    byte[] getBill(@NotNull Long ticketId) throws DataAccessException;

    List<TicketDTO> search(@NotNull TicketSearchDTO ticketSearchDTO) throws DataAccessException;

    TicketDTO search(@NotNull Long id) throws DataAccessException;
}
