package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;

import java.util.List;

public interface UsersService {

    /**
     * Create a single user
     *
     * @param dto The user to create
     * @return The created user
     * @throws DataAccessException when user couldn't be created
     */
    UserDTO create(UserDTO dto) throws DataAccessException;

    /**
     * Find all users
     *
     * @return List of all users
     * @throws DataAccessException when users couldn't be readout
     */
    List<UserDTO> findAll() throws DataAccessException;

    /**
     * Edit a single user
     *
     * @param dto new user with old id
     * @return new user
     * @throws DataAccessException when user couldn't be edited
     */
    UserDTO update(UserDTO dto) throws DataAccessException;


    /**
     * Resets the password
     *
     * @return the user
     * @throws DataAccessException when new possword could not be set
     */
    UserDTO resetPassword(UserDTO dto) throws DataAccessException;


}