package at.ac.tuwien.inso.sepm.ticketline.client.gui.admin;

import at.ac.tuwien.inso.sepm.ticketline.client.TicketlineClientApplication;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.UsersService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import com.sun.javafx.tools.packager.bundlers.Bundler;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AdminController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
    public ListView lvUsersHeading;
    public Label lblUserIDHeading;
    public Label lblUsernameHeading;
    public Label lblAdminIconHeading;
    public Label lblLockedIconHeading;

    @FXML
    private TabHeaderController tabHeaderController;

    @FXML
    private Button btnCreateUser;

    @FXML
    private Label lblUsersHeading;
    @FXML
    private Label lblUsersEmpty;

    @FXML
    private ListView<UserDTO> lvUsersOverview;

    private final MainController mainController;
    private final SpringFxmlLoader springFxmlLoader;
    private final UsersService usersService;

    private final ObservableList<UserDTO> usersObservableList = FXCollections.observableArrayList();
    private final ObservableList<UserDTO> usersHeadingObservableList = FXCollections.observableArrayList();

    public AdminController(MainController mainController, SpringFxmlLoader springFxmlLoader, UsersService usersService) {
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.usersService = usersService;
    }

    @FXML
    private void initialize() {
        tabHeaderController.setIcon(FontAwesome.Glyph.USER_SECRET);
        updateListView();
        initializeListView();
        initializeTextBindings();
    }

    private void initializeTextBindings() {
        tabHeaderController.setTitleBinding(BundleManager.getStringBinding("tab.admin"));
        btnCreateUser.textProperty().bind(BundleManager.getStringBinding("user.create"));
        lblUsersHeading.textProperty().bind(BundleManager.getStringBinding("users"));
        lblUsersEmpty.textProperty().bind(BundleManager.getStringBinding("users.empty"));
        lblUserIDHeading.textProperty().bind(BundleManager.getStringBinding("user.id"));
        lblLockedIconHeading.textProperty().bind(BundleManager.getStringBinding("user.lockState"));
        lblUsernameHeading.textProperty().bind(BundleManager.getStringBinding("user.userName"));
        lblAdminIconHeading.textProperty().bind(BundleManager.getStringBinding("user.adminState"));
    }

    private void initializeListView() {
        ListChangeListener<UserDTO> usersListener;
        lvUsersOverview.setCellFactory(lv -> {
            return new ListCell<UserDTO>() {
                @Override
                public void updateItem(UserDTO user, boolean empty) {
                    super.updateItem(user, empty);

                    if (empty || user == null) {
                        setGraphic(null);
                    } else {
                        SpringFxmlLoader.Wrapper<UserElementController> wrapper = springFxmlLoader.loadAndWrap("/fxml/admin/userElement.fxml");
                        wrapper.getController().setUser(user);
                        setGraphic(wrapper.getLoadedObject());
                        getStyleClass().add("UserRow");
                        //ContextMenu
                        final ContextMenu contextMenu = new ContextMenu();
                        if (user.getLocked()) {
                            MenuItem unlockMenuItem = new MenuItem();
                            unlockMenuItem.textProperty().bind(BundleManager.getStringBinding("admin.user.unlock"));
                            unlockMenuItem.setId("UnlockMenuItem");
                            unlockMenuItem.setOnAction(event -> {
                                setLock(user, false);
                            });
                            contextMenu.getItems().add(unlockMenuItem);

                        } else {
                            MenuItem lockMenuItem = new MenuItem();
                            lockMenuItem.textProperty().bind(BundleManager.getStringBinding("admin.user.lock"));
                            lockMenuItem.setId("LockMenuItem");
                            lockMenuItem.setOnAction(event -> {
                                setLock(user, true);
                            });
                            contextMenu.getItems().add(lockMenuItem);
                        }

                        MenuItem resetPasswordMenuItem = new MenuItem();
                        resetPasswordMenuItem.textProperty().bind(BundleManager.getStringBinding("admin.user.resetPassword"));
                        resetPasswordMenuItem.setId("ResetPasswordMenuItem");
                        resetPasswordMenuItem.setOnAction(event -> {
                            resetPassword(user);
                        });

                        contextMenu.getItems().add(resetPasswordMenuItem);
                        contextMenu.getStyleClass().add("brightContextMenu");
                        setContextMenu(contextMenu);
                    }
                }
            };
        });
    }

    private void resetPassword(UserDTO user) {
        LOGGER.info("Reset password clicked {}", user);
        SpringFxmlLoader.AnyWrapper<ResetUserpasswordController, VBox> objectWrapper = springFxmlLoader.loadAndWrapAny("/fxml/admin/resetUserpasswordComponent.fxml");
        objectWrapper.getController().setUser(user);
        Stage stage = new Stage();
        stage.titleProperty().bind(BundleManager.getStringBinding("admin.user.resetPassword"));
        stage.getIcons().add(new Image(TicketlineClientApplication.class.getResourceAsStream("/image/ticketlineIcon.png")));
        stage.setScene(new Scene(objectWrapper.getLoadedObject()));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(lvUsersOverview.getScene().getWindow());
        stage.showAndWait();
        updateListView();
    }


    private void setLock(UserDTO user, boolean lock) {
        user.setLocked(lock);
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws DataAccessException {
                usersService.update(user);
                return null;
            }

            @Override
            protected void succeeded() {
                updateListView();
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Set lock failed: {}", getException());
                JavaFXUtils.createExceptionDialog(getException(),
                    lvUsersOverview.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    private void updateListView() {
        Task<List<UserDTO>> task = new Task<List<UserDTO>>() {
            @Override
            protected List<UserDTO> call() throws DataAccessException {
                return usersService.findAll();
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: update Listview");
                usersObservableList.setAll(this.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: update Listview.({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    lvUsersOverview.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();

    }

    @FXML
    private void createUser() {
        VBox box = springFxmlLoader.load("/fxml/admin/createUserComponent.fxml");
        Stage stage = new Stage();
        stage.setScene(new Scene(box));
        stage.titleProperty().bind(BundleManager.getStringBinding("user.create"));
        stage.getIcons().add(new Image(TicketlineClientApplication.class.getResourceAsStream("/image/ticketlineIcon.png")));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(lvUsersOverview.getScene().getWindow());
        stage.showAndWait();
        updateListView();
    }

    public ObservableList<UserDTO> getUsersObservableList() {
        return usersObservableList;
    }
}
