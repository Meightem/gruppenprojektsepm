package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.client.TicketlineClientApplication;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.performance.CreatePerformanceController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.performance.PerformanceListController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.AuthenticationInformationService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PerformanceService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.beans.binding.Bindings;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Optional;

@Component
public class EventDetailedController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventDetailedController.class);

    private final AuthenticationInformationService authenticationInformationService;

    @FXML
    private TabHeaderController headerController;

    @FXML
    ListView<PerformanceDTO> lvPerformanceElements;

    @FXML
    private Label lblEvent;

    @FXML
    private Button btnBack;

    @FXML private Label lblFrom;

    @FXML
    private Button btnCreatePerformance;

    @FXML
    private PerformanceListController performanceListController;

    EventDTO eventDTO;

    private final MainController mainController;
    private final SpringFxmlLoader springFxmlLoader;
    private final PerformanceService performanceService;

    public EventDetailedController(AuthenticationInformationService authenticationInformationService, MainController mainController, SpringFxmlLoader springFxmlLoader, PerformanceService performanceService) {
        this.authenticationInformationService = authenticationInformationService;
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.performanceService = performanceService;
    }

    public void setEventDTO(EventDTO eventDTO){
        this.eventDTO = eventDTO;
        lblEvent.setText(eventDTO.getName());
        lblFrom.setText(Bindings.convert(BundleManager.getStringBinding("events.from")).getValue() +
            " " + eventDTO.getArtist().getFirstName() +
            " " + eventDTO.getArtist().getLastName());
    }

    @FXML
    private void initialize(){
        if (!isAdmin()) btnCreatePerformance.setVisible(false);
        initializeTextBindings();
        headerController.setIcon(FontAwesome.Glyph.CALENDAR);
        loadAllPerformances();
    }

    private void initializeTextBindings(){
        headerController.setTitleBinding(BundleManager.getStringBinding("tab.events"));
        btnBack.textProperty().bind(BundleManager.getStringBinding("general.back"));
        btnCreatePerformance.textProperty().bind(BundleManager.getStringBinding("perf.create"));
    }

    public void loadAllPerformances(){
        LOGGER.debug("Loading Performances from {}", eventDTO);

        Task<List<PerformanceDTO>> task = new Task<List<PerformanceDTO>>() {
            @Override
            protected List<PerformanceDTO> call() throws DataAccessException {
                return performanceService.findByEvent(eventDTO);
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                LOGGER.debug("Succeeded: loaded all performances.");
                performanceListController.getPerformanceObservableList().setAll(this.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: loading performances from {}. {}", eventDTO, getException());
                JavaFXUtils.createExceptionDialog(getException(),
                    lvPerformanceElements.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    @FXML
    public void back(ActionEvent actionEvent) {
        ((Stage) btnBack.getScene().getWindow()).close();
    }

    @FXML
    public void createPerformance() {
        SpringFxmlLoader.AnyWrapper<CreatePerformanceController, VBox> wrapper = springFxmlLoader.loadAndWrapAny(
            "/fxml/performance/createPerformanceComponent.fxml"
        );
        wrapper.getController().setEvent(eventDTO);
        Stage stage = new Stage();
        stage.setScene(new Scene(wrapper.getLoadedObject()));
        stage.titleProperty().bind(BundleManager.getStringBinding("event.create"));
        stage.getIcons().add(new Image(TicketlineClientApplication.class.getResourceAsStream("/image/ticketlineIcon.png")));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(btnCreatePerformance.getScene().getWindow());
        stage.showAndWait();
        loadAllPerformances();
    }

    private boolean isAdmin() {
        Optional<AuthenticationTokenInfo> authInfo = authenticationInformationService.getCurrentAuthenticationTokenInfo();
        if (!authInfo.isPresent()) return false;
        return authInfo.get().getRoles().contains("ADMIN");
    }
}
