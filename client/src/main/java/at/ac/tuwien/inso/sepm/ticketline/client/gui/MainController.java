package at.ac.tuwien.inso.sepm.ticketline.client.gui;

import at.ac.tuwien.inso.sepm.ticketline.client.gui.admin.AdminController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.customer.CustomerController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.event.EventController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.news.NewsController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.ticket.TicketController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.AuthenticationInformationService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Component
public class MainController {

    private static final int TAB_ICON_FONT_SIZE = 20;
    private final AuthenticationInformationService authenticationInformationService;

    @FXML
    private StackPane spMainContent;

    @FXML
    private ProgressBar pbLoadingProgress;

    @FXML
    private TabPane tpContent;

    @FXML
    private MenuBar mbMain;

    @FXML
    private Menu menuLanguage;

    @FXML
    private Menu menuApplication;

    @FXML
    private Menu menuHelp;

    @FXML
    private MenuItem mIExit;

    @FXML
    private MenuItem mIAbout;


    private Node authentication;

    private final SpringFxmlLoader springFxmlLoader;
    private final FontAwesome fontAwesome;
    private NewsController newsController;
    private AdminController adminController;
    private CustomerController customerController;
    private AuthenticationController authenticationController;
    private TicketController ticketController;

    public MainController(
        SpringFxmlLoader springFxmlLoader,
        FontAwesome fontAwesome,
        AuthenticationInformationService authenticationInformationService
    ) {
        this.springFxmlLoader = springFxmlLoader;
        this.fontAwesome = fontAwesome;
        this.authenticationInformationService = authenticationInformationService;
        authenticationInformationService.addAuthenticationChangeListener(
            authenticationTokenInfo -> Platform.runLater(() -> setAuthenticated(null != authenticationTokenInfo))
        );
    }

    @FXML
    private void initialize() {
        Platform.runLater(() -> mbMain.setUseSystemMenuBar(true));
        pbLoadingProgress.setProgress(0);
        initializeTextBindings();
        initAuthentication();
        spMainContent.getChildren().add(authentication);
        initLanguageMenu();
    }

    @FXML
    private void exitApplication(ActionEvent actionEvent) {
        Stage stage = (Stage) spMainContent.getScene().getWindow();
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    @FXML
    private void aboutApplication(ActionEvent actionEvent) {
        Stage stage = (Stage) spMainContent.getScene().getWindow();
        Stage dialog = new Stage();
        dialog.setResizable(false);
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(stage);
        dialog.setScene(new Scene(springFxmlLoader.load("/fxml/aboutDialog.fxml")));
        dialog.titleProperty().bind(BundleManager.getStringBinding("dialog.about.title"));
        dialog.showAndWait();
    }

    private void initializeTextBindings() {
        menuApplication.textProperty().bind(BundleManager.getStringBinding("menu.application"));
        menuHelp.textProperty().bind(BundleManager.getStringBinding("menu.help"));
        menuLanguage.textProperty().bind(BundleManager.getStringBinding("language"));
        mIAbout.textProperty().bind(BundleManager.getStringBinding("menu.help.about"));
        mIExit.textProperty().bind(BundleManager.getStringBinding("menu.application.exit"));
    }

    private void initLanguageMenu() {
        List<Locale> supportedLanguages = BundleManager.getSupportedLocales();
        menuLanguage.getItems().clear();
        for (Locale language : supportedLanguages) {
            MenuItem menuItem = new MenuItem();
            menuItem.setOnAction(event -> BundleManager.changeLocale(language));
            menuItem.setText(language.getDisplayLanguage(language));
            menuItem.setId(language.getDisplayLanguage(Locale.ENGLISH));
            menuLanguage.getItems().add(menuItem);
        }

    }

    private void initAuthentication() {
        SpringFxmlLoader.Wrapper<AuthenticationController> wrapper =
            springFxmlLoader.loadAndWrap("/fxml/authenticationComponent.fxml");
        authentication = wrapper.getLoadedObject();
        authenticationController = wrapper.getController();
    }

    private void initNewsTabPane() {
        SpringFxmlLoader.Wrapper<NewsController> wrapper =
            springFxmlLoader.loadAndWrap("/fxml/news/newsComponent.fxml");
        newsController = wrapper.getController();

        Glyph newsGlyph = fontAwesome.create(FontAwesome.Glyph.NEWSPAPER_ALT);
        newsGlyph.setFontSize(TAB_ICON_FONT_SIZE);
        newsGlyph.setColor(Color.WHITE);

        Tab newsTab = new Tab(null, wrapper.getLoadedObject());
        newsTab.setId("newsTab");
        newsTab.setGraphic(newsGlyph);
        tpContent.getTabs().add(newsTab);
    }

    private void initEventTabPane(){
        SpringFxmlLoader.Wrapper<EventController> wrapper =
            springFxmlLoader.loadAndWrap("/fxml/events/eventComponent.fxml");

        Glyph eventGlyph = fontAwesome.create(FontAwesome.Glyph.CALENDAR);
        eventGlyph.setFontSize(TAB_ICON_FONT_SIZE);
        eventGlyph.setColor(Color.WHITE);

        Tab eventTab = new Tab(null, wrapper.getLoadedObject());
        eventTab.setId("eventTab");
        eventTab.setGraphic(eventGlyph);
        tpContent.getTabs().add(eventTab);
    }

    private void initAdminTabPane() {
        SpringFxmlLoader.Wrapper<AdminController> wrapper =
            springFxmlLoader.loadAndWrap("/fxml/admin/adminComponent.fxml");
        adminController = wrapper.getController();

        Glyph adminGlyph = fontAwesome.create(FontAwesome.Glyph.USER_SECRET);
        adminGlyph.setFontSize(TAB_ICON_FONT_SIZE);
        adminGlyph.setColor(Color.WHITE);

        Tab adminTab = new Tab(null, wrapper.getLoadedObject());
        adminTab.setId("adminTab");
        adminTab.setGraphic(adminGlyph);
        tpContent.getTabs().add(adminTab);
    }

    private void initTicketTabPane() {
        SpringFxmlLoader.Wrapper<TicketController> wrapper =
            springFxmlLoader.loadAndWrap("/fxml/tickets/ticketComponent.fxml");
        ticketController = wrapper.getController();

        Glyph newsGlyph = fontAwesome.create(FontAwesome.Glyph.TICKET);
        newsGlyph.setFontSize(TAB_ICON_FONT_SIZE);
        newsGlyph.setColor(Color.WHITE);

        Tab ticketTab = new Tab(null, wrapper.getLoadedObject());
        ticketTab.setId("ticketTab");
        ticketTab.setGraphic(newsGlyph);
        tpContent.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                if(newValue==ticketTab){
                    ticketController.updateListView();
                }
            }
        });
        tpContent.getTabs().add(ticketTab);
    }

    private void initLogout() {
        Glyph signOutGlyph = fontAwesome.create(FontAwesome.Glyph.SIGN_OUT);
        signOutGlyph.setFontSize(TAB_ICON_FONT_SIZE);
        signOutGlyph.setColor(Color.WHITE);

        Button logoutButton = new Button();
        logoutButton.setOnAction(event -> {

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.titleProperty().bind(BundleManager.getStringBinding("dialog.logout.title"));
            alert.headerTextProperty().bind(BundleManager.getStringBinding("dialog.logout.header"));
            alert.contentTextProperty().bind(BundleManager.getStringBinding("dialog.logout.content"));
            Optional<ButtonType> result = alert.showAndWait();
            if (!result.isPresent() || ButtonType.OK.equals(result.get())) {
                setAuthenticated(false);
                event.consume();
            }
        });
        logoutButton.setGraphic(signOutGlyph);
        logoutButton.getStyleClass().addAll("tab-button", "tab");

        Tab logoutTab = new Tab();
        logoutTab.setId("logoutTab");
        logoutTab.setGraphic(logoutButton);
        tpContent.getTabs().add(logoutTab);
    }

    private void initCustomerTabPane() {

        SpringFxmlLoader.Wrapper<CustomerController> wrapper =
            springFxmlLoader.loadAndWrap("/fxml/customer/customerComponent.fxml");
        customerController = wrapper.getController();
        customerController.loadCustomers();
        Tab customerTab = new Tab(null, wrapper.getLoadedObject());
        customerTab.setId("customerTab");
        Glyph customerGlyph = fontAwesome.create(FontAwesome.Glyph.USERS);
        customerGlyph.setFontSize(TAB_ICON_FONT_SIZE);
        customerGlyph.setColor(Color.WHITE);
        customerTab.setGraphic(customerGlyph);
        tpContent.getTabs().add(customerTab);
    }

    private void setAuthenticated(boolean authenticated) {
        boolean atLoginScreen = spMainContent.getChildren().contains(authentication);
        if (authenticated && atLoginScreen) {
            tpContent.getTabs().clear();
            // The user is authenticated but wasn't before
            spMainContent.getChildren().remove(authentication);
            initNewsTabPane();
            initEventTabPane();
            initTicketTabPane();
            initCustomerTabPane();
            if (isAdmin()) {
                initAdminTabPane();
            }
            initLogout();
            tpContent.setOnKeyPressed(event -> {
                switch (event.getCode()) {
                    case F1:
                        tpContent.getSelectionModel().select(0);
                        break;
                    case F2:
                        tpContent.getSelectionModel().select(1);
                        break;
                    case F3:
                        tpContent.getSelectionModel().select(2);
                        break;
                    case F4:
                        tpContent.getSelectionModel().select(3);
                        break;
                    case F5:
                        if (!isAdmin())
                            break;

                        tpContent.getSelectionModel().select(4);
                        break;
                }
            });
        } else if (!authenticated && !atLoginScreen) {
            try {
                initAuthentication();
            } catch (Exception e) {
                // we are closing the application
            }

            tpContent.getTabs().clear();
            // The user is not authenticated and was before
            spMainContent.getChildren().add(authentication);
            authenticationController.resetInputFields();
        }

        // In all other cases we don't want to refresh the UI
    }

    private boolean isAdmin() {
        Optional<AuthenticationTokenInfo> authInfo = authenticationInformationService.getCurrentAuthenticationTokenInfo();
        if (!authInfo.isPresent()) return false;
        return authInfo.get().getRoles().contains("ADMIN");
    }

    public void setProgressbarProgress(double progress) {
        pbLoadingProgress.setProgress(progress);
    }
}
