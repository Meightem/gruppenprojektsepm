package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.time.Duration;

@Controller
@Scope("prototype")
public class EventElementController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventElementController.class);
    public Label lblType;
    public Label lblDuration;

    @FXML
    private Node mainNode;

    @FXML
    private Label lblName;

    private EventDTO event;

    @Autowired
    public EventElementController() {

    }

    @FXML
    private void initialize() {
        updateView();
    }

    public void setEvent(EventDTO event) {
        this.event = event;
        updateView();
    }

    private void updateView() {
        if (event == null) return;
        lblName.setText(event.getName());
        lblType.textProperty().bind(BundleManager.getStringBinding(event.getType() == EventType.SEATS ? "event.type.seats" : "event.type.sectors"));
        lblDuration.setText(formatDuration(event.getLength()));
        Tooltip.install(mainNode, new Tooltip(event.getContent()));
    }

    public static String formatDuration(Duration duration) {
        long seconds = duration.getSeconds();
        long absSeconds = Math.abs(seconds);
        String positive = String.format(
            "%d:%02d",
            absSeconds / 3600,
            (absSeconds % 3600) / 60,
            absSeconds % 60);
        return seconds < 0 ? "-" + positive : positive;
    }
}
