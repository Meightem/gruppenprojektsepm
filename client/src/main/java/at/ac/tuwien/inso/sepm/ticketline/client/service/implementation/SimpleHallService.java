package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.HallRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.HallService;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleHallService implements HallService {
    private final HallRestClient hallRestClient;

    public SimpleHallService(HallRestClient hallRestClient) {
        this.hallRestClient = hallRestClient;
    }

    @Override
    public List<HallDTO> search(String text) throws DataAccessException {
        if("".equals(text)){
            return hallRestClient.findAll();
        }else{
            return hallRestClient.search(text);
        }
    }
}
