package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.ArtistService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import java.util.Collections;
import java.util.List;

@Controller
public class EventArtistTabController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventLocationTabController.class);

    private final ObservableList<ArtistDTO> artistObservableList;

    @FXML
    public Label lblName;
    @FXML
    public TextField tfName;
    @FXML
    public Button eventArtistTabSearchBtn;
    public Label lblNoArtists;
    public Label lblFirstNameHeading;
    public Label lblLastNameHeading;
    @FXML
    private EventListController artistEventListController;
    @FXML
    private ListView<ArtistDTO> lvArtist;

    private ArtistService artistService;
    private MainController mainController;
    private EventService eventService;
    private SpringFxmlLoader springFxmlLoader;

    public ObservableList<ArtistDTO> getArtistObservableList() {
        return artistObservableList;
    }

    public EventArtistTabController(ArtistService artistService, MainController mainController, EventService eventService, SpringFxmlLoader springFxmlLoader) {
        this.artistObservableList = FXCollections.observableArrayList();
        this.artistService = artistService;
        this.mainController = mainController;
        this.eventService = eventService;
        this.springFxmlLoader = springFxmlLoader;
    }


    @FXML
    private void initialize(){
        initializeTextBindings();
        initializeListView();
        loadArtists();
    }

    private void initializeTextBindings() {
        lblNoArtists.textProperty().bind(BundleManager.getStringBinding("artist.empty"));
        lblName.textProperty().bind(BundleManager.getStringBinding("event.name"));
        tfName.promptTextProperty().bind(BundleManager.getStringBinding("event.name"));
        eventArtistTabSearchBtn.textProperty().bind(BundleManager.getStringBinding("events.search"));
        lblFirstNameHeading.textProperty().bind(BundleManager.getStringBinding("general.firstName"));
        lblLastNameHeading.textProperty().bind(BundleManager.getStringBinding("general.lastName"));
    }

    private void initializeListView() {
        lvArtist.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        lvArtist.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
        {
            if(newValue != null){
                loadEvents(newValue);
            }else{
                artistEventListController.getEventObservableList().clear();
            }
        });

        lvArtist.setCellFactory(lv -> {
            return new ListCell<ArtistDTO>() {
                @Override
                public void updateItem(ArtistDTO artist, boolean empty) {
                    super.updateItem(artist, empty);
                    if (empty || artist == null) {
                        setGraphic(null);
                    } else {
                        SpringFxmlLoader.Wrapper<ArtistElementController> wrapper = springFxmlLoader.loadAndWrap("/fxml/events/artistElement.fxml");
                        wrapper.getController().setArtist(artist);
                        setGraphic(wrapper.getLoadedObject());
                        getStyleClass().add("ArtistRow");
                    }
                }
            };
        });

    }

    private void loadEvents(ArtistDTO artistDTO) {
        LOGGER.debug("Loading events for {}", artistDTO);

        Task<List<EventDTO>> task = new Task<List<EventDTO>>() {
            @Override
            protected List<EventDTO> call() throws DataAccessException {
                return eventService.findByArtist(artistDTO);
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                LOGGER.debug("Succeeded: loaded events.");
                artistEventListController.getEventObservableList().setAll(this.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: loading events. ({}) ", getException().getMessage());
                LOGGER.error(getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    lvArtist.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    public void searchClicked(ActionEvent actionEvent) {
        loadArtists();
    }

    private void loadArtists() {
        Task<List<ArtistDTO>> task = new Task<List<ArtistDTO>>() {
            @Override
            protected List<ArtistDTO> call() throws DataAccessException {
                if (tfName.getText().isEmpty()) {
                    return artistService.findAll();
                } else {
                    return artistService.search(tfName.getText());
                }
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: laoded artists.");
                artistObservableList.setAll(this.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: loading artists.");
                JavaFXUtils.createExceptionDialog(getException(),
                    eventArtistTabSearchBtn.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();
    }
}
