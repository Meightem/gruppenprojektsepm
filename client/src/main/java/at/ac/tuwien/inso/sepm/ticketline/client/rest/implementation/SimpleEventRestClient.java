package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.EventRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventSearchDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.TopEventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.util.List;

@Component
public class SimpleEventRestClient implements EventRestClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleEventRestClient.class);
    private static final String EVENTS_URL = "/events";

    public SimpleEventRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    private final RestClient restClient;

    @Override
    public EventDTO create(EventDTO dto) throws DataAccessException {
        try {
            LOGGER.debug("Creating event {}", dto);
            ResponseEntity<EventDTO> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(EVENTS_URL),
                HttpMethod.POST,
                new HttpEntity<>(dto),
                new ParameterizedTypeReference<EventDTO>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.event.create",e);
        }
    }

    @Override
    public List<EventDTO> findAll() throws DataAccessException {
        try {
            LOGGER.debug("Finding all events");
            ResponseEntity<List<EventDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(EVENTS_URL),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<EventDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.events.findAll",e);
        }
    }

    @Override
    public List<TopEventDTO> findTopTenEvents(EventType type, int month, int year) throws DataAccessException {
        try {
            LOGGER.debug("Finding top ten events");
            ResponseEntity<List<TopEventDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(EVENTS_URL + "/top/" + type + "/" + month + "/" + year),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<TopEventDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.events.findTopTen",e);
        }
    }

    @Override
    public List<EventDTO> searchEvent(EventSearchDTO eventSearchDTO) throws DataAccessException {
        try {
            LOGGER.debug("Finding Events according to search parameters");
            ResponseEntity<List<EventDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(EVENTS_URL + "/search"),
                HttpMethod.POST,
                new HttpEntity<>(eventSearchDTO),
                new ParameterizedTypeReference<List<EventDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.events.searchEvents",e);
        }
    }

    @Override
    public List<TopEventDTO> getTopTenEvents(EventType type, Integer month, Integer year) throws DataAccessException {
        try {
            LOGGER.debug("Finding Events according to search parameters");
            ResponseEntity<List<TopEventDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(EVENTS_URL + "/top/" + type + "/" + month + "/" + year),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<TopEventDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            throw new DataAccessException("exception.events.searchEvents", e);
        }
    }

    @Override
    public List<EventDTO> findByArtist(ArtistDTO artistDTO) throws DataAccessException {
        try {
            LOGGER.debug("Finding Events by Artist");
            ResponseEntity<List<EventDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(EVENTS_URL + String.format("/artist/%s", artistDTO.getId())),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<EventDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            LOGGER.error("Error: {}", e.getMessage());
            String msg = String.format(BundleManager.getExceptionBundle().getString("exception.events.find"), e.getStatusCode().toString());
            throw new DataAccessException(msg);
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(e.getMessage(), e);
        }
    }
}
