package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;

import java.util.List;

public interface NewsRestClient {

    /**
     * Find all news entries.
     *
     * @return list of news entries
     * @throws DataAccessException in case something went wrong
     */
    List<SimpleNewsDTO> findAll() throws DataAccessException;

    /**
     * @param dto dto to be inserted into database
     * @throws DataAccessException in case something went wrong
     */
    public void createNews(DetailedNewsDTO dto) throws DataAccessException;


    DetailedNewsDTO findDetailed(Long id) throws DataAccessException;

    /**
     * @param newsDTO The news to be marked as read.
     * @throws DataAccessException in case something went wrong.
     */
    void markAsRead(DetailedNewsDTO newstDTO) throws DataAccessException;

    /**
     * Gets the news read by this user.
     */
    List<SimpleNewsDTO> getUnreadNews() throws DataAccessException;
}
