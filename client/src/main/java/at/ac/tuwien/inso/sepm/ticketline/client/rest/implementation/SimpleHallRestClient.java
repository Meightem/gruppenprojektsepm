package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.HallRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.List;

@Component
public class SimpleHallRestClient implements HallRestClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCustomerRestClient.class);
    private static final String HALLS_URL = "/halls";

    public SimpleHallRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    private final RestClient restClient;

    @Override
    public List<HallDTO> search(String text) throws DataAccessException {
        text = URLEncoder.encode(text, Charset.defaultCharset());
        try {
            LOGGER.debug("Searching halls with search term: " + text);
            ResponseEntity<List<HallDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(HALLS_URL + "/search/" + text),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<HallDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.hall.search",e);
        }
    }

    @Override
    public List<HallDTO> findAll() throws DataAccessException {
        try {
            LOGGER.debug("Getting all halls");
            ResponseEntity<List<HallDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(HALLS_URL),
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<HallDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.hall.search",e);
        }
    }
}
