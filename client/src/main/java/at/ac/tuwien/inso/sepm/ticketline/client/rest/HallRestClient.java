package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;

import java.util.List;

public interface HallRestClient {
    /**
     * Search halls by name
     *
     * @return List of halls
     * @throws DataAccessException when halls couldn't be read
     */
    List<HallDTO> search(String query) throws DataAccessException;

    /**
     * find all halls
     *
     * @return List of halls
     * @throws DataAccessException when halls couldn't be read
     */
    List<HallDTO> findAll() throws DataAccessException;
}
