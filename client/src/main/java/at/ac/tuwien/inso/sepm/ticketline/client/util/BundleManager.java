package at.ac.tuwien.inso.sepm.ticketline.client.util;

import at.ac.tuwien.inso.sepm.ticketline.client.service.implementation.SimpleUsersService;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static java.util.Locale.ENGLISH;
import static java.util.Locale.GERMAN;

/**
 * This class can be used to access resource bundles.
 */
public class BundleManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleUsersService.class);

    private static final List<Locale> SUPPORTED_LOCALES = Arrays.asList(GERMAN, ENGLISH);

    private static final String BASENAME = "localization.ticketlineClient";
    private static final String EXCEPTION_BASENAME = "localization.ticketlineClientExceptions";

    private static final Map<String, ResourceBundle> BUNDLES = new HashMap<>();
    private static final Map<String, ResourceBundle> EXCEPTION_BUNDLES = new HashMap<>();

    private static final ObjectProperty<ResourceBundle> currentBundle = new SimpleObjectProperty<ResourceBundle>();
    private static final ObjectProperty<ResourceBundle> currentExceptionBundle = new SimpleObjectProperty<ResourceBundle>();

    private static Locale locale = Locale.getDefault();

    static {
        SUPPORTED_LOCALES.forEach(locale -> {
            BUNDLES.put(locale.getLanguage(), ResourceBundle.getBundle(BASENAME, locale, new UTF8Control()));
            EXCEPTION_BUNDLES.put(locale.getLanguage(), ResourceBundle.getBundle(EXCEPTION_BASENAME, locale, new UTF8Control()));
        });
        currentBundle.set(BUNDLES.getOrDefault(locale.getLanguage(), BUNDLES.get(SUPPORTED_LOCALES.get(0).getLanguage())));
        currentExceptionBundle.set(EXCEPTION_BUNDLES.getOrDefault(locale.getLanguage(), EXCEPTION_BUNDLES.get(SUPPORTED_LOCALES.get(0).getLanguage())));
        currentBundle.addListener((observable, oldValue, newValue) ->
            LOGGER.debug("Language changed from {} to {}",
                oldValue.getLocale().getDisplayLanguage(ENGLISH),
                newValue.getLocale().getDisplayLanguage(ENGLISH)
            )
        );
    }

    /**
     * An empty private constructor to prevent the creation of an BundleManager Instance.
     */
    private BundleManager() {

    }

    /**
     * Gets the bundle for the current locale or if not set the default locale.
     * Please use getStringBinding/getObjectBinding... if you can instead
     *
     * @return the bundle
     */
    public static ResourceBundle getBundle() {
        return currentBundle.get();
    }

    public static Property<ResourceBundle> getBundleProperty(){
        return  currentBundle;
    }

    /**
     * Gets the exception bundle for the current locale or if not set the default local.
     * Please use getExceptionStringBinding/getObjectBinding... if you can instead
     *
     * @return the exception bundle
     */
    public static ResourceBundle getExceptionBundle() {
        return currentExceptionBundle.get();
    }
    public static ResourceBundle getExceptionBundle(Locale locale){
        return EXCEPTION_BUNDLES.getOrDefault(locale.getLanguage(), EXCEPTION_BUNDLES.get(SUPPORTED_LOCALES.get(0).getLanguage()));
    }

    /**
     * Changes the locale.
     *
     * @param locale the locale
     */
    public static void changeLocale(Locale locale) {
        if (!SUPPORTED_LOCALES.contains(locale)) {
            throw new IllegalArgumentException("Locale not supported");
        }
        currentBundle.set(BUNDLES.getOrDefault(locale.getLanguage(), BUNDLES.get(SUPPORTED_LOCALES.get(0).getLanguage())));
        currentExceptionBundle.set(EXCEPTION_BUNDLES.getOrDefault(locale.getLanguage(), EXCEPTION_BUNDLES.get(SUPPORTED_LOCALES.get(0).getLanguage())));
        BundleManager.locale = locale;
    }


    /**
     * Gets the Stringbinding for the given key.
     * If bundle changes this will update the string
     *
     * @param key key of the bundle
     * @return Stringbinding for the given key
     */
    public static StringBinding getStringBinding(String key) {
        return Bindings.createStringBinding(() -> currentBundle.get().getString(key), currentBundle);
    }

    /**
     * Gets the Objectbinding for the given key.
     * If bundle changes this will update the object
     *
     * @param key key of the bundle
     * @return Objectbinding for the given key
     */
    public static ObjectBinding getObjectBinding(String key) {
        return Bindings.createObjectBinding(() -> currentBundle.get().getObject(key), currentBundle);
    }

    /**
     * Gets the Stringbinding for the given key.
     * If bundle changes this will update the string
     *
     * @param key key of the bundle
     * @return Stringbinding for the given key
     */
    public static StringBinding getExceptionStringBinding(String key) {
        return Bindings.createStringBinding(() -> currentExceptionBundle.get().getString(key), currentExceptionBundle);
    }

    /**
     * Gets the Objectbinding for the given key.
     * If bundle changes this will update the object
     *
     * @param key key of the bundle
     * @return Objectbinding for the given key
     */
    public static ObjectBinding getExceptionObjectBinding(String key) {
        return Bindings.createObjectBinding(() -> currentExceptionBundle.get().getObject(key), currentExceptionBundle);
    }

    /**
     * Gets the supported locales.
     *
     * @return the supported locales
     */
    public static List<Locale> getSupportedLocales() {
        return SUPPORTED_LOCALES;
    }

    /**
     * UTF-8 resource bundle loader
     */
    private static class UTF8Control extends ResourceBundle.Control {

        @Override
        public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
            throws IllegalAccessException, InstantiationException, IOException {
            String bundleName = toBundleName(baseName, locale);
            String resourceName = toResourceName(bundleName, "properties");
            ResourceBundle bundle = null;
            InputStream stream = null;
            if (reload) {
                URL url = loader.getResource(resourceName);
                if (url != null) {
                    URLConnection connection = url.openConnection();
                    if (connection != null) {
                        connection.setUseCaches(false);
                        stream = connection.getInputStream();
                    }
                }
            } else {
                stream = loader.getResourceAsStream(resourceName);
            }
            if (stream != null) {
                try (InputStreamReader inputStreamReader = new InputStreamReader(stream, StandardCharsets.UTF_8)) {
                    bundle = new PropertyResourceBundle(inputStreamReader);
                }
            }
            return bundle;
        }
    }

}
