package at.ac.tuwien.inso.sepm.ticketline.client.gui;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.service.AuthenticationService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationRequest;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);

    @FXML
    private TextField txtUsername;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private Label lblUsername;

    @FXML
    private Label lblPassword;

    @FXML
    private Button btnAuthenticate;


    private final AuthenticationService authenticationService;

    private final MainController mainController;

    public AuthenticationController(AuthenticationService authenticationService, MainController mainController) {
        this.authenticationService = authenticationService;
        this.mainController = mainController;
    }

    public void resetInputFields() {
        txtUsername.setText("");
        txtPassword.setText("");
    }

    @FXML
    private void initialize() {
        initializeTextBindings();
    }

    private void initializeTextBindings() {
        txtUsername.promptTextProperty().bind(BundleManager.getStringBinding("authenticate.username"));
        txtPassword.promptTextProperty().bind(BundleManager.getStringBinding("authenticate.password"));
        lblUsername.textProperty().bind(BundleManager.getStringBinding("authenticate.username"));
        lblPassword.textProperty().bind(BundleManager.getStringBinding("authenticate.password"));
        btnAuthenticate.textProperty().bind(BundleManager.getStringBinding("authenticate.authenticate"));

    }

    @FXML
    private void handleAuthenticate(ActionEvent actionEvent) {
        Task<AuthenticationTokenInfo> task = new Task<AuthenticationTokenInfo>() {
            @Override
            protected AuthenticationTokenInfo call() throws DataAccessException {
                return authenticationService.authenticate(
                    AuthenticationRequest.builder()
                        .username(txtUsername.getText())
                        .password(txtPassword.getText())
                        .build());
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: Authentication. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    ((Node) actionEvent.getTarget()).getScene().getWindow()).showAndWait();
            }
        };
        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();
    }

}