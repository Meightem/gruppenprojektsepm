package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.PerformanceRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.util.List;
import java.util.Map;

@Component
public class SimplePerformanceRestClient implements PerformanceRestClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimplePerformanceRestClient.class);
    private static final String PERFORMANCES_URL = "/performances";

    public SimplePerformanceRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    private final RestClient restClient;

    @Override
    public PerformanceDTO create(PerformanceDTO dto) throws DataAccessException {
        try {
            LOGGER.debug("Creating performance {}", dto);
            ResponseEntity<PerformanceDTO> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(PERFORMANCES_URL),
                HttpMethod.POST,
                new HttpEntity<>(dto),
                new ParameterizedTypeReference<PerformanceDTO>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.performance.create",e);
        }
    }

    @Override
    public List<PerformanceDTO> findByEvent(EventDTO eventDTO) throws DataAccessException {
        try {
            LOGGER.debug("Finding all performances of {}", eventDTO);
            ResponseEntity<List<PerformanceDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI("/performances/byEvent/" + eventDTO.getId()),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<PerformanceDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.performance.event",e);
        }
    }

    @Override
    public List<PerformanceDTO> findByHall(HallDTO hallDTO) throws DataAccessException {
        try {
            LOGGER.debug("Finding all performances of {}", hallDTO);
            ResponseEntity<List<PerformanceDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI("/performances/byHall/" + hallDTO.getId()),
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<PerformanceDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.performance.hall",e);
        }
    }

    @Override
    public List<SeatDTO> getSeatsByPerformance(PerformanceDTO performanceDTO) throws DataAccessException {
        try {
            LOGGER.debug("getting all seats of {}", performanceDTO);
            ResponseEntity<List<SeatDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI("/performances/seats/"+performanceDTO.getId()),
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<SeatDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.performance.seats.get",e);
        }
    }

    @Override
    public Map<String, Long> getSeatsPerSectorByPerformance(PerformanceDTO performanceDTO) throws DataAccessException {
        try {
            LOGGER.debug("getting sector available seats map of {}", performanceDTO);
            ResponseEntity<Map<String, Long>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI("/performances/seats/bysector/"+performanceDTO.getId()),
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<Map<String, Long>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.performance.seatspersector.get",e);
        }
    }

    @Override
    public List<PerformanceDTO> search(PerformanceDTO criteria) throws DataAccessException {
        try {
            LOGGER.debug("searching for performances: {}", criteria);
            ResponseEntity<List<PerformanceDTO>> response = restClient.exchange(
                restClient.getServiceURI(PERFORMANCES_URL + "/search"),
                HttpMethod.POST,
                new HttpEntity<>(criteria),
                new ParameterizedTypeReference<List<PerformanceDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            LOGGER.error("Error: {}", e.getMessage());
            String msg = String.format(BundleManager.getExceptionBundle().getString("exception.performance.search"), e.getStatusCode().toString());
            throw new DataAccessException(msg);
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(e.getMessage(), e);
        }
    }
}
