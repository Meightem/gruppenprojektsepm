package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.CustomerRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleCustomerService implements CustomerService {

    private final CustomerRestClient customerRestClient;

    public SimpleCustomerService(CustomerRestClient customerRestClient) {
        this.customerRestClient = customerRestClient;
    }

    @Override
    public CustomerDTO create(CustomerDTO dto) throws DataAccessException {
        return customerRestClient.create(dto);
    }

    @Override
    public List<CustomerDTO> findAll() throws DataAccessException {
        return customerRestClient.findAll();
    }

    @Override
    public CustomerDTO update(CustomerDTO dto) throws DataAccessException {
        return customerRestClient.update(dto);
    }

    @Override
    public List<CustomerDTO> search(String text) throws DataAccessException {
        return customerRestClient.search(text);
    }
}
