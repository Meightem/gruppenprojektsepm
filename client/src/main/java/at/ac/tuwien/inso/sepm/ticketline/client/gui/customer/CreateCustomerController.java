package at.ac.tuwien.inso.sepm.ticketline.client.gui.customer;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CreateCustomerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateCustomerController.class);

    @FXML
    private TabHeaderController headerController;

    private final MainController mainController;
    private final SpringFxmlLoader springFxmlLoader;
    private final CustomerService customerService;
    @FXML
    private TextField txtLastName;
    @FXML
    private TextField txtFirstName;
    @FXML
    private Label lblFirstname;
    @FXML
    private Label lblLastname;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnSubmit;


    private CustomerDTO customerDTO;

    private boolean isCreateMode = true;

    public CreateCustomerController(MainController mainController, SpringFxmlLoader springFxmlLoader, CustomerService customerService) {
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.customerService = customerService;
    }

    @FXML
    private void initialize() {
        customerDTO = new CustomerDTO();
        customerDTO.setIsAnonymousCustomer(false);
        isCreateMode = true;
        updateView();
    }

    private void initializeTextBindings() {
        headerController.setTitleBinding(BundleManager.getStringBinding(isCreateMode ? "customer.create" : "customer.edit"));
        lblFirstname.textProperty().bind(BundleManager.getStringBinding("general.firstName"));
        lblLastname.textProperty().bind(BundleManager.getStringBinding("general.lastName"));
        btnCancel.textProperty().bind(BundleManager.getStringBinding("general.cancel"));
        btnSubmit.textProperty().bind(BundleManager.getStringBinding("general.submit"));
    }

    public void setCustomer(CustomerDTO customer) {
        this.customerDTO = customer;
        isCreateMode = false;
        updateView();
    }

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void submit(ActionEvent actionEvent) {
        Task<CustomerDTO> task = new Task<CustomerDTO>() {
            @Override
            protected CustomerDTO call() throws DataAccessException {
                customerDTO.setFirstName(txtFirstName.getText());
                customerDTO.setLastName(txtLastName.getText());
                if (isCreateMode) {
                    customerDTO = customerService.create(customerDTO);
                } else {
                    customerDTO = customerService.update(customerDTO);
                }
                return customerDTO; // We need to return an instance of Void, just return null.
                // Is `(Void) null` more void than Void?
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: Customer created.");
                Alert a = new Alert(Alert.AlertType.INFORMATION);
                a.setHeaderText(BundleManager.getBundle().getString("customer.create.success"));
                a.setContentText(String.format("%s: %s - %s %s", BundleManager.getBundle().getString("customer.name"), getValue().getId(), getValue().getFirstName(), getValue().getLastName()));
                a.showAndWait();
                ((Stage) txtFirstName.getScene().getWindow()).close();
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: Customer was not created. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    txtFirstName.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    @FXML
    private void cancel() {
        ((Stage) txtFirstName.getScene().getWindow()).close();
    }

    private void updateView() {
        headerController.setIcon(isCreateMode ? FontAwesome.Glyph.USER_PLUS : FontAwesome.Glyph.EDIT);
        initializeTextBindings();
        txtLastName.setText(customerDTO.getLastName());
        txtFirstName.setText(customerDTO.getFirstName());
    }
}
