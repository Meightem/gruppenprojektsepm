package at.ac.tuwien.inso.sepm.ticketline.client.gui.admin;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.UsersService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ResetUserpasswordController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResetUserpasswordController.class);

    @FXML
    private TabHeaderController headerController;

    private final MainController mainController;
    private final SpringFxmlLoader springFxmlLoader;
    private final UsersService usersService;
    private UserDTO user;

    @FXML
    private PasswordField txtPassword;
    @FXML
    private PasswordField txtConfirmPassword;
    @FXML
    private Label lblPassword;
    @FXML
    private Label lblPasswordConfirm;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnSubmit;

    public ResetUserpasswordController(MainController mainController, SpringFxmlLoader springFxmlLoader, UsersService usersService) {
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.usersService = usersService;
    }

    @FXML
    private void initialize() {
        headerController.setIcon(FontAwesome.Glyph.EXCHANGE);
        initializeTextBindings();
    }

    private void initializeTextBindings() {
        headerController.setTitleBinding(BundleManager.getStringBinding("admin.user.resetPassword"));
        lblPassword.textProperty().bind(BundleManager.getStringBinding("general.password"));
        lblPasswordConfirm.textProperty().bind(BundleManager.getStringBinding("general.password.confirm"));
        btnCancel.textProperty().bind(BundleManager.getStringBinding("general.cancel"));
        btnSubmit.textProperty().bind(BundleManager.getStringBinding("general.submit"));
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @FXML
    private void cancel() {
        ((Stage) txtPassword.getScene().getWindow()).close();
    }

    @FXML
    private void submit() {
        if (!txtPassword.getText().equals(txtConfirmPassword.getText())) {
            JavaFXUtils.createErrorDialog(BundleManager.getStringBinding("user.create.passwordMismatch"),
                txtPassword.getScene().getWindow()).showAndWait();
            return;
        }

        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws DataAccessException {
                user.setPassword(txtPassword.getText());
                usersService.resetPassword(user);
                return null;
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Suceeded: Password reset");
                ((Stage) txtPassword.getScene().getWindow()).close();
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: Password not reset ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    txtPassword.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }
}
