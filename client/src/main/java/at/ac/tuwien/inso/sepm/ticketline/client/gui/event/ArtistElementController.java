package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Controller
@Scope("prototype")
public class ArtistElementController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventElementController.class);

    @FXML
    private Node mainNode;

    @FXML
    private Label lblFirstName;

    @FXML
    private Label lblLastName;

    private ArtistDTO artist;

    @Autowired
    public ArtistElementController() {

    }

    @FXML
    private void initialize() {
        updateView();
    }

    public void setArtist(ArtistDTO artist) {
        this.artist = artist;
        updateView();
    }

    private void updateView() {
        if (artist == null) return;
        lblFirstName.setText(artist.getFirstName());
        lblLastName.setText(artist.getLastName());
    }
}
