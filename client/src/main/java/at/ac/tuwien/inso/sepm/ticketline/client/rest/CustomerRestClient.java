package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;

import java.util.List;

public interface CustomerRestClient {
    /**
     * Create a single customer
     *
     * @param dto The customer to create
     * @return The created customer
     * @throws DataAccessException when customer couldn't be created
     */
    CustomerDTO create(CustomerDTO dto) throws DataAccessException;

    /**
     * Find all customers
     *
     * @return List of all customers
     * @throws DataAccessException when customer couldn't be readout
     */
    List<CustomerDTO> findAll() throws DataAccessException;

    /**
     * Edit a single customer
     *
     * @param dto new customer with old id
     * @return new customer
     * @throws DataAccessException when customer couldn't be edited
     */
    CustomerDTO update(CustomerDTO dto) throws DataAccessException;


    /**
     * searches the customres according to the search term
     *
     * @param text search term
     * @return the customers,w hich match the term
     * @throws DataAccessException
     */
    List<CustomerDTO> search(String text) throws DataAccessException;

    /**
     * Fetch the anonymous customer
     * @return the customer, with isAnonymousCustomer = true
     */
    CustomerDTO getAnonymousCustomer() throws DataAccessException;
}
