package at.ac.tuwien.inso.sepm.ticketline.client.gui.news;

import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.NewsService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;


@Component
public class NewsCreationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsCreationController.class);
    private final SpringFxmlLoader springFxmlLoader;
    private final NewsService newsService;
    private final MainController mainController;

    @FXML
    private TabHeaderController headerController;
    @FXML
    private Label lblCreateNews;
    @FXML
    private TextField tfTitle;
    @FXML
    private TextArea tfText;

    private final FileChooser imageChooser;
    Path imageChooserValue;

    @FXML
    private Button btnImage;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnClose;


    public NewsCreationController(MainController mainController, SpringFxmlLoader springFxmlLoader, NewsService newsService) {
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.newsService = newsService;

        this.imageChooser = new FileChooser();
        this.imageChooser
            .getExtensionFilters()
            .addAll(new FileChooser.ExtensionFilter("PNG and JPEG", "*.png", "*.jpg", "*.jpeg"));
    }

    @FXML
    private void close() {
        ((Stage) tfText.getScene().getWindow()).close();
    }

    @FXML
    private void initialize() {
        initializeTextBindings();
        headerController.setIcon(FontAwesome.Glyph.PLUS);
        imageChooserValue = null;
        updateImageButton();
    }

    private void initializeTextBindings() {
        headerController.setTitleBinding(BundleManager.getStringBinding("news.create.header"));
        tfTitle.promptTextProperty().bind(BundleManager.getStringBinding("news.create.titlePrompt"));
        tfText.promptTextProperty().bind(BundleManager.getStringBinding("news.create.textPrompt"));
        updateImageButton();
        btnSave.textProperty().bind(BundleManager.getStringBinding("news.create.save"));
        btnClose.textProperty().bind(BundleManager.getStringBinding("news.create.close"));
    }

    @FXML
    private void onImageButtonClicked() {
        LOGGER.info("Clicked on image button");
        if (imageChooserValue != null) {
            imageChooserValue = null;
        } else {
            Stage s = (Stage) btnImage.getScene().getWindow();
            File val = imageChooser.showOpenDialog(s);
            if (val == null) return;
            imageChooserValue = val.toPath();
        }
        updateImageButton();
    }

    private void updateImageButton() {
        if (imageChooserValue == null) {
            btnImage.textProperty().bind(BundleManager.getStringBinding("news.create.image.add"));
        } else {
            btnImage.textProperty().bind(BundleManager.getStringBinding("news.create.image.clear"));
        }
    }

    @FXML
    private void onSaveButtonClicked() {
        LOGGER.info("Clicked on save button");
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                String image = null;
                if (imageChooserValue != null) {
                    image = DatatypeConverter.printBase64Binary(Files.readAllBytes(imageChooserValue));
                }


                DetailedNewsDTO dto = DetailedNewsDTO.builder()
                    .text(tfText.getText())
                    .title(tfTitle.getText())
                    .image(image)
                    .build();
                newsService.createNews(dto);
                return null; // We need to return an instance of Void, just return null.
                // Is `(Void) null` more void than Void?
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: saved news.");
                close();
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: news was not saved. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    tfText.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }
}
