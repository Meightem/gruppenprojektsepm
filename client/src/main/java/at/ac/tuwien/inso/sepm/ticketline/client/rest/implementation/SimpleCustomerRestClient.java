package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.CustomerRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.List;

@Component
public class SimpleCustomerRestClient implements CustomerRestClient {


    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCustomerRestClient.class);
    private static final String CUSTOMER_URL = "/customers";

    public SimpleCustomerRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    private final RestClient restClient;

    @Override
    public CustomerDTO create(CustomerDTO dto) throws DataAccessException {
        try {
            LOGGER.debug("Creating customer {}", dto);
            ResponseEntity<CustomerDTO> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(CUSTOMER_URL),
                HttpMethod.POST,
                new HttpEntity<>(dto),
                new ParameterizedTypeReference<CustomerDTO>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.customer.create", e);
        }
    }

    @Override
    public List<CustomerDTO> findAll() throws DataAccessException {
        try {
            LOGGER.debug("Finding all customers");
            ResponseEntity<List<CustomerDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(CUSTOMER_URL),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CustomerDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.customer.findAll", e);
        }
    }

    @Override
    public CustomerDTO update(CustomerDTO dto) throws DataAccessException {
        try {
            LOGGER.debug("Update user {}", dto);
            ResponseEntity<CustomerDTO> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(CUSTOMER_URL + "/" + dto.getId()),
                HttpMethod.PATCH,
                new HttpEntity<>(dto),
                new ParameterizedTypeReference<CustomerDTO>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.customer.update", e);
        }
    }

    @Override
    public List<CustomerDTO> search(String text) throws DataAccessException {
        text = URLEncoder.encode(text, Charset.defaultCharset());
        try {
            LOGGER.debug("Searching customers with search term: " + text);
            ResponseEntity<List<CustomerDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(CUSTOMER_URL + "/search/" + text),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CustomerDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.customer.search",e);
        }
    }

    @Override
    public CustomerDTO getAnonymousCustomer() throws DataAccessException {
        try {
            LOGGER.debug("Fetching anonymous customer");
            ResponseEntity<CustomerDTO> response = restClient.exchange(
                restClient.getServiceURI(CUSTOMER_URL + "/anonymous"),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<CustomerDTO>() {}
            );
            LOGGER.debug("Successfully fetched anon user");
            return response.getBody();
        } catch (HttpStatusCodeException e) {
            LOGGER.error("Error: {}", e.getMessage());
            String msg = String.format(BundleManager.getExceptionBundle().getString("exception.customer.search"), e.getStatusCode().toString());
            throw new DataAccessException(msg);
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException(e.getMessage(), e);
        }
    }
}
