package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventSearchDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.TopEventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;

import javax.xml.crypto.Data;
import java.util.List;

public interface EventRestClient {
    /**
     * Create a single event
     *
     * @param dto The event to create
     * @return The created event
     * @throws DataAccessException when event couldn't be created
     */
    EventDTO create(EventDTO dto) throws DataAccessException;

    /**
     * Find all event
     *
     * @return List of all event
     * @throws DataAccessException when event couldn't be read
     */
    List<EventDTO> findAll() throws DataAccessException;

    /**
     *
     * Find top events
     *
     * @param type Type of event to show
     * @param month of statistic
     * @param year of statistic
     * @return list of top events
     * @throws DataAccessException when events could not be read
     */
    List<TopEventDTO> findTopTenEvents(EventType type, int month, int year) throws DataAccessException;

    List<EventDTO> searchEvent(EventSearchDTO eventSearchDTO) throws DataAccessException;

    List<TopEventDTO> getTopTenEvents(EventType type, Integer month, Integer year) throws DataAccessException;

    /**
     * Get all events for one artist
     * @param artistDTO the artist to search for
     * @return the artist's events
     */
    List<EventDTO> findByArtist(ArtistDTO artistDTO) throws DataAccessException;
}
