package at.ac.tuwien.inso.sepm.ticketline.client.gui.performance;

import at.ac.tuwien.inso.sepm.ticketline.client.TicketlineClientApplication;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.ticket.TicketSellController;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("prototype")
public class PerformanceListController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceListController.class);
    public Label lblEventHeading;
    public Label lblPriceHeading;
    public Label lblDateHeading;
    public Label lblLocationHeading;

    @FXML
    private ListView<PerformanceDTO> lvPerformanceElements;

    @FXML
    private Label lblNoPerformances;


    private final SpringFxmlLoader springFxmlLoader;
    private final ObservableList<PerformanceDTO> performanceObservableList;

    private BooleanProperty showEventName;

    public BooleanProperty showEventNameProperty() {
        return showEventName;
    }

    public ObservableList<PerformanceDTO> getPerformanceObservableList() {
        return performanceObservableList;
    }

    public PerformanceListController(SpringFxmlLoader springFxmlLoader) {
        this.performanceObservableList = FXCollections.observableArrayList();
        this.springFxmlLoader = springFxmlLoader;
        this.showEventName = new SimpleBooleanProperty();
    }

    @FXML
    private void initialize() {
        initializeTextBindings();
        initializeListView();
    }

    private void initializeTextBindings() {
        lblNoPerformances.textProperty().bind(BundleManager.getStringBinding("perf.empty"));
        lblDateHeading.textProperty().bind(BundleManager.getStringBinding("perf.date"));
        lblEventHeading.textProperty().bind(BundleManager.getStringBinding("perf.search.event"));
        lblLocationHeading.textProperty().bind(BundleManager.getStringBinding("events.tab.location"));
        lblPriceHeading.textProperty().bind(BundleManager.getStringBinding("perf.price"));
    }

    private void initializeListView() {
        lvPerformanceElements.setCellFactory(lv -> {
            return new ListCell<PerformanceDTO>() {
                @Override
                public void updateItem(PerformanceDTO performanceDTO, boolean empty) {
                    super.updateItem(performanceDTO, empty);
                    if (empty || performanceDTO == null) {
                        setGraphic(null);
                    } else {
                        SpringFxmlLoader.Wrapper<PerformanceElementController> wrapper = springFxmlLoader.loadAndWrap("/fxml/performance/performanceElement.fxml");
                        wrapper.getController().setPerformance(performanceDTO);
                        wrapper.getController().showEventNameProperty().bind(showEventName);
                        //DoubleClick to open detailview
                        wrapper.getLoadedObject().setOnMouseClicked(event -> {
                            if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() > 1) {
                                onPerformanceSelected(performanceDTO);
                            }
                        });

                        final ContextMenu contextMenu = new ContextMenu();

                        MenuItem sellTicket = new MenuItem();
                        sellTicket.textProperty().bind(BundleManager.getStringBinding("selling.buy"));
                        sellTicket.setId("sellTicket");
                        sellTicket.setOnAction(event -> onPerformanceSelected(performanceDTO));
                        contextMenu.getItems().add(sellTicket);

                        contextMenu.getStyleClass().add("brightContextMenu");
                        setContextMenu(contextMenu);

                        setGraphic(wrapper.getLoadedObject());
                        getStyleClass().add("PerformanceRow");
                    }
                }
            };
        });
    }

    public void onPerformanceSelected(PerformanceDTO performanceDTO) {
        LOGGER.info("Performance selected {}", performanceDTO);
        SpringFxmlLoader.AnyWrapper<TicketSellController, AnchorPane> objectWrapper = springFxmlLoader.loadAndWrapAny("/fxml/ticket/ticketSellComponent.fxml");
        objectWrapper.getController().setPerformance(performanceDTO);
        objectWrapper.getController().initializeHallPlan();
        Stage stage = new Stage();
        stage.titleProperty().bind(BundleManager.getStringBinding("news.detailed.header"));
        stage.getIcons().add(new Image(TicketlineClientApplication.class.getResourceAsStream("/image/ticketlineIcon.png")));
        stage.setMaximized(true);
        stage.setScene(new Scene(objectWrapper.getLoadedObject()));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(lvPerformanceElements.getScene().getWindow());
        stage.showAndWait();
    }
}
