package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;

import java.util.List;

public interface HallService {
    /**
     * searches the halls according to the search term
     *
     * @param text search term
     * @return the halls which match the term
     * @throws DataAccessException
     */
    List<HallDTO> search(String text) throws DataAccessException;
}
