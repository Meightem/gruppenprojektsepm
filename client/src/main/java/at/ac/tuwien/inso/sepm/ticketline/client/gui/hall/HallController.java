package at.ac.tuwien.inso.sepm.ticketline.client.gui.hall;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatState;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.Sector;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.util.*;

@Component
@Scope("prototype")
public class HallController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HallController.class);
    @FXML
    AnchorPane mainHallContainer;
    @FXML
    FlowPane legend;
    @FXML
    ScrollPane scrollPane;

    private PerformanceDTO performance;

    private final ObservableList<SeatDTO> seats;
    private final ObservableList<SeatDTO> selectedSeats;

    private static double UI_SEATSIZE = 40; //In Pixel
    private static final double UI_SEATSPACING = 2; //In Pixel
    private static final double UI_XOFFSET = 2; //In seatsizes
    private static final double UI_YOFFSET = 2; //In seatsizes
    private static final double UI_MINSEATSIZE = 15;
    private static final double UI_MAXSEATSIZE = 40;

    private static final double UI_PADDINGHEIGHT = 50;
    private static final double UI_PADDINGWIDTH = 30;
    private static final boolean UI_ENABLERESIZEING = false; //At the moment to buggy to always be enabled

    public HallController() {
        seats = FXCollections.observableArrayList();
        selectedSeats = FXCollections.observableArrayList();
        this.seats.addListener((ListChangeListener<SeatDTO>) c -> updateSeats());
    }

    @FXML
    private void initialize() {
        LOGGER.debug("hall initialized.");
        if (UI_ENABLERESIZEING) {
            this.scrollPane.widthProperty().addListener(observable -> {
                updateSeats();
            });
            this.scrollPane.heightProperty().addListener(observable -> {
                updateSeats();
            });
        }
    }

    private void updateSeats() {
        setLegend();
        adjustUIScaling();
        mainHallContainer.getChildren().clear();
        if (performance.getEvent().getType() == EventType.SECTORS) {
            Map<MyPoint, SeatDTO> pointMap = getPointMap(seats);
            for (SeatDTO seat : seats) {
                SeatDTO l = pointMap.getOrDefault(new MyPoint(seat.getxCoord() - 1, seat.getyCoord()), null);
                SeatDTO t = pointMap.getOrDefault(new MyPoint(seat.getxCoord(), seat.getyCoord() - 1), null);
                SeatDTO r = pointMap.getOrDefault(new MyPoint(seat.getxCoord() + 1, seat.getyCoord()), null);
                SeatDTO b = pointMap.getOrDefault(new MyPoint(seat.getxCoord(), seat.getyCoord() + 1), null);

                SectorTileControl sectorTile = new SectorTileControl(seat, l, t, r, b, UI_SEATSIZE + UI_SEATSPACING);
                AnchorPane.setTopAnchor(sectorTile, seat.getyCoord() * (UI_SEATSIZE + UI_SEATSPACING));
                AnchorPane.setLeftAnchor(sectorTile, seat.getxCoord() * (UI_SEATSIZE + UI_SEATSPACING));
                mainHallContainer.getChildren().add(sectorTile);
            }
        } else {
            for (SeatDTO seat : seats) {
                /*This can be used to randomly set some seats as reserved or free only for testing purposes*/
                //seat.setState(Math.random() < 0.5 ? SeatState.FREE : Math.random() < 0.5 ? SeatState.RESERVED : SeatState.RESERVED);
                SeatControl seatnode = new SeatControl(seat, UI_SEATSIZE);
                if (selectedSeats.contains(seat)) {
                    seatnode.getSelectedProperty().set(true);
                }
                seatnode.getSelectedProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue) {
                        if (!selectedSeats.contains(seatnode.getSeat())) {
                            selectedSeats.add(seatnode.getSeat());
                        }
                    } else {
                        selectedSeats.remove(seatnode.getSeat());
                    }
                });
                AnchorPane.setTopAnchor(seatnode, UI_SEATSIZE * UI_YOFFSET + (double) seat.getyCoord() * (UI_SEATSIZE + UI_SEATSPACING));
                AnchorPane.setLeftAnchor(seatnode, UI_SEATSIZE * UI_XOFFSET + (double) seat.getxCoord() * (UI_SEATSIZE + UI_SEATSPACING));
                mainHallContainer.getChildren().add(seatnode);
            }
            addNumberisation();
        }
    }

    private MyPoint getMaxPoint() {
        MyPoint max = new MyPoint(Long.MIN_VALUE, Long.MIN_VALUE);
        for (SeatDTO seatDTO : this.seats) {
            if (max.x < seatDTO.getxCoord()) {
                max.setX(seatDTO.getxCoord());
            }
            if (max.y < seatDTO.getyCoord()) {
                max.setY(seatDTO.getyCoord());
            }
        }
        return max;
    }

    private void addNumberisation() {
        Set<Long> rows = new HashSet<>();
        Set<Long> cols = new HashSet<>();
        for (SeatDTO seatDTO : this.seats) {
            rows.add(seatDTO.getyCoord());
            cols.add(seatDTO.getxCoord());
        }
        addRowNumbers(rows);
        addColumnNumbers(cols);
    }

    private void addRowNumbers(Set<Long> rows) {
        for (Long row : rows) {
            Label rowLbl = new Label();
            rowLbl.setFont(new Font(UI_SEATSIZE));
            rowLbl.getStyleClass().add("rowNumber");
            rowLbl.setText(""+(row+1));
            rowLbl.setPrefSize(UI_SEATSIZE * UI_XOFFSET - UI_SEATSPACING, UI_SEATSIZE);
            rowLbl.setMaxSize(UI_SEATSIZE * UI_XOFFSET - UI_SEATSPACING, UI_SEATSIZE);
            rowLbl.setTextAlignment(TextAlignment.RIGHT);
            rowLbl.setAlignment(Pos.CENTER_RIGHT);
            AnchorPane.setLeftAnchor(rowLbl, 0.0);
            AnchorPane.setTopAnchor(rowLbl, UI_SEATSIZE * UI_YOFFSET - UI_SEATSIZE / 4 + row * (UI_SEATSIZE + UI_SEATSPACING));
            mainHallContainer.getChildren().add(rowLbl);
        }
    }

    private void addColumnNumbers(Set<Long> cols) {
        for (Long col : cols) {
            Label colLbl = new Label();
            colLbl.setFont(new Font(UI_SEATSIZE));
            colLbl.getStyleClass().add("colNumber");
            colLbl.setText(""+(col+1));
            colLbl.setRotate(270);
            colLbl.setPrefSize(UI_SEATSIZE * UI_YOFFSET - UI_SEATSPACING, UI_SEATSIZE);
            colLbl.setMaxSize(UI_SEATSIZE * UI_YOFFSET - UI_SEATSPACING, UI_SEATSIZE);
            colLbl.setTextAlignment(TextAlignment.LEFT);
            colLbl.setAlignment(Pos.CENTER_LEFT);
            AnchorPane.setLeftAnchor(colLbl, UI_SEATSIZE * UI_XOFFSET - UI_SEATSIZE / 2 + col * (UI_SEATSIZE + UI_SEATSPACING));
            AnchorPane.setTopAnchor(colLbl, 0.0);
            mainHallContainer.getChildren().add(colLbl);
        }
    }

    private void setLegend() {
        legend.getChildren().clear();
        Set<Sector> sectors = new HashSet<>();
        for (SeatDTO seat : seats) {
            sectors.add(seat.getSector());
        }
        List<Sector> sortedSectors = new ArrayList<>(sectors);
        sortedSectors.sort(Comparator.comparing(Enum::name));
        for (Sector sector : sortedSectors) {
            HBox sectorLegend = new HBox();
            sectorLegend.getStyleClass().add("hbox");

            Label lblSectorName = new Label();
            lblSectorName.getStyleClass().add("sectorname");
            HBox.setHgrow(lblSectorName, Priority.SOMETIMES);
            lblSectorName.setText(sector.name());

            Label lblSectorPrice = new Label();
            BorderPane borderPanePrice = new BorderPane();
            HBox.setHgrow(borderPanePrice, Priority.SOMETIMES);
            borderPanePrice.setCenter(lblSectorPrice);
            lblSectorPrice.getStyleClass().add("sectorprice");
            BigDecimal seatPrice = sector.getFactor().multiply(performance.getPrice()).round(new MathContext(2));
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(2);
            df.setMinimumFractionDigits(2);
            df.setGroupingUsed(false);
            lblSectorPrice.setText(String.format("(%s€)",df.format(seatPrice)));

            BorderPane borderPaneColor = new BorderPane();
            HBox.setHgrow(borderPaneColor, Priority.NEVER);
            Pane colorIndicator = new Pane();
            double indicatorSize = 0.4; // of LabelHeight
            colorIndicator.maxHeightProperty().bind(lblSectorName.heightProperty().multiply(indicatorSize));
            colorIndicator.maxWidthProperty().bind(lblSectorName.heightProperty().multiply(indicatorSize));
            colorIndicator.prefWidthProperty().bind(lblSectorName.heightProperty().multiply(indicatorSize));
            colorIndicator.prefHeightProperty().bind(lblSectorName.heightProperty().multiply(indicatorSize));

            Color color = sector.getColor();
            colorIndicator.setBackground(new Background(
                new BackgroundFill(color,
                    CornerRadii.EMPTY,
                    Insets.EMPTY)
            ));
            colorIndicator.setBorder(new Border(
                new BorderStroke(
                    color.darker(),
                    BorderStrokeStyle.SOLID,
                    CornerRadii.EMPTY,
                    new BorderWidths(2)
                )));
            borderPaneColor.setCenter(colorIndicator);
            sectorLegend.getChildren().addAll(borderPaneColor, lblSectorName ,borderPanePrice);
            legend.getChildren().add(sectorLegend);
        }
    }

    private void adjustUIScaling() {
        MyPoint maxPoint = getMaxPoint();
        double xSize = (scrollPane.getWidth()-UI_PADDINGWIDTH) / ((maxPoint.getX() + 1) + (performance.getEvent().getType() == EventType.SEATS ? UI_XOFFSET : 0.0)); //+1 because of pos 0 and only seats type has an offset
        double ySize = (scrollPane.getHeight()-UI_PADDINGHEIGHT) / ((maxPoint.getY() + 1) + (performance.getEvent().getType() == EventType.SEATS ? UI_YOFFSET : 0.0)); //+1 because of pos 0 and only seats type has an offset
        UI_SEATSIZE = Math.min(xSize, ySize) - UI_SEATSPACING;
        //if (performance.getEvent().getType() == EventType.SEATS) { //min/max only when seats
            UI_SEATSIZE = Math.min(Math.max(UI_MINSEATSIZE, UI_SEATSIZE), UI_MAXSEATSIZE);
        //}
    }

    public void setSeats(List<SeatDTO> seats) {
        if (seats == null) {
            throw new IllegalArgumentException("Seats cannot be null!");
        }
        this.seats.setAll(seats);
    }

    public ObservableList<SeatDTO> getSelectedSeats() {
        return selectedSeats;
    }

    private static Map<MyPoint, SeatDTO> getPointMap(List<SeatDTO> seats) {
        Map<MyPoint, SeatDTO> map = new HashMap<>();
        for (SeatDTO seat : seats) {
            map.put(new MyPoint(seat.getxCoord(), seat.getyCoord()), seat);
        }
        return map;
    }

    public void setPerformanceDTO(PerformanceDTO performanceDTO) {
        this.performance = performanceDTO;
        updateSeats();
    }

    private static class MyPoint {
        private long x;
        private long y;

        public MyPoint(Long x, Long y) {
            this.x = x;
            this.y = y;
        }

        public long getX() {
            return x;
        }

        public void setX(long x) {
            this.x = x;
        }

        public long getY() {
            return y;
        }

        public void setY(long y) {
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MyPoint myPoint = (MyPoint) o;
            return x == myPoint.x &&
                y == myPoint.y;
        }

        @Override
        public int hashCode() {

            return Objects.hash(x, y);
        }
    }

    public List<SeatDTO> getSeats() {
        return seats;
    }

}
