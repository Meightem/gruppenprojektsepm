package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.seat.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import javafx.collections.ObservableList;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketSearchDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface TicketRestClient {
    List<TicketDTO> findAll() throws DataAccessException;

    /**
     * Buy a ticket
     * @param customer
     * @param performance
     * @param seats
     */
    Long buyTicketForSeats(CustomerDTO customer, PerformanceDTO performance, List<SeatDTO> seats) throws DataAccessException;

    /**
     * Buy a ticket
     * @param customer
     * @param performance
     * @param sectors
     */
    Long buyTicketForSectors(CustomerDTO customer, PerformanceDTO performance, Map<String, Long> sectors) throws DataAccessException;

    void refundReserved(Long ticketId) throws DataAccessException;

    void refund(Long ticketId) throws DataAccessException;

    byte[] getBill(Long ticketId) throws DataAccessException;

    List<TicketDTO> search(TicketSearchDTO ticketSearchDTO) throws DataAccessException;

    TicketDTO search(Long id) throws DataAccessException;


    Long reserveTicketForSeats(CustomerDTO customer, PerformanceDTO performance, List<SeatDTO> selectedSeats) throws DataAccessException;

    Long reserveTicketForSectors(CustomerDTO customer, PerformanceDTO performance, Map<String, Long> sectors) throws DataAccessException;

    TicketDTO buyReservedTicketForSectors(TicketDTO ticket, Map<String, Long> sectors) throws DataAccessException;

    TicketDTO buyReservedTicketForSeats(TicketDTO ticket, List<SeatDTO> seats) throws DataAccessException;

    Map<String, Long> getSeatsBySector(Long id) throws DataAccessException;

    List<SeatDTO> getSeats(Long id) throws DataAccessException;
}
