package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.configuration.properties.RestClientConfigurationProperties;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.exception.ExceptionDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RestClient extends RestTemplate {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestClient.class);
    private final String baseUrl;

    public RestClient(
        List<ClientHttpRequestInterceptor> interceptors,
        RestClientConfigurationProperties restClientConfigurationProperties
    ) {
        super(new HttpComponentsClientHttpRequestFactory(HttpClients
            .custom()
            .setConnectionManager(new PoolingHttpClientConnectionManager())
            .build()));
        setInterceptors(interceptors);
        String computedBaseUrl = restClientConfigurationProperties.getRemote().getFullUrl();
        if (computedBaseUrl.endsWith("/")) {
            computedBaseUrl = computedBaseUrl.substring(0, computedBaseUrl.length() - 1);
        }
        baseUrl = computedBaseUrl;
    }
    /**
     * Exchanges data with the server and tries to parse error messages if they occurred
     * @implNote Only error messages get parsed, if the server is not responding a RestClientException will be thrown
     * @param url - the URL
     * @param method - the HTTP method (GET, POST, etc)
     * @param requestEntity - the entity (headers and/or body) to write to the request (may be null)
     * @param responseType - the type of the return value
     * @throws DataAccessException when an error occurred executing the request.
     * @throws org.springframework.web.client.RestClientException when response from server could not be parsed or server is not responding
     * @return the response as entity
     */

    public <T> ResponseEntity<T> parseExceptionIfPossibleExchange(URI url, HttpMethod method, @Nullable HttpEntity<?> requestEntity, ParameterizedTypeReference<T> responseType) throws DataAccessException {
        try {
            return super.exchange(url, method, requestEntity, responseType);
        } catch (HttpStatusCodeException ex) {
            ExceptionDTO exceptionDTO;
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                exceptionDTO = objectMapper.readValue(ex.getResponseBodyAsString(), ExceptionDTO.class);
            } catch (Exception e) {
                throw ex;
            }
            DataAccessException dataAccessException = new DataAccessException(exceptionDTO, ex);
            LOGGER.error(dataAccessException.getMessage());
            throw dataAccessException;
        }
    }

    /**
     * Get the full URI of the endpoint.
     *
     * @param serviceLocation the endpoints location
     * @return full URI of the endpoint
     */
    public URI getServiceURI(String serviceLocation) {
        if (!serviceLocation.startsWith("/")) {
            return URI.create(baseUrl + "/" + serviceLocation);
        }
        return URI.create(baseUrl + serviceLocation);
    }

}