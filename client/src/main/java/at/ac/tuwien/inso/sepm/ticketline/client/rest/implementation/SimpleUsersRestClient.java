package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.UsersRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.util.List;

@Component
public class SimpleUsersRestClient implements UsersRestClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleUsersRestClient.class);
    private static final String USERS_URL = "/users";

    private final RestClient restClient;

    public SimpleUsersRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    @Override
    public UserDTO createUser(UserDTO dto) throws DataAccessException {
        try {
            LOGGER.debug("Creating user {}", dto);
            ResponseEntity<UserDTO> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(USERS_URL),
                HttpMethod.POST,
                new HttpEntity<>(dto),
                new ParameterizedTypeReference<UserDTO>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.user.create",e);
        }
    }

    @Override
    public List<UserDTO> findAll() throws DataAccessException {
        try {
            LOGGER.debug("Finding all users");
            ResponseEntity<List<UserDTO>> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(USERS_URL),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<UserDTO>>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.user.find",e);
        }
    }

    @Override
    public UserDTO updateUser(UserDTO dto) throws DataAccessException {
        try {
            LOGGER.debug("Update user {}", dto);
            ResponseEntity<UserDTO> response = restClient.parseExceptionIfPossibleExchange(
                restClient.getServiceURI(USERS_URL + "/" + dto.getId()),
                HttpMethod.PATCH,
                new HttpEntity<>(dto),
                new ParameterizedTypeReference<UserDTO>() {
                }
            );
            LOGGER.debug("Result status was {} with content {}",
                response.getStatusCode(), response.getBody());
            return response.getBody();
        } catch (RestClientException e) {
            LOGGER.error("Error: {}", e.getMessage());
            throw new DataAccessException("exception.user.update",e);
        }
    }
}
