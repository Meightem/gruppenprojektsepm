package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import javafx.beans.binding.Bindings;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("prototype")
public class LocationElementController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocationElementController.class);

    @FXML
    private Node mainNode;

    @FXML
    private Label lblName;
    @FXML
    private Label lblCityPostcode;
    @FXML
    private Label lblStreet;
    @FXML
    private Label lblCountry;

    private Property<HallDTO> location;

    @Autowired
    public LocationElementController() {
        location = new SimpleObjectProperty<HallDTO>();
    }

    @FXML
    private void initialize() {

    }

    public void setLocation(HallDTO location) {
        this.location.setValue(location);
        initTextBindings();
    }

    private void initTextBindings() {
        lblName.textProperty().bind(Bindings.selectString(location, "name"));
        lblCityPostcode.textProperty().bind(
            Bindings.concat(
                Bindings.selectString(location, "city"),
                ", ",
                Bindings.selectString(location, "postcode")
            ));
        lblStreet.textProperty().bind(Bindings.selectString(location, "street"));
        lblCountry.textProperty().bind(Bindings.selectString(location, "country"));
    }
}
