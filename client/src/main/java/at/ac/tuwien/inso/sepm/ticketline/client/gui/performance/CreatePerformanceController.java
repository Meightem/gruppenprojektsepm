package at.ac.tuwien.inso.sepm.ticketline.client.gui.performance;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.HallService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PerformanceService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.client.util.TextCompleter;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import jfxtras.scene.control.LocalDateTimeTextField;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;

@Component
public class CreatePerformanceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreatePerformanceController.class);

    @FXML
    public Label lblDate;
    @FXML
    public LocalDateTimeTextField inputDate;
    @FXML
    public Label lblPrice;
    @FXML
    public TextField txtPrice;
    @FXML
    public Label lblHall;
    @FXML
    public TextField txtHall;

    @FXML
    private TabHeaderController headerController;

    private final MainController mainController;
    private final SpringFxmlLoader springFxmlLoader;
    private PerformanceService performanceService;
    private HallService hallService;

    @FXML
    private Button btnCancel;
    @FXML
    private Button btnSubmit;
    private EventDTO eventDTO;
    private TextCompleter<HallDTO> hallCompleterBinding;

    public CreatePerformanceController(MainController mainController, SpringFxmlLoader springFxmlLoader, PerformanceService performanceService, HallService hallService) {
        this.mainController = mainController;
        this.springFxmlLoader = springFxmlLoader;
        this.performanceService = performanceService;
        this.hallService = hallService;
    }


    private void updateView() {
        headerController.setIcon(FontAwesome.Glyph.CALENDAR);
        initializeTextBindings();
        initHallCompleter();
    }

    private void initHallCompleter() {
        hallCompleterBinding  = new TextCompleter<HallDTO>(
            txtHall,
            (suggestion) -> {
                if (suggestion.getUserText().isEmpty()) return Collections.emptyList();
                try {
                    return hallService.search(suggestion.getUserText());
                } catch (DataAccessException e) {
                    return Collections.emptyList();
                }
            },
            new StringConverter<>() {
                @Override
                public String toString(HallDTO object) {
                    return object.getName();
                }

                @Override
                public HallDTO fromString(String string) {
                    try {
                        return hallService.search(string).get(0);
                    } catch (DataAccessException e) {
                        return null;
                    }
                }
            }
        );
    }

    @FXML
    private void initialize() {
        updateView();
    }

    private void initializeTextBindings() {
        headerController.setTitleBinding(BundleManager.getStringBinding("perf.create"));
        lblDate.textProperty().bind(BundleManager.getStringBinding("perf.date"));
        lblPrice.textProperty().bind(BundleManager.getStringBinding("perf.price"));
        lblHall.textProperty().bind(BundleManager.getStringBinding("perf.hall"));
        btnCancel.textProperty().bind(BundleManager.getStringBinding("general.cancel"));
        btnSubmit.textProperty().bind(BundleManager.getStringBinding("general.submit"));
    }

    public void setEvent(EventDTO event) {
        this.eventDTO = event;
        updateView();
    }

    public void submit() {
        LOGGER.info("Clicked on save performance.");
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws DataAccessException {
                BigDecimal price;
                try {
                    price = new BigDecimal(txtPrice.getText()
                        .replaceAll(" ", "")
                        .replaceAll(",", "."));
                } catch (NumberFormatException e) {
                    throw new DataAccessException("exception.performance.create.price",e);
                }

                HallDTO hall = hallCompleterBinding.getValue();

                if (hall == null || hall.getId() == null) {
                    throw new DataAccessException("exception.performance.create.hall",new IllegalArgumentException());
                }

                LocalDateTime date = inputDate.getLocalDateTime();
                PerformanceDTO performance = PerformanceDTO.newBuilder()
                    .setDate(date)
                    .setPrice(price)
                    .setEvent(eventDTO)
                    .setHall(hall)
                    .build();

                performanceService.create(performance);
                return null; // We need to return an instance of Void, just return null.
                // Is `(Void) null` more void than Void?
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: created performance.");
                ((Stage) inputDate.getScene().getWindow()).close();
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: performances was not created. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    inputDate.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );

        new Thread(task).start();
    }

    @FXML
    private void cancel() {
        LOGGER.info("Clicked on cancel create performances");
        ((Stage) inputDate.getScene().getWindow()).close();
    }
}
