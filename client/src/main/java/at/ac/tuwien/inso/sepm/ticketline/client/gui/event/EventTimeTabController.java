package at.ac.tuwien.inso.sepm.ticketline.client.gui.event;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MainController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.performance.PerformanceListController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.HallService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PerformanceService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.client.util.TextCompleter;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import jfxtras.scene.control.LocalDateTimeTextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Controller
public class EventTimeTabController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventTimeTabController.class);

    @FXML
    private TextField tfHall;
    @FXML
    private TextField tfEvent;
    @FXML
    private LocalDateTimeTextField dpDate;
    @FXML
    private Button eventTimeTabSearchBtn;
    @FXML
    private TextField tfPrice;

    @FXML
    private Label lblDate;
    @FXML
    private Label lblPrice;
    @FXML
    private Label lblEvent;
    @FXML
    private Label lblHall;

    @FXML
    private PerformanceListController eventTimeTabListController;

    private final PerformanceService performanceService;
    private final EventService eventService;
    private final MainController mainController;
    private final HallService hallService;

    private TextCompleter<HallDTO> hallCompletionBinding;
    private TextCompleter<EventDTO> eventCompletionBinding;

    public EventTimeTabController(HallService hallService, MainController mainController, EventService eventService, PerformanceService performanceService){
        this.hallService = hallService;
        this.eventService = eventService;
        this.mainController = mainController;
        this.performanceService = performanceService;
    }

    @FXML
    public void initialize() {
        initializeTextBindings();
        initHallCompleter();
        initEventCompleter();
        loadPerformances();
    }

    private void initializeTextBindings() {
        lblDate.textProperty().bind(BundleManager.getStringBinding("perf.search.date"));
        dpDate.promptTextProperty().bind(BundleManager.getStringBinding("perf.search.date"));
        lblPrice.textProperty().bind(BundleManager.getStringBinding("perf.search.price"));
        tfPrice.promptTextProperty().bind(BundleManager.getStringBinding("perf.search.price"));
        lblEvent.textProperty().bind(BundleManager.getStringBinding("perf.search.event"));
        tfEvent.promptTextProperty().bind(BundleManager.getStringBinding("perf.search.event"));
        lblHall.textProperty().bind(BundleManager.getStringBinding("perf.search.hall"));
        tfHall.promptTextProperty().bind(BundleManager.getStringBinding("perf.search.hall"));
        eventTimeTabSearchBtn.textProperty().bind(BundleManager.getStringBinding("perf.search"));
    }

    private void initHallCompleter() {
        hallCompletionBinding = new TextCompleter<>(
            tfHall,
            (suggestion) -> {
                if (suggestion.getUserText().isEmpty()) return Collections.emptyList();
                try {
                    return hallService.search(suggestion.getUserText());
                } catch (DataAccessException e) {
                    return Collections.emptyList();
                }
            },
            new StringConverter<>() {
                @Override
                public String toString(HallDTO object) {
                    return object.getName();
                }

                @Override
                public HallDTO fromString(String text) {
                    try {
                        return hallService.search(text).get(0);
                    } catch (DataAccessException e) {
                        return null;
                    }
                }
            }
        );
    }

    private void initEventCompleter() {
        eventCompletionBinding = new TextCompleter<>(
            tfEvent,
            (suggestion) -> {
                if (suggestion.getUserText().isEmpty()) return Collections.emptyList();
                try {
                    return eventService.searchEvent(suggestion.getUserText(), null, null, null);
                } catch (DataAccessException e) {
                    return Collections.emptyList();
                }
            },
            new StringConverter<>() {
                @Override
                public String toString(EventDTO object) {
                    return object.getName();
                }

                @Override
                public EventDTO fromString(String string) {
                    try {
                        return eventService.searchEvent(string, null, null, null).get(0);
                    } catch (DataAccessException e) {
                        return null;
                    }
                }
            }
        );
    }

    @FXML
    public void loadPerformances() {
        LOGGER.debug("Loading performances");
        Task<List<PerformanceDTO>> task = new Task<List<PerformanceDTO>>() {
            @Override
            protected List<PerformanceDTO> call() throws DataAccessException {
                BigDecimal price = null;
                if(!tfPrice.getText().isEmpty()) {
                    try {
                        price = new BigDecimal(tfPrice.getText()
                            .replaceAll(" ", "")
                            .replaceAll(",", "."));
                    } catch (NumberFormatException e) {
                        throw new DataAccessException("exception.event.price");
                    }
                }

                return performanceService.search(PerformanceDTO.newBuilder()
                    .setDate(dpDate.getLocalDateTime())
                    .setPrice(price)
                    .setEvent(eventCompletionBinding.getValue())
                    .setHall(hallCompletionBinding.getValue())
                    .build());
            }

            @Override
            protected void succeeded() {
                LOGGER.debug("Succeeded: loaded performances.");
                eventTimeTabListController.getPerformanceObservableList().setAll(this.getValue());
            }

            @Override
            protected void failed() {
                super.failed();
                LOGGER.error("Failed: loading performances. ({}) ", getException().getMessage());
                JavaFXUtils.createExceptionDialog(getException(),
                    eventTimeTabSearchBtn.getScene().getWindow()).showAndWait();
            }
        };

        task.runningProperty().addListener((observable, oldValue, running) ->
            mainController.setProgressbarProgress(
                running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
        );
        new Thread(task).start();
    }
}
