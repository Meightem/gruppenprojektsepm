package at.ac.tuwien.inso.sepm.ticketline.client.gui.admin;

import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.text.TextAlignment;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Controller
@Scope("prototype")
public class UserElementController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserElementController.class);

    @FXML
    private Node mainNode;

    @FXML
    private Label lblUserID;

    @FXML
    private Label lblUsername;

    @FXML
    private Label lblAdminIcon;

    @FXML
    private Label lblLockedIcon;

    private UserDTO user;

    private final FontAwesome fontAwesome;

    @Autowired
    public UserElementController(FontAwesome fontAwesome) {
        this.fontAwesome = fontAwesome;
    }

    @FXML
    private void initialize() {
        updateView();
    }

    public void setUser(UserDTO user) {
        this.user = user;
        updateView();
    }

    private void updateView() {
        if (user == null) return;
        lblUserID.setText("#" + user.getId());
        lblUsername.setText(user.getUsername());
        if(user.getAdmin()) {
            lblAdminIcon.setGraphic(
                fontAwesome
                    .create(FontAwesome.Glyph.USER_SECRET)
                    .size(lblAdminIcon.getFont().getSize()));
            lblAdminIcon.setAlignment(Pos.CENTER_LEFT);
            lblAdminIcon.setPadding(new Insets(3.5,0,0,0));
        }
        lblLockedIcon.setGraphic(
            fontAwesome
                .create(user.getLocked()?FontAwesome.Glyph.LOCK:FontAwesome.Glyph.UNLOCK)
                .size(lblLockedIcon.getFont().getSize()));
        lblLockedIcon.setAlignment(Pos.CENTER_LEFT);
        lblLockedIcon.setPadding(new Insets(3.5,0,0,0));
    }
}
