package at.ac.tuwien.inso.sepm.ticketline.client.systemtest;

import at.ac.tuwien.inso.sepm.ticketline.client.gui.hall.SeatControl;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.ticket.TicketSellController;
import at.ac.tuwien.inso.sepm.ticketline.client.systemtest.base.EventBase;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.ticket.TicketDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;


public class TicketSellingTest extends EventBase {

    private ListCell<UserDTO> getCellFromListView(PerformanceDTO performance) {
        return lookup(".PerformanceRow").match(node ->
            {
                if (!(node instanceof ListCell)) return false;
                ListCell cell = (ListCell) node;
                if (cell.getItem() == null) return false;
                return ((PerformanceDTO) cell.getItem()).equals(performance);
            }
        ).queryAs(ListCell.class);
    }

    private ListCell<TicketDTO> getTicketFromListView(String customerName) {
        return lookup(".TicketRow").match(node ->
        {
            if (!(node instanceof ListCell)) return false;
            ListCell cell = (ListCell) node;
            if (cell.getItem() == null) return false;
            return ((TicketDTO) cell.getItem()).getCustomer().getFirstName().equals(customerName);
        }).queryAs(ListCell.class);
    }

    @Test
    public void sellTicketWorksWithSectors() {
        beAdmin();
        EventDTO eventDTO = createEvent(buildRandomEvent().type(EventType.SECTORS).build());
        PerformanceDTO performanceDTO = randomPerformance(eventDTO, randomHall());
        CustomerDTO customerDTO = randomCustomer();
        insideEvent(eventDTO);
        try {
            rightClickOn(getCellFromListView(performanceDTO)).moveBy(1, 1);
            waitAndClickOn("#sellTicket");
            waitFor(".cbSelectedTickets");
            lookup(".cbSelectedTickets").queryAllAs(ComboBox.class).forEach(sector -> {
                sector.getSelectionModel().select(1);
            });
            clickOn("#txtCustomerId").write(customerDTO.getFirstName());
            waitAndClickOn(TicketSellController.stringifyCustomer(customerDTO));
            clickOn("#btnBuy");
            acceptConfirmationDialog();
            assertNoErrors();
            closeInformationDialog();
            assertNoErrors();
            // go back into performance, check that no seats are free
            // ASSUMPTION: only one seat per sector
            rightClickOn(getCellFromListView(performanceDTO)).moveBy(1, 1);
            waitAndClickOn("#sellTicket");

            waitFor(".cbSelectedTickets");
            Assert.assertFalse(lookup(".cbSelectedTickets").queryAllAs(ComboBox.class).stream()
                .anyMatch(sector -> sector.getItems().size() > 1));
        } finally {
            closeCurrentWindow();
            closeCurrentWindow();
        }
    }

    @Test
    public void sellReservedTicket() {
        beAdmin();
        EventDTO eventDTO = createEvent(buildRandomEvent().type(EventType.SECTORS).build());
        PerformanceDTO performanceDTO = randomPerformance(eventDTO, randomHall());
        CustomerDTO customerDTO = randomCustomer();
        insideEvent(eventDTO);
        rightClickOn(getCellFromListView(performanceDTO)).moveBy(1, 1);
        waitAndClickOn("#sellTicket");
        waitFor(".cbSelectedTickets");
        lookup(".cbSelectedTickets").queryAllAs(ComboBox.class).forEach(sector -> {
            sector.getSelectionModel().select(1);
        });
        clickOn("#txtCustomerId").write(customerDTO.getFirstName());
        waitAndClickOn(TicketSellController.stringifyCustomer(customerDTO));
        clickOn("#btnReserve");
        acceptConfirmationDialog();
        assertNoErrors();
        closeInformationDialog();
        assertNoErrors();
        closeCurrentWindow();
        // ticket is reserved now

        waitAndClickOn("#ticketTab");
        await().atMost(5, TimeUnit.SECONDS).until(() -> getTicketFromListView(customerDTO.getFirstName()) != null);
        rightClickOn(getTicketFromListView(customerDTO.getFirstName())).moveBy(1, 1);
        waitAndClickOn("#buyReservedMenuItem");
        waitAndClickOn("#btnBuy");
        assertNoErrors();
    }

    @Test
    public void sellTicketWorksWithSeats() {
        beAdmin();
        EventDTO eventDTO = createEvent(buildRandomEvent().type(EventType.SEATS).build());
        PerformanceDTO performanceDTO = randomPerformance(eventDTO, randomHall());
        CustomerDTO customerDTO = randomCustomer();
        insideEvent(eventDTO);
        try {
            rightClickOn(getCellFromListView(performanceDTO)).moveBy(1, 1);
            waitAndClickOn("#sellTicket");
            waitFor(".seat");
            lookup(".seat").queryAllAs(SeatControl.class).forEach(seat -> {
                seat.getSelectedProperty().setValue(true);
            });

            // create new customer
            waitAndClickOn("#btnCreateCustomer2");
            setText("#txtFirstName", "Hans");
            setText("#txtLastName", "Gans");
            clickOn("#btnSubmit");
            closeInformationDialog();
            await().atMost(5, TimeUnit.SECONDS).until(() -> lookup("#txtCustomerId").queryAs(TextField.class).getText().contains("Hans"));
            clickOn("#btnBuy");
            acceptConfirmationDialog();
            assertNoErrors();
            closeInformationDialog();
            assertNoErrors();

            // go back into performance, check that no seats are free
            rightClickOn(getCellFromListView(performanceDTO)).moveBy(1, 1);
            waitAndClickOn("#sellTicket");

            waitFor(".seat");
            Assert.assertFalse(lookup(".seat").queryAllAs(SeatControl.class).stream().anyMatch(seat -> seat.isSelected()));
        } finally {
            closeCurrentWindow();
            closeCurrentWindow();
        }
    }

    @Test
    public void reserveTicketWorks() {
        beAdmin();
        EventDTO eventDTO = createEvent(buildRandomEvent().type(EventType.SECTORS).build());
        PerformanceDTO performanceDTO = randomPerformance(eventDTO, randomHall());
        CustomerDTO customerDTO = randomCustomer();
        insideEvent(eventDTO);
        try {
            rightClickOn(getCellFromListView(performanceDTO)).moveBy(1, 1);
            waitAndClickOn("#sellTicket");
            waitFor(".cbSelectedTickets");
            lookup(".cbSelectedTickets").queryAllAs(ComboBox.class).forEach(sector -> {
                sector.getSelectionModel().select(1);
            });
            clickOn("#txtCustomerId").write(customerDTO.getFirstName());
            waitAndClickOn(TicketSellController.stringifyCustomer(customerDTO));
            clickOn("#btnReserve");
            sleep(200);
            acceptConfirmationDialog();
            closeInformationDialog();
            assertNoErrors();

            // go back into performance, check that no seats are free
            // ASSUMPTION: only one seat per sector
            rightClickOn(getCellFromListView(performanceDTO)).moveBy(1, 1);
            waitAndClickOn("#sellTicket");

            waitFor(".cbSelectedTickets");
            Assert.assertFalse(lookup(".cbSelectedTickets").queryAllAs(ComboBox.class).stream()
                .anyMatch(sector -> sector.getItems().size() > 1));
        } finally {
            closeCurrentWindow();
            closeCurrentWindow();
        }
    }

    @Test
    public void sellTicketWorksWithAnonymousCustomer() {
        beAdmin();
        EventDTO eventDTO = createEvent(buildRandomEvent().type(EventType.SECTORS).build());
        PerformanceDTO performanceDTO = randomPerformance(eventDTO, randomHall());
        CustomerDTO customerDTO = randomCustomer();
        insideEvent(eventDTO);
        try {
            rightClickOn(getCellFromListView(performanceDTO)).moveBy(1, 1);
            waitAndClickOn("#sellTicket");
            waitFor(".cbSelectedTickets");
            lookup(".cbSelectedTickets").queryAllAs(ComboBox.class).forEach(sector -> {
                sector.getSelectionModel().select(1);
            });

            waitAndClickOn("#btnBuy");
            acceptConfirmationDialog();
            assertNoErrors();
            closeInformationDialog();
            assertNoErrors();

            // go back into performance, check that no seats are free
            // ASSUMPTION: only one seat per sector
            rightClickOn(getCellFromListView(performanceDTO)).moveBy(1, 1);
            waitAndClickOn("#sellTicket");

            waitFor(".cbSelectedTickets");
            Assert.assertFalse(lookup(".cbSelectedTickets").queryAllAs(ComboBox.class).stream()
                .anyMatch(sector -> sector.getItems().size() > 1));
        } finally {
            closeCurrentWindow();
            closeCurrentWindow();
        }
    }
}
