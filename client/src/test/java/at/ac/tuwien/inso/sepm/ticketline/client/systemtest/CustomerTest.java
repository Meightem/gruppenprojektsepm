package at.ac.tuwien.inso.sepm.ticketline.client.systemtest;

import at.ac.tuwien.inso.sepm.ticketline.client.systemtest.base.TestBase;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import javafx.scene.control.ListCell;
import org.junit.Test;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertFalse;

public class CustomerTest extends TestBase {

    private boolean customerVisible(String firstName) {
        return getCustomerFromListView(firstName) != null;
    }

    private void onCustomersTab() {
        beAdmin();
        waitAndClickOn("#customerTab");
    }

    private void createCustomer(String firstName, String lastName) {
        clickOn("#btnCreateCustomer");
        setText("#txtFirstName", firstName);
        setText("#txtLastName", lastName);
        clickOn("#btnSubmit");
    }

    private void searchFor(String query) {
        setText("#txtSearch", query);
        clickOn("#btnSearch");
    }

    private CustomerDTO getCustomerFromListView(String firstName) {
        ListCell<CustomerDTO> cellFromListView = getCellFromListView(firstName);
        if (cellFromListView == null) return null;
        return cellFromListView.getItem();
    }

    private ListCell<CustomerDTO> getCellFromListView(String firstName) {
        return lookup(".CustomerRow").match(node ->
            {
                if (!(node instanceof ListCell)) return false;
                ListCell cell = (ListCell) node;
                if (cell.getItem() == null) return false;
                return ((CustomerDTO) cell.getItem()).getFirstName().equals(firstName);
            }
        ).queryAs(ListCell.class);
    }

    @Test
    public void createCustomerWorks() {
        onCustomersTab();
        createCustomer("Hans", "Gans");
        closeInformationDialog();
        await().atMost(5, SECONDS).until(() -> customerVisible("Hans"));
        assertNoErrors();
    }

    @Test
    public void createCustomerRejectsEmptyFields() {
        onCustomersTab();
        createCustomer("Hans", "");
        closeErrorDialog();
        closeCurrentWindow();
        assertFalse(customerVisible("Hans"));
    }

    @Test
    public void searchCustomerFindsExactCustomer() {
        onCustomersTab();
        createCustomer("Hans", "Gans");
        closeInformationDialog();
        await().atMost(5, SECONDS).until(() -> customerVisible("Hans"));
        createCustomer("Peter", "Pilz");
        closeInformationDialog();
        await().atMost(5, SECONDS).until(() -> customerVisible("Peter"));

        searchFor("Hans");
        await().atMost(5, SECONDS).until(() -> customerVisible("Hans")&&!customerVisible("Peter"));
        assertFalse(customerVisible("Peter"));

        clickOn("#btnClear");
        await().atMost(5, SECONDS).until(() -> customerVisible("Hans"));
        await().atMost(5, SECONDS).until(() -> customerVisible("Peter"));

        searchFor("");
        await().atMost(5, SECONDS).until(() -> customerVisible("Hans"));
        await().atMost(5, SECONDS).until(() -> customerVisible("Peter"));
    }

    @Test
    public void editingCustomerWorks() {
        onCustomersTab();
        createCustomer("Hans", "Gans");
        closeInformationDialog();
        await().atMost(5, SECONDS).until(() -> customerVisible("Hans"));
        rightClickOn(getCellFromListView("Hans")).moveBy(1, 1);
        waitAndClickOn("#menuItemEdit");

        waitFor("#txtFirstName");
        setText("#txtFirstName", "Franz");
        clickOn("#btnSubmit");
        closeInformationDialog();

        await().atMost(5, SECONDS).until(() -> customerVisible("Franz"));
    }

    @Test
    public void editingCustomerRejectsEmptyFields() {
        onCustomersTab();
        createCustomer("Hans", "Gans");
        closeInformationDialog();
        await().atMost(5, SECONDS).until(() -> customerVisible("Hans"));
        rightClickOn(getCellFromListView("Hans")).moveBy(1, 1);
        waitAndClickOn("#menuItemEdit");


        clickOn("#txtFirstName");
        setText("#txtFirstName", "");
        clickOn("#btnSubmit");

        closeErrorDialog();
        closeCurrentWindow();
        await().atMost(5, SECONDS).until(() -> customerVisible("Hans"));
    }
}
