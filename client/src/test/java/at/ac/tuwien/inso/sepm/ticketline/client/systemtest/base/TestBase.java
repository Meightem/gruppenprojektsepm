package at.ac.tuwien.inso.sepm.ticketline.client.systemtest.base;

import at.ac.tuwien.inso.sepm.ticketline.client.TicketlineClientApplication;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.service.AuthenticationInformationService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.AuthenticationService;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationRequest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import javafx.application.Application;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.http.HttpStatus;
import org.testfx.api.FxRobotInterface;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestBase extends ApplicationTest {
    private TicketlineClientApplication application;

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.setProperty("java.awt.headless", "false");
        RestAssured.baseURI = "http://localhost";
        RestAssured.basePath = "/";
        RestAssured.port = 8080;
    }

    /**
     * Reset all state in the server (delete all entities, generate standard testusers)
     */
    public void resetServer() {
        Response response = RestAssured
            .given()
            .when().post("/testhelper/reset")
            .then().extract().response();
        Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
    }

    @Before
    public void setUp() throws Exception {
        resetServer();
        FxToolkit.registerPrimaryStage();
        application = (TicketlineClientApplication) FxToolkit.setupApplication(TicketlineClientApplication.class);
    }

    protected <T> T getBean(Class<T> bean) {
        return application.applicationContext.getBean(bean);
    }

    @Override
    public void start(Stage stage) {
        stage.show();
    }

    /**
     * Assuming you're on the login page, log in with given username and password
     * <p>
     * Does no verification the login worked
     *
     * @param username The username to login with
     * @param password The password to try
     */
    protected void logIn(String username, String password) {
        setText("#txtUsername", username);
        setText("#txtPassword", password);
        waitAndClickOn("#btnAuthenticate");
    }

    protected void fastLogIn(String username, String password) {
        try {
            getBean(AuthenticationService.class)
                .authenticate(AuthenticationRequest.builder()
                    .username(username)
                    .password(password)
                    .build());
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Wait for an element to appear.
     *
     * @param query The query to use.
     */
    protected void waitFor(String query) {
        await().atMost(5, SECONDS).until(() -> !lookup(query).queryAll().isEmpty());
    }

    /**
     * A very fast way to type. `setText(query, input)` does almost the same thing as
     * `waitAndClickOn(query).write(input)`, except that it does not work with event handlers such as keyPress, keyEntered, etc.
     *
     * @param query A `TextInputControl` (`TextArea` or `TextField`) is expected to be found with this.
     * @param input The text to call `TextInputControl::setText` with
     */
    protected void setText(String query, String input) {
        waitFor(query);
        lookup(query).queryAs(TextInputControl.class).setText(input);
    }

    protected FxRobotInterface waitAndClickOn(String query) {
        waitFor(query);
        return clickOn(query);
    }

    /**
     * Log in as admin
     */
    protected void beAdmin() {
        fastLogIn("admin", "password");
    }

    /**
     * Close the current error dialog/alert. Asserts that an error dialog is currently focused.
     */
    protected void closeErrorDialog() {
        await().atMost(5, SECONDS).until(() -> !lookup(".error").queryAll().isEmpty());
        closeCurrentWindow();
        assertNoErrors();
    }

    protected void acceptConfirmationDialog() {
        await().atMost(5, SECONDS).until(() -> !lookup(".confirmation").queryAll().isEmpty());
        push(KeyCode.ENTER);
        assertNoErrors();
    }

    protected void closeInformationDialog() {
        await().atMost(5, SECONDS).until(() -> !lookup(".information").queryAll().isEmpty());
        push(KeyCode.ENTER);
        assertNoErrors();
    }

    /**
     * Assert that no error dialog is shown.
     */
    protected void assertNoErrors() {
        await().atMost(5, SECONDS).until(() -> lookup(".error").queryAll().isEmpty());
    }
}
