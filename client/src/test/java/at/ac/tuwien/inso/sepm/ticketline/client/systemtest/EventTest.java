package at.ac.tuwien.inso.sepm.ticketline.client.systemtest;

import at.ac.tuwien.inso.sepm.ticketline.client.systemtest.base.EventBase;
import at.ac.tuwien.inso.sepm.ticketline.client.systemtest.base.TestBase;
import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import javafx.scene.control.ListCell;
import javafx.scene.input.KeyCode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class EventTest extends EventBase {

    private void onEventsTab() {
        beAdmin();
        waitAndClickOn("#eventTab");
        waitAndClickOn("#tabEvents");
    }

    private void createEvent(String name, String length) {
        clickOn("#btnCreateEvent");

        waitFor("#txtArtistFN");
        setText("#txtArtistFN", "My Artist FN");
        setText("#txtArtistLN", "My Artist LN");
        setText("#txtName", name);
        setText("#txtContent", "My content");
        setText("#txtLength", length);

        clickOn("#btnSubmit");
    }


    @Test
    public void createEventWorks() {
        onEventsTab();
        createEvent("my event", "69420");
        await().atMost(5, SECONDS).until(() -> eventVisible("my event"));
        assertNoErrors();
        assertTrue(eventVisible("my event"));
    }

    @Test
    public void createEventRejectsInvalidFields() {
        onEventsTab();
        try {
            createEvent("my event", "ayyy");
            closeErrorDialog();
            assertNoErrors();

            setText("#txtLength", "");
            clickOn("#btnSubmit");
            closeErrorDialog();
            assertNoErrors();
        } finally {
            closeCurrentWindow();
        }
    }

    @Test
    public void searchEvents(){
        onEventsTab();

        createEvent("Test Event", "2");

        setText("#tfName", "Te");
        clickOn("#eventEventTabSearchBtn");
        await().atMost(5, SECONDS).until(() -> eventVisible("Test Event"));
        assertTrue(eventVisible("Test Event"));

        // empty search gives all events back
        setText("#tfName", "");
        clickOn("#eventEventTabSearchBtn");
        await().atMost(5, SECONDS).until(() -> eventVisible("Test Event"));
        assertTrue(eventVisible("Test Event"));
    }
}
