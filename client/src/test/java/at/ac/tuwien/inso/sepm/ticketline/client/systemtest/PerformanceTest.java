package at.ac.tuwien.inso.sepm.ticketline.client.systemtest;

import at.ac.tuwien.inso.sepm.ticketline.client.service.HallService;
import at.ac.tuwien.inso.sepm.ticketline.client.systemtest.base.EventBase;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import javafx.scene.control.ListCell;
import javafx.scene.input.KeyCode;
import jfxtras.scene.control.LocalDateTimeTextField;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

public class PerformanceTest extends EventBase {


    protected PerformanceDTO getPerfFromListView(Long hallId) {
        ListCell<PerformanceDTO> cellFromListView = getPerfCellFromListView(hallId);
        if (cellFromListView == null) return null;
        return cellFromListView.getItem();
    }

    protected ListCell<PerformanceDTO> getPerfCellFromListView(Long hallId) {
        return lookup(".PerformanceRow").match(node ->
            {
                if (!(node instanceof ListCell)) return false;
                ListCell cell = (ListCell) node;
                if (cell.getItem() == null) return false;
                return ((PerformanceDTO) cell.getItem()).getHall().getId().equals(hallId);
            }
        ).queryAs(ListCell.class);
    }


    private boolean performanceVisible(Long hallId) {
        return getPerfFromListView(hallId) != null;
    }


    @Test
    public void createPerformanceWorks() throws Exception {
        beAdmin();
        insideEvent();

        try {
            waitAndClickOn("#btnCreatePerformance");
            waitFor("#inputDate");
            lookup("#inputDate").queryAs(LocalDateTimeTextField.class).setLocalDateTime(LocalDateTime.now());
            setText("#txtPrice", "69");
            clickOn("#txtHall").write("Ernst");
            waitAndClickOn(".auto-complete-popup");
            await().atMost(5, TimeUnit.SECONDS).until(() ->{
                clickOn(".auto-complete-popup");
                return lookup(".auto-complete-popup").queryAll().isEmpty();
            });
            waitAndClickOn("#btnSubmit");
            Long hallId = getBean(HallService.class).search("Ernst").get(0).getId();
            await().atMost(5, TimeUnit.SECONDS).until(() -> performanceVisible(hallId));
        } finally {
            closeCurrentWindow();
        }
    }


    @Test
    public void createPerformanceRejectsInvalidFields() {
        beAdmin();
        insideEvent();

        try {
            waitAndClickOn("#btnCreatePerformance");
            waitFor("#inputDate");
            lookup("#inputDate").queryAs(LocalDateTimeTextField.class).setLocalDateTime(LocalDateTime.now());
            setText("#txtPrice", "kkk");
            clickOn("#txtHall").write("Ernst");
            waitAndClickOn(".auto-complete-popup");
            await().atMost(5, TimeUnit.SECONDS).until(() ->{
                clickOn(".auto-complete-popup");
                return lookup(".auto-complete-popup").queryAll().isEmpty();
            });
            waitAndClickOn("#btnSubmit");
            closeErrorDialog();
            assertNoErrors();

            setText("#txtPrice", "");
            waitAndClickOn("#btnSubmit");
            closeErrorDialog();
            assertNoErrors();

            lookup("#inputDate").queryAs(LocalDateTimeTextField.class).setLocalDateTime(null);
            setText("#txtPrice", "69");
            waitAndClickOn("#btnSubmit");
            closeErrorDialog();
            assertNoErrors();
        } finally {
            closeCurrentWindow();
            closeCurrentWindow();
        }
    }
}
