package at.ac.tuwien.inso.sepm.ticketline.client.systemtest;

import at.ac.tuwien.inso.sepm.ticketline.client.gui.news.NewsController;
import at.ac.tuwien.inso.sepm.ticketline.client.systemtest.base.TestBase;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.File;
import java.nio.file.Files;
import java.util.Collections;

import static io.restassured.RestAssured.when;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class NewsTest extends TestBase {

    private ListCell<SimpleNewsDTO> lookForNews(String title) {
        return lookup(".NewsRow").match(node ->
            {
                if (!(node instanceof ListCell)) return false;
                ListCell cell = (ListCell) node;
                if (cell.getItem() == null) return false;
                return ((SimpleNewsDTO) cell.getItem()).getTitle().equals(title);
            }
        ).queryAs(ListCell.class);
    }

    private void unfilterNews() {
        CheckBox cb = lookup("#cbShowReadNews").queryAs(CheckBox.class);
        if (!cb.isSelected()) {
            waitAndClickOn("#cbShowReadNews");
        }
    }

    private boolean newsVisible(String title) {
        return lookForNews(title) != null;
    }

    private void createNews(String title, String text) {
        clickOn("#btnCreateNews");
        waitAndClickOn("#tfTitle");
        setText("#tfTitle", title);
        setText("#tfText", text);
        clickOn("#btnSave");
    }

    @Test
    public void testCreateNews() {
        beAdmin();
        waitAndClickOn("#newsTab");
        createNews("My Title", StringUtils.repeat("My Text", 100));
        await().atMost(5, SECONDS).until(() -> newsVisible("My Title"));
        assertNoErrors();
        clickOn(lookForNews("My Title"));
        assertThat(lookup("#detailedNewsHeader").lookup("#lblHeaderTitle").queryAs(Label.class).getText(), is("My Title"));
        assertThat(lookup("#txtText").queryAs(Text.class).getText().length(), is(700));
        assertThat(lookup("#image").queryAs(ImageView.class).getImage(), nullValue());
    }

    @Test
    public void createNewsRejectsEmptyFields() {
        beAdmin();
        waitAndClickOn("#newsTab");
        createNews("My Title", "");
        closeErrorDialog();
        assertNoErrors();
        closeCurrentWindow();
    }

    @Test
    public void testReadNews() {
        beAdmin();
        waitAndClickOn("#newsTab");
        createNews("My Title", StringUtils.repeat("Lorem Ipsum Dolor Sit Amet", 50));
        await().atMost(5, SECONDS).until(() -> newsVisible("My Title"));

        clickOn(lookForNews("My Title"));
        await().atMost(5, SECONDS).until(() -> lookup("#detailedNewsHeader").lookup("#lblHeaderTitle").queryAs(Label.class)!=null);
        assertThat(lookup("#detailedNewsHeader").lookup("#lblHeaderTitle").queryAs(Label.class).getText(), is("My Title"));
        waitAndClickOn("#logoutTab");
        acceptConfirmationDialog();
        beAdmin();
        waitAndClickOn("#newsTab");
        unfilterNews();
        await().atMost(5, SECONDS).until(() -> newsVisible("My Title"));
    }

    @Test
    public void showOnlyUnreadAfterLogin() {
        beAdmin();
        waitAndClickOn("#newsTab");

        createNews("Title1", "text1");
        await().atMost(5, SECONDS).until(() -> newsVisible("Title1"));
        createNews("Title2", "text2");
        await().atMost(5, SECONDS).until(() -> newsVisible("Title2"));

        clickOn(lookForNews("Title1"));
        await().atMost(5, SECONDS).until(() -> lookup("#detailedNewsHeader").lookup("#lblHeaderTitle").queryAs(Label.class).getText().equals("Title1"));

        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < NewsController.TIMEOUTASREAD + 500){
            try {
                wait(100);
            } catch(Exception e){}
        }
        clickOn(lookup("#newsDetailedMainNode").lookup("#btnClose").queryButton());
        waitAndClickOn("#logoutTab");
        acceptConfirmationDialog();
        beAdmin();
        waitAndClickOn("#newsTab");

        assertThat(lookup("#lvNewsElements").queryAs(ListView.class).getItems().size(), is(1));
    }

    @Test
    public void showAllItemswithoutFiltering() {
        beAdmin();
        waitAndClickOn("#newsTab");

        createNews("Title1", "text1");
        await().atMost(5, SECONDS).until(() -> newsVisible("Title1"));

        createNews("Title2", "text2");
        await().atMost(5, SECONDS).until(() -> newsVisible("Title2"));

        clickOn(lookForNews("Title1"));
        await().atMost(5, SECONDS).until(() -> lookup("#detailedNewsHeader").lookup("#lblHeaderTitle").queryAs(Label.class).getText().equals("Title1"));

        waitAndClickOn("#btnMarkAsRead");
        await().atMost(5, SECONDS).until(() -> lookup("#lvNewsElements")
                .queryAs(ListView.class)
                .getItems()
                .size() == 1);
        unfilterNews();
        await().atMost(5, SECONDS).until(() -> lookup("#lvNewsElements")
                .queryAs(ListView.class)
                .getItems()
                .size() == 2);
    }

    @Test
    public void testShowDetailed() throws Exception{
        // test detailed view contextmenu Item
        beAdmin();
        waitAndClickOn("#newsTab");
        createNews("Title detail", "This is the detailed text.");
        await().atMost(5, SECONDS).until(() -> newsVisible("Title detail"));
        clickOn(lookForNews("Title detail"));
        // check if its correct detailed news
        assertThat(lookup("#detailedNewsHeader").lookup("#lblHeaderTitle").queryAs(Label.class).getText(), is("Title detail"));
        assertThat(lookup("#txtText").queryAs(Text.class).getText(), is("This is the detailed text."));
        waitAndClickOn("#btnClose");
    }

    @Test
    public void testContextMenuMarkAsRead(){
        // test markAsRead contextmenu Item
        beAdmin();
        waitAndClickOn("#newsTab");
        createNews("Title detail 1", "This is the detailed text. 1");
        await().atMost(5, SECONDS).until(() -> newsVisible("Title detail 1"));

        createNews("Title detail 2", "This is the detailed text. 2");
        await().atMost(5, SECONDS).until(() -> newsVisible("Title detail 2"));

        rightClickOn(lookForNews("Title detail 2")).moveBy(1,1);
        waitAndClickOn("#markAsRead");
        //news should be gone
        await().atMost(5, SECONDS).until(() -> !newsVisible("Title detail 2"));

        waitAndClickOn("#cbShowReadNews");
        // news should be there
        await().atMost(5, SECONDS).until(() -> newsVisible("Title detail 2"));
    }

}
