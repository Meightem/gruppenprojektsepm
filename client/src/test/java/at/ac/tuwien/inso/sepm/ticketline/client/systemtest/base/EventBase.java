package at.ac.tuwien.inso.sepm.ticketline.client.systemtest.base;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.event.EventEventTabController;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.HallRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.HallService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PerformanceService;
import at.ac.tuwien.inso.sepm.ticketline.rest.artist.ArtistDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventType;
import at.ac.tuwien.inso.sepm.ticketline.rest.hall.HallDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.performance.PerformanceDTO;
import javafx.scene.control.ListCell;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

public class EventBase extends TestBase {
    protected EventDTO getEventFromListView(String name) {
        ListCell<EventDTO> cellFromListView = getEventCellFromListView(name);
        if (cellFromListView == null) return null;
        return cellFromListView.getItem();
    }

    protected ListCell<EventDTO> getEventCellFromListView(String name) {
        return lookup(".EventRow").match(node ->
            {
                if (!(node instanceof ListCell)) return false;
                ListCell cell = (ListCell) node;
                if (cell.getItem() == null) return false;
                return ((EventDTO) cell.getItem()).getName().equals(name);
            }
        ).queryAs(ListCell.class);
    }

    protected boolean eventVisible(String firstName) {
        return getEventFromListView(firstName) != null;
    }

    protected EventDTO createEvent(EventDTO event) {
        try {
            EventDTO rv = getBean(EventService.class).create(event);
            getBean(EventEventTabController.class).loadEvents();
            return rv;
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        }
    }

    protected EventDTO randomEvent() {
        return createEvent(buildRandomEvent().build());
    }

    protected EventDTO.Builder buildRandomEvent() {
        return EventDTO.newBuilder()
            .artist(ArtistDTO.newBuilder().firstName("Hans").lastName("Gans").build())
            .name(String.format("Event%s", (int) Math.random() * 1000))
            .content(String.format("EventContent%s", (int) Math.random() * 1000))
            .length(Duration.ofHours((int) Math.random() * 3))
            .type(EventType.SEATS);
    }

    protected void insideEvent(EventDTO event) {
        waitAndClickOn("#eventTab");
        waitAndClickOn("#tabEvents");
        await().atMost(5, TimeUnit.SECONDS).until(() -> eventVisible(event.getName()));
        rightClickOn(getEventCellFromListView(event.getName())).moveBy(1, 1);
        waitAndClickOn("#showDetailedEvent");
    }

    protected PerformanceDTO randomPerformance(EventDTO event, HallDTO hall) {
        try {
            return getBean(PerformanceService.class).create(PerformanceDTO.newBuilder()
                .setHall(hall)
                .setDate(LocalDateTime.now().plusDays(7))
                .setPrice(BigDecimal.TEN.setScale(2))
                .setEvent(event)
                .build());
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        }
    }

    protected HallDTO randomHall() {
        try {
            return getBean(HallService.class).search("4020").get(0);
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        }
    }

    protected CustomerDTO randomCustomer() {
        try {
            return getBean(CustomerService.class).create(CustomerDTO.newBuilder()
                .firstName("Hans")
                .lastName("Gans")
                .isAnonymousCustomer(false)
                .build());
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        }
    }

    protected void insideEvent() {
        insideEvent(randomEvent());
    }
}
