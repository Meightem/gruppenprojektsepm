package at.ac.tuwien.inso.sepm.ticketline.client.systemtest;

import at.ac.tuwien.inso.sepm.ticketline.client.systemtest.base.TestBase;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import javafx.scene.control.ListCell;
import org.junit.Test;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertFalse;

public class AdminTest extends TestBase {
    private String generateUserName() {
        return String.format("User%s", Math.round(Math.random() * 10000000));
    }

    private UserDTO getUserFromListView(String username) {
        ListCell<UserDTO> cellFromListView = getCellFromListView(username);
        if (cellFromListView == null) return null;
        return cellFromListView.getItem();
    }

    private ListCell<UserDTO> getCellFromListView(String username) {
        return lookup(".UserRow").match(node ->
            {
                if (!(node instanceof ListCell)) return false;
                ListCell cell = (ListCell) node;
                if (cell.getItem() == null) return false;
                return ((UserDTO) cell.getItem()).getUsername().equals(username);
            }
        ).queryAs(ListCell.class);
    }

    @Test
    public void testCreateUser() throws Exception {
        String userName = generateUserName();
        beAdmin();
        waitAndClickOn("#adminTab");
        clickOn("#btnCreateUser");
        setText("#txtUsername", userName);
        setText("#txtPassword", "newPassword");
        setText("#txtConfirmPassword", "newPassword");
        clickOn("#btnSubmit");
        closeInformationDialog();
        await().atMost(5, SECONDS).until(() -> getUserFromListView(userName) != null);
        assertNoErrors();
    }

    @Test
    public void createUserPasswordMismatch() throws Exception {
        String userName = generateUserName();
        beAdmin();
        waitAndClickOn("#adminTab");
        clickOn("#btnCreateUser");
        setText("#txtUsername", userName);
        setText("#txtPassword", "newPassword");
        setText("#txtConfirmPassword", "newPassword 2");
        clickOn("#btnSubmit");
        closeErrorDialog();
        closeCurrentWindow();
        sleep(5);
        assertFalse(getUserFromListView(userName) != null);
    }

    @Test
    public void testCreateAndUpdateUser() throws Exception {
        //Create Testuser
        String userName = generateUserName();
        String oldPassword = "somePassword";
        beAdmin();
        waitAndClickOn("#adminTab");
        waitAndClickOn("#btnCreateUser");
        setText("#txtUsername", userName);
        setText("#txtPassword", oldPassword);
        setText("#txtConfirmPassword", oldPassword);
        waitAndClickOn("#btnSubmit");
        closeInformationDialog();

        await().atMost(5, SECONDS).until(() -> getUserFromListView(userName) != null);
        UserDTO newUser = getUserFromListView(userName);

        //Test if user can login
        waitAndClickOn("#logoutTab");
        acceptConfirmationDialog();
        logIn(userName, oldPassword);
        waitAndClickOn("#logoutTab");
        acceptConfirmationDialog();

        beAdmin();
        waitAndClickOn("#adminTab");

        //Lock created testuser
        rightClickOn(getCellFromListView(userName)).moveBy(1, 1);
        waitAndClickOn("#LockMenuItem");
        await().atMost(5, SECONDS).until(() -> {
            UserDTO lockedUser = getUserFromListView(userName);
            return lockedUser != null &&
                newUser.getUsername().equals(lockedUser.getUsername()) &&
                lockedUser.getLocked();
        });
        waitAndClickOn("#logoutTab");
        acceptConfirmationDialog();
        logIn(userName, oldPassword);
        closeErrorDialog();

        beAdmin();
        waitAndClickOn("#adminTab");
        //Unlock created testuser
        rightClickOn(getCellFromListView(userName)).moveBy(1, 1);
        waitAndClickOn("#UnlockMenuItem");
        await().atMost(5, SECONDS).until(() -> {
            UserDTO lockedUser = getUserFromListView(userName);
            return lockedUser != null &&
                newUser.getUsername().equals(lockedUser.getUsername()) &&
                !lockedUser.getLocked();
        });

        //reset password of testuser
        String newPassword = "newPassword";
        rightClickOn(getCellFromListView(userName)).moveBy(1, 1);
        waitAndClickOn("#ResetPasswordMenuItem");
        setText("#txtPassword", newPassword);
        setText("#txtConfirmPassword", newPassword);
        waitAndClickOn("#btnSubmit");
        waitAndClickOn("#logoutTab");
        acceptConfirmationDialog();

        //Try old and new password
        logIn(userName, oldPassword);
        closeErrorDialog();
        logIn(userName, newPassword);
        assertNoErrors();

        //Try locking after 5 wrong passwords
        waitAndClickOn("#logoutTab");
        acceptConfirmationDialog();
        for (int i = 0; i < 5; i++) {
            waitFor("#txtUsername");
            setText("#txtUsername", userName);
            waitFor("#txtPassword");
            setText("#txtPassword", oldPassword);
            waitAndClickOn("#btnAuthenticate");
            closeErrorDialog();
        }

        logIn(userName, newPassword);
        closeErrorDialog();

        //Unlock User again
        beAdmin();
        waitAndClickOn("#adminTab");
        rightClickOn(getCellFromListView(userName)).moveBy(1, 1);
        waitAndClickOn("#UnlockMenuItem");
        waitAndClickOn("#logoutTab");
        acceptConfirmationDialog();

        //Login unlocked user
        logIn(userName, newPassword);
        assertNoErrors();
        waitAndClickOn("#logoutTab");
        acceptConfirmationDialog();
    }
}
