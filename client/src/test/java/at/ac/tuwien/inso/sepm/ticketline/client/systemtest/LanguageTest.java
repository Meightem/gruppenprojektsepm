package at.ac.tuwien.inso.sepm.ticketline.client.systemtest;

import at.ac.tuwien.inso.sepm.ticketline.client.systemtest.base.TestBase;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import javafx.scene.control.Button;
import org.junit.Assert;
import org.junit.Test;

import java.util.ResourceBundle;

public class LanguageTest extends TestBase {

    @Test
    public void testGetBundle() {
        waitAndClickOn("#menuApplication");
        waitAndClickOn("#menuLanguage");
        waitAndClickOn("#German");
        ResourceBundle deBundle = BundleManager.getBundle();
        ResourceBundle deExceptionBundle = BundleManager.getExceptionBundle();
        waitAndClickOn("#menuApplication");
        waitAndClickOn("#menuLanguage").moveBy(50, 0);
        waitAndClickOn("#English");
        ResourceBundle enBundle = BundleManager.getBundle();
        ResourceBundle enExceptionBundle = BundleManager.getBundle();
        Assert.assertNotEquals(enBundle, deBundle);
        Assert.assertNotEquals(enExceptionBundle, deExceptionBundle);
    }

    @Test
    public void testLanguageAtLoginButton() {
        waitAndClickOn("#menuApplication");
        waitAndClickOn("#menuLanguage");
        waitAndClickOn("#German");
        Button deBtnAuthenticate = lookup("#btnAuthenticate").queryAs(Button.class);
        String deButtonText = deBtnAuthenticate.getText();
        waitAndClickOn("#menuApplication");
        waitAndClickOn("#menuLanguage").moveBy(50, 0);
        waitAndClickOn("#English");
        Button enBtnAuthenticate = lookup("#btnAuthenticate").queryAs(Button.class);
        String enButtonText = deBtnAuthenticate.getText();
        Assert.assertNotEquals(enButtonText, deButtonText);
        waitAndClickOn("#menuApplication");
        waitAndClickOn("#menuLanguage");
        waitAndClickOn("#German");
        Button deBtnAuthenticate2 = lookup("#btnAuthenticate").queryAs(Button.class);
        String deButtonText2 = deBtnAuthenticate.getText();
        Assert.assertEquals(deButtonText, deButtonText2);
    }
}
